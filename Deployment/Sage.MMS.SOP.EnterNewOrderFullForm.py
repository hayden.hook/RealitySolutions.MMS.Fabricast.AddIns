clr.AddReference("RealitySolutions.MMS.Fabricast.AddIns")

from Microsoft.VisualBasic import *

from RealitySolutions.MMS.SagePy import clsLostFunctions
from RealitySolutions.MMS.Fabricast.AddIns import *

import Sage
import Sage.Accounting.SOP
from Sage.Accounting import *
from Sage.Common import *
from Sage.Common.Amendability import *
from Sage.Common.UI import *
from Sage.ObjectStore import *

from System.Collections import *
from System.Data import *
from System.Data.SqlClient import *
from System.Drawing import *
from System.Windows.Forms import ComboBoxStyle

LF = clsLostFunctions()


# Declare amendable control objects
oTabControl = None

acNewTab = None


lblAccountCustomer = None
lblContact = None
lblDeliveryMethod = None

cmbAccountCustomer = None
cmbDeliveryMethod = None

txtContact = None


oApp = Sage.Accounting.Application()

sqlConn = SqlConnection(oApp.ActiveCompany.ConnectString)


def main():
	global oTabControl, acNewTab, lblAccountCustomer, lblContact, lblDeliveryMethod, cmbAccountCustomer, cmbDeliveryMethod, txtContact, oApp, sqlConn, cmbAC, cmbDM
	
	
	# Find the grid control and add the column
	oTabControl = Form.FindControlByName("tabControl")
	
	# Add the tab page
	oTabControl.Controls.Add("System.Windows.Forms.TabPage", "tabNew")
	
	
	acNewTab = Form.FindControlByName("tabNew")
	
	acNewTab.Text = "Additional"
	acNewTab.Controls.Add("System.Windows.Forms.Label", "lblAccountCustomer")
	acNewTab.Controls.Add("System.Windows.Forms.Label", "lblContact")
	acNewTab.Controls.Add("System.Windows.Forms.Label", "lblDeliveryMethod")
	acNewTab.Controls.Add("System.Windows.Forms.ComboBox", "cmbAccountCustomer")
	acNewTab.Controls.Add("System.Windows.Forms.ComboBox", "cmbDeliveryMethod")
	acNewTab.Controls.Add("Sage.Common.Controls.MaskedTextBox", "txtContact")
	
	
	# Link the controls
	lblAccountCustomer = Form.FindControlByName("lblAccountCustomer")
	lblContact = Form.FindControlByName("lblContact")
	lblDeliveryMethod = Form.FindControlByName("lblDeliveryMethod")
	
	cmbAccountCustomer = Form.FindControlByName("cmbAccountCustomer")
	cmbDeliveryMethod = Form.FindControlByName("cmbDeliveryMethod")
	
	txtContact = Form.FindControlByName("txtContact")
	
	
	
	# Move the controls to where they need to be
	lblAccountCustomer.Location = Point(8, 8)
	lblAccountCustomer.Size = Size(100, 23)
	lblAccountCustomer.Text = "Account Customer:"
	
	lblContact.Location = Point(8, 62)
	lblContact.Size = Size(46, 18)
	lblContact.Text = "Contact:"
	
	lblDeliveryMethod.Location = Point(8, 36)
	lblDeliveryMethod.Size = Size(100, 23)
	lblDeliveryMethod.Text = "Delivery Method:"
	
	
	cmbAccountCustomer.Location = Point(120, 4)
	cmbAccountCustomer.Size = Size(176, 21)
	cmbAccountCustomer.Text = ""
	
	cmbDeliveryMethod.Location = Point(120, 32)
	cmbDeliveryMethod.Size = Size(176, 21)
	cmbDeliveryMethod.Text = ""
	
	
	# Cast the controls so we can add items to them
	cmbAC = cmbAccountCustomer.UnderlyingControl
	
	cmbAC.DropDownStyle = ComboBoxStyle.DropDownList
	cmbAC.Items.Add("Account/Customer")
	cmbAC.Items.Add("Cash")
	cmbAC.SelectedIndex = 0 # Select the first on by default
	
	
	cmbDM = cmbDeliveryMethod.UnderlyingControl
	
	cmbDM.DropDownStyle = ComboBoxStyle.DropDownList
	cmbDM.Items.Add("Our Transport")
	cmbDM.Items.Add("Collection")
	cmbDM.Items.Add("Carrier")
	cmbDM.Items.Add("Other")
	cmbDM.SelectedIndex = 0 # Select the first on by default
	
	
	txtContact.Location = Point(120, 59)
	txtContact.Size = Size(176, 21)
	txtContact.Text = ""
	
	
	
	# Add handlers
	Form.BeginSave += Form_BeginSave
	



##################################################################
# Captured handlers
##################################################################
def Form_BeginSave(sender, e):
	global cmbAC, cmbDM, txtContact
	
	
	try:
		oSOP = Form.BoundObjects[1]
		
		if oSOP is not None:
			oSOP.Fields["AccountCustomer"].Value = cmbAC.Text
			oSOP.Fields["DeliveryMethod"].Value = cmbDM.Text
			oSOP.Fields["Contact"].Value = txtContact.Text
			
			
			# NEW: Add an additional line according to analysis 4 (delivery band)
			oCharge = Sage.Accounting.SOP.SOPAdditionalChargeLine()
			oDot = Sage.Accounting.SOP.SOPStandardItemLine()
			
			oTaxCodes = oApp.TaxModule.TaxCodes
			oTaxCode = None
			
			oTaxCodes.Query.Filters.Add(Sage.ObjectStore.Filter(Sage.Accounting.TaxModule.TaxCode.FIELD_CODE, "1"))
			oTaxCode = oTaxCodes.First
			
			bolWriteCharge = True
			
			if True:
				oSOPAdditionalCharges = Sage.Accounting.SOP.SOPAdditionalCharges()
				oSOPAdditionalCharge = None
				
				oSOPAdditionalCharges.Query.Filters.Add(Sage.ObjectStore.Filter(Sage.Accounting.SOP.SOPAdditionalCharge.FIELD_CODE, "Delivery Charge"))
				oSOPAdditionalCharge = oSOPAdditionalCharges.First
				
				
				if oSOPAdditionalCharge is not None:
					oCharge.SOPOrderReturn = oSOP
					
					oCharge.StoredCharge = oSOPAdditionalCharge
					oCharge.TaxCode = oTaxCode
					
					
					strChargeCode = ""
					
					if oSOP.AnalysisCode4.ToUpper().Trim() == "":
						if Form.FindControlByName("analysisCode4MaskedTextBox") is not None:
							strChargeCode = Form.FindControlByName("analysisCode4MaskedTextBox").Text
					
					else:
						strChargeCode = oSOP.AnalysisCode4.ToUpper()
					
					
					if strChargeCode == "A":
						oCharge.ChargeValue = 12.5
						
					elif strChargeCode == "B":
						oCharge.ChargeValue = 20
						
					elif strChargeCode == "C":
						oCharge.ChargeValue = 25
						
					elif strChargeCode == "D":
						oCharge.ChargeValue = 35
						
					else:
						bolWriteCharge = False
						
					
					if bolWriteCharge == True:
						oCharge.Post()
				
				else:
					raise Exception("Unable to locate the delivery additional charge object.")
				
				if bolWriteCharge == True:
					oSOP.Lines.Add(oCharge)
			
			
			# NEW: Add out dot item for picking
			if True:
				oUnitSellingEx = type(Sage.Accounting.Exceptions.Ex20319Exception)
				oStockItemHoldingEx = type(Sage.Accounting.Exceptions.StockItemHoldingInsufficientException)
				
				oStockItems = Sage.Accounting.Stock.StockItems()
				oStockItem = oStockItems["."]
				
				if oStockItem is None:
					raise Exception("Unable to locate the picking dot item.")
				
				Sage.Accounting.Application.AllowableWarnings.Add(oDot, oUnitSellingEx)
				Sage.Accounting.Application.AllowableWarnings.Add(oDot, oStockItemHoldingEx)
				
				oDot.SOPOrderReturn = oSOP
				oDot.Item = oStockItem
				oDot.LineQuantity = 1
				oDot.UnitSellingPrice = 0
				oDot.TaxCode = oTaxCode
				
				oDot.Post()
				
				oSOP.Lines.Add(oDot)
			
			oSOP.Update()
		
		else:
			raise Exception("oSOP object is null.")
	
	except Exception, ex:
		LF.MsgBox("An error has occurred while saving the additional information.\r\n\r\n" + str(ex), MsgBoxStyle.Critical, "Error")
		return
	

def ExecuteQuery(strQuery):
	dtTEMP = DataTable("dtTEMP")

	sqlDA = SqlDataAdapter(strQuery, sqlConn)


	try:
		dtTEMP.Clear()

		sqlDA.Fill(dtTEMP)

	except:
		dtTEMP.Clear()

	finally:
		# Dispose when done
		sqlDA.Dispose()
		sqlDA = None
	
	return dtTEMP


# Run main
main()
