def acStockCode_StockItemSelected(sender, args):
	global bolAmendMode, bolReturnMode, oSellingUnit, oSellingPriceUnit, dtpLatestCost, txtLatestCost
	
	
	if bolReturnMode == False and acNewMaterialsButton is not None:
		acNewMaterialsButton.Enabled = True
	
	oSellingUnit = Form.FindControlByName("stockItemUnitLookup")
	oSellingPriceUnit = Form.FindControlByName("priceStockItemUnitLookup")
	
	
	if bolAmendMode == False and dtpLatestCost is not None and txtLatestCost is not None:
		rows = ExecuteQuery("""SELECT TOP 1 POPOrderReturnLine.UnitBuyingPrice, POPOrderReturn.DocumentDate
FROM POPOrderReturnLine
INNER JOIN POPOrderReturn ON POPOrderReturnLine.POPOrderReturnID = POPOrderReturn.POPOrderReturnID
WHERE POPOrderReturnLine.ItemCode = '%s' AND POPOrderReturnLine.LineTypeID = 0
ORDER BY POPOrderReturn.POPOrderReturnID DESC""" % acStockCode.Text)
		
		for row in rows.Rows:
			dtpLatestCost.Value = LF.CDate(row.Item[LF.ColumnIndex(rows, "DocumentDate")])
			txtLatestCost.Text = LF.Format(LF.CDec(row.Item[LF.ColumnIndex(rows, "UnitBuyingPrice")]), "0.00000")
			break

