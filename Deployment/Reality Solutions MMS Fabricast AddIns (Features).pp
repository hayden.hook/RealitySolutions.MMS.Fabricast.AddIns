<?xml version="1.0" encoding="utf-8"?>
<Project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" SchemaVersion="1.0" AddOnName="Reality Solutions MMS Fabricast AddIns (Features)" Guid="59d9a480-31ea-411b-925e-ca0657d7dbd1" Author="Daniel Knaggs (Reality Solutions)" Version="1.0" ComponentName="Reality Solutions MMS Fabricast AddIns (Features)" TargetFolder="Z:\Source Code\MMS Scripts\MMS 8.0\Fabricast">
  <AddOnDescriptionData><![CDATA[]]></AddOnDescriptionData>
  <DefinesFeatures>
    <DefinedFeature Name="Fabricast" Guid="91b93791-38ec-4536-9e77-4f29166b2532">
      <Targets>
        <Target Name="Goods In" Guid="87b34f2a-cf45-40cc-81b1-697148fd907d" Type="VBForm" Action="RealitySolutions.MMS.Fabricast.AddIns.frmGoodsIn" Launcher="Sage.MMS.Launcher.Stub">
          <Description />
        </Target>
        <Target Name="Goods Out" Guid="78131f81-366d-4451-8960-b54c5bc4eb32" Type="VBForm" Action="RealitySolutions.MMS.Fabricast.AddIns.frmGoodsOut" Launcher="Sage.MMS.Launcher.Stub">
          <Description />
        </Target>
        <Target Name="Price Check" Guid="574d624b-794f-434e-9a1c-87698e6ae092" Type="VBForm" Action="RealitySolutions.MMS.Fabricast.AddIns.frmPriceCheck" Launcher="Sage.MMS.Launcher.Stub">
          <Description />
        </Target>
        <Target Name="Reset All Stock" Guid="b63c6b7b-0ed3-4373-b6e6-7300c80f7dca" Type="VBForm" Action="RealitySolutions.MMS.Fabricast.AddIns.frmResetAllStock" Launcher="Sage.MMS.Launcher.Stub">
          <Description />
        </Target>
        <Target Name="SOP Confirm Goods Dispatched" Guid="52063d75-2bc2-40e8-9c02-4d9da2245a36" Type="VBForm" Action="RealitySolutions.MMS.Fabricast.AddIns.frmSOPConfirmGoodsDispatched" Launcher="Sage.MMS.Launcher.Stub">
          <Description />
        </Target>
        <Target Name="SOP Print Picking Lists" Guid="340eed8e-2f2e-48c3-b987-44530bd9675b" Type="VBForm" Action="RealitySolutions.MMS.Fabricast.AddIns.frmSOPPrintPickingLists" Launcher="Sage.MMS.Launcher.Stub">
          <Description />
        </Target>
        <Target Name="Stock Enquiry" Guid="64c98545-a236-4b86-ae1e-908f4db593d3" Type="VBForm" Action="RealitySolutions.MMS.Fabricast.AddIns.frmStockEnquiry" Launcher="Sage.MMS.Launcher.Stub">
          <Description />
        </Target>
        <Target Name="Treeview Searching" Guid="979acc36-1709-43bf-b748-4e95f1520603" Type="VBForm" Action="RealitySolutions.MMS.Fabricast.AddIns.frmTreeviewSearching" Launcher="Sage.MMS.Launcher.Stub">
          <Description />
        </Target>
      </Targets>
    </DefinedFeature>
  </DefinesFeatures>
</Project>