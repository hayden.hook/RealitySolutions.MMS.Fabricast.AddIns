clr.AddReference("RealitySolutions.MMS.Fabricast.AddIns")

from Microsoft.VisualBasic import *

from RealitySolutions.MMS.SagePy import clsLostFunctions
from RealitySolutions.MMS.Fabricast.AddIns import *

import Sage
import Sage.Accounting.SOP
from Sage.Accounting import *
from Sage.Common import *
from Sage.Common.Amendability import *
from Sage.Common.UI import *
from Sage.ObjectStore import *

from System import *
from System.Collections import *
from System.Data import *
from System.Data.SqlClient import *
from System.Drawing import *
from System.Windows.Forms import DateTimePickerFormat, HorizontalAlignment

LF = clsLostFunctions()
 

# Declare amendable control objects
oCommentsBox = None
oSearchBox = None

acNewMaterialsButton = None
acNewSearchButton = None

acOldSearchButton = None

acStockCode = None

popUpStock = None

oPurchaseOrderNo = None

oCertReq = None
oCertReqUL = None

oSellingUnit = None
oSellingPriceUnit = None

chkCertReq = None
cmdAmendProduct = None
cmdStockEnquiry = None
lblPON = None
oButtonPanel = None
oItemValuesGroupBox = None
oLineTypeGroupBox = None
txtPurchaseOrderNo = None

dtpLatestCost = None
lblLatestCost = None
txtLatestCost = None

bolAmendMode = False
bolReturnMode = False

strTEMP = ""


oApp = Sage.Accounting.Application()

sqlConn = SqlConnection(oApp.ActiveCompany.ConnectString)


def main():
	global oButtonPanel, oCommentsBox, oSearchBox, acNewMaterialsButton, acNewSearchButton, acStockCode, oLineTypeGroupBox, popUpStock, oItemValuesGroupBox, oPurchaseOrderNo, oCertReq, oCertReqUL, oSellingUnit, oSellingPriceUnit, bolReturnMode, strTEMP, chkCertReq, cmdAmendProduct, lblPON, txtPurchaseOrderNo, dtpLatestCost, lblLatestCost, txtLatestCost, bolAmendMode, cmdStockEnquiry
	
	
	if LF.InStr(Form.Text, "Return", CompareMethod.Text) > 0:
		bolReturnMode = True
		
	else:
		bolReturnMode = False
	
	
	oCommentsBox = Form.FindControlByName("commentsGroupBox")
	
	if oCommentsBox is not None:
		oCommentsBox.Controls.Add("System.Windows.Forms.Label", "lblLatestCost")
		oCommentsBox.Controls.Add("System.Windows.Forms.TextBox", "txtLatestCost")
		oCommentsBox.Controls.Add("System.Windows.Forms.DateTimePicker", "dtpLatestCost")
		
		lblLatestCost = Form.FindControlByName("lblLatestCost")
		lblLatestCost.Enabled = True
		lblLatestCost.Location = Point(320, 26)
		lblLatestCost.Size = Size(66, 13)
		lblLatestCost.Text = "Latest Cost:"
		
		txtLatestCost = Form.FindControlByName("txtLatestCost").UnderlyingControl
		txtLatestCost.Enabled = False
		txtLatestCost.Location = Point(390, 22)
		txtLatestCost.Size = Size(125, 21)
		txtLatestCost.Text = "0.00000"
		txtLatestCost.TextAlign = HorizontalAlignment.Right
		
		dtpLatestCost = Form.FindControlByName("dtpLatestCost").UnderlyingControl
		dtpLatestCost.Enabled = False
		dtpLatestCost.CustomFormat = "dd/MM/yyyy"
		dtpLatestCost.Format = DateTimePickerFormat.Custom
		dtpLatestCost.Location = Point(520, 22)
		dtpLatestCost.Size = Size(100, 21)
		dtpLatestCost.Value = LF.CDate("2010/01/01")
	
	if bolReturnMode == False:
		# Add the tab page
		if oCommentsBox is not None:
			oCommentsBox.Controls.Add("System.Windows.Forms.Button", "cmdMaterialsRefNumber")
			
			
			acNewMaterialsButton = Form.FindControlByName("cmdMaterialsRefNumber")
			
			acNewMaterialsButton.Enabled = True
			acNewMaterialsButton.Location = Point(210, 21)
			acNewMaterialsButton.Size = Size(87, 26)
			acNewMaterialsButton.Text = "&Materials Ref"
	
	
	acStockCode = Form.FindControlByName("stockLookup")
	popUpStock = acStockCode.UnderlyingControl
	
	
	oItemValuesGroupBox = Form.FindControlByName("itemValuesGroupBox")
	oItemValuesGroupBox.Controls.Add("Sage.Common.Controls.CheckBox", "chkCertReq")
	
	
	chkCertReq = Form.FindControlByName("chkCertReq").UnderlyingControl
	chkCertReq.CheckAlign = ContentAlignment.MiddleRight
	chkCertReq.Location = Point(487, 97)
	chkCertReq.Size = Size(217, 24)
	chkCertReq.Text = "Certification Required?"
	chkCertReq.TextAlign = ContentAlignment.MiddleLeft
	chkCertReq.UseVisualStyleBackColor = True
	
	oButtonPanel = Form.FindControlByName("buttonPanel")
	oButtonPanel.Controls.Add("Sage.Common.Controls.Button", "cmdAmendProduct")
	oButtonPanel.Controls.Add("Sage.Common.Controls.Button", "cmdStockEnquiry")
	
	
	cmdAmendProduct = Form.FindControlByName("cmdAmendProduct").UnderlyingControl
	cmdAmendProduct.Location = Point(547, 24)
	cmdAmendProduct.Size = Size(75, 23)
	cmdAmendProduct.Text = "Amend Pro&duct"
	cmdAmendProduct.UseVisualStyleBackColor = True
	
	cmdStockEnquiry = Form.FindControlByName("cmdStockEnquiry").UnderlyingControl
	cmdStockEnquiry.Location = Point(615, 24)
	cmdStockEnquiry.Size = Size(75, 23)
	cmdStockEnquiry.Text = "Stock &Enquiry"
	cmdStockEnquiry.UseVisualStyleBackColor = True
	
	
	oLineTypeGroupBox = Form.FindControlByName("lineTypeGroupBox")
	oLineTypeGroupBox.Controls.Add("Sage.Common.Controls.Label", "lblPON")
	oLineTypeGroupBox.Controls.Add("Sage.Common.Controls.MaskedTextBox", "txtPurchaseOrderNo")
	
	lblPON = Form.FindControlByName("lblPON")
	lblPON.Location = Point(528, 22)
	lblPON.Size = Size(46, 18)
	lblPON.Text = "PO Number:"
	
	oPurchaseOrderNo = Form.FindControlByName("txtPurchaseOrderNo")
	oPurchaseOrderNo.Location = Point(596, 19)
	oPurchaseOrderNo.Size = Size(113, 21)
	oPurchaseOrderNo.Text = ""
	
	
	oCertReq = Form.FindControlByName("chkCertReq")
	oCertReqUL = oCertReq.UnderlyingControl
	
	
	# Cheat something - This how the develeopers at Sage tell the item is being edited!!!
	if Form.Text == "SOP - Edit Order Item Line":
		bolAmendMode = True
		
		if acNewMaterialsButton is not None:
			acNewMaterialsButton.Enabled = True
		
		# Load in the temp variable
		try:
			oSOPLine = Form.BoundObjects[0]
			
			if oSOPLine is not None:
				strTEMP = oSOPLine.Fields["MaterialsRefNumber"].Value.ToString()
				
				oPurchaseOrderNo.Text = oSOPLine.Fields["PurchaseOrderNumber"].GetString()
				oCertReqUL.Checked = oSOPLine.Fields["CertificationRequired"].GetBoolean()
				
			else:
				raise Exception("oSOPLine object is null.")
			
		except Exception, ex:
			LF.MsgBox("An error has occurred while loading the additional information on startup.\r\n\r\n" + str(ex), MsgBoxStyle.Critical, "Error")
			return
	
	else:
		bolAmendMode = False
	
	
	# Override the search button - Ensure the search button is enabled in the settings
	oSearchBox = Form.FindControlByName("searchPanel")
	oSearchBox.Controls.Add("System.Windows.Forms.Button", "cmdNewSearchButton")
	
	acNewSearchButton = Form.FindControlByName("cmdNewSearchButton")
	acOldSearchButton = Form.FindControlByName("searchButton")
	
	acNewSearchButton.Enabled = True
	acNewSearchButton.Location = Point(5, 0)
	acNewSearchButton.Size = Size(30, 22)
	acNewSearchButton.Text = "S"
	acNewSearchButton.BringToFront()
	
	acOldSearchButton.Visible = False
	acOldSearchButton.SendToBack()
	
	
	oSellingUnit = Form.FindControlByName("stockItemUnitLookup")
	oSellingPriceUnit = Form.FindControlByName("priceStockItemUnitLookup")
	
	oSellingUnit.Enabled = True
	oSellingPriceUnit.Enabled = True
	
	
	
	# NEW: Now sort the amend product button out
	cmdAmendProduct.Click += cmdAmendProduct_Click
	cmdStockEnquiry.Click += cmdStockEnquiry_Click
	
	
	# Add handlers
	Form.EndSave += Form_EndSave
	
	if bolReturnMode == False:
		acNewMaterialsButton.Click += cmdNewMaterialsButton_Click
	
	acNewSearchButton.Click += cmdNewSearchButton_Click
	
	popUpStock.StockItemSelected += acStockCode_StockItemSelected


##################################################################
# Captured handlers
##################################################################
def Form_EndSave(b):
	global acNewMaterialsButton, strTEMP
	
	
	if (b == True) and (bolReturnMode == False):
		try:
			oSOPLine = Form.BoundObjects[0]
			
			if (oSOPLine is not None):
				oSOPLine.Fields["MaterialsRefNumber"].Value = strTEMP
				oSOPLine.Fields["PurchaseOrderNumber"].Value = oPurchaseOrderNo.Text
				oSOPLine.Fields["CertificationRequired"].Value = oCertReqUL.Checked
				oSOPLine.Fields["LatestCost"].Value = txtLatestCost.Text
				oSOPLine.Fields["LatestCostDate"].Value = dtpLatestCost.Value
				oSOPLine.Update()
				
				
				# Disable the button
				if acNewMaterialsButton is not None:
					acNewMaterialsButton.Enabled = False
				
				# Reset temp variable
				strTEMP = None
				oPurchaseOrderNo.Text = ""
				
			else:
				raise Exception("oSOPLine object is null.")
			
		except Exception, ex:
			LF.MsgBox("An error has occurred while saving the additional information.\r\n\r\n" + str(ex), MsgBoxStyle.Critical, "Error")
			return


def cmdNewMaterialsButton_Click(sender, e):
	global strTEMP
	
	
	try:
		oSOPLine = Form.BoundObjects(0)
		
		if (oSOPLine is not None):
			frmX = frmSOPMaterialsRefNumber()
			
			frmX.Reference = strTEMP
			frmX.ShowDialog()
			
			
			# Set temp variable
			if (frmX.Reference is not None):
				strTEMP = frmX.Reference
			
		else:
			raise Exception("oSOPLine object is null.")
	
	except Exception, ex:
		LF.MsgBox("An error has occurred while loading the additional information.\r\n\r\n" + str(ex), MsgBoxStyle.Critical, "Error")
		return

def cmdNewSearchButton_Click(sender, e):
	global popUpStock
	
	
	try:
		frmX = frmTreeviewSearching()
		
		frmX.ShowDialog()
		
		# Set temp variable
		if frmX.StockCode <> "":
			oStockCodes = Sage.Accounting.Stock.StockItems()
			oStockCode = oStockCodes[frmX.StockCode]
			
			args = Sage.MMS.Controls.StockItemSelectedEventArgs()
			args.SelectedStockItem = oStockCode
			
			popUpStock.StockItem = oStockCode
			popUpStock.OnStockItemSelected(args)
		
	except Exception, ex:
		LF.MsgBox("An error has occurred while loading the search treeview.\r\n\r\n" + str(ex), MsgBoxStyle.Critical, "Error")
		return

def cmdAmendProduct_Click(sender, e):
	global popUpStock
	
	
	oArray = Array[object]([popUpStock.StockItem.Item])
	
	iArgs = Sage.Common.UI.FormArguments(oArray, type(Sage.Accounting.Stock.StockItem), Sage.Common.CustomAttributes.eParameterType.DbKey)
	
	frmX = Sage.MMS.Stock.AmendItemDetailsForm(iArgs)
	frmX.ShowDialog()
	frmX = None

def cmdStockEnquiry_Click(sender, e):
	global popUpStock
	
	
	frmX = frmStockEnquiry()
	frmX.StockItem = popUpStock.StockItem
	frmX.ShowDialog()
	frmX = None

def acStockCode_StockItemSelected(sender, args):
	global bolAmendMode, bolReturnMode, oSellingUnit, oSellingPriceUnit, dtpLatestCost, txtLatestCost
	
	
	if bolReturnMode == False and acNewMaterialsButton is not None:
		acNewMaterialsButton.Enabled = True
	
	oSellingUnit = Form.FindControlByName("stockItemUnitLookup")
	oSellingPriceUnit = Form.FindControlByName("priceStockItemUnitLookup")
	
	
	if bolAmendMode == False and dtpLatestCost is not None and txtLatestCost is not None:
		rows = ExecuteQuery("""SELECT TOP 1 POPOrderReturnLine.UnitBuyingPrice, POPOrderReturn.DocumentDate
FROM POPOrderReturnLine
INNER JOIN POPOrderReturn ON POPOrderReturnLine.POPOrderReturnID = POPOrderReturn.POPOrderReturnID
WHERE POPOrderReturnLine.ItemCode = '%s' AND POPOrderReturnLine.LineTypeID = 0
ORDER BY POPOrderReturn.POPOrderReturnID DESC""" % acStockCode.Text)
		
		for row in rows.Rows:
			dtpLatestCost.Value = LF.CDate(row.Item[LF.ColumnIndex(rows, "DocumentDate")])
			txtLatestCost.Text = LF.Format(LF.CDec(row.Item[LF.ColumnIndex(rows, "UnitBuyingPrice")]), "0.00000")
			break


def ExecuteQuery(strQuery):
	dtTEMP = DataTable("dtTEMP")

	sqlDA = SqlDataAdapter(strQuery, sqlConn)


	try:
		dtTEMP.Clear()

		sqlDA.Fill(dtTEMP)

	except:
		dtTEMP.Clear()

	finally:
		# Dispose when done
		sqlDA.Dispose()
		sqlDA = None

	return dtTEMP

# Run main
main()
