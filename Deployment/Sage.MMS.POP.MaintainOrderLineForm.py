clr.AddReference("RealitySolutions.MMS.Fabricast.AddIns")

from Microsoft.VisualBasic import *

from RealitySolutions.MMS.SagePy import clsLostFunctions
from RealitySolutions.MMS.Fabricast.AddIns import *

import Sage
import Sage.Accounting.SOP
from Sage.Accounting import *
from Sage.Common import *
from Sage.Common.Amendability import *
from Sage.Common.UI import *
from Sage.ObjectStore import *

from System.Collections import *
from System.Drawing import *

LF = clsLostFunctions()


# Declare amendable control objects
oCommentsBox = None
oSearchBox = None

acNewMaterialsButton = None
acNewSearchButton = None

acOldSearchButton = None


acStockCode = None

popUpStock = None


strTEMP = ""


def main():
	global oCommentsBox, oSearchBox, acNewMaterialsButton, acNewSearchButton, acOldSearchButton, acStockCode, popUpStock, strTEMP
	
	
	# Find the grid control and add the column
	acStockCode = Form.FindControlByName("stockLookup")
	popUpStock = acStockCode.UnderlyingControl
	
	
	# Override the search button - The search button has to enabled in the settings
	oSearchBox = Form.FindControlByName("searchPanel")
	oSearchBox.Controls.Add("System.Windows.Forms.Button", "cmdNewSearchButton")
	
	acOldSearchButton = Form.FindControlByName("searchButton")
	acNewSearchButton = Form.FindControlByName("cmdNewSearchButton")
	
	acNewSearchButton.Enabled = True
	acNewSearchButton.Location = Point(5, 0)
	acNewSearchButton.Size = Size(30, 22)
	acNewSearchButton.Text = "S"
	
	acNewSearchButton.BringToFront()
	
	
	acOldSearchButton.Visible = False
	acOldSearchButton.SendToBack()
	
	
	
	# Add handlers
	Form.Closing += Form_Closing
	
	acNewSearchButton.Click += cmdNewSearchButton_Click
	


##################################################################
# Captured handlers
##################################################################
def Form_Closing(sender, e):
	global strTEMP
	
	
	# We should dispose all the controls we#ve created...
	strTEMP = None


def cmdNewSearchButton_Click(sender, e):
	try:
		frmX = frmTreeviewSearching()
		
		frmX.ShowDialog()
		
		
		# Set temp variable
		if frmX.StockCode <> "":
			oStockCodes = Sage.Accounting.Stock.StockItems()
			oStockCode = oStockCodes[frmX.StockCode]
			
			args = Sage.MMS.Controls.StockItemSelectedEventArgs()
			args.SelectedStockItem = oStockCode
			
			popUpStock.StockItem = oStockCode
			popUpStock.OnStockItemSelected(args)
		
	except Exception, ex:
		LF.MsgBox("An error has occurred while loading the search treeview.\r\n\r\n" + str(ex), MsgBoxStyle.Critical, "Error")
		return
	

# Run main
main()
