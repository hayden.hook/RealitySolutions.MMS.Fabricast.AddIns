clr.AddReference("RealitySolutions.MMS.Fabricast.AddIns")

from Microsoft.VisualBasic import *

from RealitySolutions.MMS.SagePy import clsLostFunctions
from RealitySolutions.MMS.Fabricast.AddIns import *

import Sage
import Sage.Accounting.SOP
from Sage.Accounting import *
from Sage.Common import *
from Sage.Common.Amendability import *
from Sage.Common.UI import *
from Sage.ObjectStore import *

from System.Collections import *
from System.Data import *
from System.Data.SqlClient import *
from System.Drawing import *
from System.Windows.Forms import ComboBoxStyle

LF = clsLostFunctions()
 

# Declare amendable control objects
oSOPOrder = None


oTabControl = None

acNewTab = None
acLogTab = None


lblAccountCustomer = None
lblDeliveryMethod = None

cmbAccountCustomer = None
cmbDeliveryMethod = None

txtContact = None

oAccountSelectionGroupBox = None

oLogGrid = None
oLogGridUL = None

oRefresh = None
oRefreshUL = None

cmbAC = None
cmbDM = None


oApp = Sage.Accounting.Application()

sqlConn = SqlConnection(oApp.ActiveCompany.ConnectString)


def main():
	global oAccountSelectionGroupBox,  oSOPOrder, oTabControl, acNewTab, acLogTab, lblAccountCustomer, lblDeliveryMethod, cmbAccountCustomer, cmbDeliveryMethod, txtContact, oLogGrid, oLogGridUL, oRefresh, oRefreshUL, cmbAC, cmbDM, oLogGrid, oLogGridUL
	
	
	# Find the grid control and add the column
	oTabControl = Form.FindControlByName("tabControl")
	
	# Add the tab page
	oTabControl.Controls.Add("System.Windows.Forms.TabPage", "tabNew")
	oTabControl.Controls.Add("System.Windows.Forms.TabPage", "tabLog")
	
	
	acNewTab = Form.FindControlByName("tabNew")
	
	acNewTab.Text = "Additional"
	
	
	# Add the controls
	acNewTab.Controls.Add("System.Windows.Forms.Label", "lblAccountCustomer")
	acNewTab.Controls.Add("System.Windows.Forms.Label", "lblDeliveryMethod")
	acNewTab.Controls.Add("System.Windows.Forms.ComboBox", "cmbAccountCustomer")
	acNewTab.Controls.Add("System.Windows.Forms.ComboBox", "cmbDeliveryMethod")
	
	
	acLogTab = Form.FindControlByName("tabLog")
	
	acLogTab.Text = "Log"
	acLogTab.Controls.Add("Sage.ObjectStore.Controls.Grid", "grdLog")
	
	
	# Link the controls
	lblAccountCustomer = Form.FindControlByName("lblAccountCustomer")
	lblDeliveryMethod = Form.FindControlByName("lblDeliveryMethod")
	
	cmbAccountCustomer = Form.FindControlByName("cmbAccountCustomer")
	cmbDeliveryMethod = Form.FindControlByName("cmbDeliveryMethod")
	
	txtContact = Form.FindControlByName("txtContact")
	
	oAccountSelectionGroupBox = Form.FindControlByName("accountSelectionGroupBox")
	oAccountSelectionGroupBox.Controls.Add("Sage.Common.Controls.Button", "cmdRefresh")
	
	
	
	# Move the controls to where they need to be
	lblAccountCustomer.Location = Point(8, 8)
	lblAccountCustomer.Size = Size(100, 23)
	lblAccountCustomer.Text = "Account Customer:"
	
	lblDeliveryMethod.Location = Point(8, 36)
	lblDeliveryMethod.Size = Size(100, 23)
	lblDeliveryMethod.Text = "Delivery Method:"
	
	
	cmbAccountCustomer.Location = Point(120, 4)
	cmbAccountCustomer.Size = Size(176, 21)
	cmbAccountCustomer.Text = ""
	
	cmbDeliveryMethod.Location = Point(120, 32)
	cmbDeliveryMethod.Size = Size(176, 21)
	cmbDeliveryMethod.Text = ""
	
	
	# Cast the controls so we can add items to them
	cmbAC = cmbAccountCustomer.UnderlyingControl
	
	cmbAC.DropDownStyle = ComboBoxStyle.DropDownList
	cmbAC.Items.Add("Account/Customer")
	cmbAC.Items.Add("Cash")
	cmbAC.SelectedIndex = 0 # Select the first on by default
	
	
	cmbDM = cmbDeliveryMethod.UnderlyingControl
	
	cmbDM.DropDownStyle = ComboBoxStyle.DropDownList
	cmbDM.Items.Add("Our Transport")
	cmbDM.Items.Add("Collection")
	cmbDM.Items.Add("Carrier")
	cmbDM.Items.Add("Other")
	cmbDM.SelectedIndex = 0 # Select the first on by default
	
	
	oLogGrid = Form.FindControlByName("grdLog")
	oLogGridUL = oLogGrid.UnderlyingControl
	
	oRefresh = Form.FindControlByName("cmdRefresh")
	oRefreshUL = oRefresh.UnderlyingControl
	
	
	oRefreshUL.Font = Font("Tahoma", 8.25)
	oRefreshUL.Location = Point(143, 9)
	oRefreshUL.Size = Size(75, 23)
	oRefreshUL.TabIndex = 5
	oRefreshUL.Text = "&Refresh"
	oRefreshUL.UseCompatibleTextRendering = True
	oRefreshUL.UseVisualStyleBackColor = True
	
	
	oLogGrid = Form.FindControlByName("grdLog")
	oLogGridUL = oLogGrid.UnderlyingControl
	
	oLogGridUL.Location = Point(3, 3)
	oLogGridUL.Size = Size(758, 459)
	oLogGridUL.Text = ""
	
	a = Sage.ObjectStore.Controls.GridColumn()
	b = Sage.ObjectStore.Controls.GridColumn()
	c = Sage.ObjectStore.Controls.GridColumn()
	
	a.AutoSize = True
	a.Caption = "Date"
	a.DisplayMember = "DateOfMessage"
	a.Width = 251
	
	b.AutoSize = True
	b.Caption = "Message"
	b.DisplayMember = "Message"
	b.Width = 251
	
	c.AutoSize = True
	c.Caption = "Username"
	c.DisplayMember = "Username"
	c.Width = 252
	
	oLogGridUL.Columns.Add(a)
	oLogGridUL.Columns.Add(b)
	oLogGridUL.Columns.Add(c)
	
	
	
	# Add handlers
	Form.DataRefresh += Form_DataRefresh
	
	oRefreshUL.Click += cmdRefresh_Click



##################################################################
# Captured handlers
##################################################################
def Form_DataRefresh(args):
	global oLogGridUL, oSOPOrder
	
	
	oSOPOrder = args.Source
	
	if oSOPOrder is not None:
		# Log grid
		try:
			dtTEMP = ExecuteQuery("SELECT [Date], Message, SageUser FROM tblSOPNotes WHERE SOPOrderReturnID = %s ORDER BY [Date] DESC" % oSOPOrder.SOPOrderReturn)
			rowEach = None
			
			oItems = SOPPOPLogEntityCollection()
			oItem = None
			
			
			iDate = LF.ColumnIndex(dtTEMP, "Date")
			iMessage = LF.ColumnIndex(dtTEMP, "Message")
			iSageUser = LF.ColumnIndex(dtTEMP, "SageUser")
			
			for rowEach in dtTEMP.Rows:
				oItem = SOPPOPLogEntity()
				
				oItem.DateOfMessage = LF.CDate(rowEach.Item[iDate])
				oItem.Message = rowEach.Item[iMessage].ToString()
				oItem.Username = rowEach.Item[iSageUser].ToString()
				
				oItems.Add(oItem)
			
			# Set the grid to use the metadata
			oLogGridUL.DataSource = oItems
			
		except Exception, ex:
			LF.MsgBox("An error has occurred while populating the payments grid.\r\n\r\n" + str(ex), MsgBoxStyle.Critical, "Error")
			return


def cmdRefresh_Click(sender, e):
	global oSOPOrder
	
	
	oOrderNo = Form.FindControlByName("orderNoMaskedTextBox")
	
	oSOPOrders = Sage.Accounting.SOP.SOPOrders()
	oSOPOrders.Query.Filters.Add(Sage.ObjectStore.Filter("DocumentNo", oOrderNo.Text))
	oSOPOrders.Query.Find()
	oSOPOrders.Find()
	
	oSOPOrder = oSOPOrders.First

	args = Sage.Common.UI.DataRefreshEventArgs()
	args.Source = oSOPOrder
	
	Form_DataRefresh(args)


def LocateTextInCombo(cmbCombo, strCombo):
	intX = 0
	
	for intX in xrange(0, cmbCombo.Items.Count):
		if cmbCombo.Items[intX].ToString() == strCombo:
			cmbCombo.SelectedIndex = intX
			break

def ExecuteQuery(strQuery):
	dtTEMP = DataTable("dtTEMP")

	sqlDA = SqlDataAdapter(strQuery, sqlConn)


	try:
		dtTEMP.Clear()

		sqlDA.Fill(dtTEMP)

	except:
		dtTEMP.Clear()

	finally:
		# Dispose when done
		sqlDA.Dispose()
		sqlDA = None

	return dtTEMP


# Run main
main()
