<?xml version="1.0" encoding="utf-8"?>
<Project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" SchemaVersion="1.0" AddOnName="Reality Solutions MMS Fabricast AddIns" Guid="dfa7c5df-9329-4a24-bd2d-1970ed6c53bc" Author="Reality Solutions Limited" Version="1.9" ComponentName="Reality Solutions MMS Fabricast AddIns" TargetFolder="Z:\Source Code\MMS Scripts\MMS 8.0\Fabricast">
  <AddOnDescriptionData><![CDATA[]]></AddOnDescriptionData>
  <Files>
    <File Type="ClientSideAssembly" Path="..\..\..\RealitySolutions.MMS.Fabricast.AddIns\bin\BabelOut\RealitySolutions.MMS.Fabricast.AddIns.dll" Folder="DLLs" />
    <File Type="ClientSideAssembly" Path="..\..\..\RealitySolutions.MMS.Fabricast.AddIns\bin\RealitySolutions.Licensing.dll" Folder="DLLs" />
    <File Type="ObjectStoreFile" Path=".\SageObjectStore.AddIn" Folder="ObjectStore" />
    <File Type="FormScript" Path=".\Sage.MMS.POP.MaintainOrderLineForm.py" Folder="Scripts" />
    <File Type="FormScript" Path=".\Sage.MMS.SOP.AmendOrderDetailsForm.py" Folder="Scripts" />
    <File Type="FormScript" Path=".\Sage.MMS.SOP.EnterNewOrderFullForm.py" Folder="Scripts" />
    <File Type="FormScript" Path=".\Sage.MMS.SOP.PrintInvoicesForm.py" Folder="Scripts" />
    <File Type="FormScript" Path=".\Sage.MMS.SOP.SOPOrderLine.py" Folder="Scripts" />
    <File Type="FormScript" Path=".\Sage.MMS.SOP.ViewOrderDetailsForm.py" Folder="Scripts" />
    <File xsi:type="PPDBScript" Type="DBScript" Path=".\Fabricast.sql" Folder="SQL" />
  </Files>
</Project>