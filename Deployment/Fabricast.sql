BEGIN TRANSACTION


EXECUTE spCreateAddInTable 'SOPOrderReturn', 'SOPOrderReturnID', 'SOPOrderReturnX'

EXECUTE spCreateColumn 'SOPOrderReturnX', 'Contact', 'varchar', '66', '2', 0, 0, 0, 1, '''''', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnX', 'AccountCustomer', 'nvarchar', '66', '2', 0, 0, 0, 1, '''''', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnX', 'DeliveryMethod', 'nvarchar', '66', '2', 0, 0, 0, 1, '''''', 'None'

EXECUTE spCreateAddInTable 'SOPOrderReturnArch', 'SOPOrderReturnID', 'SOPOrderReturnArchX'

EXECUTE spCreateColumn 'SOPOrderReturnArchX', 'Contact', 'nvarchar', '66', '2', 0, 0, 0, 1, '''''', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnArchX', 'AccountCustomer', 'nvarchar', '66', '2', 0, 0, 0, 1, '''''', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnArchX', 'DeliveryMethod', 'nvarchar', '66', '2', 0, 0, 0, 1, '''''', 'None'

EXECUTE spCreateAddInTable 'SOPOrderReturnLine', 'SOPOrderReturnLineID', 'SOPOrderReturnLineX'

EXECUTE spCreateColumn 'SOPOrderReturnLineX', 'MaterialsRefNumber', 'varchar', '22', '2', 0, 0, 0, 1, '''''', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnLineX', 'RandomLength', 'bit', '13', '2', 0, 0, 0, 1, '0', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnLineX', 'PurchaseOrderNumber', 'varchar', '66', '2', 0, 0, 0, 1, '''''', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnLineX', 'CertificationRequired', 'bit', '13', '2', 0, 0, 0, 1, '0', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnLineX', 'LatestCost', 'decimal', '23', '5', 1, 0, 0, 1, '0', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnLineX', 'LatestCostDate', 'datetime', '13', '2', 0, 0, 1, 0, '', 'None'

EXECUTE spCreateAddInTable 'SOPOrderReturnLineArch', 'SOPOrderReturnLineID', 'SOPOrderReturnLineArchX'

EXECUTE spCreateColumn 'SOPOrderReturnLineArchX', 'MaterialsRefNumber', 'nvarchar', '22', '2', 0, 0, 0, 1, '''''', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnLineArchX', 'RandomLength', 'bit', '13', '2', 0, 0, 0, 1, '0', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnLineArchX', 'PurchaseOrderNumber', 'nvarchar', '66', '2', 0, 0, 0, 1, '''''', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnLineArchX', 'CertificationRequired', 'bit', '13', '2', 0, 0, 0, 1, '0', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnLineArchX', 'LatestCost', 'decimal', '23', '5', 1, 0, 0, 1, '0', 'None'
EXECUTE spCreateColumn 'SOPOrderReturnLineArchX', 'LatestCostDate', 'datetime', '13', '2', 0, 0, 1, 0, '', 'None'


COMMIT TRANSACTION


IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryRefDivision]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW dbo.qryRefDivision
		AS
		SELECT DISTINCT 
							  dbo.StockItemSearchCatVal.ItemID, dbo.SearchCategory.SearchCategoryID, dbo.SearchValue.SearchValueID, 
							  dbo.SearchValue.Name AS ValueName
		FROM         dbo.SearchCategory INNER JOIN
							  dbo.SearchValue ON dbo.SearchCategory.SearchCategoryID = dbo.SearchValue.SearchCategoryID INNER JOIN
							  dbo.StockItemSearchCatVal ON dbo.SearchCategory.SearchCategoryID = dbo.StockItemSearchCatVal.SearchCategoryID AND 
							  dbo.SearchValue.SearchValueID = dbo.StockItemSearchCatVal.SearchValueID
		WHERE     (dbo.SearchCategory.Name = ''Division'')
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryRefGrade]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW dbo.qryRefGrade
		AS
		SELECT DISTINCT 
							  dbo.StockItemSearchCatVal.ItemID, dbo.SearchCategory.SearchCategoryID, dbo.SearchValue.SearchValueID, 
							  dbo.SearchValue.Name AS ValueName
		FROM         dbo.SearchCategory INNER JOIN
							  dbo.SearchValue ON dbo.SearchCategory.SearchCategoryID = dbo.SearchValue.SearchCategoryID INNER JOIN
							  dbo.StockItemSearchCatVal ON dbo.SearchCategory.SearchCategoryID = dbo.StockItemSearchCatVal.SearchCategoryID AND 
							  dbo.SearchValue.SearchValueID = dbo.StockItemSearchCatVal.SearchValueID
		WHERE     (dbo.SearchCategory.Name = ''Grade'')
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryRefMaterial]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW dbo.qryRefMaterial
		AS
		SELECT DISTINCT 
							  dbo.StockItemSearchCatVal.ItemID, dbo.SearchCategory.SearchCategoryID, dbo.SearchValue.SearchValueID, 
							  dbo.SearchValue.Name AS ValueName
		FROM         dbo.SearchCategory INNER JOIN
							  dbo.SearchValue ON dbo.SearchCategory.SearchCategoryID = dbo.SearchValue.SearchCategoryID INNER JOIN
							  dbo.StockItemSearchCatVal ON dbo.SearchCategory.SearchCategoryID = dbo.StockItemSearchCatVal.SearchCategoryID AND 
							  dbo.SearchValue.SearchValueID = dbo.StockItemSearchCatVal.SearchValueID
		WHERE     (dbo.SearchCategory.Name = ''Material'')
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryRefShape]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW dbo.qryRefShape
		AS
		SELECT DISTINCT 
							  dbo.StockItemSearchCatVal.ItemID, dbo.SearchCategory.SearchCategoryID, dbo.SearchValue.SearchValueID, 
							  dbo.SearchValue.Name AS ValueName
		FROM         dbo.SearchCategory INNER JOIN
							  dbo.SearchValue ON dbo.SearchCategory.SearchCategoryID = dbo.SearchValue.SearchCategoryID INNER JOIN
							  dbo.StockItemSearchCatVal ON dbo.SearchCategory.SearchCategoryID = dbo.StockItemSearchCatVal.SearchCategoryID AND 
							  dbo.SearchValue.SearchValueID = dbo.StockItemSearchCatVal.SearchValueID
		WHERE     (dbo.SearchCategory.Name = ''Shape'')
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryTreeviewSearch]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW dbo.qryTreeviewSearch
		AS
		SELECT     dbo.StockItem.ItemID, dbo.StockItem.Code, dbo.StockItem.Name, dbo.StockItem.StockUnitName, dbo.SearchValue.Name AS CatName, 
							  dbo.Warehouse.Name AS WarehouseName, dbo.WarehouseItem.QuantityOnPOPOrder AS OnOrder, 
							  dbo.WarehouseItem.ConfirmedQtyInStock + dbo.WarehouseItem.UnconfirmedQtyInStock AS InStock, 
							  dbo.WarehouseItem.QuantityAllocatedSOP + dbo.WarehouseItem.QuantityAllocatedStock + dbo.WarehouseItem.QuantityAllocatedBOM AS Allocated, 
							  dbo.WarehouseItem.QuantityOnPOPOrder + dbo.WarehouseItem.ConfirmedQtyInStock + dbo.WarehouseItem.UnconfirmedQtyInStock - dbo.WarehouseItem.QuantityAllocatedSOP
							   + dbo.WarehouseItem.QuantityAllocatedStock + dbo.WarehouseItem.QuantityAllocatedBOM AS FreeStock
		FROM         dbo.StockItem INNER JOIN
							  dbo.StockItemSearchCatVal ON dbo.StockItem.ItemID = dbo.StockItemSearchCatVal.ItemID INNER JOIN
							  dbo.SearchValue ON dbo.StockItemSearchCatVal.SearchValueID = dbo.SearchValue.SearchValueID INNER JOIN
							  dbo.WarehouseItem ON dbo.StockItem.ItemID = dbo.WarehouseItem.ItemID INNER JOIN
							  dbo.Warehouse ON dbo.WarehouseItem.WarehouseID = dbo.Warehouse.WarehouseID
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryPopulateTreeview]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW dbo.qryPopulateTreeview
		AS
		SELECT DISTINCT TOP 100 PERCENT
								  (SELECT     ValueName
									FROM          qryRefDivision
									WHERE      StockItemSearchCatVal.ItemID = ItemID) AS Division,
								  (SELECT     ValueName
									FROM          qryRefMaterial
									WHERE      StockItemSearchCatVal.ItemID = ItemID) AS Material,
								  (SELECT     ValueName
									FROM          qryRefShape
									WHERE      StockItemSearchCatVal.ItemID = ItemID) AS Shape,
								  (SELECT     ValueName
									FROM          qryRefGrade
									WHERE      StockItemSearchCatVal.ItemID = ItemID) AS Grade
		FROM         dbo.StockItem INNER JOIN
							  dbo.StockItemSearchCatVal ON dbo.StockItem.ItemID = dbo.StockItemSearchCatVal.ItemID
		ORDER BY Division, Material, Shape, Grade
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryPopulateTreeviewWithItemID]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW dbo.qryPopulateTreeviewWithItemID
		AS
		SELECT DISTINCT TOP 100 PERCENT dbo.StockItem.ItemID,
								  (SELECT     ValueName
									FROM          qryRefDivision
									WHERE      StockItemSearchCatVal.ItemID = ItemID) AS Division,
								  (SELECT     ValueName
									FROM          qryRefMaterial
									WHERE      StockItemSearchCatVal.ItemID = ItemID) AS Material,
								  (SELECT     ValueName
									FROM          qryRefShape
									WHERE      StockItemSearchCatVal.ItemID = ItemID) AS Shape,
								  (SELECT     ValueName
									FROM          qryRefGrade
									WHERE      StockItemSearchCatVal.ItemID = ItemID) AS Grade
		FROM         dbo.StockItem INNER JOIN
							  dbo.StockItemSearchCatVal ON dbo.StockItem.ItemID = dbo.StockItemSearchCatVal.ItemID
		ORDER BY Division, Material, Shape, Grade
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryTreeviewSearch2]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW dbo.qryTreeviewSearch2
		AS
		SELECT DISTINCT 
							  dbo.qryTreeviewSearch.Code, dbo.qryTreeviewSearch.Name, dbo.qryTreeviewSearch.StockUnitName, dbo.qryTreeviewSearch.OnOrder, 
							  dbo.qryTreeviewSearch.InStock, dbo.qryTreeviewSearch.FreeStock, dbo.qryTreeviewSearch.Allocated, dbo.qryPopulateTreeviewWithItemID.Division, 
							  dbo.qryPopulateTreeviewWithItemID.Material, dbo.qryPopulateTreeviewWithItemID.Shape, dbo.qryPopulateTreeviewWithItemID.Grade
		FROM         dbo.qryPopulateTreeviewWithItemID INNER JOIN
							  dbo.qryTreeviewSearch ON dbo.qryPopulateTreeviewWithItemID.ItemID = dbo.qryTreeviewSearch.ItemID
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryTreeviewSearchGroupedByWarehouse]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW dbo.qryTreeviewSearchGroupedByWarehouse
		AS
		SELECT     Code, Name, StockUnitName, SUM(OnOrder) AS OnOrder, SUM(InStock) AS InStock, SUM(FreeStock) AS FreeStock, SUM(Allocated) AS Allocated, 
							  Division, Material, Shape, Grade
		FROM         dbo.qryTreeviewSearch2
		GROUP BY Code, Name, StockUnitName, Division, Material, Shape, Grade
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qrySOPShortageRaw0]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW [dbo].[qrySOPShortageRaw0]
		AS
		SELECT     dbo.SOPOrderReturn.SOPOrderReturnID, dbo.SOPOrderReturn.DocumentNo, SUM(ISNULL(dbo.SOPOrderReturnLine.LineQuantity, 0) 
							  - ISNULL(dbo.SOPOrderReturnLine.AllocatedQuantity, 0)) AS Shortage
		FROM         dbo.SOPOrderReturn LEFT OUTER JOIN
							  dbo.SOPOrderReturnLine ON dbo.SOPOrderReturn.SOPOrderReturnID = dbo.SOPOrderReturnLine.SOPOrderReturnID
		WHERE     (dbo.SOPOrderReturn.DocumentStatusID = 0) AND (dbo.SOPOrderReturn.DocumentTypeID = 0) AND (dbo.SOPOrderReturnLine.LineTypeID = 0)
		GROUP BY dbo.SOPOrderReturn.SOPOrderReturnID, dbo.SOPOrderReturn.DocumentNo
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qrySOPShortageRaw1]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW [dbo].[qrySOPShortageRaw1]
		AS
		SELECT     dbo.SOPOrderReturn.SOPOrderReturnID, dbo.SOPOrderReturn.DocumentNo, SUM(0) AS Shortage
		FROM         dbo.SOPOrderReturn LEFT OUTER JOIN
							  dbo.SOPOrderReturnLine ON dbo.SOPOrderReturn.SOPOrderReturnID = dbo.SOPOrderReturnLine.SOPOrderReturnID
		WHERE     (dbo.SOPOrderReturn.DocumentStatusID = 0) AND (dbo.SOPOrderReturn.DocumentTypeID = 0) AND (dbo.SOPOrderReturnLine.LineTypeID = 2)
		GROUP BY dbo.SOPOrderReturn.SOPOrderReturnID, dbo.SOPOrderReturn.DocumentNo
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qrySOPShortage]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW [dbo].[qrySOPShortage]
		AS
		SELECT     SOPOrderReturnID, DocumentNo, Shortage
		FROM         dbo.qrySOPShortageRaw0
		UNION ALL
		SELECT     SOPOrderReturnID, DocumentNo, Shortage
		FROM         dbo.qrySOPShortageRaw1
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qrySOPPrintPickingLists]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW [dbo].[qrySOPPrintPickingLists]
		AS
		SELECT     dbo.SOPOrderReturn.DocumentNo, dbo.SOPOrderReturn.DocumentDate, dbo.SOPOrderReturn.PromisedDeliveryDate, 
							  dbo.SLCustomerAccount.CustomerAccountNumber, dbo.SLCustomerAccount.CustomerAccountName, 
							  dbo.SOPOrderReturn.CustomerDocumentNo, dbo.SOPOrderReturn.TotalGrossValue
		FROM         dbo.SOPOrderReturn INNER JOIN
							  dbo.SLCustomerAccount ON dbo.SOPOrderReturn.CustomerID = dbo.SLCustomerAccount.SLCustomerAccountID INNER JOIN
							  dbo.qrySOPShortage ON dbo.SOPOrderReturn.SOPOrderReturnID = dbo.qrySOPShortage.SOPOrderReturnID
		WHERE     (dbo.qrySOPShortage.Shortage = 0)
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[tblPOPNotes]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
	CREATE TABLE [dbo].[tblPOPNotes](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[LogType] [int] NULL,
		[Message] [ntext] NULL,
		[POPOrderReturnID] [bigint] NULL,
		[POPOrderReturnLineID] [bigint] NULL,
		[Date] [datetime] NULL,
		[SageUser] [nvarchar](50) NULL
	) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[tblSOPNotes]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
	CREATE TABLE [dbo].[tblSOPNotes](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[LogType] [int] NOT NULL,
		[Message] [ntext] NOT NULL,
		[SOPOrderReturnId] [bigint] NOT NULL,
		[SOPOrderReturnLineId] [bigint] NOT NULL,
		[Date] [datetime] NOT NULL,
		[SageUser] [nvarchar](50) NOT NULL
	) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryStockEnquiryPORaw]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW [dbo].[qryStockEnquiryPORaw]
		AS
		SELECT     dbo.StockItem.ItemID, dbo.TransactionHistory.TransactionDate, dbo.TransactionType.TransactionTypeName, 
							  dbo.PLSupplierAccount.SupplierAccountName, dbo.TransactionHistory.Reference, dbo.TransactionHistory.SecondReference, 
							  dbo.TransactionHistory.Quantity, dbo.TransactionHistory.UnitCostPrice, ISNULL(dbo.POPOrderReturnLine.UnitBuyingPrice, 0) AS UnitSellingPrice, 
							  ISNULL(dbo.POPOrderReturnLine.UnitBuyingPrice, 0) - ISNULL(dbo.TransactionHistory.UnitCostPrice, 0) AS ProfitValue, 
							  (CASE WHEN ISNULL(dbo.pOPOrderReturnLine.UnitbuyingPrice, 0) <> 0 THEN ISNULL(dbo.pOPOrderReturnLine.UnitbuyingPrice, 0) 
							  - ISNULL(dbo.TransactionHistory.UnitCostPrice, 0) / ISNULL(dbo.pOPOrderReturnLine.UnitbuyingPrice, 0) * 100 ELSE 0 END) AS ProfitPercentage, 
							  dbo.TransactionHistory.TransactionHistoryID, dbo.POPOrderReturnLine.ItemCode
		FROM         dbo.POPOrderReturnLine INNER JOIN
							  dbo.POPOrderReturn ON dbo.POPOrderReturnLine.POPOrderReturnID = dbo.POPOrderReturn.POPOrderReturnID INNER JOIN
							  dbo.StockItem INNER JOIN
							  dbo.TransactionHistory ON dbo.StockItem.ItemID = dbo.TransactionHistory.ItemID INNER JOIN
							  dbo.TransactionType ON dbo.TransactionHistory.TransactionTypeID = dbo.TransactionType.TransactionTypeID ON 
							  dbo.POPOrderReturn.DocumentNo = dbo.TransactionHistory.Reference AND dbo.POPOrderReturnLine.ItemCode = dbo.StockItem.Code INNER JOIN
							  dbo.PLSupplierAccount ON dbo.POPOrderReturn.SupplierID = dbo.PLSupplierAccount.PLSupplierAccountID
		WHERE     (dbo.TransactionType.TransactionTypeID = 17) AND (dbo.POPOrderReturnLine.LineTypeID = 0)
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryStockEnquirySORaw]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW [dbo].[qryStockEnquirySORaw]
		AS
		SELECT     dbo.StockItem.ItemID, dbo.TransactionHistory.TransactionDate, dbo.TransactionType.TransactionTypeName, 
							  dbo.SLCustomerAccount.CustomerAccountName, dbo.TransactionHistory.Reference, dbo.TransactionHistory.SecondReference, 
							  - dbo.TransactionHistory.Quantity AS Quantity, dbo.TransactionHistory.UnitCostPrice, ISNULL(dbo.SOPOrderReturnLine.UnitSellingPrice, 0) 
							  AS UnitSellingPrice, ISNULL(dbo.SOPOrderReturnLine.UnitSellingPrice, 0) - ISNULL(dbo.SOPOrderReturnLine.CostPrice, 0) AS ProfitValue, 
							  (CASE WHEN ISNULL(dbo.SOPOrderReturnLine.UnitSellingPrice, 0) <> 0 THEN ISNULL(dbo.SOPOrderReturnLine.UnitSellingPrice, 0) 
							  - ISNULL(dbo.SOPOrderReturnLine.CostPrice, 0) / ISNULL(dbo.SOPOrderReturnLine.UnitSellingPrice, 0) * 100 ELSE 0 END) AS ProfitPercentage, 
							  dbo.TransactionHistory.TransactionHistoryID, dbo.SOPOrderReturnLine.CostPrice
		FROM         dbo.SOPOrderReturnLine INNER JOIN
							  dbo.SOPOrderReturn ON dbo.SOPOrderReturnLine.SOPOrderReturnID = dbo.SOPOrderReturn.SOPOrderReturnID INNER JOIN
							  dbo.StockItem INNER JOIN
							  dbo.TransactionHistory ON dbo.StockItem.ItemID = dbo.TransactionHistory.ItemID INNER JOIN
							  dbo.TransactionType ON dbo.TransactionHistory.TransactionTypeID = dbo.TransactionType.TransactionTypeID ON 
							  dbo.SOPOrderReturn.DocumentNo = dbo.TransactionHistory.Reference AND dbo.SOPOrderReturnLine.ItemCode = dbo.StockItem.Code INNER JOIN
							  dbo.SLCustomerAccount ON dbo.SOPOrderReturn.CustomerID = dbo.SLCustomerAccount.SLCustomerAccountID
		WHERE     (dbo.TransactionType.TransactionTypeID = 15) AND (dbo.SOPOrderReturnLine.LineTypeID = 0)
		GROUP BY dbo.StockItem.ItemID, dbo.TransactionHistory.TransactionDate, dbo.TransactionType.TransactionTypeName, 
							  dbo.SLCustomerAccount.CustomerAccountName, dbo.TransactionHistory.Reference, dbo.TransactionHistory.SecondReference, 
							  - dbo.TransactionHistory.Quantity, dbo.TransactionHistory.UnitCostPrice, ISNULL(dbo.SOPOrderReturnLine.UnitSellingPrice, 0), 
							  ISNULL(dbo.SOPOrderReturnLine.UnitSellingPrice, 0) - ISNULL(dbo.SOPOrderReturnLine.CostPrice, 0), dbo.TransactionHistory.TransactionHistoryID, 
							  dbo.SOPOrderReturnLine.CostPrice
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryStockEnquiry]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW [dbo].[qryStockEnquiry]
		AS
		SELECT     ItemID, TransactionDate, TransactionTypeName, CustomerAccountName, Reference, SecondReference, Quantity, UnitCostPrice, UnitSellingPrice, 
							  ProfitValue, ProfitPercentage
		FROM         qryStockEnquirySORaw
		UNION ALL
		SELECT     ItemID, TransactionDate, TransactionTypeName, SupplierAccountName, Reference, SecondReference, Quantity, UnitCostPrice, UnitSellingPrice, 
							  ProfitValue, ProfitPercentage
		FROM         qryStockEnquiryPORaw
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[qryStockHistoryEnquiry]') AND OBJECTPROPERTY(id, N'IsView') = 1)
BEGIN
	EXEC('
		CREATE VIEW [dbo].[qryStockHistoryEnquiry]
		AS
		SELECT     dbo.StockItem.ItemID, dbo.TransactionHistory.TransactionDate, dbo.TransactionType.TransactionTypeName, 
					  CASE WHEN dbo.TransactionHistory.SourceAreaTypeID = 1 THEN dbo.SLCustomerAccount.CustomerAccountNumber + '' / '' + dbo.SLCustomerAccount.CustomerAccountName
					   WHEN dbo.TransactionHistory.SourceAreaTypeID = 0 THEN dbo.PLSupplierAccount.SupplierAccountNumber + '' / '' + dbo.PLSupplierAccount.SupplierAccountName ELSE
					   '''' END AS CustomerSupplier, dbo.TransactionHistory.Reference, dbo.TransactionHistory.SecondReference, 
					  CASE WHEN TransactionGroupID = 2 THEN dbo.TransactionHistory.Quantity ELSE 0 END AS QtyIn, 
					  CASE WHEN TransactionGroupID = 1 THEN dbo.TransactionHistory.Quantity ELSE 0 END AS QtyOut, 
					  ISNULL(CASE WHEN dbo.TransactionHistory.SourceAreaTypeID = 1 THEN ISNULL(dbo.SOPOrderReturnLine.CostPrice, dbo.TransactionHistory.UnitCostPrice) 
					  ELSE dbo.TransactionHistory.UnitCostPrice END, 0) AS UnitCostPrice, dbo.TransactionHistory.UnitIssuePrice, 
					  ISNULL(CASE WHEN (dbo.TransactionHistory.IssueValue <> 0 AND dbo.TransactionHistory.SourceAreaTypeID = 1) 
					  THEN (dbo.TransactionHistory.UnitIssuePrice - CostPrice) / dbo.TransactionHistory.UnitIssuePrice * 100 WHEN (dbo.TransactionHistory.IssueValue <> 0 AND 
					  dbo.TransactionHistory.SourceAreaTypeID <> 1) THEN (dbo.TransactionHistory.UnitIssuePrice - UnitCostPrice) 
					  / dbo.TransactionHistory.UnitIssuePrice * 100 ELSE 0 END, 0) AS MarginPC
	FROM         dbo.SOPOrderReturn INNER JOIN
					  dbo.SLCustomerAccount ON dbo.SOPOrderReturn.CustomerID = dbo.SLCustomerAccount.SLCustomerAccountID RIGHT OUTER JOIN
					  dbo.SOPDespatchReceiptLine INNER JOIN
					  dbo.SOPOrderReturnLine ON dbo.SOPDespatchReceiptLine.SOPOrderReturnLineID = dbo.SOPOrderReturnLine.SOPOrderReturnLineID RIGHT OUTER JOIN
					  dbo.StockItem INNER JOIN
					  dbo.TransactionHistory ON dbo.StockItem.ItemID = dbo.TransactionHistory.ItemID INNER JOIN
					  dbo.TransactionType ON dbo.TransactionHistory.TransactionTypeID = dbo.TransactionType.TransactionTypeID ON 
					  dbo.SOPDespatchReceiptLine.DespatchReceiptQuantity = dbo.TransactionHistory.Quantity AND 
					  dbo.SOPDespatchReceiptLine.OrderReturnNo = dbo.TransactionHistory.Reference AND 
					  dbo.SOPDespatchReceiptLine.DespatchReceiptNo = dbo.TransactionHistory.SecondReference AND dbo.SOPOrderReturnLine.ItemCode = dbo.StockItem.Code ON 
					  dbo.SOPOrderReturn.DocumentNo = dbo.TransactionHistory.Reference LEFT OUTER JOIN
					  dbo.PLSupplierAccount INNER JOIN
					  dbo.POPOrderReturn ON dbo.PLSupplierAccount.PLSupplierAccountID = dbo.POPOrderReturn.SupplierID ON 
					  dbo.TransactionHistory.Reference = dbo.POPOrderReturn.DocumentNo
	WHERE     (dbo.TransactionType.TransactionGroupID = 1) OR
					  (dbo.TransactionType.TransactionGroupID = 2)
	')
END
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[tblSOPNotes]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
BEGIN
	CREATE TABLE [dbo].[tblTreeviewLastNode](
		[ID] [bigint] IDENTITY(1,1) NOT NULL,
		[Username] [nvarchar](50) NULL,
		[LastNode] [nvarchar](50) NULL
	) ON [PRIMARY]
END
GO
