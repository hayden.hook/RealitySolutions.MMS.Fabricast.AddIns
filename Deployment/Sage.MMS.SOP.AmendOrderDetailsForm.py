clr.AddReference("RealitySolutions.MMS.Fabricast.AddIns")

from Microsoft.VisualBasic import *

from RealitySolutions.MMS.SagePy import clsLostFunctions
from RealitySolutions.MMS.Fabricast.AddIns import *

import Sage
import Sage.Accounting.SOP
from Sage.Accounting import *
from Sage.Common import *
from Sage.Common.Amendability import *
from Sage.Common.UI import *
from Sage.ObjectStore import *

import System
from System.Collections import *
from System.Data import *
from System.Data.SqlClient import *
from System.Drawing import *
from System.Windows.Forms import ComboBoxStyle

LF = clsLostFunctions()


# Declare amendable control objects
oTabControl = None

acNewTab = None
acLogTab = None


lblAccountCustomer = None
lblContact = None
lblDeliveryMethod = None

cmbAccountCustomer = None
cmbDeliveryMethod = None

txtContact = None

oLogGrid = None
oLogGridUL = None

cmbAC = None
cmbDM = None


oApp = Sage.Accounting.Application()

sqlConn = SqlConnection(oApp.ActiveCompany.ConnectString)


def main():
	global oTabControl, acNewTab, acLogTab, lblAccountCustomer, lblContact, lblDeliveryMethod, cmbAccountCustomer, cmbDeliveryMethod, txtContact, oLogGrid, oLogGridUL, cmbAC, cmbDM, oApp, sqlConn
	
	
	# Find the grid control and add the column
	oTabControl = Form.FindControlByName("tabControl")
	
	# Add the tab page
	oTabControl.Controls.Add("System.Windows.Forms.TabPage", "tabNew")
	oTabControl.Controls.Add("System.Windows.Forms.TabPage", "tabLog")
	
	
	acNewTab = Form.FindControlByName("tabNew")
	
	acNewTab.Text = "Additional"
	acNewTab.Controls.Add("System.Windows.Forms.Label", "lblAccountCustomer")
	acNewTab.Controls.Add("System.Windows.Forms.Label", "lblContact")
	acNewTab.Controls.Add("System.Windows.Forms.Label", "lblDeliveryMethod")
	acNewTab.Controls.Add("System.Windows.Forms.ComboBox", "cmbAccountCustomer")
	acNewTab.Controls.Add("System.Windows.Forms.ComboBox", "cmbDeliveryMethod")
	acNewTab.Controls.Add("Sage.Common.Controls.MaskedTextBox", "txtContact")
	
	
	acLogTab = Form.FindControlByName("tabLog")
	
	acLogTab.Text = "Log"
	acLogTab.Controls.Add("Sage.ObjectStore.Controls.Grid", "grdLog")
	
	
	# Link the controls
	lblAccountCustomer = Form.FindControlByName("lblAccountCustomer")
	lblContact = Form.FindControlByName("lblContact")
	lblDeliveryMethod = Form.FindControlByName("lblDeliveryMethod")
	
	cmbAccountCustomer = Form.FindControlByName("cmbAccountCustomer")
	cmbDeliveryMethod = Form.FindControlByName("cmbDeliveryMethod")
	
	txtContact = Form.FindControlByName("txtContact")
	
	
	
	# Move the controls to where they need to be
	lblAccountCustomer.Location = Point(8, 8)
	lblAccountCustomer.Size = Size(100, 23)
	lblAccountCustomer.Text = "Account Customer:"
	
	lblContact.Location = Point(8, 62)
	lblContact.Size = Size(46, 18)
	lblContact.Text = "Contact:"
	
	lblDeliveryMethod.Location = Point(8, 36)
	lblDeliveryMethod.Size = Size(100, 23)
	lblDeliveryMethod.Text = "Delivery Method:"
	
	
	cmbAccountCustomer.Location = Point(120, 4)
	cmbAccountCustomer.Size = Size(176, 21)
	cmbAccountCustomer.Text = ""
	
	cmbDeliveryMethod.Location = Point(120, 32)
	cmbDeliveryMethod.Size = Size(176, 21)
	cmbDeliveryMethod.Text = ""
	
	txtContact.Location = Point(120, 59)
	txtContact.Size = Size(176, 21)
	txtContact.Text = ""
	
	
	# Cast the controls so we can add items to them
	cmbAC = cmbAccountCustomer.UnderlyingControl
	
	cmbAC.DropDownStyle = ComboBoxStyle.DropDownList
	cmbAC.Items.Add("Account/Customer")
	cmbAC.Items.Add("Cash")
	cmbAC.SelectedIndex = 0 # Select the first on by default
	
	
	cmbDM = cmbDeliveryMethod.UnderlyingControl
	
	cmbDM.DropDownStyle = ComboBoxStyle.DropDownList
	cmbDM.Items.Add("Our Transport")
	cmbDM.Items.Add("Collection")
	cmbDM.Items.Add("Carrier")
	cmbDM.Items.Add("Other")
	cmbDM.SelectedIndex = 0 # Select the first on by default
	
	
	oLogGrid = Form.FindControlByName("grdLog")
	oLogGridUL = oLogGrid.UnderlyingControl
	
	oLogGridUL.Location = Point(3, 3)
	oLogGridUL.Size = Size(758, 459)
	oLogGridUL.Text = ""
	
	a = Sage.ObjectStore.Controls.GridColumn()
	b = Sage.ObjectStore.Controls.GridColumn()
	c = Sage.ObjectStore.Controls.GridColumn()
	
	a.AutoSize = True
	a.Caption = "Date"
	a.DisplayMember = "DateOfMessage"
	a.Width = 251
	
	b.AutoSize = True
	b.Caption = "Message"
	b.DisplayMember = "Message"
	b.Width = 251
	
	c.AutoSize = True
	c.Caption = "Username"
	c.DisplayMember = "Username"
	c.Width = 252
	
	oLogGridUL.Columns.Add(a)
	oLogGridUL.Columns.Add(b)
	oLogGridUL.Columns.Add(c)
	
	
	
	# Add handlers
	Form.DataRefresh += Form_DataRefresh
	Form.BeginSave += Form_BeginSave



##################################################################
# Captured handlers
##################################################################
def Form_DataRefresh(args):
	global cmbAC, cmbDM, oLogGridUL, oSOPOrder
	
	
	oSOPOrder = args.Source
	
	if oSOPOrder is not None:
		LocateTextInCombo(cmbAC, oSOPOrder.Fields["AccountCustomer"].GetString())
		LocateTextInCombo(cmbDM, oSOPOrder.Fields["DeliveryMethod"].GetString())
		txtContact.Text = oSOPOrder.Fields["Contact"].GetString()
		
		# Log grid
		try:
			dtTEMP = ExecuteQuery("SELECT [Date], Message, SageUser FROM tblSOPNotes WHERE SOPOrderReturnID = %s ORDER BY [Date] DESC" % oSOPOrder.SOPOrderReturn)
			
			oItems = SOPPOPLogEntityCollection()
			oItem = None
			
			
			iDate = LF.ColumnIndex(dtTEMP, "Date")
			iMessage = LF.ColumnIndex(dtTEMP, "Message")
			iSageUser = LF.ColumnIndex(dtTEMP, "SageUser")
			
			for rowEach in dtTEMP.Rows:
				oItem = SOPPOPLogEntity()
				
				oItem.DateOfMessage = LF.CDate(rowEach.Item[iDate])
				oItem.Message = rowEach.Item[iMessage].ToString()
				oItem.Username = rowEach.Item[iSageUser].ToString()
				
				oItems.Add(oItem)
			
			# Set the grid to use the metadata
			oLogGridUL.DataSource = oItems
			
		except Exception, ex:
			LF.MsgBox("An error has occurred while populating the payments grid.\r\n\r\n" + str(ex), MsgBoxStyle.Critical, "Error")
			return
	
def Form_BeginSave(sender, e):
	global cmbAC, cmbDM
	
	
	try:
		oSOP = Form.BoundObjects[1]
		
		if oSOP is not None:
			oSOP.Fields["AccountCustomer"].Value = cmbAC.Text
			oSOP.Fields["DeliveryMethod"].Value = cmbDM.Text
			oSOP.Fields["Contact"].Value = txtContact.Text
			
			
			# NEW: Ensure we have our dot item if there are no stock items
			oDot = Sage.Accounting.SOP.SOPStandardItemLine()
			
			bolDotFound = False
			
			for oItem in oSOP.Lines:
				if oItem.LineType == Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeStandard:
					
					if oItem.ItemCode == ".":
						bolDotFound = True
						break
			
			if bolDotFound == False:
				# Dot not found, create it
				oUnitSellingEx = type(Sage.Accounting.Exceptions.Ex20319Exception)
				oStockItemHoldingEx = type(Sage.Accounting.Exceptions.StockItemHoldingInsufficientException)
				
				oStockItems = Sage.Accounting.Stock.StockItems()
				oStockItem = oStockItems["."]
				
				if oStockItem is None:
					raise Exception("Unable to locate the picking dot item.")
				
				oTaxCodes = oApp.TaxModule.TaxCodes
				oTaxCode = None
				
				oTaxCodes.Query.Filters.Add(Sage.ObjectStore.Filter(Sage.Accounting.TaxModule.TaxCode.FIELD_CODE, "1"))
				oTaxCode = oTaxCodes.First
				
				if oTaxCode is None:
					raise Exception("Unable to locate the T1 tax code.")
				
				Sage.Accounting.Application.AllowableWarnings.Add(oDot, oUnitSellingEx)
				Sage.Accounting.Application.AllowableWarnings.Add(oDot, oStockItemHoldingEx)
				
				oDot.SOPOrderReturn = oSOP
				oDot.Item = oStockItem
				oDot.LineQuantity = 1
				oDot.UnitSellingPrice = 0
				oDot.TaxCode = oTaxCode
				
				oDot.Post()
				
				oSOP.Lines.Add(oDot)
				oSOP.Update()
				
				
				# NEW: Setting the allocation qty doesn#t allocate the line...
				oAllo = Sage.Accounting.SOP.SOPAllocationAdjustment()
				
				oAllo.OrderNumberFrom = oSOP.DocumentNo
				oAllo.OrderNumberTo = oSOP.DocumentNo
				
				try:
					oAllo.PopulateManualAllocations()
					
				except:
					# Can cause an exception when the free text item is dispatched
					pass
				
				for oManLine in oAllo.ManualLines:
					for oAdjLine in oManLine.AllocationAdjustmentLines:
						
						if oAdjLine.ItemCode == ".":
							oAdjLine.NewAllocatedQuantity = oAdjLine.OutstandingQuantity
							oAdjLine.Post()
							break
				
			oSOP.Update()
		
		else:
			raise Exception("oSOP object is null.")
		
	except Exception, ex:
		LF.MsgBox("An error has occurred while saving the additional information.\r\n\r\n" + str(ex), MsgBoxStyle.Critical, "Error")
		return
	

def LocateTextInCombo(cmbCombo, strCombo):
	intX = 0
	
	for intX in xrange(0, cmbCombo.Items.Count):
		if cmbCombo.Items[intX].ToString() == strCombo:
			cmbCombo.SelectedIndex = intX
			break
			

def ExecuteQuery(strQuery):
	dtTEMP = DataTable("dtTEMP")

	sqlDA = SqlDataAdapter(strQuery, sqlConn)


	try:
		dtTEMP.Clear()

		sqlDA.Fill(dtTEMP)

	except:
		dtTEMP.Clear()

	finally:
		# Dispose when done
		sqlDA.Dispose()
		sqlDA = None

	return dtTEMP


# Run main
main()
