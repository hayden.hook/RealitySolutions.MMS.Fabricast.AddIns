clr.AddReference("RealitySolutions.MMS.Fabricast.AddIns")

from Microsoft.VisualBasic import *

from RealitySolutions.MMS.SagePy import clsLostFunctions
from RealitySolutions.MMS.Fabricast.AddIns import *

import Sage
import Sage.Accounting.SOP
from Sage.Accounting import *
from Sage.Common import *
from Sage.Common.Amendability import *
from Sage.Common.UI import *
from Sage.ObjectStore import *

from System.Collections import *
from System.Data import *
from System.Data.SqlClient import *
from System.Drawing import *

LF = clsLostFunctions()
 

# Declare amendable control objects
oGrid = None
oButtonPanel = None

oOldPrintButton = None
oOldPrintAllButton = None

oNewPrintButton = None
oNewPrintAllButton = None

oDisplayButton = None


def main():
	global oGrid, oButtonPanel, oOldPrintButton, oOldPrintAllButton, oNewPrintButton, oNewPrintAllButton, oDisplayButton
	
	
	# Add the new label
	oButtonPanel = Form.FindControlByName("buttonPanel")
	oButtonPanel.Controls.Add("System.Windows.Forms.Button", "cmdNewPrint")
	oButtonPanel.Controls.Add("System.Windows.Forms.Button", "cmdNewPrintAll")
	
	oDisplayButton = Form.FindControlByName("displayButton")
	
	
	# Locate it
	oNewPrintButton = Form.FindControlByName("cmdNewPrint")
	oNewPrintAllButton = Form.FindControlByName("cmdNewPrintAll")
	
	
	# Configure it
	oNewPrintButton.Enabled = False
	oNewPrintButton.Height = 26
	oNewPrintButton.Width = 65
	oNewPrintButton.Location = Point(0, 10)
	oNewPrintButton.Text = "&Print"
	oNewPrintButton.BringToFront()
	
	oNewPrintAllButton.Enabled = False
	oNewPrintAllButton.Height = 26
	oNewPrintAllButton.Width = 65
	oNewPrintAllButton.Location = Point(70, 10)
	oNewPrintAllButton.Text = "Print &All"
	oNewPrintAllButton.BringToFront()
	
	
	oOldPrintButton = Form.FindControlByName("printButton")
	oOldPrintAllButton = Form.FindControlByName("printAllButton")
	
	oGrid = Form.FindControlByName("list")
	
	
	oOldPrintButton.Visible = True
	oOldPrintButton.SendToBack()
	
	oOldPrintAllButton.Visible = True
	oOldPrintAllButton.SendToBack()
	
	
	# Add handlers
	oNewPrintButton.Click += cmdNewPrint_Click
	oNewPrintAllButton.Click += cmdNewPrintAll_Click
	
	oDisplayButton.Click += cmdDisplay_Click
	


##################################################################
# Captured handlers
##################################################################
def cmdNewPrint_Click(sender, e):
	if CheckPrice(False) == True:
		
		if LF.MsgBox("Are you sure you invoice the selected orders?", MsgBoxStyle.Question | MsgBoxStyle.YesNo | MsgBoxStyle.DefaultButton2, "Question") == MsgBoxResult.Yes:
			oButton = oOldPrintButton.UnderlyingControl
			oButton.PerformClick()
	
def cmdNewPrintAll_Click(sender, e):
	if CheckPrice(True) == True:
		
		if LF.MsgBox("Are you sure you invoice the all of the orders?", MsgBoxStyle.Question | MsgBoxStyle.YesNo | MsgBoxStyle.DefaultButton2, "Question") == MsgBoxResult.Yes:
			oButton = oOldPrintAllButton.UnderlyingControl
			oButton.PerformClick()
	

def cmdDisplay_Click(sender, e):
	global oGrid, oNewPrintButton, oNewPrintAllButton
	
	
	oDCGrid = oGrid.UnderlyingControl
	
	if oDCGrid is not None:
	
		if oDCGrid.Items.Count == 0:
			oNewPrintButton.Enabled = False
			oNewPrintAllButton.Enabled = False
		
		else:
			oNewPrintButton.Enabled = True
			oNewPrintAllButton.Enabled = True
	


########################################
# Functions
########################################
def CheckPrice(bolPrintAll):
	global oGrid
	
	
	oDCGrid = oGrid.UnderlyingControl
	
	if oDCGrid is not None:
		intX = 0
		
		rowCurrent = None
		
		sopOrders = None
		sopOrder = None
		
		for rowCurrent in oDCGrid.Items:
			# Not the best way of doing it, but it'll do
			ignore = False
			
			if bolPrintAll == False:
				
				if rowCurrent.Selected == False:
					ignore = True
					
				else:
					intX += 1
			
			
			if ignore == False:
				# Locate the SOP the collection so we can get the spare bit flag
				sopOrders = Sage.Accounting.SOP.SOPOrders()
				sopOrders.Query.Filters.Add(Sage.ObjectStore.Filter(Sage.Accounting.SOP.SOPOrder.FIELD_DOCUMENTNO, rowCurrent.SubItems[0].Text))
				sopOrders.Query.Find()
				
				if sopOrders.Count == 1:
					sopOrder = sopOrders.First
					
					if sopOrder.SpareBit3 == False:
						# STOP!
						LF.MsgBox("Order number " + rowCurrent.SubItems[0].Text + " hasn't been price checked.\r\n\r\n" + "Cannot invoice.", MsgBoxStyle.Exclamation, "Not Price Checked")
						return False
					
				else:
					LF.MsgBox("Failed to locate SOP number " + rowCurrent.SubItems[0].Text + " in the collection.", MsgBoxStyle.Critical, "SOP Not Found")
					return False
			
		if (bolPrintAll == False) and (intX > 0):
			return True
			
		elif (bolPrintAll == True):
			return True
			
		else:
			return False
			
	else:
		return False
	

# Run main
main()
