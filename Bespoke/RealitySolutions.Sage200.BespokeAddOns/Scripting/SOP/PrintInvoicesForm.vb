﻿Imports System.Drawing

Namespace Scripting.SOP
    Public Class PrintInvoicesForm
        Inherits FormScript(Of Sage.MMS.SOP.PrintInvoicesForm)
        Dim LF = New clsLostFunctions()


        Dim oGrid = Nothing
        Dim oButtonPanel = Nothing
        Dim oOldPrintButton = Nothing
        Dim oOldPrintAllButton = Nothing
        Dim oNewPrintButton As Sage.Common.Controls.Button = Nothing
        Dim oNewPrintAllButton As Sage.Common.Controls.Button = Nothing
        Dim oDisplayButton = Nothing
        Public Overrides Sub Main()


            oButtonPanel = Form.FindControlByName("buttonPanel")
            oButtonPanel.Controls.Add("System.Windows.Forms.Button", "cmdNewPrint")
            oButtonPanel.Controls.Add("System.Windows.Forms.Button", "cmdNewPrintAll")

            oDisplayButton = CType(Form.FindControlByName("displayButton"), Sage.Common.Controls.Button)
            oNewPrintButton = CType(Form.FindControlByName("cmdNewPrint"), Sage.Common.Controls.Button)
            oDisplayButton = CType(Form.FindControlByName("cmdNewPrintAll"), Sage.Common.Controls.Button)

            oNewPrintButton.Enabled = False
            oNewPrintButton.Height = 26
            oNewPrintButton.Width = 65
            oNewPrintButton.Location = New Point(0, 10)
            oNewPrintButton.Text = "&Print"
            oNewPrintButton.BringToFront()

            oNewPrintAllButton.Enabled = False
            oNewPrintAllButton.Height = 26
            oNewPrintAllButton.Width = 65
            oNewPrintAllButton.Location = New Point(70, 10)
            oNewPrintAllButton.Text = "Print &All"
            oNewPrintAllButton.BringToFront()


            oOldPrintButton = Form.FindControlByName("printButton")
            oOldPrintAllButton = Form.FindControlByName("printAllButton")

            oGrid = Form.FindControlByName("list")


            oOldPrintButton.Visible = True
            oOldPrintButton.SendToBack()

            oOldPrintAllButton.Visible = True
            oOldPrintAllButton.SendToBack()


            AddHandler oNewPrintButton.Click, AddressOf cmdNewPrint_Click
            AddHandler oNewPrintAllButton.Click, AddressOf cmdNewPrintAll_Click
            AddHandler oDisplayButton.Click, AddressOf cmdDisplay_Click


        End Sub



        Sub cmdNewPrint_Click(sender As Object, e As EventArgs)
            If CheckPrice(False) = True Then
                If LF.MsgBox("Are you sure you invoice the selected orders?", MsgBoxStyle.Question, MsgBoxStyle.YesNo, MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.Yes Then
                    Dim oButton = oOldPrintButton.UnderlyingControl
                    oButton.PerformClick()
                End If
            End If
        End Sub



        Sub cmdNewPrintAll_Click(sender As Object, e As EventArgs)
            If CheckPrice(True) = True Then
                If LF.MsgBox("Are you sure you invoice the all of the orders?", MsgBoxStyle.Question, MsgBoxStyle.YesNo, MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.Yes Then
                    Dim oButton = oOldPrintAllButton.UnderlyingControl
                    oButton.PerformClick()
                End If
            End If


        End Sub



        Sub cmdDisplay_Click(sender As Object, e As EventArgs)
            Dim oDCGrid = oGrid.UnderlyingControl

            If oDCGrid = Not Nothing Then
                If oDCGrid.Items.Count = 0 Then
                    oNewPrintButton.Enabled = False
                    oNewPrintAllButton.Enabled = False

                Else
                    oNewPrintButton.Enabled = True
                    oNewPrintAllButton.Enabled = True
                End If
            End If
        End Sub


        Function CheckPrice(bolPrintAll)


            Dim oDCGrid = oGrid.UnderlyingControl

            If oDCGrid = Not Nothing Then
                Dim intX = 0

                Dim rowCurrent = Nothing

                Dim sopOrders = Nothing
                Dim sopOrder = Nothing

                For rowCurrent = 0 To oDCGrid.Items
                    Dim ignore = False

                    If bolPrintAll = False Then
                        If rowCurrent.Selected = False Then
                            ignore = True

                        Else
                            intX += 1
                        End If
                    End If



                    If ignore = False Then
                        sopOrders = New Sage.Accounting.SOP.SOPOrders()

                        ' TODO Figure out filter
                        sopOrders.Query.Filters.Add(Sage.ObjectStore.Filter(Sage.Accounting.SOP.SOPOrder.FIELD_DOCUMENTNO, rowCurrent.SubItems(0).Text))
                        sopOrders.Query.Find()

                        If sopOrders.Count = 1 Then
                            sopOrder = sopOrders.First

                            If sopOrder.SpareBit3 = False Then
                                LF.MsgBox("Order number " + rowCurrent.SubItems(0).Text + " hasn't been price checked.\r\n\r\n" + "Cannot invoice.", MsgBoxStyle.Exclamation, "Not Price Checked")
                                Return False
                            End If


                        Else
                            LF.MsgBox("Failed to locate SOP number " + rowCurrent.SubItems(0).Text + " in the collection.", MsgBoxStyle.Critical, "SOP Not Found")
                            Return False
                        End If
                    End If
                Next


                If (bolPrintAll = False) And (intX > 0) Then
                    Return True

                ElseIf (bolPrintAll = True) Then
                    Return True

                Else
                    Return False
                End If

            Else
                Return False
            End If

        End Function

    End Class
End Namespace

