﻿Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms
Imports Sage.Common.UI

Namespace Scripting.SOP
    Public Class AmendOrderDetailsForm
        Inherits FormScript(Of Sage.MMS.SOP.AmendOrderDetailsForm)

        ' TODO this method is from the `RealitySolutions.MMS.SagePy` library??
        Dim LF = New clsLostFunctions()

        Dim oTabControl = Nothing

        Dim acNewTab = Nothing
        Dim acLogTab = Nothing

        Dim lblAccountCustomer = Nothing
        Dim lblContact = Nothing
        Dim lblDeliveryMethod = Nothing
        Dim cmbAccountCustomer = Nothing
        Dim cmbDeliveryMethod = Nothing
        Dim txtContact = Nothing
        Dim oLogGrid = Nothing
        Dim oLogGridUL = Nothing
        Dim cmbAC = Nothing
        Dim cmbDM = Nothing

        Dim oApp = New Sage.Accounting.Application()
        Dim sqlConn = New SqlConnection(oApp.ActiveCompany.ConnectString)


        Public Overrides Sub Main()


            Dim oTabControl = Form.FindControlByName("tabControl")

            oTabControl.Controls.Add("System.Windows.Forms.TabPage", "tabNew")
            oTabControl.Controls.Add("System.Windows.Forms.TabPage", "tabLog")


            Dim acNewTab = Form.FindControlByName("tabNew")

            acNewTab.Text = "Additional"
            acNewTab.Controls.Add("System.Windows.Forms.Label", "lblAccountCustomer")
            acNewTab.Controls.Add("System.Windows.Forms.Label", "lblContact")
            acNewTab.Controls.Add("System.Windows.Forms.Label", "lblDeliveryMethod")
            acNewTab.Controls.Add("System.Windows.Forms.ComboBox", "cmbAccountCustomer")
            acNewTab.Controls.Add("System.Windows.Forms.ComboBox", "cmbDeliveryMethod")
            acNewTab.Controls.Add("Sage.Common.Controls.MaskedTextBox", "txtContact")


            Dim acLogTab = Form.FindControlByName("tabLog")

            acLogTab.Text = "Log"
            acLogTab.Controls.Add("Sage.ObjectStore.Controls.Grid", "grdLog")


            Dim lblAccountCustomer = Form.FindControlByName("lblAccountCustomer")
            Dim lblContact = Form.FindControlByName("lblContact")
            Dim lblDeliveryMethod = Form.FindControlByName("lblDeliveryMethod")

            Dim cmbAccountCustomer = Form.FindControlByName("cmbAccountCustomer")
            Dim cmbDeliveryMethod = Form.FindControlByName("cmbDeliveryMethod")

            Dim txtContact = Form.FindControlByName("txtContact")


            lblAccountCustomer.Location = New Point(8, 8)
            lblAccountCustomer.Size = New Size(100, 23)
            lblAccountCustomer.Text = "Account Customer:"

            lblContact.Location = New Point(8, 62)
            lblContact.Size = New Size(46, 18)
            lblContact.Text = "Contact:"

            lblDeliveryMethod.Location = New Point(8, 36)
            lblDeliveryMethod.Size = New Size(100, 23)
            lblDeliveryMethod.Text = "Delivery Method:"


            cmbAccountCustomer.Location = New Point(120, 4)
            cmbAccountCustomer.Size = New Size(176, 21)
            cmbAccountCustomer.Text = ""

            cmbDeliveryMethod.Location = New Point(120, 32)
            cmbDeliveryMethod.Size = New Size(176, 21)
            cmbDeliveryMethod.Text = ""

            txtContact.Location = New Point(120, 59)
            txtContact.Size = New Size(176, 21)
            txtContact.Text = ""


            Dim cmbAC = cmbAccountCustomer.UnderlyingControl

            ' TODO All commented out code
            'cmbAC.DropDownStyle = ComboBoxStyle.DropDownList
            'cmbAC.Items.Add("Account/Customer")
            'cmbAC.Items.Add("Cash")
            'cmbAC.SelectedIndex = 0


            cmbDM = cmbDeliveryMethod.UnderlyingControl

            cmbDM.DropDownStyle = ComboBoxStyle.DropDownList
            cmbDM.Items.Add("Our Transport")
            cmbDM.Items.Add("Collection")
            cmbDM.Items.Add("Carrier")
            cmbDM.Items.Add("Other")
            cmbDM.SelectedIndex = 0


            oLogGrid = Form.FindControlByName("grdLog")
            oLogGridUL = oLogGrid.UnderlyingControl

            oLogGridUL.Location = New Point(3, 3)
            oLogGridUL.Size = New Size(758, 459)
            oLogGridUL.Text = ""

            Dim a = New Sage.ObjectStore.Controls.GridColumn()
            Dim b = New Sage.ObjectStore.Controls.GridColumn()
            Dim c = New Sage.ObjectStore.Controls.GridColumn()

            a.AutoSize = True
            a.Caption = "Date"
            a.DisplayMember = "DateOfMessage"
            a.Width = 251

            b.AutoSize = True
            b.Caption = "Message"
            b.DisplayMember = "Message"
            b.Width = 251

            c.AutoSize = True
            c.Caption = "Username"
            c.DisplayMember = "Username"
            c.Width = 252

            oLogGridUL.Columns.Add(a)
            oLogGridUL.Columns.Add(b)
            oLogGridUL.Columns.Add(c)


            AddHandler Form.DataRefresh, AddressOf Form_DataRefresh
            AddHandler Form.BeginSave, AddressOf Form_BeginSave



        End Sub

        Public Sub Form_BeginSave(sender As Object, e As Object)
            Try

                Dim oSOP = Form.BoundObjects(1)

                Dim oItem
                Dim bolDotFound
                Dim oDot

                If oSOP = Not Nothing Then
                    oSOP.Fields("AccountCustomer").Value = cmbAC.Text
                    oSOP.Fields("DeliveryMethod").Value = cmbDM.Text
                    oSOP.Fields("Contact").Value = txtContact.Text


                    '  NEW: Ensure we have our dot item if there are no stock items

                    oDot = New Sage.Accounting.SOP.SOPStandardItemLine()


                    bolDotFound = False

                    For oItem = 0 To oSOP.Lines.Length - 1
                        If oSOP.Lines(oItem).LineType = Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeStandard Then
                        End If
                    Next


                    If oItem.ItemCode = "." Then
                    End If
                    bolDotFound = True

                    Dim oStockItem
                    Dim oStockItems
                    Dim oStockItemHoldingEx
                    Dim oUnitSellingEx
                    If bolDotFound = False Then
                        '  Dot not found, create it

                        ' TODO Figure out a way to get a variable as the *type* of a class
                        oUnitSellingEx = Type(Sage.Accounting.Exceptions.Ex20319Exception)
                        oStockItemHoldingEx = Type(Sage.Accounting.Exceptions.StockItemHoldingInsufficientException)
                    End If


                    oStockItems = New Sage.Accounting.Stock.StockItems()

                    oStockItem = oStockItems(".")

                    Dim oTaxCode
                    Dim oTaxCodes
                    If oStockItem Is Nothing Then
                        ' raiseEvent Exception("Unable to locate the picking dot item.")
                    End If


                    oTaxCodes = oApp.TaxModule.TaxCodes

                    oTaxCode = Nothing

                    ' TODO Figure out what the filter is
                    oTaxCodes.Query.Filters.Add(Sage.ObjectStore.Filter(Sage.Accounting.TaxModule.TaxCode.FIELD_CODE, "1"))
                    oTaxCode = oTaxCodes.First

                    Dim oAdjLine
                    Dim oAllo
                    If oTaxCode Is Nothing Then
                        '                           raiseEvent Exception("Unable to locate the T1 tax code.")
                    End If

                    Sage.Accounting.Application.AllowableWarnings.Add(oDot, oUnitSellingEx)
                    Sage.Accounting.Application.AllowableWarnings.Add(oDot, oStockItemHoldingEx)

                    oDot.SOPOrderReturn = oSOP
                    oDot.Item = oStockItem
                    oDot.LineQuantity = 1
                    oDot.UnitSellingPrice = 0
                    oDot.TaxCode = oTaxCode

                    oDot.Post()

                    oSOP.Lines.Add(oDot)
                    oSOP.refresh()


                    '  NEW: Setting the allocation qty doesn't allocate the line...

                    oAllo = New Sage.Accounting.SOP.SOPAllocationAdjustment()

                    oAllo.OrderNumberFrom = oSOP.DocumentNo
                    oAllo.OrderNumberTo = oSOP.DocumentNo

                    Try
                        oAllo.PopulateManualAllocations()


                    Catch ex As Exception
                        '  Can cause an exception when the free text item is dispatched

                    End Try

                    oAdjLine = 0
                    For oManLine = 0 To oAllo.ManualLines.Length - 1
                        For oAdjLine = oAdjLine To oAllo.ManualLines(oManLine).AllocationAdjustmentLines.Length - 1
                            If oAdjLine.ItemCode = "." Then
                                oAdjLine.NewAllocatedQuantity = oAdjLine.OutstandingQuantity
                                oAdjLine.Post()
                                Exit For

                            End If


                        Next
                    Next


                End If
                oSOP.refresh()

            Catch ex As Exception
                LF.MsgBox("An error has occurred while saving the additional information.\r" + vbNewLine + "\r" + vbNewLine + "" + ex.Message, MsgBoxStyle.Critical, "Error")

            End Try
        End Sub

        Private Sub Form_DataRefresh(args As DataRefreshEventArgs)


            Dim oSOPOrder = args.Source

            If Not oSOPOrder = Nothing Then
                LocateTextInCombo(cmbAC, oSOPOrder.Fields("AccountCustomer").GetString())
                LocateTextInCombo(cmbDM, oSOPOrder.Fields("DeliveryMethod").GetString())
                txtContact.Text = oSOPOrder.Fields("Contact").GetString()

                Try
                    Dim dtTEMP = ExecuteQuery($"SELECT (Date), Message, SageUser FROM tblSOPNotes WHERE SOPOrderReturnID = {oSOPOrder.SOPOrderReturn} ORDER BY (Date) DESC")

                    ' TODO Cool more SagePy lib functions / Classes
                    Dim oItems = New SOPPOPLogEntityCollection()
                    Dim oItem = Nothing


                    Dim iDate = LF.ColumnIndex(dtTEMP, "Date")
                    Dim iMessage = LF.ColumnIndex(dtTEMP, "Message")
                    Dim iSageUser = LF.ColumnIndex(dtTEMP, "SageUser")

                    For Each rowEach In dtTEMP.Rows
                        ' TODO Cool more SagePy lib functions / Classes
                        oItem = New SOPPOPLogEntity()

                        oItem.DateOfMessage = LF.CDate(rowEach.Item(iDate))
                        oItem.Message = rowEach.Item(iMessage).ToString()
                        oItem.Username = rowEach.Item(iSageUser).ToString()

                        oItems.Add(oItem)

                    Next

                    oLogGridUL.DataSource = oItems

                Catch ex As Exception
                    LF.MsgBox("An error has occurred while populating the payments grid.\r\n\r\n" + Str(ex), MsgBoxStyle.Critical, "Error")
                End Try

            End If

        End Sub

        Sub LocateTextInCombo(cmbCombo, strCombo)
            For index = 0 To cmbCombo.Items.Count
                If cmbCombo.Items(index).ToString() = strCombo Then
                    cmbCombo.SelectedIndex = index
                    Exit Sub

                End If
            Next

        End Sub

    End Class

End Namespace
