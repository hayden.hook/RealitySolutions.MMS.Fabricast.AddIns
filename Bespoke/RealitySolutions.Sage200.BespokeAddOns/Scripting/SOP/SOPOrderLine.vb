﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Windows.Forms

Namespace Scripting.SOP
    Public Class SOPOrderLine
        Inherits FormScript(Of Sage.MMS.SOP.SOPOrderLine)

        Public LF = New clsLostFunctions()
        Public oCommentsBox = Nothing
        Public oSearchBox = Nothing
        Public acNewMaterialsButton = Nothing
        Public acNewSearchButton = Nothing
        Public acOldSearchButton = Nothing
        Public acStockCode = Nothing
        Public popUpStock = Nothing
        Public oPurchaseOrderNo = Nothing
        Public oCertReq = Nothing
        Public oCertReqUL = Nothing
        Public oSellingUnit = Nothing
        Public oSellingPriceUnit = Nothing
        Public chkCertReq = Nothing
        Public cmdAmendProduct = Nothing
        Public cmdStockEnquiry = Nothing
        Public lblPON = Nothing
        Public oButtonPanel = Nothing
        Public oItemValuesGroupBox = Nothing
        Public oLineTypeGroupBox = Nothing
        Public txtPurchaseOrderNo = Nothing
        Public dtpLatestCost = Nothing
        Public lblLatestCost = Nothing
        Public txtLatestCost = Nothing
        Public bolAmendMode = False
        Public bolReturnMode = False
        Public strTEMP = ""
        Public oApp = New Sage.Accounting.Application()
        Public sqlConn = New SqlConnection(oApp.ActiveCompany.ConnectString)

        Public Overrides Sub Main()


            If LF.InStr(Form.Text, "Return", CompareMethod.Text) > 0 Then
                bolReturnMode = True

            Else
                bolReturnMode = False

            End If

            oCommentsBox = Form.FindControlByName("commentsGroupBox")

            If oCommentsBox = Not Nothing Then
                oCommentsBox.Controls.Add("System.Windows.Forms.Label", "lblLatestCost")
                oCommentsBox.Controls.Add("System.Windows.Forms.TextBox", "txtLatestCost")
                oCommentsBox.Controls.Add("System.Windows.Forms.DateTimePicker", "dtpLatestCost")

                lblLatestCost = Form.FindControlByName("lblLatestCost")
                lblLatestCost.Enabled = True
                lblLatestCost.Location = New Point(320, 26)
                lblLatestCost.Size = New Size(66, 13)
                lblLatestCost.Text = "Latest Cost:"

                txtLatestCost = Form.FindControlByName("txtLatestCost").UnderlyingControl
                txtLatestCost.Enabled = False
                txtLatestCost.Location = New Point(390, 22)
                txtLatestCost.Size = New Size(125, 21)
                txtLatestCost.Text = "0.00000"
                txtLatestCost.TextAlign = HorizontalAlignment.Right

                dtpLatestCost = Form.FindControlByName("dtpLatestCost").UnderlyingControl
                dtpLatestCost.Enabled = False
                dtpLatestCost.CustomFormat = "dd/MM/yyyy"
                dtpLatestCost.Format = DateTimePickerFormat.Custom
                dtpLatestCost.Location = New Point(520, 22)
                dtpLatestCost.Size = New Size(100, 21)
                dtpLatestCost.Value = LF.CDate("2010/01/01")
            End If

            If bolReturnMode = False Then
                If oCommentsBox = Not Nothing Then
                    oCommentsBox.Controls.Add("System.Windows.Forms.Button", "cmdMaterialsRefNumber")


                    acNewMaterialsButton = Form.FindControlByName("cmdMaterialsRefNumber")

                    acNewMaterialsButton.Enabled = True
                    acNewMaterialsButton.Location = New Point(210, 21)
                    acNewMaterialsButton.Size = New Size(87, 26)
                    acNewMaterialsButton.Text = "&Materials Ref"


                End If
            End If

            acStockCode = Form.FindControlByName("stockLookup")
            popUpStock = acStockCode.UnderlyingControl


            oItemValuesGroupBox = Form.FindControlByName("itemValuesGroupBox")
            oItemValuesGroupBox.Controls.Add("Sage.Common.Controls.CheckBox", "chkCertReq")


            chkCertReq = Form.FindControlByName("chkCertReq").UnderlyingControl
            chkCertReq.CheckAlign = ContentAlignment.MiddleRight
            chkCertReq.Location = New Point(487, 97)
            chkCertReq.Size = New Size(217, 24)
            chkCertReq.Text = "Certification Required?"
            chkCertReq.TextAlign = ContentAlignment.MiddleLeft
            chkCertReq.UseVisualStyleBackColorisualStyleBackColorisualStyleBackColor = UseVisualStyleBackColor = True

            oButtonPanel = Form.FindControlByName("buttonPanel")
            oButtonPanel.Controls.Add("Sage.Common.Controls.Button", "cmdAmendProduct")
            oButtonPanel.Controls.Add("Sage.Common.Controls.Button", "cmdStockEnquiry")


            cmdAmendProduct = Form.FindControlByName("cmdAmendProduct").UnderlyingControl
            cmdAmendProduct.Location = New Point(547, 24)
            cmdAmendProduct.Size = New Size(75, 23)
            cmdAmendProduct.Text = "Amend Pro&duct"
            cmdAmendProduct.UseVisualStyleBackColorisualStyleBackColor = UseVisualStyleBackColor = True

            cmdStockEnquiry = Form.FindControlByName("cmdStockEnquiry").UnderlyingControl
            cmdStockEnquiry.Location = New Point(615, 24)
            cmdStockEnquiry.Size = New Point(75, 23)
            cmdStockEnquiry.Text = "Stock &Enquiry"
            cmdStockEnquiry.UseVisualStyleBackColorisualStyleBackColor = UseVisualStyleBackColor = True


            oLineTypeGroupBox = Form.FindControlByName("lineTypeGroupBox")
            oLineTypeGroupBox.Controls.Add("Sage.Common.Controls.Label", "lblPON")
            oLineTypeGroupBox.Controls.Add("Sage.Common.Controls.MaskedTextBox", "txtPurchaseOrderNo")

            lblPON = Form.FindControlByName("lblPON")
            lblPON.Location = New Point(528, 22)
            lblPON.Size = New Point(46, 18)
            lblPON.Text = "PO Number:"

            oPurchaseOrderNo = Form.FindControlByName("txtPurchaseOrderNo")
            oPurchaseOrderNo.Location = New Point(596, 19)
            oPurchaseOrderNo.Size = New Point(113, 21)
            oPurchaseOrderNo.Text = ""


            oCertReq = Form.FindControlByName("chkCertReq")
            oCertReqUL = oCertReq.UnderlyingControl

            If Form.Text == "SOP - Edit Order Item Line" Then
                bolAmendMode = True

                If acNewMaterialsButton = Not Nothing Then
                    acNewMaterialsButton.Enabled = True
                End If

                Dim oSOPLine = Form.BoundObjects(0)

                If oSOPLine = Not Nothing Then
                    strTEMP = oSOPLine.Fields("MaterialsRefNumber").Value.ToString()

                    oPurchaseOrderNo.Text = oSOPLine.Fields("PurchaseOrderNumber").GetString()
                    oCertReqUL.Checked = oSOPLine.Fields("CertificationRequired").GetBoolean()
                End If

            Else
                bolAmendMode = False

            End If

            oSearchBox = Form.FindControlByName("searchPanel")
            oSearchBox.Controls.Add("System.Windows.Forms.Button", "cmdNewSearchButton")

            acNewSearchButton = Form.FindControlByName("cmdNewSearchButton")
            acOldSearchButton = Form.FindControlByName("searchButton")

            acNewSearchButton.Enabled = True
            acNewSearchButton.Location = New Point(5, 0)
            acNewSearchButton.Size = New Size(30, 22)
            acNewSearchButton.Text = "S"
            acNewSearchButton.BringToFront()

            acOldSearchButton.Visible = False
            acOldSearchButton.SendToBack()


            oSellingUnit = Form.FindControlByName("stockItemUnitLookup")
            oSellingPriceUnit = Form.FindControlByName("priceStockItemUnitLookup")

            oSellingUnit.Enabled = True
            oSellingPriceUnit.Enabled = True



            AddHandler cmdAmendProduct.Click, AddressOf cmdAmendProduct_Click
            AddHandler cmdStockEnquiry.Click, AddressOf cmdStockEnquiry_Click
            AddHandler Form.EndSave, AddressOf Form_EndSave

            If bolReturnMode = False Then
                AddHandler acNewMaterialsButton.Click, AddressOf cmdNewMaterialsButton_Click
            End If

            AddHandler acNewSearchButton.Click, AddressOf cmdNewSearchButton_Click
            AddHandler popUpStock.StockItemSelected, AddressOf acStockCode_StockItemSelected
        End Sub

        Private Sub cmdNewMaterialsButton_Click(sender As Object, e As EventArgs)
            Dim oSOPLine = Form.BoundObjects(0)

            If (oSOPLine = Not Nothing) Then
                Dim frmX = New frmSOPMaterialsRefNumber()

                frmX.Reference = strTEMP
                frmX.ShowDialog()

                If (frmX.Reference = Not Nothing) Then
                    strTEMP = frmX.Reference
                End If
            End If


        End Sub

        Private Sub cmdNewSearchButton_Click(sender As Object, e As EventArgs)
            Dim frmX = New frmTreeviewSearching()

            frmX.ShowDialog()

            If frmX.StockCode <> "" Then

                Dim oStockCodes = New Sage.Accounting.Stock.StockItems()
                Dim oStockCode = oStockCodes(frmX.StockCode)

                Dim args = New Sage.MMS.Controls.StockItemSelectedEventArgs()
                args.SelectedStockItem = oStockCode

                popUpStock.StockItem = oStockCode
                popUpStock.OnStockItemSelected(args)
            End If
        End Sub

        Private Sub cmdAmendProduct_Click(sender As Object, e As EventArgs)
            Dim oArray = {popUpStock.StockItem.Item}

            ' TODO Class type issue
            Dim iArgs = New Sage.Common.UI.FormArguments(oArray, Type(Sage.Accounting.Stock.StockItem), Sage.Common.CustomAttributes.eParameterType.DbKey)

            Dim frmX = New Sage.MMS.Stock.AmendItemDetailsForm(iArgs)
            frmX.ShowDialog()
            frmX = Nothing
        End Sub

        Private Sub cmdStockEnquiry_Click(sender As Object, e As EventArgs)
            Dim frmX = New frmStockEnquiry()
            frmX.StockItem = popUpStock.StockItem
            frmX.ShowDialog()
            frmX = Nothing
        End Sub
        Private Sub acStockCode_StockItemSelected(sender As Object, e As EventArgs)
            Dim dtpLatestCost
            dtpLatestCost = Form.FindControlByName("dtpLatestCost").UnderlyingControl
            dtpLatestCost.Enabled = False
            dtpLatestCost.CustomFormat = "dd/MM/yyyy"
            dtpLatestCost.Format = DateTimePickerFormat.Custom
            dtpLatestCost.Location = New Point(520, 22)
            dtpLatestCost.Size = New Size(100, 21)
            dtpLatestCost.Value = LF.CDate("2010/01/01")


            Dim bolReturnMode
            Dim bolAmendMode
            Dim oSellingPriceUnit
            Dim oSellingUnit
            Dim acNewMaterialsButton
            Dim txtLatestCost
            Dim rows

            ' Fix declaration of bolReturnMode and acNewMaterialsButton
            If bolReturnMode = False And acNewMaterialsButton = Not Nothing Then
                acNewMaterialsButton.Enabled = True
            End If


            oSellingUnit = Form.FindControlByName("stockItemUnitLookup")

            oSellingPriceUnit = Form.FindControlByName("priceStockItemUnitLookup")


            ' Fix declaration of bolAmendMode and txtLatestCost
            If bolAmendMode = False And dtpLatestCost = Not Nothing And txtLatestCost = Not Nothing Then

                rows = ExecuteQuery("SELECT TOP 1 POPOrderReturnLine.UnitBuyingPrice, POPOrderReturn.DocumentDate FROM POPOrderReturnLine INNER JOIN POPOrderReturn ON POPOrderReturnLine.POPOrderReturnID = POPOrderReturn.POPOrderReturnID WHERE POPOrderReturnLine.ItemCode = '" + (acStockCode.Text).ToString + "' AND POPOrderReturnLine.LineTypeID = 0 ORDER BY POPOrderReturn.POPOrderReturnID DESC")
            End If

            For row = 0 To rows.Rows.Length - 1

                ' TODO Fix dtp not being declared as intended
                dtpLatestCost.Value = Mid(LF.CDate(row.Item, LF.ColumnIndex(rows, "DocumentDate") + 1, 1))

                txtLatestCost.Text = Mid(LF.Format(LF.CDec(row.Item, LF.ColumnIndex(rows, "UnitBuyingPrice") + 1, 1)), "0.00000")
            Next

        End Sub

        Private Sub Form_EndSave(b As Boolean)
            If (b = True) And (bolReturnMode = False) Then
                Dim oSOPLine = Form.BoundObjects(0)

                If (oSOPLine = Not Nothing) Then
                    oSOPLine.Fields("MaterialsRefNumber").Value = strTEMP
                    oSOPLine.Fields("PurchaseOrderNumber").Value = oPurchaseOrderNo.Text
                    oSOPLine.Fields("CertificationRequired").Value = oCertReqUL.Checked
                    oSOPLine.Fields("LatestCost").Value = txtLatestCost.Text
                    oSOPLine.Fields("LatestCostDate").Value = dtpLatestCost.Value
                    oSOPLine.Update()


                    If acNewMaterialsButton = Not Nothing Then
                        acNewMaterialsButton.Enabled = False
                    End If


                    strTEMP = Nothing
                    oPurchaseOrderNo.Text = ""

                End If
            End If

        End Sub


    End Class
End Namespace

