﻿Imports System.Drawing

Namespace Scripting.POP
    Public Class MaintainOrderLineForm
        Inherits FormScript(Of Sage.MMS.POP.MaintainOrderLineForm)

        Dim popUpStock = Nothing
        Public Overrides Sub Main()
            popUpStock = Form.FindControlByName("stockLookup").UnderlyingControl
            Dim oSearchBox = Form.FindControlByName("searchPanel")
            oSearchBox.Controls.Add("System.Windows.Forms.Button", "cmdNewSearchButton")

            Dim acOldSearchButton = Form.FindControlByName("searchButton")
            Dim acNewSearchButton = Form.FindControlByName("cmdNewSearchButton")

            acNewSearchButton.Enabled = True
            acNewSearchButton.Location = New Point(5, 0)
            acNewSearchButton.Size = New Size(30, 22)
            acNewSearchButton.Text = "S"

            acNewSearchButton.BringToFront()

            acOldSearchButton.Visible = False
            acOldSearchButton.SendToBack()
            AddHandler acNewSearchButton.Click, AddressOf cmdNewSearchButton_Click

        End Sub

        Private Sub cmdNewSearchButton_Click(sender As Object, e As EventArgs)
            Dim frmX = New frmTreeviewSearching()

            frmX.ShowDialog()


            If frmX.StockCode = "" Then

                Dim oStockCodes = New Sage.Accounting.Stock.StockItems()
                Dim oStockCode = oStockCodes(frmX.StockCode)

                Dim args = New Sage.MMS.Controls.StockItemSelectedEventArgs()
                args.SelectedStockItem = oStockCode

                popUpStock.StockItem = oStockCode
                popUpStock.OnStockItemSelected(args)
            End If

        End Sub
    End Class
End Namespace
