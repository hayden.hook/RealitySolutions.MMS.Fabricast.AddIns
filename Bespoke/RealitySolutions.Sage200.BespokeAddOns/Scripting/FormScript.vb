﻿Imports RealitySolutions.Licensing.Core.Client

Namespace Scripting

    Public MustInherit Class FormScript
        Public Overridable Property Form As Sage.Common.Amendability.AmendableForm

        MustOverride Sub Main()
        Function GetAvailabilityExtender(name As String) As Sage.Common.Controls.AvailabilityExtender
            Dim h = Form.GetAvailabilityExtenders()
            For Each k As String In h.Keys
                If k = name Then Return DirectCast(h(k), Sage.Common.Controls.AvailabilityExtender)
            Next
            Throw New KeyNotFoundException(String.Format("No availability extender called {0} was found", name))
        End Function

        Sub SetAvailability(e As Sage.Common.Controls.AvailabilityExtender, value As Boolean, ParamArray controlNames() As String)
            For Each name In controlNames
                e.SetDetermineAvailability(Form.FindControlByName(name).UnderlyingControl, value)
            Next
            ApplyAvailability(e)
        End Sub

        Sub SetAvailability(e As Sage.Common.Controls.AvailabilityExtender, value As Boolean, ParamArray controls() As System.Windows.Forms.Control)
            For Each ctrl In controls
                e.SetDetermineAvailability(ctrl, value)
            Next
            ApplyAvailability(e)
        End Sub

        Sub SetAvailability(extenderName As String, value As Boolean, ParamArray controlNames() As String)
            Dim a = GetAvailabilityExtender(extenderName)
            SetAvailability(a, value, controlNames)
        End Sub

        Sub SetAvailability(extenderName As String, value As Boolean, ParamArray controls() As System.Windows.Forms.Control)
            Dim a = GetAvailabilityExtender(extenderName)
            SetAvailability(a, value, controls)
        End Sub

        Sub ApplyAvailability(e As Sage.Common.Controls.AvailabilityExtender)
            Select Case e.StateOfExtender
                Case Sage.Common.Controls.AvailabilityState.Available
                    e.IndicateAvailable()
                Case Sage.Common.Controls.AvailabilityState.Disabled
                    e.IndicateDisabled()
                Case Sage.Common.Controls.AvailabilityState.ReadOnly
                    e.IndicateReadOnly()
                Case Sage.Common.Controls.AvailabilityState.Unavailable
                    e.IndicateUnavailable()
            End Select
        End Sub

        Public Function FindControl(name As String) As Object
            Dim x = Form.FindControlByName(name)
            If x Is Nothing Then Return Nothing
            Return x.UnderlyingControl
        End Function

        Public Function FindControl(Of T As System.Windows.Forms.Control)(name As String) As T
            Return DirectCast(FindControl(name), T)
        End Function

        Public Iterator Function BoundObjects() As IEnumerable(Of Object)
            If Form.BoundObjects Is Nothing Then Exit Function

            For Each boundobject In Form.BoundObjects
                If boundobject Is Nothing Then Continue For

                Yield boundobject
            Next
        End Function

        Public Function CastableBoundObjects(Of T)() As IEnumerable(Of T)
            Return BoundObjects.Where(Function(x) GetType(T).IsAssignableFrom(x.GetType())).Select(Function(x) DirectCast(x, T))
        End Function

        Public Function TypedBoundObjects(Of T)() As IEnumerable(Of T)
            Return BoundObjects.OfType(Of T)()
        End Function

        Shared _notifyPropertyChanged As Reflection.MethodInfo

        Public Function GetPropertyName(Of TModel, TProperty)([property] As Expressions.Expression(Of Func(Of TModel, TProperty))) As String
            Dim memberExpression As Expressions.MemberExpression
            If [property].Body.GetType() Is GetType(Expressions.UnaryExpression) Then
                memberExpression = DirectCast(DirectCast([property].Body, Expressions.UnaryExpression).Operand, Expressions.MemberExpression)
            Else
                memberExpression = DirectCast([property].Body, Expressions.MemberExpression)
            End If

            Return memberExpression.Member.Name
        End Function

        Public Sub NotifyPropertyChanged(Of T As Sage.ObjectStore.MetaDataObject)(obj As T, propertyName As String)
            ' this is a protected sub on metadataobject, so need to reflect it off
            If _notifyPropertyChanged Is Nothing Then
                _notifyPropertyChanged = GetType(Sage.ObjectStore.MetaDataObject).GetMethod("NotifyPropertyChanged",
                                                                                                Reflection.BindingFlags.Instance + Reflection.BindingFlags.NonPublic,
                                                                                                Nothing,
                                                                                                {GetType(String)},
                                                                                                Nothing)
            End If

            _notifyPropertyChanged.Invoke(obj, {propertyName})
        End Sub

        Public Sub NotifyPropertyChanged(Of T As Sage.ObjectStore.MetaDataObject)(obj As T, [property] As Expressions.Expression(Of Func(Of T, String)))
            NotifyPropertyChanged(obj, GetPropertyName([property]))
        End Sub

        Public Function FindButton(name As String) As Sage.Common.Controls.Button
            Return FindControl(Of Sage.Common.Controls.Button)(name)
        End Function
    End Class

    Public MustInherit Class FormScript(Of T As System.Windows.Forms.Form)
        Inherits FormScript


        Public ReadOnly Property UnderlyingForm As T
            Get
                Return DirectCast(Form.UnderlyingControl, T)
            End Get
        End Property

        MustOverride Overrides Sub Main()

        Protected Function GetPrivateFieldValue(fieldName As String) As Object
            Return GetType(T).GetField(fieldName, Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(UnderlyingForm)
        End Function

    End Class

#Region " ScriptLauncher "

    Public Class MatchDecendantTypesAttribute
        Inherits Attribute
    End Class

    Public Class FormScriptOptions
        Public Property FormScriptType As Type
        Public Property MatchDescendantTypes As Boolean
    End Class

    Public Class ScriptLauncher
        Shared map As Dictionary(Of Type, List(Of FormScriptOptions))

        Shared ReadOnly Property TypeMap As Dictionary(Of Type, List(Of FormScriptOptions))
            Get
                If map Is Nothing Then
                    map = New Dictionary(Of Type, List(Of FormScriptOptions))

                    Dim baseType = GetType(FormScript(Of ))

                    For Each t In Reflection.Assembly.GetExecutingAssembly().GetExportedTypes()

                        If IsGenericSubClass(baseType, t) Then
                            Dim CustomisedType = t.BaseType.GetGenericArguments(0)

                            If Not map.ContainsKey(CustomisedType) Then map(CustomisedType) = New List(Of FormScriptOptions)
                            Dim attrs = t.GetCustomAttributes(GetType(MatchDecendantTypesAttribute), False)
                            map(CustomisedType).Add(New FormScriptOptions() With {.FormScriptType = t, .MatchDescendantTypes = attrs.Count <> 0})
                        End If
                    Next
                End If

                Return map
            End Get
        End Property

        Shared Sub Run(f As Sage.Common.Amendability.AmendableForm)
            Try
                Dim underlyingType = f.UnderlyingControl.GetType()

                Dim ancestry As New List(Of Type)
                Dim currentType = underlyingType
                While True
                    If currentType Is Nothing Then Exit While
                    ancestry.Add(currentType)
                    If currentType IsNot GetType(System.Windows.Forms.Form) AndAlso currentType IsNot GetType(Sage.MMS.BaseForm) Then
                        currentType = currentType.BaseType
                    Else
                        Exit While
                    End If
                End While

                ancestry.Reverse()

                For Each tp In ancestry
                    If TypeMap.ContainsKey(tp) Then
                        For Each t In TypeMap(tp)

                            Dim instance As FormScript = DirectCast(Activator.CreateInstance(t.FormScriptType), FormScript)
                            Dim launch As Boolean = True
                            If tp IsNot underlyingType Then
                                If Not t.MatchDescendantTypes Then launch = False
                            End If

                            If launch Then
                                ' Licensing check
                                Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

                                If intLicense <= 0 Then
                                    MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
                                    Exit Sub
                                ElseIf intLicense > 0 And intLicense <= 7 Then
                                    MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
                                ElseIf intLicense > 7 And intLicense <= 30 Then
                                    MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
                                End If

                                instance.Form = f
                                instance.Main()
                            End If
                        Next
                    End If
                Next
            Catch ex As Exception
                MsgBox(ex.ToString())
            End Try
        End Sub

        Private Shared Function IsGenericSubClass(parent As Type, child As Type) As Boolean
            If child Is parent Then Return False

            Dim parameters = parent.GetGenericArguments()
            Dim isParameterlessGeneric = Not (parameters IsNot Nothing AndAlso parameters.Length > 0 AndAlso (parameters(0).Attributes And Reflection.TypeAttributes.BeforeFieldInit) = Reflection.TypeAttributes.BeforeFieldInit)

            While child IsNot Nothing AndAlso Not (child Is GetType(Object))
                Dim cur = GetFullTypeDefinition(child)

                If (parent Is cur) OrElse (isParameterlessGeneric AndAlso cur.GetInterfaces().Select(Function(i) GetFullTypeDefinition(i)).Contains(GetFullTypeDefinition(parent))) Then
                    Return True

                ElseIf Not isParameterlessGeneric Then
                    If GetFullTypeDefinition(parent) Is cur AndAlso Not cur.IsInterface Then
                        If VerifyGenericArguments(GetFullTypeDefinition(parent), cur) Then Return True
                    Else
                        For Each item In child.GetInterfaces().Where(Function(i) GetFullTypeDefinition(parent) Is GetFullTypeDefinition(i))
                            If VerifyGenericArguments(parent, item) Then Return True
                        Next
                    End If
                End If

                child = child.BaseType
            End While

            Return False
        End Function

        Private Shared Function GetFullTypeDefinition(tp As Type) As Type
            If tp.IsGenericType Then
                Return tp.GetGenericTypeDefinition()
            Else
                Return tp
            End If
        End Function

        Private Shared Function VerifyGenericArguments(parent As Type, child As Type) As Boolean
            Dim childArguments = child.GetGenericArguments()
            Dim parentArguments = parent.GetGenericArguments()

            If childArguments.Length <> parentArguments.Length Then
                Return False
            End If

            For i = 0 To childArguments.Length - 1
                If Not (childArguments(i).Assembly Is parentArguments(i).Assembly AndAlso
                        childArguments(i).Name = parentArguments(i).Name AndAlso
                        childArguments(i).Namespace = parentArguments(i).Namespace) Then
                    Return False
                End If
            Next

            Return True
        End Function
    End Class
#End Region

End Namespace
