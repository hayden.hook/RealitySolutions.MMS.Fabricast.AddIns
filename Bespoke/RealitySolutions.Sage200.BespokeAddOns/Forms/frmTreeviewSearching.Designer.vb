<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTreeviewSearching
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tvProducts = New System.Windows.Forms.TreeView
        Me.fraSearch = New System.Windows.Forms.GroupBox
        Me.cmdSearch = New System.Windows.Forms.Button
        Me.txtSearchDescription = New Sage.ObjectStore.Controls.MaskedTextBox
        Me.txtSearchCode = New Sage.ObjectStore.Controls.MaskedTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.lblCode = New System.Windows.Forms.Label
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.lblRecordsFound = New System.Windows.Forms.Label
        Me.dgProducts = New System.Windows.Forms.DataGridView
        Me.fraSearch.SuspendLayout()
        CType(Me.dgProducts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tvProducts
        '
        Me.tvProducts.HideSelection = False
        Me.tvProducts.Location = New System.Drawing.Point(4, 4)
        Me.tvProducts.Name = "tvProducts"
        Me.tvProducts.Size = New System.Drawing.Size(184, 444)
        Me.tvProducts.TabIndex = 0
        '
        'fraSearch
        '
        Me.fraSearch.Controls.Add(Me.cmdSearch)
        Me.fraSearch.Controls.Add(Me.txtSearchDescription)
        Me.fraSearch.Controls.Add(Me.txtSearchCode)
        Me.fraSearch.Controls.Add(Me.lblDescription)
        Me.fraSearch.Controls.Add(Me.lblCode)
        Me.fraSearch.Location = New System.Drawing.Point(192, 4)
        Me.fraSearch.Name = "fraSearch"
        Me.fraSearch.Size = New System.Drawing.Size(632, 68)
        Me.fraSearch.TabIndex = 1
        Me.fraSearch.TabStop = False
        Me.fraSearch.Text = "Search"
        '
        'cmdSearch
        '
        Me.cmdSearch.Location = New System.Drawing.Point(552, 40)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(75, 23)
        Me.cmdSearch.TabIndex = 4
        Me.cmdSearch.Text = "&Search"
        Me.cmdSearch.UseVisualStyleBackColor = True
        '
        'txtSearchDescription
        '
        Me.txtSearchDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchDescription.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSearchDescription.Location = New System.Drawing.Point(76, 40)
        Me.txtSearchDescription.Name = "txtSearchDescription"
        Me.txtSearchDescription.Size = New System.Drawing.Size(468, 21)
        Me.txtSearchDescription.TabIndex = 3
        '
        'txtSearchCode
        '
        Me.txtSearchCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtSearchCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSearchCode.Location = New System.Drawing.Point(76, 16)
        Me.txtSearchCode.Name = "txtSearchCode"
        Me.txtSearchCode.Size = New System.Drawing.Size(164, 21)
        Me.txtSearchCode.TabIndex = 2
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(8, 44)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(64, 13)
        Me.lblDescription.TabIndex = 1
        Me.lblDescription.Text = "Description:"
        '
        'lblCode
        '
        Me.lblCode.AutoSize = True
        Me.lblCode.Location = New System.Drawing.Point(8, 20)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(36, 13)
        Me.lblCode.TabIndex = 0
        Me.lblCode.Text = "Code:"
        '
        'cmdOK
        '
        Me.cmdOK.Location = New System.Drawing.Point(192, 424)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 3
        Me.cmdOK.Text = "&Select"
        Me.cmdOK.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Location = New System.Drawing.Point(752, 424)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 4
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'lblRecordsFound
        '
        Me.lblRecordsFound.Location = New System.Drawing.Point(272, 428)
        Me.lblRecordsFound.Name = "lblRecordsFound"
        Me.lblRecordsFound.Size = New System.Drawing.Size(476, 13)
        Me.lblRecordsFound.TabIndex = 5
        Me.lblRecordsFound.Text = "0 records found"
        Me.lblRecordsFound.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'dgProducts
        '
        Me.dgProducts.AllowUserToAddRows = False
        Me.dgProducts.AllowUserToDeleteRows = False
        Me.dgProducts.AllowUserToResizeRows = False
        Me.dgProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgProducts.Location = New System.Drawing.Point(192, 76)
        Me.dgProducts.MultiSelect = False
        Me.dgProducts.Name = "dgProducts"
        Me.dgProducts.ReadOnly = True
        Me.dgProducts.RowHeadersVisible = False
        Me.dgProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgProducts.Size = New System.Drawing.Size(632, 344)
        Me.dgProducts.TabIndex = 19
        '
        'frmTreeviewSearching
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(829, 451)
        Me.Controls.Add(Me.dgProducts)
        Me.Controls.Add(Me.lblRecordsFound)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.fraSearch)
        Me.Controls.Add(Me.tvProducts)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmTreeviewSearching"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Product Search"
        Me.fraSearch.ResumeLayout(False)
        Me.fraSearch.PerformLayout()
        CType(Me.dgProducts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tvProducts As System.Windows.Forms.TreeView
    Friend WithEvents fraSearch As System.Windows.Forms.GroupBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents txtSearchDescription As Sage.ObjectStore.Controls.MaskedTextBox
    Friend WithEvents txtSearchCode As Sage.ObjectStore.Controls.MaskedTextBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdSearch As System.Windows.Forms.Button
    Friend WithEvents lblRecordsFound As System.Windows.Forms.Label
    Friend WithEvents dgProducts As System.Windows.Forms.DataGridView
End Class
