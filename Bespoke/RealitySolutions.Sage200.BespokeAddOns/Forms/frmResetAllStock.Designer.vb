<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmResetAllStock
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdResetStockLevels = New System.Windows.Forms.Button
        Me.lblInfo = New System.Windows.Forms.Label
        Me.cmdResetPrecision = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cmdResetStockLevels
        '
        Me.cmdResetStockLevels.Location = New System.Drawing.Point(36, 36)
        Me.cmdResetStockLevels.Name = "cmdResetStockLevels"
        Me.cmdResetStockLevels.Size = New System.Drawing.Size(208, 23)
        Me.cmdResetStockLevels.TabIndex = 0
        Me.cmdResetStockLevels.Text = "&Reset Stock Levels"
        Me.cmdResetStockLevels.UseVisualStyleBackColor = True
        '
        'lblInfo
        '
        Me.lblInfo.AutoSize = True
        Me.lblInfo.Location = New System.Drawing.Point(4, 4)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(48, 13)
        Me.lblInfo.TabIndex = 1
        Me.lblInfo.Text = "<INFO>"
        '
        'cmdResetPrecision
        '
        Me.cmdResetPrecision.Location = New System.Drawing.Point(36, 64)
        Me.cmdResetPrecision.Name = "cmdResetPrecision"
        Me.cmdResetPrecision.Size = New System.Drawing.Size(208, 23)
        Me.cmdResetPrecision.TabIndex = 2
        Me.cmdResetPrecision.Text = "&Update Stock UoM Precision"
        Me.cmdResetPrecision.UseVisualStyleBackColor = True
        '
        'frmResetAllStock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(280, 93)
        Me.Controls.Add(Me.cmdResetPrecision)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.cmdResetStockLevels)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaximizeBox = False
        Me.Name = "frmResetAllStock"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reset All Stock Levels"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdResetStockLevels As System.Windows.Forms.Button
    Friend WithEvents lblInfo As System.Windows.Forms.Label
    Friend WithEvents cmdResetPrecision As System.Windows.Forms.Button
End Class
