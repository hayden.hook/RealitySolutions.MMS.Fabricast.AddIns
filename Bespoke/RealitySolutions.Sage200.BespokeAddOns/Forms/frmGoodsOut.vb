Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports RealitySolutions.Licensing.Core.Client

Public Class frmGoodsOut
    Private bolBlockEditLine As Boolean = True

    Private bolCurrentlySaving As Boolean = False


#Region " ENUM: logType() "
    Public Enum logType
        removeLine = 1
        addLine = 2
        changeUOM = 3
    End Enum
#End Region


#Region " SUBROUTINE: frmGoodsOut_Load() "
    Private Sub frmGoodsOut_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        SetupDirectSQLConnection()


        ' Gets round a bug where if no SOP is selected and the form is closed: it will throw a null exception
        sopLookup.SOPOrder = Nothing


        ' The SOP control doesn't have the usual query property, but we can use the datasource...
        Dim sopOrders As Sage.Accounting.SOP.SOPOrders = DirectCast(sopLookup.DataSource, Sage.Accounting.SOP.SOPOrders)

        sopOrders.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.SOP.SOPOrder.FIELD_DOCUMENTSTATUSDBKEY, 0)) ' Live
        sopOrders.Find()


        With sopLookup
            .DataSource = Nothing
            .DataSource = sopOrders
        End With
    End Sub
#End Region


#Region " SUBROUTINE: cmdSave_Click() "
    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If MsgBox("Are you sure you want to save?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.Yes Then
            Dim oActiveLock As Sage.Accounting.Common.ActiveLock = Nothing
            'Dim oTransaction As New Sage.ObjectStore.PersistentObjectTransaction

            Try
                Dim lvItem As ListViewItem = Nothing

                Dim sopOrder As Sage.Accounting.SOP.SOPOrderReturn = Nothing
                Dim sopLine As Sage.Accounting.SOP.SOPOrderReturnLine = Nothing


                Dim intCurrentID As Int64 = 0
                Dim intRows As Integer = 0

                Dim decSellingUnit As Decimal = 0D
                Dim decAmount As Decimal = 0D
                Dim decDifference As Decimal = 0D

                Dim decNet As Decimal = 0D
                Dim decTax As Decimal = 0D


                Dim htSellingUnit As New Hashtable


                bolCurrentlySaving = True


                Dim oSOPDespatchReceiptAdjustment As Sage.Accounting.SOP.SOPDespatchReceiptAdjustment = New Sage.Accounting.SOP.SOPDespatchReceiptAdjustment
                Dim oSOPDespatchReceiptItem As Sage.Accounting.SOP.SOPDespatchReceiptItem = Nothing

                Dim oUnits As Sage.Accounting.Stock.Units = Nothing
                Dim oUnit As Sage.Accounting.Stock.Unit = Nothing

                Dim oStockItemUnit As Sage.Accounting.Stock.StockItemUnit = Nothing

                Dim oStockItems As Sage.Accounting.Stock.StockItems = Nothing
                Dim oStockItem As Sage.Accounting.Stock.StockItem = Nothing


                For Each lvItem In lvSOPitems.Items
                    Select Case lvItem.SubItems(13).Text
                        Case "EnumLineTypeStandard"
                            ' Move on...
                            If (CDec(lvItem.SubItems(8).Text) > 0.0#) And (CDec(lvItem.SubItems(10).Text) > 0.0#) Then
                                ' Get the original SOP line back - we'll adjust it later
                                sopLine = DirectCast(lvItem.Tag, Sage.Accounting.SOP.SOPOrderReturnLine)
                                sopOrder = sopLine.SOPOrderReturn
                                intCurrentID = CLng(sopLine.SOPOrderReturnLine)
                                decSellingUnit = DanRound(CDec(lvItem.SubItems(10).Text) / CDec(lvItem.SubItems(8).Text), "0.0000000") ' 0.0000000

                                If IsWholeNumber(decSellingUnit) = False Then
                                    decSellingUnit += 0.0000001D
                                End If


                                ' NEW NEW: Let's sort this rounding issue...
                                htSellingUnit.Add(CLng(sopLine.SOPOrderReturnLine), decSellingUnit)


                                ' NEW: Create a lock
                                oActiveLock = Sage.Accounting.Application.Lock(sopOrder)


                                ' cancel the sopline as it is (this de-allocates stock) at the current uom multiplier
                                sopLine.CancelLine()
                                SOPLog(logType.removeLine, String.Format("Remove line (stock code={0}, details='{1}', qty={2}, selling unit='{3}', price='{4}', qty to allocate={5}, allocated={6}, unit price={7}, stock item unit={8}, unit cost={9}, unit discount % ={10}, unit discount={11}, disc unit price={12})", _
                                                                         sopLine.ItemCode, _
                                                                         sopLine.ItemDescription, _
                                                                         sopLine.LineQuantity, _
                                                                         lvItem.SubItems(4).Text, _
                                                                         sopLine.LineTotalValue, _
                                                                         sopLine.LineQuantity - sopLine.AllocatedQuantity, _
                                                                         sopLine.AllocatedQuantity, _
                                                                         sopLine.UnitSellingPrice, _
                                                                         "", _
                                                                         sopLine.CostPrice, _
                                                                         sopLine.UnitDiscountPercent, _
                                                                         sopLine.UnitDiscountValue, _
                                                                         sopLine.UnitSellingPrice - sopLine.UnitDiscountValue), _
                                                            CLng(sopLine.SOPOrderReturn.SOPOrderReturn), CLng(sopLine.SOPOrderReturnLine))

                                ' locate stock item
                                Dim oStockItems2 As New Sage.Accounting.Stock.StockItems
                                Dim oStockItem2 As Sage.Accounting.Stock.StockItem = oStockItems2(sopLine.ItemCode)


                                ' edit stock item's unit 
                                Dim oStockItemUnits2 As Sage.Accounting.Stock.StockItemUnits = oStockItem2.Units
                                Dim oStockItemUnit2 As Sage.Accounting.Stock.StockItemUnit = Nothing

                                oUnits = New Sage.Accounting.Stock.Units()
                                oUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.Unit.FIELD_NAME, lvItem.SubItems(4).Text))
                                oUnit = oUnits.First

                                oStockItemUnits2.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_UNITOBJECT, oUnit))
                                oStockItemUnit2 = oStockItemUnits2.First

                                oStockItemUnits2.Dispose()
                                oStockItemUnits2 = Nothing

                                oUnit.Dispose()
                                oUnit = Nothing
                                oUnits.Dispose()
                                oUnits = Nothing

                                'oStockItem2.Units
                                Dim oldMultiple As Decimal = oStockItemUnit2.MultipleOfBaseUnit


                                Try
                                    oStockItemUnit2.MultipleOfBaseUnit = decSellingUnit

                                Catch

                                End Try

                                oStockItemUnit2.Update()

                                SOPLog(logType.changeUOM, String.Format("Changed UOM from {0} to {1}", oldMultiple, decSellingUnit), CLng(sopLine.SOPOrderReturn.SOPOrderReturn), 0)


                                ' now recreate this line on the s op
                                Dim sopLine2 As New Sage.Accounting.SOP.SOPStandardItemLine
                                Dim sopLine_Read As Sage.Accounting.SOP.SOPStandardItemLine = DirectCast(sopLine, Sage.Accounting.SOP.SOPStandardItemLine)

                                Dim stockItems As New Sage.Accounting.Stock.StockItems
                                Dim stockItem As Sage.Accounting.Stock.StockItem = stockItems(sopLine_Read.ItemCode)

                                Dim exceptionType As Type = Nothing


                                ' NEW: Auto-complete the different if it's less than zero
                                If sopLine_Read.LineQuantity - CDec(lvItem.SubItems(8).Text) <= 0D Then
                                    lvItem.SubItems(11).Text = "Y"
                                End If


                                ' set required fields on new sopline
                                exceptionType = GetType(Sage.Accounting.Exceptions.Ex20319Exception)
                                Sage.Accounting.Application.AllowableWarnings.Add(sopLine2, exceptionType)

                                exceptionType = GetType(Sage.Accounting.Exceptions.Ex20320Exception)
                                Sage.Accounting.Application.AllowableWarnings.Add(sopLine2, exceptionType)

                                exceptionType = GetType(Sage.Accounting.Exceptions.StockItemHoldingInsufficientException)
                                Sage.Accounting.Application.AllowableWarnings.Add(sopLine2, exceptionType)

                                With sopLine2
                                    ' NEW: The order these are done in is CRITICAL!!!
                                    .SOPOrderReturn = sopOrder
                                    .Item = sopLine_Read.Item
                                    .ItemDescription = sopLine_Read.ItemDescription
                                    .WarehouseItem = sopLine_Read.WarehouseItem
                                    .FulfilmentMethod = Sage.Accounting.Stock.SOPOrderFulfilmentMethodEnum.EnumFulfilmentFromStock
                                    .SellingUnit = sopLine_Read.SellingUnit
                                    .PricingUnit = sopLine_Read.PricingUnit
                                    .LineQuantity = CDec(lvItem.SubItems(8).Text)
                                    .CostPrice = sopLine_Read.CostPrice
                                    .UnitSellingPrice = sopLine_Read.UnitSellingPrice
                                    .PrintSequenceNumber += CShort(1)

                                    Try
                                        .UnitDiscountPercent = sopLine_Read.UnitDiscountPercent

                                    Catch
                                        ' Bug in S200, it's not actually a "allowable exception" but property still gets set
                                    End Try

                                    .TaxCode = sopLine_Read.TaxCode

                                    .Fields("MaterialsRefNumber").Value = CStr(.Fields("MaterialsRefNumber").Value) & " " & lvItem.SubItems(12).Text


                                    .Post()
                                End With

                                sopOrder.Lines.Add(sopLine2)
                                sopOrder.Update()

                                SOPLog(logType.removeLine, String.Format("Add line (stock code={0}, details='{1}', qty={2}, selling unit='{3}', price='{4}', qty to allocate={5}, allocated={6}, unit price={7}, stock item unit={8}, unit cost={9}, unit discount % ={10}, unit discount={11}, disc unit price={12})", _
                                                                         sopLine2.ItemCode, _
                                                                         sopLine2.ItemDescription, _
                                                                         sopLine2.LineQuantity, _
                                                                         lvItem.SubItems(4).Text, _
                                                                         sopLine2.LineTotalValue, _
                                                                         sopLine2.LineQuantity - sopLine.AllocatedQuantity, _
                                                                         sopLine2.AllocatedQuantity, _
                                                                         sopLine2.UnitSellingPrice, _
                                                                         "", _
                                                                         sopLine2.CostPrice, _
                                                                         sopLine2.UnitDiscountPercent, _
                                                                         sopLine2.UnitDiscountValue, _
                                                                         sopLine2.UnitSellingPrice - sopLine2.UnitDiscountValue), _
                                                            CLng(sopLine2.SOPOrderReturn.SOPOrderReturn), CLng(sopLine2.SOPOrderReturnLine))

                                ' reset the stockitemunit uom multiple to its original value
                                Try
                                    oStockItemUnit2.MultipleOfBaseUnit = oldMultiple

                                Catch

                                End Try

                                oStockItemUnit2.Update()
                                oStockItemUnit2.Dispose()

                                SOPLog(logType.changeUOM, String.Format("Changed UOM back to {0}", oldMultiple), CLng(sopLine.SOPOrderReturn.SOPOrderReturn), 0)


                                ' NEW: If not complete re-create another item with the difference on it
                                If lvItem.SubItems(11).Text = "N" Then
                                    Dim sopLine3 As New Sage.Accounting.SOP.SOPStandardItemLine

                                    exceptionType = GetType(Sage.Accounting.Exceptions.Ex20319Exception)
                                    Sage.Accounting.Application.AllowableWarnings.Add(sopLine3, exceptionType)

                                    exceptionType = GetType(Sage.Accounting.Exceptions.Ex20320Exception)
                                    Sage.Accounting.Application.AllowableWarnings.Add(sopLine3, exceptionType)

                                    exceptionType = GetType(Sage.Accounting.Exceptions.StockItemHoldingInsufficientException)
                                    Sage.Accounting.Application.AllowableWarnings.Add(sopLine3, exceptionType)

                                    With sopLine3
                                        ' NEW: The order these are done in is CRITICAL!!!
                                        .SOPOrderReturn = sopOrder
                                        .Item = sopLine_Read.Item
                                        .ItemDescription = sopLine_Read.ItemDescription
                                        .WarehouseItem = sopLine_Read.WarehouseItem
                                        .FulfilmentMethod = Sage.Accounting.Stock.SOPOrderFulfilmentMethodEnum.EnumFulfilmentFromStock
                                        .SellingUnit = sopLine_Read.SellingUnit
                                        .PricingUnit = sopLine_Read.PricingUnit
                                        .LineQuantity = sopLine_Read.LineQuantity - CDec(lvItem.SubItems(8).Text)
                                        .CostPrice = sopLine_Read.CostPrice
                                        .UnitSellingPrice = sopLine_Read.UnitSellingPrice

                                        Try
                                            .UnitDiscountPercent = sopLine_Read.UnitDiscountPercent

                                        Catch
                                            ' Bug in S200, it's not actually a "allowable exception" but property still gets set
                                        End Try

                                        .TaxCode = sopLine_Read.TaxCode
                                        .PrintSequenceNumber -= CShort(1)

                                        .Fields("MaterialsRefNumber").Value = CStr(.Fields("MaterialsRefNumber").Value) & " " & lvItem.SubItems(12).Text


                                        .Post()
                                    End With

                                    ' NEW: This ONLY works here for some reason...
                                    'With sopLine3
                                    '    .AdjustAllocatedQuantity(0D)
                                    '    .AdjustAvailableForDespatch(0D)

                                    '    .Update()
                                    'End With

                                    sopOrder.Lines.Add(sopLine3)
                                    sopOrder.Update()


                                    With sopLine3
                                        For Each oLine As Sage.Accounting.SOP.SOPAllocationLine In .AllocationLines
                                            With oLine
                                                .AdjustAllocatedQuantity(0D)

                                                .Update()
                                            End With
                                        Next

                                        .AdjustAllocatedQuantity(0D)
                                        .AdjustAvailableForDespatch(0D)

                                        .Update()
                                    End With
                                End If


                                ' NEW: Remove the lock
                                If Not oActiveLock Is Nothing Then
                                    oActiveLock.Dispose()
                                End If
                            End If
                    End Select
                Next


                Dim oSOPOrders As New Sage.Accounting.SOP.SOPOrders

                oSOPOrders.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.SOP.SOPOrder.FIELD_DOCUMENTNO, sopLookup.SOPOrder.DocumentNo))
                oSOPOrders.Find()

                Dim oSOPOrder As Sage.Accounting.SOP.SOPOrder = oSOPOrders.First

                If oSOPOrder Is Nothing Then
                    Throw New Exception("Unable to locate the SOP order for updating the values.  Please try opening the order yourself and click on 'Save'.")
                End If

                With oSOPOrder
                    .CalculateValues()

                    .Lines.Query.Sorts.Clear()
                    .Lines.Query.Sorts.Add(New Sage.ObjectStore.Sort(Sage.Accounting.SOP.SOPOrderReturnLine.FIELD_PRINTSEQUENCENUMBER, True))

                    For Each oLine As Sage.Accounting.SOP.SOPOrderReturnLine In .Lines

                        With oLine
                            ' NEW: Remove the dot line
                            Select Case .LineType
                                Case Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeStandard
                                    If .ItemCode = "." Then
                                        .CancelLine()
                                    End If

                                Case Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeFreeText
                                    .ConfirmationIntentType = Sage.Accounting.SOP.SOPConfirmationIntentEnum.ConfirmOnDespatch
                            End Select
                        End With
                    Next

                    .UpdatePrintSequenceNumber()
                    .Update()
                End With


                ' NEW: Commit the transaction, we're done
                'With oTransaction
                '    .Commit()
                '    .Dispose()
                'End With

                'oTransaction = Nothing


                Dim oArray(0) As Object
                oArray(0) = oSOPOrder.SOPOrderReturn

                Dim oArgs As New Sage.Common.UI.FormArguments(oArray, GetType(Sage.Accounting.SOP.SOPOrder), Sage.Common.CustomAttributes.eParameterType.DbKey)


                ' NEW NEW: Fire the amend allocation screen
                Dim frmY As New Sage.MMS.SOP.AmendStockAllocationsForm(oArgs)

                With frmY
                    .ShowDialog()
                    .Dispose()
                End With

                frmY = Nothing



                ' NEW NEW NEW: Rounding issue...
                StartTransaction(sqlConnToSage)

                For Each objKey As Object In htSellingUnit.Keys
                    Dim sqlComm As New SqlCommand("UPDATE SOPOrderReturnLine SET SellingUnitMultiple = @SellingUnitMultiple, PricingUnitMultiple = @PricingUnitMultiple WHERE SOPOrderReturnLineID = @SOPOrderReturnLineID", sqlConnToSage)

                    With sqlComm
                        With .Parameters
                            .AddWithValue("@SellingUnitMultiple", CDec(htSellingUnit.Item(objKey)))
                            .AddWithValue("@PricingUnitMultiple", CDec(htSellingUnit.Item(objKey)))
                            .AddWithValue("@SOPOrderReturnLineID", CLng(objKey))
                        End With

                        .ExecuteNonQuery()
                        .Dispose()
                    End With

                    sqlComm = Nothing
                Next

                CommitTransaction(sqlConnToSage)



                ' NEW: Fire up Sage's own dispatch form
                Dim frmX As New Sage.MMS.SOP.ConfirmDespatchForm(oArgs)

                With frmX
                    .ShowDialog()
                    .Dispose()
                End With

                frmX = Nothing


                ' Clean up
                oSOPOrder = Nothing

                bolCurrentlySaving = False

                Me.Close()
                Exit Sub

            Catch ex As Exception
                'If Not oTransaction Is Nothing Then
                '    oTransaction.Dispose()
                '    oTransaction = Nothing
                'End If

                RollbackTransaction()

                MsgBox("An error has occurred while saving the dispatch." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
                Exit Sub
            End Try
        End If
    End Sub
#End Region

#Region " SUBROUTINE: cmdClose_Click() "
    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub
#End Region


#Region " SUBROUTINE: lvSOPitems_DoubleClick() "
    Private Sub lvSOPitems_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvSOPitems.DoubleClick
        If bolBlockEditLine = False Then

            If lvSOPitems.SelectedItems.Count = 1 Then

                Select Case lvSOPitems.SelectedItems(0).SubItems(13).Text
                    Case "EnumLineTypeStandard"
                        Dim frmX As New frmGoodsOutPopup

                        With frmX
                            .ItemSelected = lvSOPitems.SelectedItems(0)


                            .ShowDialog(Me)


                            ' Update the details
                            If Not frmX.ItemSelected Is Nothing Then
                                With lvSOPitems.SelectedItems(0)
                                    .SubItems(8).Text = frmX.ItemSelected.SubItems(8).Text
                                    .SubItems(10).Text = frmX.ItemSelected.SubItems(10).Text
                                    .SubItems(11).Text = frmX.ItemSelected.SubItems(11).Text
                                    .SubItems(12).Text = frmX.ItemSelected.SubItems(12).Text
                                End With
                            End If
                        End With

                        frmX.Dispose()
                        frmX = Nothing

                    Case "EnumLineTypeFreeText"
                        lvSOPitems.SelectedItems(0).SubItems(8).Text = lvSOPitems.SelectedItems(0).SubItems(7).Text
                        lvSOPitems.SelectedItems(0).SubItems(11).Text = "Y"
                End Select
            End If
        End If
    End Sub
#End Region


#Region " SUBROUTINE: sopLookup_SOPOrderSelected() "
    Private Sub sopLookup_SOPOrderSelected(ByVal sender As Object, ByVal args As Sage.MMS.Controls.SOPOrderLookupItemSelectedEventArgs) Handles sopLookup.SOPOrderSelected
        Try
            If Not (args.SelectedSOPOrder) Is Nothing Then

                If (bolCurrentlySaving = False) And (lvSOPitems.Items.Count > 0) Then
                    ' Warn the user
                    If MsgBox("There are items currently being edited, do you want still want to load the new SOP?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.No Then
                        Exit Sub
                    End If
                End If



                Dim sopSelected As Sage.Accounting.SOP.SOPOrder = args.SelectedSOPOrder

                Dim sopLine As Sage.Accounting.SOP.SOPOrderReturnLine = Nothing

                Dim lstItem As ListViewItem = Nothing


                ' Clear items
                lvSOPitems.Items.Clear()


                txtOrderNo.Text = sopSelected.DocumentNo
                txtCustomerName.Text = sopSelected.Customer.Name


                Dim oStockItems As Sage.Accounting.Stock.StockItems = Nothing
                Dim oStockItem As Sage.Accounting.Stock.StockItem = Nothing

                Dim oUnits As Sage.Accounting.Stock.Units = Nothing
                Dim oUnit As Sage.Accounting.Stock.Unit = Nothing

                Dim oStockItemUnit As Sage.Accounting.Stock.StockItemUnit = Nothing



                For Each sopLine In sopSelected.Lines
                    Select Case sopLine.LineType
                        Case Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeStandard
                            If sopLine.ItemCode = "." Then
                                Exit Select
                            End If


                            If (sopLine.LineQuantity - sopLine.DespatchReceiptQuantity > 0D) Then
                                lstItem = lvSOPitems.Items.Add(sopSelected.DocumentNo)
                                lstItem.Tag = sopLine ' This is so we can make it a bit easier when raising the D note

                                With lstItem.SubItems
                                    oStockItems = New Sage.Accounting.Stock.StockItems
                                    oStockItem = oStockItems(sopLine.ItemCode)

                                    oUnits = New Sage.Accounting.Stock.Units()


                                    ' Populate
                                    .Add(sopSelected.Customer.Name)
                                    .Add(sopLine.ItemCode)
                                    .Add(sopLine.ItemDescription)
                                    .Add(sopLine.SellingUnitDescription)

                                    .Add(Format(sopLine.LineQuantity, "0.00000"))
                                    .Add(Format(sopLine.DespatchReceiptQuantity, "0.00000"))

                                    .Add(Format(sopLine.LineQuantity - sopLine.DespatchReceiptQuantity, "0.00000"))
                                    .Add("0.00000")


                                    oUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.Unit.FIELD_NAME, sopLine.SellingUnitDescription))
                                    oUnit = oUnits.First

                                    oStockItem.Units.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_UNITOBJECT, oUnit))
                                    oStockItemUnit = oStockItem.Units.First

                                    .Add(Format((sopLine.LineQuantity - sopLine.DespatchReceiptQuantity) * oStockItemUnit.MultipleOfBaseUnit, "0.00000"))
                                    .Add("0.00000")

                                    .Add("N")
                                    .Add(sopLine.Fields("MaterialsRefNumber").GetString())
                                    .Add(sopLine.LineType.ToString())
                                End With
                            End If

                        Case Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeFreeText, Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeComment
                            lstItem = lvSOPitems.Items.Add(sopSelected.DocumentNo)
                            lstItem.Tag = sopLine ' This is so we can make it a bit easier when raising the D note

                            With lstItem.SubItems
                                ' Populate
                                .Add(sopSelected.Customer.Name)
                                .Add(sopLine.ItemCode)
                                .Add(sopLine.ItemDescription)
                                .Add(sopLine.SellingUnitDescription)

                                .Add(Format(sopLine.LineQuantity, "0.00000"))
                                .Add(Format(sopLine.DespatchReceiptQuantity, "0.00000"))

                                .Add(Format(sopLine.LineQuantity - sopLine.DespatchReceiptQuantity, "0.00000"))
                                .Add("0.00000")

                                .Add(Format((sopLine.LineQuantity - sopLine.DespatchReceiptQuantity), "0.00000"))
                                .Add("0.00000")

                                .Add("N")
                                .Add(sopLine.Fields("MaterialsRefNumber").GetString())
                                .Add(sopLine.LineType.ToString())
                            End With
                    End Select
                Next


                ' Dispose when done
                sopSelected.Dispose()

                Try
                    oStockItems.Dispose()
                    oStockItem.Dispose()

                    oUnits.Dispose()
                    oUnit.Dispose()

                    oStockItemUnit.Dispose()

                Catch

                End Try

                bolBlockEditLine = False

            Else
                bolBlockEditLine = True
            End If

        Catch ex As Exception
            bolBlockEditLine = True
            cmdSave.Enabled = False


            MsgBox("An error has occurred while loading the SOP order lines to the grid." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region


#Region " SUBROUTINE: SOPLog() "
    Private Sub SOPLog(ByVal type As Integer, ByVal message As String, ByVal SOPOrderReturnId As Int64, ByVal SOPOrderReturnLineId As Int64)
        Dim s As New SqlClient.SqlCommand("INSERT INTO tblSOPNotes(LogType, Message, SOPOrderReturnID, SOPOrderReturnLineID, Date, SageUser) " & _
                                          "VALUES(@LogType, @Message, @SOPOrderReturnID, @SOPOrderReturnLineID, @Date, @SageUser)", sqlConnToSage)

        With s.Parameters
            .AddWithValue("LogType", type)
            .AddWithValue("Message", message)
            .AddWithValue("SOPOrderReturnID", SOPOrderReturnId)
            .AddWithValue("SOPOrderReturnLineID", SOPOrderReturnLineId)
            .AddWithValue("Date", Format(DateTime.Now(), "yyyy-MM-dd HH:mm:ss"))
            .AddWithValue("SageUser", Sage.Accounting.Application.ActiveUser.UserName)
        End With

        s.ExecuteNonQuery()
        s.Dispose()
        s = Nothing
    End Sub
#End Region

End Class