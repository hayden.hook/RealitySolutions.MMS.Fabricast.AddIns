Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports RealitySolutions.Licensing.Core.Client

Public Class frmSOPLines
    Private sopCurrentOrder As Sage.Accounting.SOP.SOPOrder = Nothing


#Region " PROPERTY: SOPOrder() "
    Public Property SOPOrder() As Sage.Accounting.SOP.SOPOrder
        Get
            Return sopCurrentOrder
        End Get
        Set(ByVal value As Sage.Accounting.SOP.SOPOrder)
            sopCurrentOrder = value
        End Set
    End Property
#End Region


#Region " SUBROUTINE: frmSOPLines_Load() "
    Private Sub frmSOPLines_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        Try
            ' Populate the lines of the order
            lvLines.BeginUpdate()


            Dim sopLines As Sage.Accounting.SOP.SOPOrderReturnLines = sopCurrentOrder.Lines
            Dim sopLine As Sage.Accounting.SOP.SOPOrderReturnLine = Nothing

            Dim oStockItems As New Sage.Accounting.Stock.StockItems
            Dim oStockItem As Sage.Accounting.Stock.StockItem = Nothing

            Dim oUnits As New Sage.Accounting.Stock.Units
            Dim oUnit As Sage.Accounting.Stock.Unit = Nothing

            Dim oBaseUnit As Sage.Accounting.Stock.Unit = Nothing
            Dim oSellingUnit As Sage.Accounting.Stock.Unit = Nothing

            Dim oStockItemUnits As New Sage.Accounting.Stock.StockItemUnits
            Dim oStockItemUnit As Sage.Accounting.Stock.StockItemUnit = Nothing



            lvLines.Items.Clear()


            For Each sopLine In sopLines
                Try
                    Select Case sopLine.LineType
                        Case Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeStandard, Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeFreeText
                            Dim lvNew As New ListViewItem(sopLine.ItemCode)

                            With lvNew
                                .Tag = sopLine

                                With .SubItems
                                    oStockItem = oStockItems(sopLine.ItemCode)

                                    If oStockItem Is Nothing Then
                                        Throw New Exception("Failed to locate the '" & sopLine.ItemCode & "' stock code.")
                                    End If


                                    ' We have to work this out...
                                    Dim oSellingSIU As Sage.Accounting.Stock.StockItemUnit = Nothing


                                    oUnits = New Sage.Accounting.Stock.Units
                                    oUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.Unit.FIELD_NAME, sopLine.SellingUnitDescription))
                                    oUnits.Find()

                                    oSellingUnit = oUnits.First


                                    oStockItemUnits = New Sage.Accounting.Stock.StockItemUnits
                                    oStockItemUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_UNITOBJECT, oSellingUnit))
                                    oStockItemUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_ITEMOBJECT, oStockItem))
                                    oStockItemUnits.Find()
                                    oSellingSIU = oStockItemUnits.First


                                    .Add(sopLine.ItemDescription)
                                    .Add(Format(oSellingSIU.MultipleOfBaseUnit * sopLine.LineQuantity, "0.00000"))
                                    .Add(oStockItem.BaseUnitName)
                                    .Add(Format(sopLine.LineQuantity, "0.00000"))
                                    .Add(sopLine.SellingUnitDescription)
                                    .Add(Format(CDec(sopLine.Fields("LatestCost").Value), "0.00000"))


                                    ' Get the conversation factor
                                    Dim decQty As Decimal = IfNullReturnDecimalZero(DanLookup("Quantity", "TransactionHistory", "ItemID = " & CLng(oStockItem.Item) & " AND TransactionTypeID = 15 AND Reference = '" & sopCurrentOrder.DocumentNo & "'", , sqlConnToSage))

                                    If decQty = 0D Then
                                        Throw New Exception("Failed to locate the dispatched quantity.")
                                    End If



                                    .Add(Format(oStockItem.AverageBuyingPrice * decQty, "0.00"))
                                    .Add(Format(sopLine.UnitSellingPrice, "0.00"))
                                    .Add(Format(sopLine.LineQuantity * sopLine.UnitSellingPrice, "0.00"))
                                    .Add(Format(sopLine.LineTotalValue, "0.00"))

                                    .Add(Format(sopLine.LineTotalValue - (oStockItem.AverageBuyingPrice * decQty), "0.00"))

                                    If (oStockItem.AverageBuyingPrice * decQty) = 0D Then
                                        .Add("100.00")

                                    Else
                                        If sopLine.LineTotalValue = 0D Then
                                            .Add("0.00")

                                        Else
                                            .Add(Format(((sopLine.LineTotalValue - (oStockItem.AverageBuyingPrice * decQty)) / sopLine.LineTotalValue) * 100, "0.00"))
                                        End If
                                    End If
                                End With
                            End With


                            lvLines.Items.Add(lvNew)
                    End Select

                Catch 'ex As Exception
                    'MsgBox(ex.ToString)
                End Try
            Next


            ' Show the totals
            CalculateTotals()


            lvLines.EndUpdate()

        Catch ex As Exception
            MsgBox("An error has occurred while loading the SOP lines." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region


#Region " SUBROUTINE: cmdSave_Click() "
    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        If MsgBox("Are you sure you want to save?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.Yes Then
            Try
                Dim lvEach As ListViewItem = Nothing

                For Each lvEach In lvLines.Items
                    Dim sopOrderLine As Sage.Accounting.SOP.SOPOrderReturnLine = DirectCast(lvEach.Tag, Sage.Accounting.SOP.SOPOrderReturnLine)
                    Dim sopStdLine As Sage.Accounting.SOP.SOPStandardItemLine = Nothing
                    Dim sopFreeLine As Sage.Accounting.SOP.SOPFreeTextLine = Nothing

                    Dim exceptionType As Type = Nothing

                    Select Case sopOrderLine.LineType
                        Case Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeStandard
                            sopStdLine = DirectCast(sopOrderLine, Sage.Accounting.SOP.SOPStandardItemLine)


                            exceptionType = GetType(Sage.Accounting.Exceptions.PriceAndDiscountHaveManualValuesException)
                            Sage.Accounting.Application.AllowableWarnings.Add(sopStdLine, exceptionType)

                            exceptionType = GetType(Sage.Accounting.Exceptions.Ex20319Exception)
                            Sage.Accounting.Application.AllowableWarnings.Add(sopStdLine, exceptionType)


                            With sopStdLine
                                .UnitSellingPrice = CDec(lvEach.SubItems(7).Text)
                                .CostPrice = sopOrderLine.CostPrice ' CDec(lvEach.SubItems(4).Text)

                                .UpdateHeaderValues()
                                .Update()
                            End With

                        Case Sage.Accounting.OrderProcessing.LineTypeEnum.EnumLineTypeFreeText
                            sopFreeLine = DirectCast(sopOrderLine, Sage.Accounting.SOP.SOPFreeTextLine)


                            exceptionType = GetType(Sage.Accounting.Exceptions.PriceAndDiscountHaveManualValuesException)
                            Sage.Accounting.Application.AllowableWarnings.Add(sopFreeLine, exceptionType)

                            exceptionType = GetType(Sage.Accounting.Exceptions.Ex20319Exception)
                            Sage.Accounting.Application.AllowableWarnings.Add(sopFreeLine, exceptionType)


                            With sopFreeLine
                                .UnitSellingPrice = CDec(lvEach.SubItems(7).Text)
                                .CostPrice = sopOrderLine.CostPrice ' CDec(lvEach.SubItems(4).Text)

                                .UpdateHeaderValues()
                                .Update()
                            End With
                    End Select
                Next


                ' Done
                Me.Close()
                Exit Sub

            Catch ex As Exception
                MsgBox("An error has occurred while saving the SOP lines to the database." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
                Exit Sub
            End Try
        End If
    End Sub
#End Region

#Region " SUBROUTINE: cmdClose_Click() "
    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub
#End Region

#Region " SUBROUTINE: cmdEdit_Click() "
    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If lvLines.SelectedItems.Count = 1 Then
            Dim frmX As New frmSOPLinePrompt

            With frmX
                .SOPReturnLine = DirectCast(lvLines.SelectedItems(0).Tag, Sage.Accounting.SOP.SOPOrderReturnLine)

                .UnitCost = .SOPReturnLine.CostPrice 'CDec(lvLines.SelectedItems(0).SubItems(5).Text)
                .UnitPrice = CDec(lvLines.SelectedItems(0).SubItems(7).Text)

                .ShowDialog()


                If (.UnitCost <> -1D) And (.UnitPrice <> -1D) Then
                    'lvLines.SelectedItems(0).SubItems(5).Text = Format(.UnitCost, "0.00")
                    lvLines.SelectedItems(0).SubItems(7).Text = Format(.UnitPrice, "0.00")

                    With DirectCast(lvLines.SelectedItems(0).Tag, Sage.Accounting.SOP.SOPOrderReturnLine)
                        .CostPrice = frmX.UnitCost
                        .UnitSellingPrice = frmX.UnitPrice
                        .LineTotalValue = frmX.SOPReturnLine.LineQuantity * frmX.UnitPrice
                    End With

                    lvLines.SelectedItems(0).SubItems(9).Text = Format(frmX.SOPReturnLine.LineQuantity * frmX.UnitPrice, "0.00")


                    ' NEW: Don't forget to update the grid!
                    'If .UnitPrice = 0D Then
                    '    lvLines.SelectedItems(0).SubItems(9).Text = "0.00"

                    'Else
                    '    lvLines.SelectedItems(0).SubItems(9).Text = Format(.UnitPrice - .UnitCost, "0.00")
                    'End If

                    'If .UnitCost = 0D Then
                    '    lvLines.SelectedItems(0).SubItems(10).Text = "100.00"

                    'Else
                    '    lvLines.SelectedItems(0).SubItems(10).Text = Format(((.UnitPrice - .UnitCost) / .UnitCost) * 100, "0.00")
                    'End If


                    ' NEW: Don't forget the grand totals!
                    CalculateTotals()
                End If


                .Dispose()
            End With

            frmX = Nothing
        End If
    End Sub
#End Region

#Region " SUBROUTINE: cmdStockEnquiry_Click() "
    Private Sub cmdStockEnquiry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdStockEnquiry.Click
        If lvLines.SelectedItems.Count = 1 Then

            If lvLines.SelectedItems(0).Text <> "" Then
                Dim frmX As New frmStockEnquiry

                With frmX
                    Dim oStockItems As New Sage.Accounting.Stock.StockItems

                    .StockItem = oStockItems(DirectCast(lvLines.SelectedItems(0).Tag, Sage.Accounting.SOP.SOPOrderReturnLine).ItemCode)
                    .ShowDialog(Me)

                    .Dispose()
                End With

                frmX = Nothing
            End If

        Else
            MsgBox("Please select a order to lookup the stock for.", MsgBoxStyle.Exclamation, "Nothing Selected")

            lvLines.Focus()
            Exit Sub
        End If
    End Sub
#End Region


#Region " SUBROUTINE: lvLines_DoubleClick() "
    Private Sub lvLines_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvLines.DoubleClick
        cmdEdit.PerformClick()
    End Sub
#End Region


#Region " SUBROUTINE: CalculateTotals() "
    Private Sub CalculateTotals()
        Dim decCost As Decimal = 0D
        Dim decSOPCost As Decimal = 0D
        Dim decSellingPrice As Decimal = 0D
        Dim decTotalSellingPrice As Decimal = 0D
        Dim decProfit As Decimal = 0D
        Dim decLineTotal As Decimal = 0D

        For Each lvEach As ListViewItem In lvLines.Items
            Dim sopLine As Sage.Accounting.SOP.SOPOrderReturnLine = DirectCast(lvEach.Tag, Sage.Accounting.SOP.SOPOrderReturnLine)

            With sopLine
                decCost += CDec(lvEach.SubItems(6).Text) 'sopLine.CostPrice
                decSOPCost += sopLine.CostPrice
                decSellingPrice += sopLine.UnitSellingPrice
                decTotalSellingPrice += (sopLine.LineQuantity * sopLine.UnitSellingPrice)
                decProfit += CDec(lvEach.SubItems(10).Text) '(sopLine.UnitSellingPrice - sopLine.CostPrice)
                decLineTotal += sopLine.LineTotalValue
            End With
        Next


        ' Done, show the values
        txtCost.Text = Format(decCost, "0.00")
        txtSellingPrice.Text = Format(decSellingPrice, "0.00")
        txtTotalSP.Text = Format(decTotalSellingPrice, "0.00")
        txtProfit.Text = Format(decProfit, "0.00")
        txtTotalLT.Text = Format(decLineTotal, "0.00")

        If decSellingPrice = 0D Then
            txtProfitPercentage.Text = "0.00"

        Else
            If decLineTotal = 0D Then
                txtProfitPercentage.Text = "100.00"

            Else
                txtProfitPercentage.Text = Format((decProfit / decLineTotal) * 100, "0.00")
            End If
        End If
    End Sub
#End Region

End Class