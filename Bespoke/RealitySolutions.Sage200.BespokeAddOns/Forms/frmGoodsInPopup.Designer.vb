<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGoodsInPopup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblUnit2 = New System.Windows.Forms.Label
        Me.lblUnit1 = New System.Windows.Forms.Label
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.chkCompleted = New System.Windows.Forms.CheckBox
        Me.txtQtyOutstanding4 = New System.Windows.Forms.TextBox
        Me.txtQtyOutstanding3 = New System.Windows.Forms.TextBox
        Me.txtQtyOutstanding2 = New System.Windows.Forms.TextBox
        Me.txtQtyOutstanding1 = New System.Windows.Forms.TextBox
        Me.txtQtyReceived = New System.Windows.Forms.TextBox
        Me.txtQtyOrdered = New System.Windows.Forms.TextBox
        Me.txtBuyingUnit = New System.Windows.Forms.TextBox
        Me.txtItemName = New System.Windows.Forms.TextBox
        Me.txtItemCode = New System.Windows.Forms.TextBox
        Me.txtSupplierName = New System.Windows.Forms.TextBox
        Me.txtOrderNumber = New System.Windows.Forms.TextBox
        Me.lblCompleted = New System.Windows.Forms.Label
        Me.lblQtyRequired = New System.Windows.Forms.Label
        Me.lblQtyDispatched = New System.Windows.Forms.Label
        Me.lblQtyOrdered = New System.Windows.Forms.Label
        Me.lblSellingUnit = New System.Windows.Forms.Label
        Me.lblItemName = New System.Windows.Forms.Label
        Me.lblItemCode = New System.Windows.Forms.Label
        Me.lblCustomerName = New System.Windows.Forms.Label
        Me.lblOrderNumber = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblUnit2
        '
        Me.lblUnit2.AutoSize = True
        Me.lblUnit2.Location = New System.Drawing.Point(296, 200)
        Me.lblUnit2.Name = "lblUnit2"
        Me.lblUnit2.Size = New System.Drawing.Size(47, 13)
        Me.lblUnit2.TabIndex = 53
        Me.lblUnit2.Text = "<UNIT>"
        '
        'lblUnit1
        '
        Me.lblUnit1.AutoSize = True
        Me.lblUnit1.Location = New System.Drawing.Point(296, 176)
        Me.lblUnit1.Name = "lblUnit1"
        Me.lblUnit1.Size = New System.Drawing.Size(47, 13)
        Me.lblUnit1.TabIndex = 52
        Me.lblUnit1.Text = "<UNIT>"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Location = New System.Drawing.Point(268, 252)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 51
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdOK
        '
        Me.cmdOK.Location = New System.Drawing.Point(8, 252)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = True
        '
        'chkCompleted
        '
        Me.chkCompleted.AutoSize = True
        Me.chkCompleted.Location = New System.Drawing.Point(100, 224)
        Me.chkCompleted.Name = "chkCompleted"
        Me.chkCompleted.Size = New System.Drawing.Size(15, 14)
        Me.chkCompleted.TabIndex = 49
        Me.chkCompleted.UseVisualStyleBackColor = True
        '
        'txtQtyOutstanding4
        '
        Me.txtQtyOutstanding4.Location = New System.Drawing.Point(200, 196)
        Me.txtQtyOutstanding4.Name = "txtQtyOutstanding4"
        Me.txtQtyOutstanding4.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyOutstanding4.TabIndex = 48
        Me.txtQtyOutstanding4.Text = "0.00000"
        Me.txtQtyOutstanding4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtyOutstanding3
        '
        Me.txtQtyOutstanding3.Location = New System.Drawing.Point(104, 196)
        Me.txtQtyOutstanding3.Name = "txtQtyOutstanding3"
        Me.txtQtyOutstanding3.ReadOnly = True
        Me.txtQtyOutstanding3.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyOutstanding3.TabIndex = 47
        Me.txtQtyOutstanding3.Text = "0.00000"
        Me.txtQtyOutstanding3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtyOutstanding2
        '
        Me.txtQtyOutstanding2.Location = New System.Drawing.Point(200, 172)
        Me.txtQtyOutstanding2.Name = "txtQtyOutstanding2"
        Me.txtQtyOutstanding2.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyOutstanding2.TabIndex = 46
        Me.txtQtyOutstanding2.Text = "0.00000"
        Me.txtQtyOutstanding2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtyOutstanding1
        '
        Me.txtQtyOutstanding1.Location = New System.Drawing.Point(104, 172)
        Me.txtQtyOutstanding1.Name = "txtQtyOutstanding1"
        Me.txtQtyOutstanding1.ReadOnly = True
        Me.txtQtyOutstanding1.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyOutstanding1.TabIndex = 45
        Me.txtQtyOutstanding1.Text = "0.00000"
        Me.txtQtyOutstanding1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtyReceived
        '
        Me.txtQtyReceived.Location = New System.Drawing.Point(104, 148)
        Me.txtQtyReceived.Name = "txtQtyReceived"
        Me.txtQtyReceived.ReadOnly = True
        Me.txtQtyReceived.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyReceived.TabIndex = 44
        Me.txtQtyReceived.Text = "0.00000"
        Me.txtQtyReceived.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtyOrdered
        '
        Me.txtQtyOrdered.Location = New System.Drawing.Point(104, 124)
        Me.txtQtyOrdered.Name = "txtQtyOrdered"
        Me.txtQtyOrdered.ReadOnly = True
        Me.txtQtyOrdered.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyOrdered.TabIndex = 43
        Me.txtQtyOrdered.Text = "0.00000"
        Me.txtQtyOrdered.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBuyingUnit
        '
        Me.txtBuyingUnit.Location = New System.Drawing.Point(104, 100)
        Me.txtBuyingUnit.Name = "txtBuyingUnit"
        Me.txtBuyingUnit.ReadOnly = True
        Me.txtBuyingUnit.Size = New System.Drawing.Size(92, 21)
        Me.txtBuyingUnit.TabIndex = 41
        '
        'txtItemName
        '
        Me.txtItemName.Location = New System.Drawing.Point(104, 76)
        Me.txtItemName.Name = "txtItemName"
        Me.txtItemName.ReadOnly = True
        Me.txtItemName.Size = New System.Drawing.Size(188, 21)
        Me.txtItemName.TabIndex = 40
        '
        'txtItemCode
        '
        Me.txtItemCode.Location = New System.Drawing.Point(104, 52)
        Me.txtItemCode.Name = "txtItemCode"
        Me.txtItemCode.ReadOnly = True
        Me.txtItemCode.Size = New System.Drawing.Size(188, 21)
        Me.txtItemCode.TabIndex = 39
        '
        'txtSupplierName
        '
        Me.txtSupplierName.Location = New System.Drawing.Point(104, 28)
        Me.txtSupplierName.Name = "txtSupplierName"
        Me.txtSupplierName.ReadOnly = True
        Me.txtSupplierName.Size = New System.Drawing.Size(188, 21)
        Me.txtSupplierName.TabIndex = 38
        '
        'txtOrderNumber
        '
        Me.txtOrderNumber.Location = New System.Drawing.Point(104, 4)
        Me.txtOrderNumber.Name = "txtOrderNumber"
        Me.txtOrderNumber.ReadOnly = True
        Me.txtOrderNumber.Size = New System.Drawing.Size(188, 21)
        Me.txtOrderNumber.TabIndex = 37
        '
        'lblCompleted
        '
        Me.lblCompleted.AutoSize = True
        Me.lblCompleted.Location = New System.Drawing.Point(8, 224)
        Me.lblCompleted.Name = "lblCompleted"
        Me.lblCompleted.Size = New System.Drawing.Size(67, 13)
        Me.lblCompleted.TabIndex = 36
        Me.lblCompleted.Text = "Completed?:"
        '
        'lblQtyRequired
        '
        Me.lblQtyRequired.AutoSize = True
        Me.lblQtyRequired.Location = New System.Drawing.Point(8, 176)
        Me.lblQtyRequired.Name = "lblQtyRequired"
        Me.lblQtyRequired.Size = New System.Drawing.Size(91, 13)
        Me.lblQtyRequired.TabIndex = 35
        Me.lblQtyRequired.Text = "Qty Outstanding:"
        '
        'lblQtyDispatched
        '
        Me.lblQtyDispatched.AutoSize = True
        Me.lblQtyDispatched.Location = New System.Drawing.Point(8, 152)
        Me.lblQtyDispatched.Name = "lblQtyDispatched"
        Me.lblQtyDispatched.Size = New System.Drawing.Size(76, 13)
        Me.lblQtyDispatched.TabIndex = 34
        Me.lblQtyDispatched.Text = "Qty Received:"
        '
        'lblQtyOrdered
        '
        Me.lblQtyOrdered.AutoSize = True
        Me.lblQtyOrdered.Location = New System.Drawing.Point(8, 128)
        Me.lblQtyOrdered.Name = "lblQtyOrdered"
        Me.lblQtyOrdered.Size = New System.Drawing.Size(72, 13)
        Me.lblQtyOrdered.TabIndex = 33
        Me.lblQtyOrdered.Text = "Qty Ordered:"
        '
        'lblSellingUnit
        '
        Me.lblSellingUnit.AutoSize = True
        Me.lblSellingUnit.Location = New System.Drawing.Point(8, 104)
        Me.lblSellingUnit.Name = "lblSellingUnit"
        Me.lblSellingUnit.Size = New System.Drawing.Size(65, 13)
        Me.lblSellingUnit.TabIndex = 31
        Me.lblSellingUnit.Text = "Buying Unit:"
        '
        'lblItemName
        '
        Me.lblItemName.AutoSize = True
        Me.lblItemName.Location = New System.Drawing.Point(8, 80)
        Me.lblItemName.Name = "lblItemName"
        Me.lblItemName.Size = New System.Drawing.Size(63, 13)
        Me.lblItemName.TabIndex = 30
        Me.lblItemName.Text = "Item Name:"
        '
        'lblItemCode
        '
        Me.lblItemCode.AutoSize = True
        Me.lblItemCode.Location = New System.Drawing.Point(8, 56)
        Me.lblItemCode.Name = "lblItemCode"
        Me.lblItemCode.Size = New System.Drawing.Size(61, 13)
        Me.lblItemCode.TabIndex = 29
        Me.lblItemCode.Text = "Item Code:"
        '
        'lblCustomerName
        '
        Me.lblCustomerName.AutoSize = True
        Me.lblCustomerName.Location = New System.Drawing.Point(8, 32)
        Me.lblCustomerName.Name = "lblCustomerName"
        Me.lblCustomerName.Size = New System.Drawing.Size(79, 13)
        Me.lblCustomerName.TabIndex = 28
        Me.lblCustomerName.Text = "Supplier Name:"
        '
        'lblOrderNumber
        '
        Me.lblOrderNumber.AutoSize = True
        Me.lblOrderNumber.Location = New System.Drawing.Point(8, 8)
        Me.lblOrderNumber.Name = "lblOrderNumber"
        Me.lblOrderNumber.Size = New System.Drawing.Size(79, 13)
        Me.lblOrderNumber.TabIndex = 27
        Me.lblOrderNumber.Text = "Order Number:"
        '
        'frmGoodsInPopup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(348, 279)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblUnit2)
        Me.Controls.Add(Me.lblUnit1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.chkCompleted)
        Me.Controls.Add(Me.txtQtyOutstanding4)
        Me.Controls.Add(Me.txtQtyOutstanding3)
        Me.Controls.Add(Me.txtQtyOutstanding2)
        Me.Controls.Add(Me.txtQtyOutstanding1)
        Me.Controls.Add(Me.txtQtyReceived)
        Me.Controls.Add(Me.txtQtyOrdered)
        Me.Controls.Add(Me.txtBuyingUnit)
        Me.Controls.Add(Me.txtItemName)
        Me.Controls.Add(Me.txtItemCode)
        Me.Controls.Add(Me.txtSupplierName)
        Me.Controls.Add(Me.txtOrderNumber)
        Me.Controls.Add(Me.lblCompleted)
        Me.Controls.Add(Me.lblQtyRequired)
        Me.Controls.Add(Me.lblQtyDispatched)
        Me.Controls.Add(Me.lblQtyOrdered)
        Me.Controls.Add(Me.lblSellingUnit)
        Me.Controls.Add(Me.lblItemName)
        Me.Controls.Add(Me.lblItemCode)
        Me.Controls.Add(Me.lblCustomerName)
        Me.Controls.Add(Me.lblOrderNumber)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmGoodsInPopup"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Details"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblUnit2 As System.Windows.Forms.Label
    Friend WithEvents lblUnit1 As System.Windows.Forms.Label
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents chkCompleted As System.Windows.Forms.CheckBox
    Friend WithEvents txtQtyOutstanding4 As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyOutstanding3 As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyOutstanding2 As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyOutstanding1 As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyReceived As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyOrdered As System.Windows.Forms.TextBox
    Friend WithEvents txtBuyingUnit As System.Windows.Forms.TextBox
    Friend WithEvents txtItemName As System.Windows.Forms.TextBox
    Friend WithEvents txtItemCode As System.Windows.Forms.TextBox
    Friend WithEvents txtSupplierName As System.Windows.Forms.TextBox
    Friend WithEvents txtOrderNumber As System.Windows.Forms.TextBox
    Friend WithEvents lblCompleted As System.Windows.Forms.Label
    Friend WithEvents lblQtyRequired As System.Windows.Forms.Label
    Friend WithEvents lblQtyDispatched As System.Windows.Forms.Label
    Friend WithEvents lblQtyOrdered As System.Windows.Forms.Label
    Friend WithEvents lblSellingUnit As System.Windows.Forms.Label
    Friend WithEvents lblItemName As System.Windows.Forms.Label
    Friend WithEvents lblItemCode As System.Windows.Forms.Label
    Friend WithEvents lblCustomerName As System.Windows.Forms.Label
    Friend WithEvents lblOrderNumber As System.Windows.Forms.Label
End Class
