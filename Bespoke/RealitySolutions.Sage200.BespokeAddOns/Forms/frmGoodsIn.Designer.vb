<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGoodsIn
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.fraAccountSelection = New System.Windows.Forms.GroupBox
        Me.SupplierLookup1 = New Sage.MMS.Controls.SupplierLookup
        Me.txtName = New Sage.Common.Controls.MaskedTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.fraSupplier = New System.Windows.Forms.GroupBox
        Me.txtNarrative = New Sage.Common.Controls.MaskedTextBox
        Me.dtpDate = New Sage.Common.Controls.DatePicker
        Me.txtReference = New Sage.Common.Controls.MaskedTextBox
        Me.lblNarrative = New System.Windows.Forms.Label
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblReference = New System.Windows.Forms.Label
        Me.fraReceivingWarehouse = New System.Windows.Forms.GroupBox
        Me.WarehousesLookup1 = New Sage.MMS.Controls.WarehousesLookup
        Me.lblWarehouse = New System.Windows.Forms.Label
        Me.lvDetails = New System.Windows.Forms.ListView
        Me.chOrderNo = New System.Windows.Forms.ColumnHeader
        Me.chOrderDate = New System.Windows.Forms.ColumnHeader
        Me.chSupplier = New System.Windows.Forms.ColumnHeader
        Me.chItemCode = New System.Windows.Forms.ColumnHeader
        Me.chItemName = New System.Windows.Forms.ColumnHeader
        Me.chBuyingUnit = New System.Windows.Forms.ColumnHeader
        Me.chQty = New System.Windows.Forms.ColumnHeader
        Me.chQtyReceived = New System.Windows.Forms.ColumnHeader
        Me.chQtyOutstanding1 = New System.Windows.Forms.ColumnHeader
        Me.chQtyOutstanding2 = New System.Windows.Forms.ColumnHeader
        Me.chQtyOutstanding3 = New System.Windows.Forms.ColumnHeader
        Me.chQtyOutstanding4 = New System.Windows.Forms.ColumnHeader
        Me.chComplete = New System.Windows.Forms.ColumnHeader
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdClose = New System.Windows.Forms.Button
        Me.fraAccountSelection.SuspendLayout()
        Me.fraSupplier.SuspendLayout()
        Me.fraReceivingWarehouse.SuspendLayout()
        CType(Me.WarehousesLookup1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'fraAccountSelection
        '
        Me.fraAccountSelection.Controls.Add(Me.SupplierLookup1)
        Me.fraAccountSelection.Controls.Add(Me.txtName)
        Me.fraAccountSelection.Controls.Add(Me.lblName)
        Me.fraAccountSelection.Location = New System.Drawing.Point(4, 4)
        Me.fraAccountSelection.Name = "fraAccountSelection"
        Me.fraAccountSelection.Size = New System.Drawing.Size(368, 128)
        Me.fraAccountSelection.TabIndex = 0
        Me.fraAccountSelection.TabStop = False
        Me.fraAccountSelection.Text = "Account selection"
        '
        'SupplierLookup1
        '
        Me.SupplierLookup1.Location = New System.Drawing.Point(12, 16)
        Me.SupplierLookup1.Name = "SupplierLookup1"
        Me.SupplierLookup1.ShowPostCodePanel = True
        Me.SupplierLookup1.Size = New System.Drawing.Size(250, 82)
        Me.SupplierLookup1.StartTextBoxesAt = 101
        Me.SupplierLookup1.TabIndex = 5
        Me.SupplierLookup1.TabStop = False
        Me.SupplierLookup1.Text = "SupplierLookup1"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.SystemColors.Window
        Me.txtName.Enabled = False
        Me.txtName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtName.Location = New System.Drawing.Point(112, 100)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(248, 21)
        Me.txtName.TabIndex = 4
        '
        'lblName
        '
        Me.lblName.AutoSize = True
        Me.lblName.Location = New System.Drawing.Point(12, 104)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(38, 13)
        Me.lblName.TabIndex = 3
        Me.lblName.Text = "Name:"
        '
        'fraSupplier
        '
        Me.fraSupplier.Controls.Add(Me.txtNarrative)
        Me.fraSupplier.Controls.Add(Me.dtpDate)
        Me.fraSupplier.Controls.Add(Me.txtReference)
        Me.fraSupplier.Controls.Add(Me.lblNarrative)
        Me.fraSupplier.Controls.Add(Me.lblDate)
        Me.fraSupplier.Controls.Add(Me.lblReference)
        Me.fraSupplier.Location = New System.Drawing.Point(376, 4)
        Me.fraSupplier.Name = "fraSupplier"
        Me.fraSupplier.Size = New System.Drawing.Size(368, 128)
        Me.fraSupplier.TabIndex = 1
        Me.fraSupplier.TabStop = False
        Me.fraSupplier.Text = "Supplier goods received note details"
        Me.fraSupplier.Visible = False
        '
        'txtNarrative
        '
        Me.txtNarrative.BackColor = System.Drawing.SystemColors.Window
        Me.txtNarrative.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNarrative.Location = New System.Drawing.Point(80, 100)
        Me.txtNarrative.Name = "txtNarrative"
        Me.txtNarrative.Size = New System.Drawing.Size(276, 21)
        Me.txtNarrative.TabIndex = 6
        '
        'dtpDate
        '
        Me.dtpDate.BackColor = System.Drawing.SystemColors.Window
        Me.dtpDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dtpDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.dtpDate.FlatStyle = System.Windows.Forms.FlatStyle.Standard
        Me.dtpDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Location = New System.Drawing.Point(80, 40)
        Me.dtpDate.MaxLength = 10
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(144, 21)
        Me.dtpDate.TabIndex = 3
        Me.dtpDate.Tooltip = ""
        Me.dtpDate.Value = New Date(CType(0, Long))
        '
        'txtReference
        '
        Me.txtReference.BackColor = System.Drawing.SystemColors.Window
        Me.txtReference.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtReference.Location = New System.Drawing.Point(80, 16)
        Me.txtReference.Name = "txtReference"
        Me.txtReference.Size = New System.Drawing.Size(144, 21)
        Me.txtReference.TabIndex = 5
        '
        'lblNarrative
        '
        Me.lblNarrative.AutoSize = True
        Me.lblNarrative.Location = New System.Drawing.Point(8, 104)
        Me.lblNarrative.Name = "lblNarrative"
        Me.lblNarrative.Size = New System.Drawing.Size(56, 13)
        Me.lblNarrative.TabIndex = 3
        Me.lblNarrative.Text = "Narrative:"
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Location = New System.Drawing.Point(12, 44)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(34, 13)
        Me.lblDate.TabIndex = 2
        Me.lblDate.Text = "Date:"
        '
        'lblReference
        '
        Me.lblReference.AutoSize = True
        Me.lblReference.Location = New System.Drawing.Point(12, 20)
        Me.lblReference.Name = "lblReference"
        Me.lblReference.Size = New System.Drawing.Size(61, 13)
        Me.lblReference.TabIndex = 1
        Me.lblReference.Text = "Reference:"
        '
        'fraReceivingWarehouse
        '
        Me.fraReceivingWarehouse.Controls.Add(Me.WarehousesLookup1)
        Me.fraReceivingWarehouse.Controls.Add(Me.lblWarehouse)
        Me.fraReceivingWarehouse.Enabled = False
        Me.fraReceivingWarehouse.Location = New System.Drawing.Point(4, 140)
        Me.fraReceivingWarehouse.Name = "fraReceivingWarehouse"
        Me.fraReceivingWarehouse.Size = New System.Drawing.Size(740, 48)
        Me.fraReceivingWarehouse.TabIndex = 2
        Me.fraReceivingWarehouse.TabStop = False
        Me.fraReceivingWarehouse.Text = "Receiving warehouse"
        Me.fraReceivingWarehouse.Visible = False
        '
        'WarehousesLookup1
        '
        Me.WarehousesLookup1.AllowSinglePartialMatch = True
        Me.WarehousesLookup1.BackColor = System.Drawing.SystemColors.Window
        Me.WarehousesLookup1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.WarehousesLookup1.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.WarehousesLookup1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WarehousesLookup1.Location = New System.Drawing.Point(112, 20)
        Me.WarehousesLookup1.MaxLength = 32767
        Me.WarehousesLookup1.Name = "WarehousesLookup1"
        Me.WarehousesLookup1.Size = New System.Drawing.Size(148, 21)
        Me.WarehousesLookup1.TabIndex = 3
        Me.WarehousesLookup1.Tooltip = ""
        Me.WarehousesLookup1.Value = Nothing
        '
        'lblWarehouse
        '
        Me.lblWarehouse.AutoSize = True
        Me.lblWarehouse.Location = New System.Drawing.Point(12, 24)
        Me.lblWarehouse.Name = "lblWarehouse"
        Me.lblWarehouse.Size = New System.Drawing.Size(66, 13)
        Me.lblWarehouse.TabIndex = 4
        Me.lblWarehouse.Text = "Warehouse:"
        '
        'lvDetails
        '
        Me.lvDetails.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.chOrderNo, Me.chOrderDate, Me.chSupplier, Me.chItemCode, Me.chItemName, Me.chBuyingUnit, Me.chQty, Me.chQtyReceived, Me.chQtyOutstanding1, Me.chQtyOutstanding2, Me.chQtyOutstanding3, Me.chQtyOutstanding4, Me.chComplete})
        Me.lvDetails.FullRowSelect = True
        Me.lvDetails.GridLines = True
        Me.lvDetails.HideSelection = False
        Me.lvDetails.Location = New System.Drawing.Point(4, 136)
        Me.lvDetails.MultiSelect = False
        Me.lvDetails.Name = "lvDetails"
        Me.lvDetails.Size = New System.Drawing.Size(740, 344)
        Me.lvDetails.TabIndex = 3
        Me.lvDetails.UseCompatibleStateImageBehavior = False
        Me.lvDetails.View = System.Windows.Forms.View.Details
        '
        'chOrderNo
        '
        Me.chOrderNo.Text = "Order No"
        Me.chOrderNo.Width = 70
        '
        'chOrderDate
        '
        Me.chOrderDate.Text = "Order Date"
        Me.chOrderDate.Width = 70
        '
        'chSupplier
        '
        Me.chSupplier.Text = "Supplier"
        Me.chSupplier.Width = 0
        '
        'chItemCode
        '
        Me.chItemCode.Text = "Item Code"
        Me.chItemCode.Width = 80
        '
        'chItemName
        '
        Me.chItemName.Text = "Item Name"
        Me.chItemName.Width = 120
        '
        'chBuyingUnit
        '
        Me.chBuyingUnit.Text = "Unit"
        Me.chBuyingUnit.Width = 50
        '
        'chQty
        '
        Me.chQty.Text = "Ordered"
        Me.chQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chQty.Width = 75
        '
        'chQtyReceived
        '
        Me.chQtyReceived.Text = "Received"
        Me.chQtyReceived.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chQtyReceived.Width = 75
        '
        'chQtyOutstanding1
        '
        Me.chQtyOutstanding1.Text = "Outstanding"
        Me.chQtyOutstanding1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chQtyOutstanding1.Width = 75
        '
        'chQtyOutstanding2
        '
        Me.chQtyOutstanding2.Text = "Ent 1"
        Me.chQtyOutstanding2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chQtyOutstanding2.Width = 0
        '
        'chQtyOutstanding3
        '
        Me.chQtyOutstanding3.Text = ""
        Me.chQtyOutstanding3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chQtyOutstanding3.Width = 0
        '
        'chQtyOutstanding4
        '
        Me.chQtyOutstanding4.Text = "Ent 2"
        Me.chQtyOutstanding4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chQtyOutstanding4.Width = 0
        '
        'chComplete
        '
        Me.chComplete.Text = "Complete"
        Me.chComplete.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(4, 484)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(75, 23)
        Me.cmdSave.TabIndex = 4
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdClose
        '
        Me.cmdClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdClose.Location = New System.Drawing.Point(668, 484)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(75, 23)
        Me.cmdClose.TabIndex = 5
        Me.cmdClose.Text = "&Close"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'frmGoodsIn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdClose
        Me.ClientSize = New System.Drawing.Size(749, 510)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.lvDetails)
        Me.Controls.Add(Me.fraReceivingWarehouse)
        Me.Controls.Add(Me.fraSupplier)
        Me.Controls.Add(Me.fraAccountSelection)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmGoodsIn"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Goods In"
        Me.fraAccountSelection.ResumeLayout(False)
        Me.fraAccountSelection.PerformLayout()
        Me.fraSupplier.ResumeLayout(False)
        Me.fraSupplier.PerformLayout()
        Me.fraReceivingWarehouse.ResumeLayout(False)
        Me.fraReceivingWarehouse.PerformLayout()
        CType(Me.WarehousesLookup1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents fraAccountSelection As System.Windows.Forms.GroupBox
    Friend WithEvents fraSupplier As System.Windows.Forms.GroupBox
    Friend WithEvents fraReceivingWarehouse As System.Windows.Forms.GroupBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtName As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents txtNarrative As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents dtpDate As Sage.Common.Controls.DatePicker
    Friend WithEvents txtReference As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents lblNarrative As System.Windows.Forms.Label
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblReference As System.Windows.Forms.Label
    Friend WithEvents lblWarehouse As System.Windows.Forms.Label
    Friend WithEvents lvDetails As System.Windows.Forms.ListView
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents SupplierLookup1 As Sage.MMS.Controls.SupplierLookup
    Friend WithEvents chOrderNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents chSupplier As System.Windows.Forms.ColumnHeader
    Friend WithEvents chItemCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents chItemName As System.Windows.Forms.ColumnHeader
    Friend WithEvents chBuyingUnit As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQty As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyReceived As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyOutstanding1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyOutstanding2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyOutstanding3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyOutstanding4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chComplete As System.Windows.Forms.ColumnHeader
    Friend WithEvents chOrderDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents WarehousesLookup1 As Sage.MMS.Controls.WarehousesLookup
End Class
