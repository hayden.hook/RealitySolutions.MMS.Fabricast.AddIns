﻿Public Class RSBaseForm
    Private Sub RSBaseForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Sage.Accounting.Application.IsConnected Then
            Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(RealitySolutions.Licensing.Core.Client.Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

            If intLicense <= 0 Then
                MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
                Me.Close()
            ElseIf intLicense > 0 And intLicense <= 7 Then
                MsgBox(String.Format("Your license is almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
            ElseIf intLicense > 7 And intLicense <= 30 Then
                MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
            End If
        End If

        PictureBox1.Location = New Drawing.Point(Me.Width - 160, 7)
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Process.Start("https://realitysolutions.co.uk/")
    End Sub
End Class