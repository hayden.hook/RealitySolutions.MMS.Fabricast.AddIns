﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RSExceptionForm
    Inherits RSBaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RSExceptionForm))
        Me.OKButton = New Sage.Common.Controls.Button()
        Me.CopyButton = New Sage.Common.Controls.Button()
        Me.SaveButton = New Sage.Common.Controls.Button()
        Me.EmailButton = New Sage.Common.Controls.Button()
        Me.LogoPanel = New Sage.Common.Controls.Panel()
        Me.LogoPictureBox = New Sage.Common.Controls.PictureBox()
        Me.HelpLabel = New System.Windows.Forms.Label()
        Me.HelpLabel2 = New System.Windows.Forms.Label()
        Me.ExceptionPanel = New Sage.Common.Controls.Panel()
        Me.ExceptionMaskedTextBox = New Sage.Common.Controls.MaskedTextBox()
        CType(Me.buttonPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.buttonPanel.SuspendLayout()
        Me.mainPanel.SuspendLayout()
        Me.panelContent.SuspendLayout()
        Me.LogoPanel.SuspendLayout()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ExceptionPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'buttonPanel
        '
        Me.buttonPanel.Controls.Add(Me.EmailButton)
        Me.buttonPanel.Controls.Add(Me.SaveButton)
        Me.buttonPanel.Controls.Add(Me.CopyButton)
        Me.buttonPanel.Controls.Add(Me.OKButton)
        Me.buttonPanel.Location = New System.Drawing.Point(0, 266)
        Me.buttonPanel.Padding = New System.Windows.Forms.Padding(10, 12, 10, 12)
        Me.buttonPanel.Size = New System.Drawing.Size(513, 50)
        Me.buttonPanel.UseThemes = True
        '
        'mainPanel
        '
        Me.mainPanel.Controls.Add(Me.ExceptionPanel)
        Me.mainPanel.Controls.Add(Me.LogoPanel)
        Me.mainPanel.Size = New System.Drawing.Size(513, 254)
        '
        'panelContent
        '
        Me.panelContent.Location = New System.Drawing.Point(5, 35)
        Me.panelContent.Size = New System.Drawing.Size(513, 316)
        '
        'OKButton
        '
        Me.OKButton.ControlAlignment = Sage.Common.Controls.ControlAlignmentInContainer.Right
        Me.OKButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OKButton.Location = New System.Drawing.Point(438, 12)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.Size = New System.Drawing.Size(65, 26)
        Me.OKButton.TabIndex = 1
        Me.OKButton.Text = "OK"
        Me.OKButton.UseVisualStyleBackColor = True
        '
        'CopyButton
        '
        Me.CopyButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CopyButton.Location = New System.Drawing.Point(188, 12)
        Me.CopyButton.Name = "CopyButton"
        Me.CopyButton.Size = New System.Drawing.Size(102, 26)
        Me.CopyButton.TabIndex = 2
        Me.CopyButton.Text = "Copy to Clipboard"
        Me.CopyButton.UseVisualStyleBackColor = True
        '
        'SaveButton
        '
        Me.SaveButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SaveButton.Location = New System.Drawing.Point(104, 12)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(74, 26)
        Me.SaveButton.TabIndex = 3
        Me.SaveButton.Text = "Save as File"
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'EmailButton
        '
        Me.EmailButton.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EmailButton.Location = New System.Drawing.Point(10, 12)
        Me.EmailButton.Name = "EmailButton"
        Me.EmailButton.Size = New System.Drawing.Size(84, 26)
        Me.EmailButton.TabIndex = 4
        Me.EmailButton.Text = "Send as Email"
        Me.EmailButton.UseVisualStyleBackColor = True
        '
        'LogoPanel
        '
        Me.LogoPanel.BackColor = System.Drawing.SystemColors.Control
        Me.LogoPanel.Controls.Add(Me.LogoPictureBox)
        Me.LogoPanel.Controls.Add(Me.HelpLabel)
        Me.LogoPanel.Controls.Add(Me.HelpLabel2)
        Me.LogoPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.LogoPanel.Location = New System.Drawing.Point(10, 0)
        Me.LogoPanel.Name = "LogoPanel"
        Me.LogoPanel.Size = New System.Drawing.Size(493, 70)
        Me.LogoPanel.TabIndex = 0
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LogoPictureBox.Image = CType(resources.GetObject("LogoPictureBox.Image"), System.Drawing.Image)
        Me.LogoPictureBox.Location = New System.Drawing.Point(305, 3)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(185, 64)
        Me.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.LogoPictureBox.TabIndex = 0
        Me.LogoPictureBox.TabStop = False
        '
        'HelpLabel
        '
        Me.HelpLabel.AutoSize = True
        Me.HelpLabel.BackColor = System.Drawing.Color.Transparent
        Me.HelpLabel.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HelpLabel.ForeColor = System.Drawing.Color.Black
        Me.HelpLabel.Location = New System.Drawing.Point(3, 15)
        Me.HelpLabel.Name = "HelpLabel"
        Me.HelpLabel.Size = New System.Drawing.Size(222, 15)
        Me.HelpLabel.TabIndex = 1
        Me.HelpLabel.Text = "Sage 200 has encountered a problem."
        '
        'HelpLabel2
        '
        Me.HelpLabel2.AutoSize = True
        Me.HelpLabel2.BackColor = System.Drawing.Color.Transparent
        Me.HelpLabel2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HelpLabel2.ForeColor = System.Drawing.Color.Black
        Me.HelpLabel2.Location = New System.Drawing.Point(3, 31)
        Me.HelpLabel2.Name = "HelpLabel2"
        Me.HelpLabel2.Size = New System.Drawing.Size(110, 15)
        Me.HelpLabel2.TabIndex = 0
        Me.HelpLabel2.Text = "Please see below."
        '
        'ExceptionPanel
        '
        Me.ExceptionPanel.Controls.Add(Me.ExceptionMaskedTextBox)
        Me.ExceptionPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ExceptionPanel.Location = New System.Drawing.Point(10, 70)
        Me.ExceptionPanel.Name = "ExceptionPanel"
        Me.ExceptionPanel.Padding = New System.Windows.Forms.Padding(0, 10, 0, 0)
        Me.ExceptionPanel.Size = New System.Drawing.Size(493, 173)
        Me.ExceptionPanel.TabIndex = 1
        '
        'ExceptionMaskedTextBox
        '
        Me.ExceptionMaskedTextBox.BackColor = System.Drawing.SystemColors.Window
        Me.ExceptionMaskedTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ExceptionMaskedTextBox.Enabled = False
        Me.ExceptionMaskedTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ExceptionMaskedTextBox.Location = New System.Drawing.Point(0, 10)
        Me.ExceptionMaskedTextBox.Multiline = True
        Me.ExceptionMaskedTextBox.Name = "ExceptionMaskedTextBox"
        Me.ExceptionMaskedTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.ExceptionMaskedTextBox.Size = New System.Drawing.Size(493, 163)
        Me.ExceptionMaskedTextBox.TabIndex = 0
        '
        'RSExceptionForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(523, 356)
        Me.Name = "RSExceptionForm"
        Me.Text = "Handled Exception"
        CType(Me.buttonPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.buttonPanel.ResumeLayout(False)
        Me.mainPanel.ResumeLayout(False)
        Me.panelContent.ResumeLayout(False)
        Me.LogoPanel.ResumeLayout(False)
        Me.LogoPanel.PerformLayout()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ExceptionPanel.ResumeLayout(False)
        Me.ExceptionPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EmailButton As Sage.Common.Controls.Button
    Friend WithEvents SaveButton As Sage.Common.Controls.Button
    Friend WithEvents CopyButton As Sage.Common.Controls.Button
    Friend WithEvents OKButton As Sage.Common.Controls.Button
    Friend WithEvents ExceptionPanel As Sage.Common.Controls.Panel
    Friend WithEvents LogoPanel As Sage.Common.Controls.Panel
    Friend WithEvents LogoPictureBox As Sage.Common.Controls.PictureBox
    Friend WithEvents HelpLabel2 As System.Windows.Forms.Label
    Friend WithEvents ExceptionMaskedTextBox As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents HelpLabel As System.Windows.Forms.Label
End Class
