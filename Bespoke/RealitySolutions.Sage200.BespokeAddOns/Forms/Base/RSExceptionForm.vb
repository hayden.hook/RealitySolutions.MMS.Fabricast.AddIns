﻿Imports System.Windows.Forms

Public Class RSExceptionForm

    Public Property Exception As Exception

    Private Sub RSExceptionForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ExceptionMaskedTextBox.Text = Exception.ToString()
        OKButton.Focus()
    End Sub

    Private Sub EmailButton_Click(sender As Object, e As EventArgs) Handles EmailButton.Click
        Dim oMailMessage As New Net.Mail.MailMessage(My.Settings.FromAddress, My.Settings.ToAddress)
        oMailMessage.CC.Add(My.Settings.CcAddress)
        oMailMessage.Subject = "Sage 200 Exception, see details below"
        oMailMessage.Body = String.Format("Company: {0}" & vbCrLf & "Username: {1}" & vbCrLf & vbCrLf & " {2}", Sage.Accounting.Application.StaticActiveCompanyName,
                                          Sage.Accounting.Application.ActiveUserName, ExceptionMaskedTextBox.Text)

        Dim oSMTPClient As New Net.Mail.SmtpClient(My.Settings.EmailServer, My.Settings.EmailPort)
        oSMTPClient.Send(oMailMessage)

        MsgBox("Exception report has been sent to Reality Solutions", MsgBoxStyle.Information, "Send Error Report")
    End Sub

    Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
        Using oSaveDialog As New System.Windows.Forms.SaveFileDialog()
            oSaveDialog.Filter = "txt files (*.txt)|*.txt"

            If oSaveDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                My.Computer.FileSystem.WriteAllText(oSaveDialog.FileName, ExceptionMaskedTextBox.Text, False)
            End If
        End Using
    End Sub

    Private Sub CopyButton_Click(sender As Object, e As EventArgs) Handles CopyButton.Click
        Clipboard.Clear()
        Clipboard.SetText(ExceptionMaskedTextBox.Text)
    End Sub

    Private Sub OKButton_Click(sender As Object, e As EventArgs) Handles OKButton.Click
        DialogResult = System.Windows.Forms.DialogResult.OK
    End Sub

End Class

