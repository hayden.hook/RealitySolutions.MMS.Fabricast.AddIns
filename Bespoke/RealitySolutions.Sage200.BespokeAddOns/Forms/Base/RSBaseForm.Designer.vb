﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RSBaseForm
    Inherits Sage.MMS.BaseForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(RSBaseForm))
        Me.PictureBox1 = New Sage.Common.Controls.PictureBox()
        CType(Me.buttonPanel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelContent.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'buttonPanel
        '
        Me.buttonPanel.Location = New System.Drawing.Point(0, 347)
        Me.buttonPanel.Padding = New System.Windows.Forms.Padding(10, 11, 10, 11)
        Me.buttonPanel.Size = New System.Drawing.Size(582, 48)
        Me.buttonPanel.UseThemes = True
        '
        'mainPanel
        '
        Me.mainPanel.Location = New System.Drawing.Point(0, 11)
        Me.mainPanel.Padding = New System.Windows.Forms.Padding(10, 0, 10, 11)
        Me.mainPanel.Size = New System.Drawing.Size(582, 335)
        '
        'panelContent
        '
        Me.panelContent.Location = New System.Drawing.Point(5, 77)
        Me.panelContent.Padding = New System.Windows.Forms.Padding(0, 11, 0, 0)
        Me.panelContent.Size = New System.Drawing.Size(582, 395)
        '

        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = My.Resources.Resources.RSLogo_WhiteBlack
        Me.PictureBox1.Location = New System.Drawing.Point(417, 4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(98, 22)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'RSBaseForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(592, 477)
        Me.Controls.Add(Me.PictureBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "RSBaseForm"
        Me.Text = "RSBaseForm"
        Me.Controls.SetChildIndex(Me.PictureBox1, 0)
        CType(Me.buttonPanel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelContent.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents PictureBox1 As Sage.Common.Controls.PictureBox
End Class
