﻿Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports RealitySolutions.Licensing.Core.Client

Public Class frmSOPPrintPickingLists

#Region " SUBROUTINE: frmSOPPrintPickingLists_FormClosing() "
    Private Sub frmSOPPrintPickingLists_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not sqlConnToSage Is Nothing Then
            If sqlConnToSage.State = ConnectionState.Open Then
                sqlConnToSage.Close()
            End If
        End If
    End Sub
#End Region
#Region " SUBROUTINE: frmSOPPrintPickingLists_Load() "
    Private Sub frmSOPPrintPickingLists_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        Try
            ' Connect to Sage
            sqlConnToSage = New SqlConnection(oApplication.ActiveCompany.ConnectString)

            If sqlConnToSage.State = ConnectionState.Closed Then
                sqlConnToSage.Open()
            End If

        Catch ex As Exception
            MsgBox("An error has occurred while connecting to Sage 200." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region


#Region " SUBROUTINE: cmdDisplay_Click() "
    Private Sub cmdDisplay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDisplay.Click
        Try
            Dim dtTEMP As DataTable = ExecuteQuery("SELECT DISTINCT * FROM qrySOPPrintPickingLists " _
                                                 & "WHERE 1 = 1" & IIf(dtpOrderDate.Checked = True, " AND (DocumentDate >= '" & Format(dtpOrderDate.Value, "yyyy/MM/dd 00:00:00") & "' AND DocumentDate < '" & Format(dtpOrderDate.Value.AddDays(1.0#), "yyyy/MM/dd 00:00:00") & "')", "").ToString() & IIf(dtpConfirmedDelDate.Checked = True, " AND (PromisedDeliveryDate >= '" & Format(dtpConfirmedDelDate.Value, "yyyy/MM/dd 00:00:00") & "' AND PromisedDeliveryDate < '" & Format(dtpConfirmedDelDate.Value.AddDays(1.0#), "yyyy/MM/dd 00:00:00") & "')", "").ToString() & " " _
                                                 & "ORDER BY DocumentNo DESC", sqlConnToSage)
            Dim rowEach As DataRow = Nothing

            Dim lvItem As ListViewItem = Nothing


            Dim decTotal As Decimal = 0D

            lvOrders.BeginUpdate()
            lvOrders.Items.Clear()

            For Each rowEach In dtTEMP.Rows
                lvItem = lvOrders.Items.Add(rowEach.Item("DocumentNo").ToString())

                With lvItem.SubItems
                    .Add(Format(CDate(rowEach.Item("DocumentDate")), "dd/MM/yyyy"))
                    .Add(rowEach.Item("CustomerAccountNumber").ToString())
                    .Add(rowEach.Item("CustomerAccountName").ToString())
                    .Add(rowEach.Item("CustomerDocumentNo").ToString())
                    .Add(Format(CDec(rowEach.Item("TotalGrossValue")), "0.00"))

                    decTotal += CDec(rowEach.Item("TotalGrossValue"))
                End With
            Next

            lvOrders.EndUpdate()


            txtNoOfOrders.Text = Format(dtTEMP.Rows.Count, "0")
            txtTotalValue.Text = Format(decTotal, "0.00")
            Exit Sub

        Catch ex As Exception
            MsgBox("An error has occurred while displaying the SOPs for picking." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region

#Region " SUBROUTINE: cmdPrint_Click() "
    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
        If lvOrders.SelectedItems.Count = 0 Then
            MsgBox("Please select at least one order to print the picking list for.", MsgBoxStyle.Exclamation, "Nothing Selected")

            lvOrders.Focus()
            Exit Sub
        End If


        Try
            Dim lvItem As ListViewItem = Nothing


            Dim oCoordinator As New Sage.Accounting.SOP.SOPPrintPickingListCoordinator

            oCoordinator.ClearCriteria()
            oCoordinator.PrintOnlyPrevUnprinted = True
            oCoordinator.PrintPickingLists()

            If oCoordinator.PickingListItems.IsEmpty = False Then
                Dim oPickReport As L100REPORTSLib.Report = Sage.Accounting.Application.ReportingApplication.CreateReport()
                Dim oPickRepAppData As L100REPORTSLib.RepAppData = oPickReport.RepAppData

                Dim oPickCriteria As L100REPORTSLib.Criteria = oPickReport.Criteria
                Dim oPickCriterion As L100REPORTSLib.Criterion = Nothing


                Dim oSOPOrders As New Sage.Accounting.SOP.SOPOrders
                Dim oSOPOrder As Sage.Accounting.SOP.SOPOrder = Nothing


                ' Prepare the picking note
                oPickCriterion = oPickCriteria.Add()
                oPickCriterion.Type = L100REPORTSLib.CRITERIAENUM.CRITERIA_SOPOrderReturn_OrderDocumentNumber

                oPickCriterion = oPickCriteria.Add()
                oPickCriterion.Type = L100REPORTSLib.CRITERIAENUM.CRITERIA_SalesLedger_AccountNumber

                oPickCriterion = oPickCriteria.Add()
                oPickCriterion.Type = L100REPORTSLib.CRITERIAENUM.CRITERIA_SOPAllocationLine_PickingListPrintStatusID


                If oCoordinator.PrintOnlyPrevUnprinted Then
                    oPickCriterion.Low = Convert.ToDouble(0)
                    oPickCriterion.High = Convert.ToDouble(0)
                End If


                oPickCriterion = oPickCriteria.Add()
                oPickCriterion.Type = L100REPORTSLib.CRITERIAENUM.CRITERIA_SOPOrderReturn_DocumentDate

                oPickCriterion = oPickCriteria.Add()
                oPickCriterion.Type = L100REPORTSLib.CRITERIAENUM.CRITERIA_SOPOrderReturnLine_PromisedDeliveryDate

                oPickCriterion = oPickCriteria.Add()
                oPickCriterion.Type = L100REPORTSLib.CRITERIAENUM.CRITERIA_Warehouse_WarehouseName

                oPickCriterion = oPickCriteria.Add()
                oPickCriterion.Type = L100REPORTSLib.CRITERIAENUM.CRITERIA_SOPAllocationLine_ItemCode

                oPickReport.RepAppData.KeyValues.KeyType = L100REPORTSLib.KEYTYPEENUM.KEYTYPE_SOPOrderReturn_SOPOrderReturnID



                ' Now pick the items
                Dim oItems As Sage.Accounting.SOP.SOPPrintPickingListItems = oCoordinator.PickingListItems

                Dim intPickCount As Integer = 0

                For Each PickingListItem As Sage.Accounting.SOP.SOPPrintPickingListItem In oItems

                    If HasOrderBeenSelected(PickingListItem.DocumentNo) = True Then
                        Dim oDummy As Object = CType(PickingListItem.OrderID, Double)

                        Dim oConcurrencyLockKey As New Sage.Accounting.Common.ConcurrencyLockKey("SOPOrder", PickingListItem.OrderID.ToString())
                        Dim oConcurrencyLock As Sage.Accounting.Common.ConcurrencyLock = oCoordinator.LockingCoordinator.CreateConcurrencyLock(oConcurrencyLockKey)

                        With oConcurrencyLock
                            .AddFieldValue(PickingListItem.DocumentNo)
                            .AddFieldValue(PickingListItem.DocumentDate.ToShortDateString())
                            .AddFieldValue(PickingListItem.CustomerReference)
                            .AddFieldValue(PickingListItem.CustomerName)
                            .AddFieldValue(PickingListItem.DocumentValue.ToString())
                        End With


                        ' Add this item to the reports
                        oPickReport.RepAppData.KeyValues.Add(oDummy)

                        oConcurrencyLock.Dispose()
                        oConcurrencyLock = Nothing


                        intPickCount += 1
                    End If
                Next

                ' NEW: Make sure we have something!
                If intPickCount = 0 Then
                    Throw New Exception("There doesn't appear to be any picked items on the report, cannot continue.")
                End If


                ' Print!
                'oPickReport.OutputMode = L100REPORTSLib.OutputMode.omPreview
                oPickReport.FileName = "SOP\Picking List.srt"
                oPickReport.Run()


                ' Dispose objects  
                oPickReport = Nothing
                oPickRepAppData = Nothing
                oPickCriteria = Nothing
                oPickCriterion = Nothing


                oItems.Dispose()
                oItems = Nothing

                ' Set the picking list printed flag  
                oCoordinator.UpdatePrintStatus()
            End If


            ' NEW: Refresh the list
            cmdDisplay.PerformClick()
            Exit Sub

        Catch ex As Exception
            MsgBox("An error has occurred while printing the picking lists." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region

#Region " SUBROUTINE: cmdPrintAll_Click() "
    Private Sub cmdPrintAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrintAll.Click
        cmdSelectAll.PerformClick()
        cmdPrint.PerformClick()
    End Sub
#End Region

#Region " SUBROUTINE: cmdSelectAll_Click() "
    Private Sub cmdSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSelectAll.Click
        Dim lvItem As ListViewItem = Nothing

        For Each lvItem In lvOrders.Items
            lvItem.Selected = True
        Next
    End Sub
#End Region

#Region " SUBROUTINE: cmdDeselectAll_Click() "
    Private Sub cmdDeselectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDeselectAll.Click
        Dim lvItem As ListViewItem = Nothing

        For Each lvItem In lvOrders.Items
            lvItem.Selected = False
        Next
    End Sub
#End Region

#Region " SUBROUTINE: cmdCancel_Click() "
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub
#End Region


#Region " FUNCTION: HasOrderBeenSelected() "
    Private Function HasOrderBeenSelected(ByVal strDocumentNo As String) As Boolean
        For Each lvItem As ListViewItem In lvOrders.SelectedItems
            If lvItem.Text = strDocumentNo Then
                Return True
            End If
        Next

        Return False
    End Function
#End Region

End Class