Option Explicit On
Option Strict On

Imports System
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports RealitySolutions.Licensing.Core.Client

Public Class frmGoodsOutPopup
    Private lstItem As ListViewItem = Nothing

    Private bolRandomLength As Boolean = False

    Private decMultiplier As Decimal = 1D

#Region " PROPERTY: ItemSelected() "
    Public Property ItemSelected() As ListViewItem
        Get
            Return lstItem
        End Get
        Set(ByVal value As ListViewItem)
            lstItem = value
        End Set
    End Property
#End Region


#Region " SUBROUTINE: frmPopup_Closing() "
    Private Sub frmPopup_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

    End Sub
#End Region
#Region " SUBROUTINE: frmPopup_Load() "
    Private Sub frmPopup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        ' Load the details in from the currently selected list item
        Dim sqlConn As SqlConnection = Nothing

        Try
            sqlConn = ReturnDirectSQLConnection()
            SetupDirectSQLConnection()


            Dim sopLine As Sage.Accounting.SOP.SOPOrderReturnLine = Nothing

            With lstItem
                sopLine = DirectCast(.Tag, Sage.Accounting.SOP.SOPOrderReturnLine)

                ' NEW: Check the unit if it's "random length" and if so, change it to metres
                If .SubItems(4).Text = "Random Length" Then
                    bolRandomLength = True



                    Dim sopStdLine As Sage.Accounting.SOP.SOPStandardItemLine = DirectCast(sopLine, Sage.Accounting.SOP.SOPStandardItemLine)

                    Dim oStockItemUnits As New Sage.Accounting.Stock.StockItemUnits
                    Dim oStockItemUnit As Sage.Accounting.Stock.StockItemUnit = Nothing

                    Dim oUnits As New Sage.Accounting.Stock.Units
                    Dim oUnit As Sage.Accounting.Stock.Unit = Nothing


                    Dim oMetresUnit As Sage.Accounting.Stock.Unit = Nothing
                    Dim oRandomLengthUnit As Sage.Accounting.Stock.Unit = Nothing


                    oUnits = New Sage.Accounting.Stock.Units
                    oUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.Unit.FIELD_NAME, "Random Length"))
                    oUnits.Find()

                    oRandomLengthUnit = oUnits.First


                    oUnits = New Sage.Accounting.Stock.Units
                    oUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.Unit.FIELD_NAME, "Metres"))
                    oUnits.Find()

                    oMetresUnit = oUnits.First



                    Dim oMetresSIU As Sage.Accounting.Stock.StockItemUnit = Nothing
                    Dim oRandomLengthSIU As Sage.Accounting.Stock.StockItemUnit = Nothing

                    Dim oSIs As New Sage.Accounting.Stock.StockItems
                    Dim oSI As Sage.Accounting.Stock.StockItem = oSIs(sopLine.ItemCode)

                    oStockItemUnits = New Sage.Accounting.Stock.StockItemUnits
                    oStockItemUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_UNITOBJECT, oMetresUnit))
                    oStockItemUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_ITEMOBJECT, oSI))
                    oStockItemUnits.Find()
                    oMetresSIU = oStockItemUnits.First

                    oStockItemUnits = New Sage.Accounting.Stock.StockItemUnits
                    oStockItemUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_UNITOBJECT, oRandomLengthUnit))
                    oStockItemUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_ITEMOBJECT, oSI))
                    oStockItemUnits.Find()
                    oRandomLengthSIU = oStockItemUnits.First


                    decMultiplier = DanRound(Math.Max(oRandomLengthSIU.MultipleOfBaseUnit, 1D) / Math.Max(oMetresSIU.MultipleOfBaseUnit, 1D), "0.0000000") + 0.0000001D


                    ' Forcefully change the unit via SQL
                    StartTransaction()


                    Dim sqlComm As SqlCommand = Nothing


                    sqlComm = New SqlCommand("UPDATE SOPStandardItemLink SET SellingUnitID = @SellingUnitID WHERE SOPOrderReturnLineID = @SOPOrderReturnLineID", sqlConnToSage)

                    With sqlComm.Parameters
                        .AddWithValue("@SellingUnitID", CLng(oMetresSIU.StockItemUnit))
                        .AddWithValue("@SOPOrderReturnLineID", CLng(sopLine.SOPOrderReturnLine))
                    End With

                    If sqlComm.ExecuteNonQuery() <> 1 Then
                        Throw New Exception("Unable to locate the SOP standard item to update the stock item unit.")
                    End If

                    sqlComm.Dispose()
                    sqlComm = Nothing


                    sqlComm = New SqlCommand("UPDATE SOPOrderReturnLine SET SellingUnitDescription = @SellingUnitDescription, SellingMultOfStockUnit = @SellingMultOfStockUnit, LineQuantity = @Conversion * LineQuantity WHERE SOPOrderReturnLineID = @SOPOrderReturnLineID", sqlConnToSage)

                    With sqlComm.Parameters
                        .AddWithValue("@SellingUnitDescription", "Metres")
                        .AddWithValue("@SellingMultOfStockUnit", oMetresSIU.MultipleOfBaseUnit) ' oStockItemUnit.MultipleOfBaseUni)
                        .AddWithValue("@Conversion", decMultiplier)
                        .AddWithValue("@SOPOrderReturnLineID", CLng(sopLine.SOPOrderReturnLine))
                    End With

                    If sqlComm.ExecuteNonQuery() <> 1 Then
                        Throw New Exception("Unable to locate the SOP return line to update the stock item unit description.")
                    End If

                    sqlComm.Dispose()
                    sqlComm = Nothing

                    CommitTransaction()


                    ' Now we need to update the tag to reflect the line changes
                    .Tag = sopLine

                    .SubItems(4).Text = "Metres"
                    .SubItems(9).Text = Format(oMetresSIU.MultipleOfBaseUnit * CDec(.SubItems(7).Text), "0.00000")
                End If


                ' Move on
                txtOrderNumber.Text = .SubItems(0).Text
                txtCustomerName.Text = .SubItems(1).Text
                txtItemCode.Text = .SubItems(2).Text
                txtItemName.Text = .SubItems(3).Text
                txtSellingUnit.Text = .SubItems(4).Text
                txtQtyOrdered.Text = Format(decMultiplier * CDec(.SubItems(5).Text), "0.00000")
                txtQtyDispatched.Text = .SubItems(6).Text

                txtQtyRequired1.Text = Format(decMultiplier * CDec(.SubItems(7).Text), "0.00000")
                txtQtyRequired2.Text = .SubItems(8).Text

                ' This is a dirty fix, but it'll do
                If CDec(txtQtyRequired2.Text) = 0D Then
                    txtQtyRequired2.Text = txtQtyRequired1.Text
                End If

                txtQtyRequired3.Text = Format(decMultiplier * CDec(.SubItems(9).Text), "0.00000")
                txtQtyRequired4.Text = .SubItems(10).Text

                Select Case .SubItems(11).Text
                    Case "Y"
                        chkCompleted.Checked = True

                    Case "N"
                        chkCompleted.Checked = False
                End Select

                txtMaterialsRef.Text = .SubItems(12).Text
            End With


            ' Unit labels
            Dim oStockItems As New Sage.Accounting.Stock.StockItems
            Dim oStockItem As Sage.Accounting.Stock.StockItem = oStockItems(txtItemCode.Text)


            lblUnit1.Text = txtSellingUnit.Text
            lblUnit2.Text = oStockItem.BaseUnitName


            If lblUnit1.Text = lblUnit2.Text Then
                txtQtyRequired1.Enabled = False
                txtQtyRequired2.Enabled = False
            End If


            ' Dispose
            oStockItems.Dispose()
            oStockItem.Dispose()

            oStockItems = Nothing
            oStockItem = Nothing

        Catch ex As Exception
            RollbackTransaction()
            cmdOK.Enabled = False

            MsgBox("An error has occurred while loading the order line details." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")

        Finally
            If Not sqlConn Is Nothing Then
                sqlConn.Dispose()
                sqlConn = Nothing
            End If
        End Try
    End Sub
#End Region


#Region " SUBROUTINE: cmdOK_Click() "
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If CDec(txtQtyRequired2.Text) = 0.0# Then
            MsgBox("Please enter a valid amount.", MsgBoxStyle.Exclamation, "Invalid Amount")

            txtQtyRequired2.Focus()
            Exit Sub
        End If

        If CDec(txtQtyRequired4.Text) = 0.0# Then
            MsgBox("Please enter a valid amount.", MsgBoxStyle.Exclamation, "Invalid Amount")

            txtQtyRequired4.Focus()
            Exit Sub
        End If


        ' Put the information back again
        With lstItem
            .SubItems(8).Text = txtQtyRequired2.Text
            .SubItems(10).Text = txtQtyRequired4.Text
            .SubItems(12).Text = txtMaterialsRef.Text

            If CDec(txtQtyRequired4.Text) >= CDec(txtQtyRequired3.Text) Then
                ' Force completion
                chkCompleted.Checked = True
            End If


            If chkCompleted.Checked = True Then
                .SubItems(11).Text = "Y"

            Else
                .SubItems(11).Text = "N"
            End If
        End With


        '' NEW: Confirm the conversation factor if completed
        'If chkCompleted.Checked = True Then
        '    Dim decFactor As Decimal = CDec(txtQtyRequired4.Text) / CDec(txtQtyRequired2.Text)

        '    Dim sopLine As Sage.Accounting.SOP.SOPOrderReturnLine = DirectCast(lstItem.Tag, Sage.Accounting.SOP.SOPOrderReturnLine)

        '    Dim oStockItemUnits As New Sage.Accounting.Stock.StockItemUnits
        '    Dim oStockItemUnit As Sage.Accounting.Stock.StockItemUnit = Nothing

        '    Dim oStockCodes As New Sage.Accounting.Stock.StockItems
        '    Dim oStockCode As Sage.Accounting.Stock.StockItem = oStockCodes(sopLine.ItemCode)

        '    If oStockCode Is Nothing Then
        '        Throw New Exception("Failed to locate the stock code.")
        '    End If


        '    Dim oUnits As New Sage.Accounting.Stock.Units
        '    Dim oUnit As Sage.Accounting.Stock.Unit = Nothing

        '    oUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.Unit.FIELD_NAME, sopLine.SellingUnitDescription)) 'oStockCode.BaseUnitName
        '    oUnits.Find()

        '    oUnit = oUnits.First

        '    If oUnit Is Nothing Then
        '        Throw New Exception("Failed to locate the 'metres' unit.")
        '    End If

        '    oStockItemUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_ITEMDBKEY, oStockCode.Item))
        '    oStockItemUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_UNITDBKEY, oUnit.Unit))
        '    oStockItemUnits.Find()

        '    oStockItemUnit = oStockItemUnits.First

        '    If oStockItemUnit Is Nothing Then
        '        Throw New Exception("Failed to locate the stock item unit.")
        '    End If


        '    Dim frmX As New frmStockItemUnitFactor

        '    With frmX
        '        .Factor = decFactor
        '        .StockItemUnit = oStockItemUnit
        '        .SellingUnitName = sopLine.SellingUnitDescription

        '        .ShowDialog(Me)

        '        If .Factor <> 0D Then
        '            ' Update the line with the new factor
        '            lstItem.SubItems(9).Text = Format(.Factor, "0.00000")
        '        End If

        '        .Dispose()
        '    End With

        '    frmX = Nothing
        'End If

        Me.Close()
        Exit Sub
    End Sub
#End Region

#Region " SUBROUTINE: cmdCancel_Click() "
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        lstItem = Nothing

        Me.Close()
    End Sub
#End Region


#Region " SUBROUTINE: txtQtyRequired2_LostFocus() "
    Private Sub txtQtyRequired2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQtyRequired2.LostFocus
        If IsNumeric(txtQtyRequired2.Text) = False Then
            txtQtyRequired2.Text = "0.00000"

        Else
            If bolRandomLength = False Then

                If CDec(txtQtyRequired2.Text) > (CDec(txtQtyRequired1.Text) + (CDec(txtQtyRequired1.Text) * (ERROR_THRESHOLD / 100))) Then

                    If MsgBox("The qty entered is beyond the threshold, continue?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.No Then
                        txtQtyRequired2.Text = Format(CDec(txtQtyRequired1.Text), "0.00000")

                    Else
                        txtQtyRequired2.Text = Format(CDec(txtQtyRequired2.Text), "0.00000")
                    End If

                Else
                    txtQtyRequired2.Text = Format(CDec(txtQtyRequired2.Text), "0.00000")
                End If

            Else
                If CDec(txtQtyRequired2.Text) = 0D Then
                    ' Divide by zero

                Else
                    Dim decNormalUnit As Decimal = CDec(txtQtyRequired3.Text) / CDec(txtQtyRequired1.Text)
                    Dim decNewUnit As Decimal = CDec(txtQtyRequired4.Text) / CDec(txtQtyRequired2.Text)

                    If decNewUnit > decNormalUnit + (decNormalUnit * (ERROR_THRESHOLD / 100)) Then

                        If MsgBox("The qty entered is beyond the threshold, continue?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.No Then
                            txtQtyRequired2.Text = Format(CDec(txtQtyRequired1.Text), "0.00000")

                        Else
                            txtQtyRequired2.Text = Format(CDec(txtQtyRequired2.Text), "0.00000")
                        End If

                    Else
                        txtQtyRequired2.Text = Format(CDec(txtQtyRequired2.Text), "0.00000")
                    End If
                End If
            End If
        End If


        ' Check for negative
        If CDec(txtQtyRequired2.Text) < 0.0# Then
            txtQtyRequired2.Text = "0.00000"
        End If


        If lblUnit1.Text = lblUnit2.Text Then
            txtQtyRequired4.Text = txtQtyRequired2.Text
        End If
    End Sub
#End Region

#Region " SUBROUTINE: txtQtyRequired4_LostFocus() "
    Private Sub txtQtyRequired4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQtyRequired4.LostFocus
        If IsNumeric(txtQtyRequired4.Text) = False Then
            txtQtyRequired4.Text = "0.00000"

        Else
            If bolRandomLength = False Then

                If CDec(txtQtyRequired4.Text) > (CDec(txtQtyRequired3.Text) + (CDec(txtQtyRequired3.Text) * (ERROR_THRESHOLD / 100))) Then
                    If MsgBox("The qty entered is beyond the threshold, continue?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.No Then
                        txtQtyRequired4.Text = Format(CDec(txtQtyRequired3.Text), "0.00000")

                    Else
                        txtQtyRequired4.Text = Format(CDec(txtQtyRequired4.Text), "0.00000")
                    End If

                Else
                    txtQtyRequired4.Text = Format(CDec(txtQtyRequired4.Text), "0.00000")
                End If

            Else
                If CDec(txtQtyRequired4.Text) = 0D Then
                    ' Divide by zero

                Else

                    Dim decNormalUnit As Decimal = CDec(txtQtyRequired3.Text) / CDec(txtQtyRequired1.Text)
                    Dim decNewUnit As Decimal = CDec(txtQtyRequired4.Text) / CDec(txtQtyRequired2.Text)

                    If decNewUnit > decNormalUnit + (decNormalUnit * (ERROR_THRESHOLD / 100)) Then

                        If MsgBox("The qty entered is beyond the threshold, continue?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.No Then
                            txtQtyRequired4.Text = Format(CDec(txtQtyRequired3.Text), "0.00000")

                        Else
                            txtQtyRequired4.Text = Format(CDec(txtQtyRequired4.Text), "0.00000")
                        End If

                    Else
                        txtQtyRequired4.Text = Format(CDec(txtQtyRequired4.Text), "0.00000")
                    End If
                End If
            End If


            ' Check for negative
            If CDec(txtQtyRequired4.Text) < 0D Then
                txtQtyRequired4.Text = "0.00000"
            End If
        End If


        If lblUnit1.Text = lblUnit2.Text Then
            txtQtyRequired2.Text = txtQtyRequired4.Text
        End If
    End Sub
#End Region

End Class