Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports RealitySolutions.Licensing.Core.Client

Public Class frmTreeviewSearching
    Private dtProducts As New DataTable
    Private dcStockCode As New DataColumn("Stock Code", System.Type.GetType("System.String"))
    Private dcDescription As New DataColumn("Description", System.Type.GetType("System.String"))
    Private dcUnits As New DataColumn("Units", System.Type.GetType("System.String"))
    Private dcOnOrder As New DataColumn("On Order", System.Type.GetType("System.Decimal"))
    Private dcInStock As New DataColumn("In Stock", System.Type.GetType("System.Decimal"))
    Private dcAllocated As New DataColumn("Allocated", System.Type.GetType("System.Decimal"))
    Private dcFreeStock As New DataColumn("Free Stock", System.Type.GetType("System.Decimal"))


    Private strProductCode As String = ""

    Private bolAllowNodeSQL As Boolean = False


#Region " PROPERTY: StockCode() "
    Public ReadOnly Property StockCode() As String
        Get
            Return strProductCode
        End Get
    End Property
#End Region


#Region " SUBROUTINE: frmTreeviewSearching_Closing() "
    Private Sub frmTreeviewSearching_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

    End Sub
#End Region
#Region " SUBROUTINE: frmTreeviewSearching_Load() "
    Private Sub frmTreeviewSearching_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        Try
            bolAllowNodeSQL = False


            SetupDirectSQLConnection()


            With dtProducts
                With .Columns
                    .Add(dcStockCode)
                    .Add(dcDescription)
                    .Add(dcUnits)
                    .Add(dcOnOrder)
                    .Add(dcInStock)
                    .Add(dcAllocated)
                    .Add(dcFreeStock)
                End With
            End With



            ' Populate the treeview using the view
            Dim dtTEMP As DataTable = ExecuteQuery("SELECT Division, Material, Shape, Grade" & vbCrLf _
                                                 & "FROM qryPopulateTreeView" & vbCrLf _
                                                 & "ORDER BY Division, Material, Shape, Grade")
            Dim rowCurrent As DataRow = Nothing

            Dim trvDivision As TreeNode = Nothing
            Dim trvMaterial As TreeNode = Nothing
            Dim trvShape As TreeNode = Nothing
            Dim trvGrade As TreeNode = Nothing


            Dim strPreviousDivision As String = ""
            Dim strPreviousMaterial As String = ""
            Dim strPreviousShape As String = ""
            Dim strPreviousGrade As String = ""

            Dim strCurrentDivision As String = ""
            Dim strCurrentMaterial As String = ""
            Dim strCurrentShape As String = ""
            Dim strCurrentGrade As String = ""

            Dim strTEMP As String = ""


            tvProducts.BeginUpdate()
            tvProducts.Nodes.Clear()


            For Each rowCurrent In dtTEMP.Rows
                strCurrentDivision = IfNullReturnEmpty(rowCurrent.Item("Division"))
                strCurrentMaterial = IfNullReturnEmpty(rowCurrent.Item("Material"))
                strCurrentShape = IfNullReturnEmpty(rowCurrent.Item("Shape"))
                strCurrentGrade = IfNullReturnEmpty(rowCurrent.Item("Grade"))


                If strPreviousDivision <> strCurrentDivision Then
                    strTEMP = IfNullReturnEmpty(rowCurrent.Item("Division"))

                    If strTEMP <> "" Then
                        trvDivision = tvProducts.Nodes.Add(strTEMP)
                        trvDivision.Tag = strCurrentDivision
                    End If
                End If

                If strPreviousMaterial <> strCurrentMaterial Then
                    strTEMP = IfNullReturnEmpty(rowCurrent.Item("Material"))

                    If strTEMP <> "" Then
                        trvMaterial = trvDivision.Nodes.Add(strTEMP)
                        trvMaterial.Tag = strCurrentDivision & "^" & strCurrentMaterial
                    End If
                End If

                If (strPreviousDivision <> strCurrentDivision) Or (strPreviousMaterial <> strCurrentMaterial) Then
                    ' Force new records
                    strTEMP = IfNullReturnEmpty(rowCurrent.Item("Shape"))

                    If strTEMP <> "" Then
                        trvShape = trvMaterial.Nodes.Add(strTEMP)
                        trvShape.Tag = strCurrentDivision & "^" & strCurrentMaterial & "^" & strCurrentShape
                    End If


                    strTEMP = IfNullReturnEmpty(rowCurrent.Item("Grade"))

                    If strTEMP <> "" Then
                        trvGrade = trvShape.Nodes.Add(strTEMP)
                        trvGrade.Tag = strCurrentDivision & "^" & strCurrentMaterial & "^" & strCurrentShape & "^" & strCurrentGrade
                    End If

                Else
                    If strPreviousShape <> strCurrentShape Then
                        strTEMP = IfNullReturnEmpty(rowCurrent.Item("Shape"))

                        If strTEMP <> "" Then
                            trvShape = trvMaterial.Nodes.Add(strTEMP)
                            trvShape.Tag = strCurrentDivision & "^" & strCurrentMaterial & "^" & strCurrentShape
                        End If
                    End If

                    If strPreviousGrade <> strCurrentGrade Then
                        strTEMP = IfNullReturnEmpty(rowCurrent.Item("Grade"))

                        If strTEMP <> "" Then
                            trvGrade = trvShape.Nodes.Add(strTEMP)
                            trvGrade.Tag = strCurrentDivision & "^" & strCurrentMaterial & "^" & strCurrentShape & "^" & strCurrentGrade
                        End If
                    End If
                End If


                strPreviousDivision = strCurrentDivision
                strPreviousMaterial = strCurrentMaterial
                strPreviousShape = strCurrentShape
                strPreviousGrade = strCurrentGrade
            Next

            tvProducts.EndUpdate()


            ' NEW: Have we got a previous node to reload?
            bolAllowNodeSQL = True


            Dim dtNode As DataTable = ExecuteQuery("SELECT LastNode FROM tblTreeviewLastNode WHERE Username = '" & Sage.Accounting.Application.ActiveUser.UserName & "'", sqlConnToSage)

            If dtNode.Rows.Count > 0 Then
                For Each rowNode As DataRow In dtNode.Rows
                    Dim strNode As String = rowNode.Item("LastNode").ToString()

                    FindTagInNodes(strNode, tvProducts.Nodes)
                    Exit For ' Only expecting one
                Next

            Else
                If tvProducts.Nodes.Count > 0 Then
                    tvProducts.SelectedNode = tvProducts.Nodes(0)
                    tvProducts.Nodes(0).EnsureVisible()
                End If
            End If

            dtNode.Dispose()
            dtNode = Nothing

        Catch ex As Exception
            MsgBox("An error has occurred while loading the form." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region


#Region " SUBROUTINE: cmdSearch_Click() "
    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        If (txtSearchCode.Text = "") And (txtSearchDescription.Text = "") Then
            MsgBox("An enter either a stock code or a description to search on.", MsgBoxStyle.Exclamation, "No Criteria")

            txtSearchCode.Focus()
            Exit Sub

        Else
            Dim strSearchCode As String = txtSearchCode.Text.Trim()
            Dim strSearchDesc As String = txtSearchDescription.Text.Trim()

            Dim strSQL As String = "SELECT * FROM qryTreeviewSearchGroupedByWarehouse " _
                                 & "WHERE " & IIf(strSearchCode = "", "", "Code LIKE '%" & strSearchCode & "%' AND ").ToString() _
                                 & IIf(strSearchDesc = "", "", "Name LIKE '%" & strSearchDesc & "%' AND ").ToString() _
                                 & "1 = 1"

            Dim dtTEMP As DataTable = ExecuteQuery(strSQL)
            Dim rowCurrent As DataRow = Nothing


            ' Put the results in the listview
            dtProducts.Rows.Clear()


            Dim lvItem As ListViewItem = Nothing

            For Each rowCurrent In dtTEMP.Rows
                Dim rowNew As DataRow = dtProducts.NewRow()

                With rowNew
                    .Item("Stock Code") = rowCurrent.Item("Code")
                    .Item("Description") = rowCurrent.Item("Name")
                    .Item("Units") = rowCurrent.Item("StockUnitName")
                    .Item("On Order") = rowCurrent.Item("OnOrder")
                    .Item("In Stock") = rowCurrent.Item("InStock")
                    .Item("Allocated") = rowCurrent.Item("Allocated")
                    .Item("Free Stock") = rowCurrent.Item("FreeStock")
                End With

                dtProducts.Rows.Add(rowNew)
            Next

            lblRecordsFound.Text = dtTEMP.Rows.Count & " records found"


            With dgProducts
                .DataSource = dtProducts

                With .Columns("Stock Code")
                    .ReadOnly = True
                End With

                With .Columns("Description")
                    .ReadOnly = True
                End With

                With .Columns("Units")
                    .ReadOnly = True
                End With

                With .Columns("On Order")
                    .ReadOnly = True

                    With .DefaultCellStyle
                        .Alignment = DataGridViewContentAlignment.MiddleRight
                        .Format = "0.00"
                    End With
                End With

                With .Columns("In Stock")
                    .ReadOnly = True

                    With .DefaultCellStyle
                        .Alignment = DataGridViewContentAlignment.MiddleRight
                        .Format = "0.00"
                    End With
                End With

                With .Columns("Allocated")
                    .ReadOnly = True

                    With .DefaultCellStyle
                        .Alignment = DataGridViewContentAlignment.MiddleRight
                        .Format = "0.00"
                    End With
                End With

                With .Columns("Free Stock")
                    .ReadOnly = True

                    With .DefaultCellStyle
                        .Alignment = DataGridViewContentAlignment.MiddleRight
                        .Format = "0.00"
                    End With
                End With
            End With
        End If
    End Sub
#End Region

#Region " SUBROUTINE: cmdOK_Click() "
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If dgProducts.SelectedRows.Count = 1 Then
            strProductCode = dgProducts.SelectedRows(0).Cells("Stock Code").Value.ToString()
            Me.Close()

        Else
            MsgBox("Please select one product in the list.", MsgBoxStyle.Exclamation, "No Product Selected")
            Exit Sub
        End If
    End Sub
#End Region

#Region " SUBROUTINE: cmdCancel_Click() "
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        strProductCode = ""
        Me.Close()
    End Sub
#End Region


#Region " SUBROUTINE: tvProducts_AfterSelect() "
    Private Sub tvProducts_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvProducts.AfterSelect
        Try
            If Not (e.Node Is Nothing) Then
                ' Populate the list with the results
                Dim strSplity() As String = Split(e.Node.Tag.ToString(), "^", , CompareMethod.Text)

                If strSplity.Length > 0 Then
                    Dim dtTEMP As DataTable = Nothing
                    Dim rowCurrent As DataRow = Nothing

                    Select Case strSplity.Length
                        Case 1
                            ' Just the division
                            dtTEMP = ExecuteQuery("SELECT * " _
                                                & "FROM qryTreeviewSearchGroupedByWarehouse " _
                                                & "WHERE Division = '" & strSplity(0) & "'")

                        Case 2
                            ' Division and material
                            dtTEMP = ExecuteQuery("SELECT * " _
                                                & "FROM qryTreeviewSearchGroupedByWarehouse " _
                                                & "WHERE Division = '" & strSplity(0) & "' AND Material = '" & strSplity(1) & "'")

                        Case 3
                            ' Division, material and shape
                            dtTEMP = ExecuteQuery("SELECT * " _
                                                & "FROM qryTreeviewSearchGroupedByWarehouse " _
                                                & "WHERE Division = '" & strSplity(0) & "' AND Material = '" & strSplity(1) & "' AND Shape = '" & strSplity(2) & "'")

                        Case 4
                            ' Division, material, shape and grade
                            dtTEMP = ExecuteQuery("SELECT * " _
                                                & "FROM qryTreeviewSearchGroupedByWarehouse " _
                                                & "WHERE Division = '" & strSplity(0) & "' AND Material = '" & strSplity(1) & "' AND Shape = '" & strSplity(2) & "' AND Grade = '" & strSplity(3) & "'")

                        Case Else
                            MsgBox("The node's tag seems to have an invalid number of IDs.", MsgBoxStyle.Exclamation, "Invalid TreeNode ID Tag")
                            Exit Sub
                    End Select


                    ' Put the results in the listview
                    dtProducts.Rows.Clear()


                    Dim lvItem As ListViewItem = Nothing

                    For Each rowCurrent In dtTEMP.Rows
                        Dim rowNew As DataRow = dtProducts.NewRow()

                        With rowNew
                            .Item("Stock Code") = rowCurrent.Item("Code")
                            .Item("Description") = rowCurrent.Item("Name")
                            .Item("Units") = rowCurrent.Item("StockUnitName")
                            .Item("On Order") = rowCurrent.Item("OnOrder")
                            .Item("In Stock") = rowCurrent.Item("InStock")
                            .Item("Allocated") = rowCurrent.Item("Allocated")
                            .Item("Free Stock") = rowCurrent.Item("FreeStock")
                        End With

                        dtProducts.Rows.Add(rowNew)
                    Next

                    lblRecordsFound.Text = dtTEMP.Rows.Count & " records found"


                    With dgProducts
                        .DataSource = dtProducts


                        With .Columns("Stock Code")
                            .ReadOnly = True
                        End With

                        With .Columns("Description")
                            .ReadOnly = True
                        End With

                        With .Columns("Units")
                            .ReadOnly = True
                        End With

                        With .Columns("On Order")
                            .ReadOnly = True

                            With .DefaultCellStyle
                                .Alignment = DataGridViewContentAlignment.MiddleRight
                                .Format = "0.00"
                            End With
                        End With

                        With .Columns("In Stock")
                            .ReadOnly = True

                            With .DefaultCellStyle
                                .Alignment = DataGridViewContentAlignment.MiddleRight
                                .Format = "0.00"
                            End With
                        End With

                        With .Columns("Allocated")
                            .ReadOnly = True

                            With .DefaultCellStyle
                                .Alignment = DataGridViewContentAlignment.MiddleRight
                                .Format = "0.00"
                            End With
                        End With

                        With .Columns("Free Stock")
                            .ReadOnly = True

                            With .DefaultCellStyle
                                .Alignment = DataGridViewContentAlignment.MiddleRight
                                .Format = "0.00"
                            End With
                        End With
                    End With


                    ' NEW: Save the node ID for reading back later
                    If bolAllowNodeSQL = True Then
                        Dim sqlComm As New SqlCommand("DELETE FROM tblTreeviewLastNode WHERE Username = @Username", sqlConnToSage)

                        With sqlComm
                            With .Parameters
                                .AddWithValue("@Username", Sage.Accounting.Application.ActiveUser.UserName)
                            End With

                            .ExecuteNonQuery()
                            .Dispose()
                        End With

                        sqlComm = Nothing



                        sqlComm = New SqlCommand("INSERT INTO tblTreeviewLastNode(Username, LastNode)" & vbCrLf _
                                               & "VALUES(@Username, @LastNode)", sqlConnToSage)

                        With sqlComm
                            With .Parameters
                                .AddWithValue("@Username", Sage.Accounting.Application.ActiveUser.UserName)
                                .AddWithValue("@LastNode", e.Node.Tag.ToString())
                            End With

                            .ExecuteNonQuery()
                            .Dispose()
                        End With

                        sqlComm = Nothing
                    End If

                Else
                    MsgBox("The node's tag seems to be invalid.", MsgBoxStyle.Exclamation, "Invalid TreeNode Tag")
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            MsgBox("An error has occurred while querying the treeview node." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region


#Region " SUBROUTINE: lvProducts_DoubleClick() "
    Private Sub lvProducts_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        cmdOK.PerformClick()
    End Sub
#End Region


#Region " SUBROUTINE: FindTagInNodes() "
    Private Sub FindTagInNodes(ByVal strTag As String, ByVal tvNodes As TreeNodeCollection)
        For Each tvNode As TreeNode In tvNodes
            If tvNode.Tag.ToString() = strTag Then
                tvProducts.SelectedNode = tvNode

                With tvNode
                    .EnsureVisible()
                End With
                Exit Sub
            End If

            FindTagInNodes(strTag, tvNode.Nodes)
        Next
    End Sub
#End Region

End Class