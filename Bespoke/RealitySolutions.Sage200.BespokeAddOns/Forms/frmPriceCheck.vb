Option Explicit On
Option Strict On

Imports System
Imports System.Windows.Forms
Imports RealitySolutions.Licensing.Core.Client

Public Class frmPriceCheck

#Region " SUBROUTINE: frmPriceCheck_Load() "
    Private Sub frmPriceCheck_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        Try
            SetupDirectSQLConnection()

            txtFilter.Text = Sage.Accounting.Application.ActiveUser.UserName
            cmdFilter.PerformClick()


        Catch ex As Exception
            lvOrders.EndUpdate()

            MsgBox("An error has occurred while loading the price check screen." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region


#Region " SUBROUTINE: cmdOK_Click() "
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If lvOrders.SelectedItems.Count = 0 Then
            MsgBox("Please select at least one order to mark as 'Price Checked'.", MsgBoxStyle.Exclamation, "Nothing Selected")

            lvOrders.Focus()
            Exit Sub
        End If


        If MsgBox("Are you sure you want to mark the selected orders as 'Price Checked'?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.Yes Then
            Try
                Dim lvCurrent As ListViewItem = Nothing

                Dim sopOrder As Sage.Accounting.SOP.SOPOrder = Nothing


                For Each lvCurrent In lvOrders.SelectedItems
                    sopOrder = DirectCast(lvCurrent.Tag, Sage.Accounting.SOP.SOPOrder)

                    ' Simply as set the spare bit flag!
                    sopOrder.SpareBit3 = True
                    sopOrder.Update()
                Next

                Me.Close()

            Catch ex As Exception
                MsgBox("An error has occurred while setting the price check flag." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
                Exit Sub
            End Try
        End If
    End Sub
#End Region

#Region " SUBROUTINE: cmdCancel_Click() "
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub
#End Region

#Region " SUBROUTINE: cmdEdit_Click() "
    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If lvOrders.SelectedItems.Count = 1 Then
            Dim frmX As New frmSOPLines

            With frmX
                .SOPOrder = DirectCast(lvOrders.SelectedItems(0).Tag, Sage.Accounting.SOP.SOPOrder)

                .ShowDialog()


                .Dispose()
            End With


            frmX = Nothing


            ' Refresh
            frmPriceCheck_Load(sender, e)
        End If
    End Sub
#End Region

#Region " SUBROUTINE: cmdFilter_Click() "
    Private Sub cmdFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFilter.Click
        Dim sopOrders As New Sage.Accounting.SOP.SOPOrders

        Dim lvNew As ListViewItem = Nothing


        lvOrders.BeginUpdate()
        lvOrders.Items.Clear()


        ' Filter the order so only orders that have SpareBit3 flag NOT set
        With sopOrders
            With .Query
                With .Filters
                    .Add(New Sage.ObjectStore.Filter(Sage.Accounting.SOP.SOPOrder.FIELD_SPAREBIT3, chkPriceChecked.Checked))
                    .Add(New Sage.ObjectStore.Filter(Sage.Accounting.SOP.SOPOrder.FIELD_DOCUMENTSTATUSOBJECT, Sage.Accounting.OrderProcessing.DocumentStatusEnum.EnumDocumentStatusLive))

                    If txtFilter.Text <> "" Then
                        .Add(New Sage.ObjectStore.Filter(Sage.Accounting.SOP.SOPOrder.FIELD_DOCUMENTCREATEDBY, txtFilter.Text))
                    End If
                End With

                .Find()

                With .Sorts
                    .Add(New Sage.ObjectStore.Sort(Sage.Accounting.SOP.SOPOrder.FIELD_DOCUMENTNO, False))
                End With
            End With

            .Find()
        End With


        For Each sopOrder As Sage.Accounting.SOP.SOPOrder In sopOrders
            lvNew = lvOrders.Items.Add(sopOrder.DocumentNo)
            lvNew.Tag = sopOrder

            With lvNew.SubItems
                .Add(Format(sopOrder.DocumentDate, "dd/MM/yyyy"))
                .Add(sopOrder.Customer.Name)
                .Add(Format(sopOrder.TotalNetValue, "0.00"))
            End With
        Next

        lvOrders.EndUpdate()
    End Sub
#End Region


#Region " SUBROUTINE: lvOrders_DoubleClick() "
    Private Sub lvOrders_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvOrders.DoubleClick
        cmdEdit.PerformClick()
    End Sub
#End Region

End Class