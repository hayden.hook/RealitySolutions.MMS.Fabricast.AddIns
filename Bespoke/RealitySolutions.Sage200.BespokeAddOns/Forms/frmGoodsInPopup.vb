Option Explicit On
Option Strict On

Imports System
Imports System.Windows.Forms
Imports RealitySolutions.Licensing.Core.Client

Public Class frmGoodsInPopup
    Private lstItem As ListViewItem = Nothing

    Private popLine As Sage.Accounting.POP.POPOrderReturnLine = Nothing

#Region " PROPERTY: ItemSelected() "
    Public Property ItemSelected() As ListViewItem
        Get
            Return lstItem
        End Get
        Set(ByVal value As ListViewItem)
            lstItem = value
        End Set
    End Property
#End Region


#Region " SUBROUTINE: frmGoodsInPopup_Closing() "
    Private Sub frmGoodsInPopup_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

    End Sub
#End Region
#Region " SUBROUTINE: frmGoodsInPopup_Load() "
    Private Sub frmGoodsInPopup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        ' Load the details in from the currently selected list item
        Try
            popLine = DirectCast(lstItem.Tag, Sage.Accounting.POP.POPOrderReturnLine)

            With lstItem
                txtOrderNumber.Text = .SubItems(0).Text
                txtSupplierName.Text = .SubItems(2).Text
                txtItemCode.Text = .SubItems(3).Text
                txtItemName.Text = .SubItems(4).Text
                txtBuyingUnit.Text = .SubItems(5).Text
                txtQtyOrdered.Text = .SubItems(6).Text
                txtQtyReceived.Text = Format(popLine.ReceiptReturnQuantity, "0.00000") '.SubItems(7).Text

                txtQtyOutstanding1.Text = .SubItems(8).Text
                txtQtyOutstanding2.Text = .SubItems(9).Text

                txtQtyOutstanding3.Text = .SubItems(10).Text
                txtQtyOutstanding4.Text = .SubItems(11).Text

                Select Case .SubItems(12).Text
                    Case "Y"
                        chkCompleted.Checked = True

                    Case "N"
                        chkCompleted.Checked = False
                End Select
            End With


            ' Unit labels
            Dim oStockItems As New Sage.Accounting.Stock.StockItems
            Dim oStockItem As Sage.Accounting.Stock.StockItem = oStockItems(txtItemCode.Text)


            lblUnit1.Text = txtBuyingUnit.Text
            lblUnit2.Text = oStockItem.BaseUnitName

            If lblUnit1.Text = lblUnit2.Text Then
                txtQtyOutstanding1.Enabled = False
                txtQtyOutstanding2.Enabled = False
            End If


            ' Dispose
            oStockItems.Dispose()
            oStockItem.Dispose()

            oStockItems = Nothing
            oStockItem = Nothing

        Catch ex As Exception
            cmdOK.Enabled = False

            MsgBox("An error has occurred while loading the order line details." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region


#Region " SUBROUTINE: cmdOK_Click "
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If CDec(txtQtyOutstanding2.Text) = 0.0# Then
            MsgBox("Please enter a valid amount.", MsgBoxStyle.Exclamation, "Invalid Amount")

            txtQtyOutstanding2.Focus()
            Exit Sub
        End If

        If CDec(txtQtyOutstanding4.Text) = 0.0# Then
            MsgBox("Please enter a valid amount.", MsgBoxStyle.Exclamation, "Invalid Amount")

            txtQtyOutstanding4.Focus()
            Exit Sub
        End If


        ' Put the information back again
        With lstItem
            .SubItems(7).Text = Format(CDec(popLine.ReceiptReturnQuantity) + CDec(txtQtyOutstanding2.Text), "0.00000")
            .SubItems(9).Text = txtQtyOutstanding2.Text
            .SubItems(11).Text = txtQtyOutstanding4.Text

            If chkCompleted.Checked = True Then
                .SubItems(12).Text = "Y"

            Else
                .SubItems(12).Text = "N"
            End If
        End With

        Me.Close()
    End Sub
#End Region

#Region " SUBROUTINE: cmdCancel_Click() "
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        lstItem = Nothing

        Me.Close()
    End Sub
#End Region


#Region " SUBROUTINE: txtQtyOutstanding2_LostFocus() "
    Private Sub txtQtyOutstanding2_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQtyOutstanding2.LostFocus
        If IsNumeric(txtQtyOutstanding2.Text) = False Then
            txtQtyOutstanding2.Text = "0.00000"

        Else
            If CDec(txtQtyOutstanding2.Text) > (CDec(txtQtyOutstanding1.Text) + (CDec(txtQtyOutstanding1.Text) * (ERROR_THRESHOLD / 100))) Then
                txtQtyOutstanding2.Text = Format((CDec(txtQtyOutstanding1.Text) + (CDec(txtQtyOutstanding1.Text) * (ERROR_THRESHOLD / 100))), "0.00000")

            Else
                txtQtyOutstanding2.Text = Format(CDec(txtQtyOutstanding2.Text), "0.00000")
            End If
        End If


        ' Check for negative
        If CDec(txtQtyOutstanding2.Text) < 0.0# Then
            txtQtyOutstanding2.Text = "0.00000"
        End If


        If lblUnit1.Text = lblUnit2.Text Then
            txtQtyOutstanding4.Text = txtQtyOutstanding2.Text
        End If
    End Sub
#End Region

#Region " SUBROUTINE: txtQtyOutstanding4_LostFocus() "
    Private Sub txtQtyOutstanding4_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQtyOutstanding4.LostFocus
        If IsNumeric(txtQtyOutstanding4.Text) = False Then
            txtQtyOutstanding4.Text = "0.00000"

        Else
            If CDec(txtQtyOutstanding4.Text) > (CDec(txtQtyOutstanding3.Text) + (CDec(txtQtyOutstanding3.Text) * (ERROR_THRESHOLD / 100))) Then
                txtQtyOutstanding4.Text = Format(CDec(txtQtyOutstanding3.Text), "0.00000")

            Else
                txtQtyOutstanding4.Text = Format(CDec(txtQtyOutstanding4.Text), "0.00000")
            End If
        End If


        ' Check for negative
        If CDec(txtQtyOutstanding4.Text) < 0.0# Then
            txtQtyOutstanding4.Text = "0.00000"
        End If


        If lblUnit1.Text = lblUnit2.Text Then
            txtQtyOutstanding2.Text = txtQtyOutstanding4.Text
        End If
    End Sub
#End Region

End Class