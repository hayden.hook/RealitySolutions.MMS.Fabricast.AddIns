<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGoodsOutPopup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblOrderNumber = New System.Windows.Forms.Label
        Me.lblCustomerName = New System.Windows.Forms.Label
        Me.lblItemCode = New System.Windows.Forms.Label
        Me.lblItemName = New System.Windows.Forms.Label
        Me.lblSellingUnit = New System.Windows.Forms.Label
        Me.lblQtyOrdered = New System.Windows.Forms.Label
        Me.lblQtyDispatched = New System.Windows.Forms.Label
        Me.lblQtyRequired = New System.Windows.Forms.Label
        Me.lblCompleted = New System.Windows.Forms.Label
        Me.txtOrderNumber = New System.Windows.Forms.TextBox
        Me.txtCustomerName = New System.Windows.Forms.TextBox
        Me.txtItemCode = New System.Windows.Forms.TextBox
        Me.txtItemName = New System.Windows.Forms.TextBox
        Me.txtSellingUnit = New System.Windows.Forms.TextBox
        Me.txtQtyOrdered = New System.Windows.Forms.TextBox
        Me.txtQtyDispatched = New System.Windows.Forms.TextBox
        Me.txtQtyRequired1 = New System.Windows.Forms.TextBox
        Me.txtQtyRequired2 = New System.Windows.Forms.TextBox
        Me.txtQtyRequired3 = New System.Windows.Forms.TextBox
        Me.txtQtyRequired4 = New System.Windows.Forms.TextBox
        Me.chkCompleted = New System.Windows.Forms.CheckBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.lblUnit1 = New System.Windows.Forms.Label
        Me.lblUnit2 = New System.Windows.Forms.Label
        Me.lblMaterialsRef = New System.Windows.Forms.Label
        Me.txtMaterialsRef = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'lblOrderNumber
        '
        Me.lblOrderNumber.AutoSize = True
        Me.lblOrderNumber.Location = New System.Drawing.Point(8, 8)
        Me.lblOrderNumber.Name = "lblOrderNumber"
        Me.lblOrderNumber.Size = New System.Drawing.Size(79, 13)
        Me.lblOrderNumber.TabIndex = 0
        Me.lblOrderNumber.Text = "Order Number:"
        '
        'lblCustomerName
        '
        Me.lblCustomerName.AutoSize = True
        Me.lblCustomerName.Location = New System.Drawing.Point(8, 32)
        Me.lblCustomerName.Name = "lblCustomerName"
        Me.lblCustomerName.Size = New System.Drawing.Size(87, 13)
        Me.lblCustomerName.TabIndex = 1
        Me.lblCustomerName.Text = "Customer Name:"
        '
        'lblItemCode
        '
        Me.lblItemCode.AutoSize = True
        Me.lblItemCode.Location = New System.Drawing.Point(8, 56)
        Me.lblItemCode.Name = "lblItemCode"
        Me.lblItemCode.Size = New System.Drawing.Size(61, 13)
        Me.lblItemCode.TabIndex = 2
        Me.lblItemCode.Text = "Item Code:"
        '
        'lblItemName
        '
        Me.lblItemName.AutoSize = True
        Me.lblItemName.Location = New System.Drawing.Point(8, 80)
        Me.lblItemName.Name = "lblItemName"
        Me.lblItemName.Size = New System.Drawing.Size(63, 13)
        Me.lblItemName.TabIndex = 3
        Me.lblItemName.Text = "Item Name:"
        '
        'lblSellingUnit
        '
        Me.lblSellingUnit.AutoSize = True
        Me.lblSellingUnit.Location = New System.Drawing.Point(8, 104)
        Me.lblSellingUnit.Name = "lblSellingUnit"
        Me.lblSellingUnit.Size = New System.Drawing.Size(63, 13)
        Me.lblSellingUnit.TabIndex = 4
        Me.lblSellingUnit.Text = "Selling Unit:"
        '
        'lblQtyOrdered
        '
        Me.lblQtyOrdered.AutoSize = True
        Me.lblQtyOrdered.Location = New System.Drawing.Point(8, 128)
        Me.lblQtyOrdered.Name = "lblQtyOrdered"
        Me.lblQtyOrdered.Size = New System.Drawing.Size(72, 13)
        Me.lblQtyOrdered.TabIndex = 6
        Me.lblQtyOrdered.Text = "Qty Ordered:"
        '
        'lblQtyDispatched
        '
        Me.lblQtyDispatched.AutoSize = True
        Me.lblQtyDispatched.Location = New System.Drawing.Point(8, 152)
        Me.lblQtyDispatched.Name = "lblQtyDispatched"
        Me.lblQtyDispatched.Size = New System.Drawing.Size(85, 13)
        Me.lblQtyDispatched.TabIndex = 7
        Me.lblQtyDispatched.Text = "Qty Dispatched:"
        '
        'lblQtyRequired
        '
        Me.lblQtyRequired.AutoSize = True
        Me.lblQtyRequired.Location = New System.Drawing.Point(8, 176)
        Me.lblQtyRequired.Name = "lblQtyRequired"
        Me.lblQtyRequired.Size = New System.Drawing.Size(75, 13)
        Me.lblQtyRequired.TabIndex = 8
        Me.lblQtyRequired.Text = "Qty Required:"
        '
        'lblCompleted
        '
        Me.lblCompleted.AutoSize = True
        Me.lblCompleted.Location = New System.Drawing.Point(8, 248)
        Me.lblCompleted.Name = "lblCompleted"
        Me.lblCompleted.Size = New System.Drawing.Size(67, 13)
        Me.lblCompleted.TabIndex = 9
        Me.lblCompleted.Text = "Completed?:"
        '
        'txtOrderNumber
        '
        Me.txtOrderNumber.Location = New System.Drawing.Point(100, 4)
        Me.txtOrderNumber.Name = "txtOrderNumber"
        Me.txtOrderNumber.ReadOnly = True
        Me.txtOrderNumber.Size = New System.Drawing.Size(188, 21)
        Me.txtOrderNumber.TabIndex = 10
        '
        'txtCustomerName
        '
        Me.txtCustomerName.Location = New System.Drawing.Point(100, 28)
        Me.txtCustomerName.Name = "txtCustomerName"
        Me.txtCustomerName.ReadOnly = True
        Me.txtCustomerName.Size = New System.Drawing.Size(188, 21)
        Me.txtCustomerName.TabIndex = 11
        '
        'txtItemCode
        '
        Me.txtItemCode.Location = New System.Drawing.Point(100, 52)
        Me.txtItemCode.Name = "txtItemCode"
        Me.txtItemCode.ReadOnly = True
        Me.txtItemCode.Size = New System.Drawing.Size(188, 21)
        Me.txtItemCode.TabIndex = 12
        '
        'txtItemName
        '
        Me.txtItemName.Location = New System.Drawing.Point(100, 76)
        Me.txtItemName.Name = "txtItemName"
        Me.txtItemName.ReadOnly = True
        Me.txtItemName.Size = New System.Drawing.Size(188, 21)
        Me.txtItemName.TabIndex = 13
        '
        'txtSellingUnit
        '
        Me.txtSellingUnit.Location = New System.Drawing.Point(100, 100)
        Me.txtSellingUnit.Name = "txtSellingUnit"
        Me.txtSellingUnit.ReadOnly = True
        Me.txtSellingUnit.Size = New System.Drawing.Size(92, 21)
        Me.txtSellingUnit.TabIndex = 14
        '
        'txtQtyOrdered
        '
        Me.txtQtyOrdered.Location = New System.Drawing.Point(100, 124)
        Me.txtQtyOrdered.Name = "txtQtyOrdered"
        Me.txtQtyOrdered.ReadOnly = True
        Me.txtQtyOrdered.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyOrdered.TabIndex = 16
        Me.txtQtyOrdered.Text = "0.00000"
        Me.txtQtyOrdered.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtyDispatched
        '
        Me.txtQtyDispatched.Location = New System.Drawing.Point(100, 148)
        Me.txtQtyDispatched.Name = "txtQtyDispatched"
        Me.txtQtyDispatched.ReadOnly = True
        Me.txtQtyDispatched.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyDispatched.TabIndex = 17
        Me.txtQtyDispatched.Text = "0.00000"
        Me.txtQtyDispatched.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtyRequired1
        '
        Me.txtQtyRequired1.Location = New System.Drawing.Point(100, 172)
        Me.txtQtyRequired1.Name = "txtQtyRequired1"
        Me.txtQtyRequired1.ReadOnly = True
        Me.txtQtyRequired1.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyRequired1.TabIndex = 18
        Me.txtQtyRequired1.Text = "0.00000"
        Me.txtQtyRequired1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtyRequired2
        '
        Me.txtQtyRequired2.Location = New System.Drawing.Point(196, 172)
        Me.txtQtyRequired2.Name = "txtQtyRequired2"
        Me.txtQtyRequired2.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyRequired2.TabIndex = 19
        Me.txtQtyRequired2.Text = "0.00000"
        Me.txtQtyRequired2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtyRequired3
        '
        Me.txtQtyRequired3.Location = New System.Drawing.Point(100, 196)
        Me.txtQtyRequired3.Name = "txtQtyRequired3"
        Me.txtQtyRequired3.ReadOnly = True
        Me.txtQtyRequired3.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyRequired3.TabIndex = 20
        Me.txtQtyRequired3.Text = "0.00000"
        Me.txtQtyRequired3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQtyRequired4
        '
        Me.txtQtyRequired4.Location = New System.Drawing.Point(196, 196)
        Me.txtQtyRequired4.Name = "txtQtyRequired4"
        Me.txtQtyRequired4.Size = New System.Drawing.Size(92, 21)
        Me.txtQtyRequired4.TabIndex = 21
        Me.txtQtyRequired4.Text = "0.00000"
        Me.txtQtyRequired4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkCompleted
        '
        Me.chkCompleted.AutoSize = True
        Me.chkCompleted.Location = New System.Drawing.Point(100, 248)
        Me.chkCompleted.Name = "chkCompleted"
        Me.chkCompleted.Size = New System.Drawing.Size(15, 14)
        Me.chkCompleted.TabIndex = 22
        Me.chkCompleted.UseVisualStyleBackColor = True
        '
        'cmdOK
        '
        Me.cmdOK.Location = New System.Drawing.Point(8, 276)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 23
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Location = New System.Drawing.Point(264, 276)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 24
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'lblUnit1
        '
        Me.lblUnit1.AutoSize = True
        Me.lblUnit1.Location = New System.Drawing.Point(292, 176)
        Me.lblUnit1.Name = "lblUnit1"
        Me.lblUnit1.Size = New System.Drawing.Size(47, 13)
        Me.lblUnit1.TabIndex = 25
        Me.lblUnit1.Text = "<UNIT>"
        '
        'lblUnit2
        '
        Me.lblUnit2.AutoSize = True
        Me.lblUnit2.Location = New System.Drawing.Point(292, 200)
        Me.lblUnit2.Name = "lblUnit2"
        Me.lblUnit2.Size = New System.Drawing.Size(47, 13)
        Me.lblUnit2.TabIndex = 26
        Me.lblUnit2.Text = "<UNIT>"
        '
        'lblMaterialsRef
        '
        Me.lblMaterialsRef.AutoSize = True
        Me.lblMaterialsRef.Location = New System.Drawing.Point(8, 224)
        Me.lblMaterialsRef.Name = "lblMaterialsRef"
        Me.lblMaterialsRef.Size = New System.Drawing.Size(74, 13)
        Me.lblMaterialsRef.TabIndex = 27
        Me.lblMaterialsRef.Text = "Materials Ref:"
        '
        'txtMaterialsRef
        '
        Me.txtMaterialsRef.Location = New System.Drawing.Point(100, 220)
        Me.txtMaterialsRef.Name = "txtMaterialsRef"
        Me.txtMaterialsRef.Size = New System.Drawing.Size(188, 21)
        Me.txtMaterialsRef.TabIndex = 28
        '
        'frmGoodsOutPopup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(344, 305)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtMaterialsRef)
        Me.Controls.Add(Me.lblMaterialsRef)
        Me.Controls.Add(Me.lblUnit2)
        Me.Controls.Add(Me.lblUnit1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.chkCompleted)
        Me.Controls.Add(Me.txtQtyRequired4)
        Me.Controls.Add(Me.txtQtyRequired3)
        Me.Controls.Add(Me.txtQtyRequired2)
        Me.Controls.Add(Me.txtQtyRequired1)
        Me.Controls.Add(Me.txtQtyDispatched)
        Me.Controls.Add(Me.txtQtyOrdered)
        Me.Controls.Add(Me.txtSellingUnit)
        Me.Controls.Add(Me.txtItemName)
        Me.Controls.Add(Me.txtItemCode)
        Me.Controls.Add(Me.txtCustomerName)
        Me.Controls.Add(Me.txtOrderNumber)
        Me.Controls.Add(Me.lblCompleted)
        Me.Controls.Add(Me.lblQtyRequired)
        Me.Controls.Add(Me.lblQtyDispatched)
        Me.Controls.Add(Me.lblQtyOrdered)
        Me.Controls.Add(Me.lblSellingUnit)
        Me.Controls.Add(Me.lblItemName)
        Me.Controls.Add(Me.lblItemCode)
        Me.Controls.Add(Me.lblCustomerName)
        Me.Controls.Add(Me.lblOrderNumber)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmGoodsOutPopup"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Details"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblOrderNumber As System.Windows.Forms.Label
    Friend WithEvents lblCustomerName As System.Windows.Forms.Label
    Friend WithEvents lblItemCode As System.Windows.Forms.Label
    Friend WithEvents lblItemName As System.Windows.Forms.Label
    Friend WithEvents lblSellingUnit As System.Windows.Forms.Label
    Friend WithEvents lblQtyOrdered As System.Windows.Forms.Label
    Friend WithEvents lblQtyDispatched As System.Windows.Forms.Label
    Friend WithEvents lblQtyRequired As System.Windows.Forms.Label
    Friend WithEvents lblCompleted As System.Windows.Forms.Label
    Friend WithEvents txtOrderNumber As System.Windows.Forms.TextBox
    Friend WithEvents txtCustomerName As System.Windows.Forms.TextBox
    Friend WithEvents txtItemCode As System.Windows.Forms.TextBox
    Friend WithEvents txtItemName As System.Windows.Forms.TextBox
    Friend WithEvents txtSellingUnit As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyOrdered As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyDispatched As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyRequired1 As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyRequired2 As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyRequired3 As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyRequired4 As System.Windows.Forms.TextBox
    Friend WithEvents chkCompleted As System.Windows.Forms.CheckBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents lblUnit1 As System.Windows.Forms.Label
    Friend WithEvents lblUnit2 As System.Windows.Forms.Label
    Friend WithEvents lblMaterialsRef As System.Windows.Forms.Label
    Friend WithEvents txtMaterialsRef As System.Windows.Forms.TextBox
End Class
