﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStockEnquiry
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.slStock = New Sage.MMS.Controls.StockLookup
        Me.cmdClose = New System.Windows.Forms.Button
        Me.cmdSearch = New System.Windows.Forms.Button
        Me.fraStock = New System.Windows.Forms.GroupBox
        Me.txtFreeStock = New System.Windows.Forms.TextBox
        Me.lblFreeStock = New System.Windows.Forms.Label
        Me.txtAllocated = New System.Windows.Forms.TextBox
        Me.lblAllocated = New System.Windows.Forms.Label
        Me.txtInStock = New System.Windows.Forms.TextBox
        Me.lblInStock = New System.Windows.Forms.Label
        Me.txtOnOrder = New System.Windows.Forms.TextBox
        Me.lblOnOrder = New System.Windows.Forms.Label
        Me.txtWeightMtr = New System.Windows.Forms.TextBox
        Me.lblWeightMtr = New System.Windows.Forms.Label
        Me.txtBaseUnit = New System.Windows.Forms.TextBox
        Me.lblBaseUnit = New System.Windows.Forms.Label
        Me.txtLastPurchasePrice = New System.Windows.Forms.TextBox
        Me.lblLastPurchasePrice = New System.Windows.Forms.Label
        Me.txtLastPurchaseDate = New System.Windows.Forms.TextBox
        Me.lblLastPurchaseDate = New System.Windows.Forms.Label
        Me.lblBalance = New System.Windows.Forms.Label
        Me.txtIn = New System.Windows.Forms.TextBox
        Me.txtOut = New System.Windows.Forms.TextBox
        Me.txtBalance = New System.Windows.Forms.TextBox
        Me.dgTransactions = New System.Windows.Forms.DataGridView
        Me.lblIn = New System.Windows.Forms.Label
        Me.lblOut = New System.Windows.Forms.Label
        CType(Me.slStock, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.fraStock.SuspendLayout()
        CType(Me.dgTransactions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'slStock
        '
        Me.slStock.CodeLabelText = "Code:"
        Me.slStock.CodeLeft = 38
        Me.slStock.CodeText = ""
        Me.slStock.CodeWidth = 190
        Me.slStock.EditCodeText = ""
        Me.slStock.EditNameText = ""
        Me.slStock.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.slStock.Location = New System.Drawing.Point(28, 20)
        Me.slStock.Name = "slStock"
        Me.slStock.NameAllowBlank = True
        Me.slStock.NameLabelLeft = 0
        Me.slStock.NameLeft = 40
        Me.slStock.NameText = ""
        Me.slStock.Size = New System.Drawing.Size(900, 24)
        Me.slStock.SupressNotAllowedForSalesOrderItems = False
        Me.slStock.TabIndex = 0
        Me.slStock.TabStop = False
        Me.slStock.Text = "StockLookup1"
        '
        'cmdClose
        '
        Me.cmdClose.Location = New System.Drawing.Point(944, 568)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(75, 23)
        Me.cmdClose.TabIndex = 10
        Me.cmdClose.Text = "&Close"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'cmdSearch
        '
        Me.cmdSearch.Location = New System.Drawing.Point(932, 20)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(75, 23)
        Me.cmdSearch.TabIndex = 1
        Me.cmdSearch.Text = "&Search"
        Me.cmdSearch.UseVisualStyleBackColor = True
        '
        'fraStock
        '
        Me.fraStock.Controls.Add(Me.txtFreeStock)
        Me.fraStock.Controls.Add(Me.lblFreeStock)
        Me.fraStock.Controls.Add(Me.txtAllocated)
        Me.fraStock.Controls.Add(Me.lblAllocated)
        Me.fraStock.Controls.Add(Me.txtInStock)
        Me.fraStock.Controls.Add(Me.lblInStock)
        Me.fraStock.Controls.Add(Me.txtOnOrder)
        Me.fraStock.Controls.Add(Me.lblOnOrder)
        Me.fraStock.Controls.Add(Me.txtWeightMtr)
        Me.fraStock.Controls.Add(Me.lblWeightMtr)
        Me.fraStock.Controls.Add(Me.txtBaseUnit)
        Me.fraStock.Controls.Add(Me.lblBaseUnit)
        Me.fraStock.Controls.Add(Me.slStock)
        Me.fraStock.Controls.Add(Me.cmdSearch)
        Me.fraStock.Location = New System.Drawing.Point(8, 8)
        Me.fraStock.Name = "fraStock"
        Me.fraStock.Size = New System.Drawing.Size(1012, 100)
        Me.fraStock.TabIndex = 0
        Me.fraStock.TabStop = False
        Me.fraStock.Text = "Stock"
        '
        'txtFreeStock
        '
        Me.txtFreeStock.Location = New System.Drawing.Point(776, 72)
        Me.txtFreeStock.Name = "txtFreeStock"
        Me.txtFreeStock.ReadOnly = True
        Me.txtFreeStock.Size = New System.Drawing.Size(100, 21)
        Me.txtFreeStock.TabIndex = 13
        Me.txtFreeStock.Text = "0.00000"
        Me.txtFreeStock.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblFreeStock
        '
        Me.lblFreeStock.AutoSize = True
        Me.lblFreeStock.Location = New System.Drawing.Point(704, 76)
        Me.lblFreeStock.Name = "lblFreeStock"
        Me.lblFreeStock.Size = New System.Drawing.Size(62, 13)
        Me.lblFreeStock.TabIndex = 12
        Me.lblFreeStock.Text = "Free Stock:"
        '
        'txtAllocated
        '
        Me.txtAllocated.Location = New System.Drawing.Point(540, 72)
        Me.txtAllocated.Name = "txtAllocated"
        Me.txtAllocated.ReadOnly = True
        Me.txtAllocated.Size = New System.Drawing.Size(100, 21)
        Me.txtAllocated.TabIndex = 11
        Me.txtAllocated.Text = "0.00000"
        Me.txtAllocated.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAllocated
        '
        Me.lblAllocated.AutoSize = True
        Me.lblAllocated.Location = New System.Drawing.Point(468, 76)
        Me.lblAllocated.Name = "lblAllocated"
        Me.lblAllocated.Size = New System.Drawing.Size(55, 13)
        Me.lblAllocated.TabIndex = 10
        Me.lblAllocated.Text = "Allocated:"
        '
        'txtInStock
        '
        Me.txtInStock.Location = New System.Drawing.Point(304, 72)
        Me.txtInStock.Name = "txtInStock"
        Me.txtInStock.ReadOnly = True
        Me.txtInStock.Size = New System.Drawing.Size(100, 21)
        Me.txtInStock.TabIndex = 9
        Me.txtInStock.Text = "0.00000"
        Me.txtInStock.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInStock
        '
        Me.lblInStock.AutoSize = True
        Me.lblInStock.Location = New System.Drawing.Point(232, 76)
        Me.lblInStock.Name = "lblInStock"
        Me.lblInStock.Size = New System.Drawing.Size(50, 13)
        Me.lblInStock.TabIndex = 8
        Me.lblInStock.Text = "In Stock:"
        '
        'txtOnOrder
        '
        Me.txtOnOrder.Location = New System.Drawing.Point(68, 72)
        Me.txtOnOrder.Name = "txtOnOrder"
        Me.txtOnOrder.ReadOnly = True
        Me.txtOnOrder.Size = New System.Drawing.Size(100, 21)
        Me.txtOnOrder.TabIndex = 7
        Me.txtOnOrder.Text = "0.00000"
        Me.txtOnOrder.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOnOrder
        '
        Me.lblOnOrder.AutoSize = True
        Me.lblOnOrder.Location = New System.Drawing.Point(8, 76)
        Me.lblOnOrder.Name = "lblOnOrder"
        Me.lblOnOrder.Size = New System.Drawing.Size(56, 13)
        Me.lblOnOrder.TabIndex = 6
        Me.lblOnOrder.Text = "On Order:"
        '
        'txtWeightMtr
        '
        Me.txtWeightMtr.Location = New System.Drawing.Point(304, 48)
        Me.txtWeightMtr.Name = "txtWeightMtr"
        Me.txtWeightMtr.ReadOnly = True
        Me.txtWeightMtr.Size = New System.Drawing.Size(100, 21)
        Me.txtWeightMtr.TabIndex = 5
        Me.txtWeightMtr.Text = "0.00000"
        Me.txtWeightMtr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblWeightMtr
        '
        Me.lblWeightMtr.AutoSize = True
        Me.lblWeightMtr.Location = New System.Drawing.Point(232, 52)
        Me.lblWeightMtr.Name = "lblWeightMtr"
        Me.lblWeightMtr.Size = New System.Drawing.Size(65, 13)
        Me.lblWeightMtr.TabIndex = 4
        Me.lblWeightMtr.Text = "Weight/Mtr:"
        '
        'txtBaseUnit
        '
        Me.txtBaseUnit.Location = New System.Drawing.Point(68, 48)
        Me.txtBaseUnit.Name = "txtBaseUnit"
        Me.txtBaseUnit.ReadOnly = True
        Me.txtBaseUnit.Size = New System.Drawing.Size(100, 21)
        Me.txtBaseUnit.TabIndex = 3
        '
        'lblBaseUnit
        '
        Me.lblBaseUnit.AutoSize = True
        Me.lblBaseUnit.Location = New System.Drawing.Point(8, 52)
        Me.lblBaseUnit.Name = "lblBaseUnit"
        Me.lblBaseUnit.Size = New System.Drawing.Size(56, 13)
        Me.lblBaseUnit.TabIndex = 2
        Me.lblBaseUnit.Text = "Base Unit:"
        '
        'txtLastPurchasePrice
        '
        Me.txtLastPurchasePrice.Location = New System.Drawing.Point(120, 544)
        Me.txtLastPurchasePrice.Name = "txtLastPurchasePrice"
        Me.txtLastPurchasePrice.ReadOnly = True
        Me.txtLastPurchasePrice.Size = New System.Drawing.Size(100, 21)
        Me.txtLastPurchasePrice.TabIndex = 3
        Me.txtLastPurchasePrice.Text = "0.00000"
        Me.txtLastPurchasePrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLastPurchasePrice
        '
        Me.lblLastPurchasePrice.AutoSize = True
        Me.lblLastPurchasePrice.Location = New System.Drawing.Point(12, 548)
        Me.lblLastPurchasePrice.Name = "lblLastPurchasePrice"
        Me.lblLastPurchasePrice.Size = New System.Drawing.Size(104, 13)
        Me.lblLastPurchasePrice.TabIndex = 2
        Me.lblLastPurchasePrice.Text = "Last Purchase Price:"
        '
        'txtLastPurchaseDate
        '
        Me.txtLastPurchaseDate.Location = New System.Drawing.Point(120, 568)
        Me.txtLastPurchaseDate.Name = "txtLastPurchaseDate"
        Me.txtLastPurchaseDate.ReadOnly = True
        Me.txtLastPurchaseDate.Size = New System.Drawing.Size(100, 21)
        Me.txtLastPurchaseDate.TabIndex = 5
        '
        'lblLastPurchaseDate
        '
        Me.lblLastPurchaseDate.AutoSize = True
        Me.lblLastPurchaseDate.Location = New System.Drawing.Point(12, 572)
        Me.lblLastPurchaseDate.Name = "lblLastPurchaseDate"
        Me.lblLastPurchaseDate.Size = New System.Drawing.Size(104, 13)
        Me.lblLastPurchaseDate.TabIndex = 4
        Me.lblLastPurchaseDate.Text = "Last Purchase Date:"
        '
        'lblBalance
        '
        Me.lblBalance.AutoSize = True
        Me.lblBalance.Location = New System.Drawing.Point(364, 572)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(48, 13)
        Me.lblBalance.TabIndex = 6
        Me.lblBalance.Text = "Balance:"
        Me.lblBalance.Visible = False
        '
        'txtIn
        '
        Me.txtIn.Location = New System.Drawing.Point(416, 544)
        Me.txtIn.Name = "txtIn"
        Me.txtIn.ReadOnly = True
        Me.txtIn.Size = New System.Drawing.Size(96, 21)
        Me.txtIn.TabIndex = 7
        Me.txtIn.Text = "0.00"
        Me.txtIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIn.Visible = False
        '
        'txtOut
        '
        Me.txtOut.Location = New System.Drawing.Point(516, 544)
        Me.txtOut.Name = "txtOut"
        Me.txtOut.ReadOnly = True
        Me.txtOut.Size = New System.Drawing.Size(96, 21)
        Me.txtOut.TabIndex = 8
        Me.txtOut.Text = "0.00"
        Me.txtOut.Visible = False
        '
        'txtBalance
        '
        Me.txtBalance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalance.Location = New System.Drawing.Point(416, 568)
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.ReadOnly = True
        Me.txtBalance.Size = New System.Drawing.Size(196, 21)
        Me.txtBalance.TabIndex = 9
        Me.txtBalance.Text = "0.00"
        Me.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtBalance.Visible = False
        '
        'dgTransactions
        '
        Me.dgTransactions.AllowUserToAddRows = False
        Me.dgTransactions.AllowUserToDeleteRows = False
        Me.dgTransactions.AllowUserToResizeRows = False
        Me.dgTransactions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgTransactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgTransactions.Location = New System.Drawing.Point(8, 112)
        Me.dgTransactions.MultiSelect = False
        Me.dgTransactions.Name = "dgTransactions"
        Me.dgTransactions.ReadOnly = True
        Me.dgTransactions.RowHeadersVisible = False
        Me.dgTransactions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgTransactions.Size = New System.Drawing.Size(1012, 428)
        Me.dgTransactions.TabIndex = 1
        '
        'lblIn
        '
        Me.lblIn.AutoSize = True
        Me.lblIn.Location = New System.Drawing.Point(392, 548)
        Me.lblIn.Name = "lblIn"
        Me.lblIn.Size = New System.Drawing.Size(17, 13)
        Me.lblIn.TabIndex = 19
        Me.lblIn.Text = "In"
        Me.lblIn.TextAlign = System.Drawing.ContentAlignment.TopRight
        Me.lblIn.Visible = False
        '
        'lblOut
        '
        Me.lblOut.AutoSize = True
        Me.lblOut.Location = New System.Drawing.Point(616, 548)
        Me.lblOut.Name = "lblOut"
        Me.lblOut.Size = New System.Drawing.Size(25, 13)
        Me.lblOut.TabIndex = 20
        Me.lblOut.Text = "Out"
        Me.lblOut.Visible = False
        '
        'frmStockEnquiry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1025, 596)
        Me.Controls.Add(Me.lblOut)
        Me.Controls.Add(Me.lblIn)
        Me.Controls.Add(Me.dgTransactions)
        Me.Controls.Add(Me.txtBalance)
        Me.Controls.Add(Me.txtOut)
        Me.Controls.Add(Me.txtIn)
        Me.Controls.Add(Me.lblBalance)
        Me.Controls.Add(Me.txtLastPurchaseDate)
        Me.Controls.Add(Me.lblLastPurchaseDate)
        Me.Controls.Add(Me.txtLastPurchasePrice)
        Me.Controls.Add(Me.lblLastPurchasePrice)
        Me.Controls.Add(Me.fraStock)
        Me.Controls.Add(Me.cmdClose)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmStockEnquiry"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Stock Enquiry"
        CType(Me.slStock, System.ComponentModel.ISupportInitialize).EndInit()
        Me.fraStock.ResumeLayout(False)
        Me.fraStock.PerformLayout()
        CType(Me.dgTransactions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents slStock As Sage.MMS.Controls.StockLookup
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents cmdSearch As System.Windows.Forms.Button
    Friend WithEvents fraStock As System.Windows.Forms.GroupBox
    Friend WithEvents txtWeightMtr As System.Windows.Forms.TextBox
    Friend WithEvents lblWeightMtr As System.Windows.Forms.Label
    Friend WithEvents txtBaseUnit As System.Windows.Forms.TextBox
    Friend WithEvents lblBaseUnit As System.Windows.Forms.Label
    Friend WithEvents txtLastPurchasePrice As System.Windows.Forms.TextBox
    Friend WithEvents lblLastPurchasePrice As System.Windows.Forms.Label
    Friend WithEvents txtLastPurchaseDate As System.Windows.Forms.TextBox
    Friend WithEvents lblLastPurchaseDate As System.Windows.Forms.Label
    Friend WithEvents lblBalance As System.Windows.Forms.Label
    Friend WithEvents txtIn As System.Windows.Forms.TextBox
    Friend WithEvents txtOut As System.Windows.Forms.TextBox
    Friend WithEvents txtBalance As System.Windows.Forms.TextBox
    Friend WithEvents dgTransactions As System.Windows.Forms.DataGridView
    Friend WithEvents txtInStock As System.Windows.Forms.TextBox
    Friend WithEvents lblInStock As System.Windows.Forms.Label
    Friend WithEvents txtOnOrder As System.Windows.Forms.TextBox
    Friend WithEvents lblOnOrder As System.Windows.Forms.Label
    Friend WithEvents txtAllocated As System.Windows.Forms.TextBox
    Friend WithEvents lblAllocated As System.Windows.Forms.Label
    Friend WithEvents txtFreeStock As System.Windows.Forms.TextBox
    Friend WithEvents lblFreeStock As System.Windows.Forms.Label
    Friend WithEvents lblIn As System.Windows.Forms.Label
    Friend WithEvents lblOut As System.Windows.Forms.Label
End Class
