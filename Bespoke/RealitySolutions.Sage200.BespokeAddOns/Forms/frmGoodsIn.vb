Option Explicit On
Option Strict On

Imports System
Imports System.Windows.Forms
Imports RealitySolutions.Licensing.Core.Client

Public Class frmGoodsIn
    Private bolBlockEditLine As Boolean = True
    Private bolCurrentlySaving As Boolean = False


#Region " ENUM: logType() "
    Public Enum logType
        removeLine = 1
        addLine = 2
        changeUOM = 3
    End Enum
#End Region


#Region " SUBROUTINE: frmGoodsIn_Load() "
    Private Sub frmGoodsIn_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        SetupDirectSQLConnection()


        ' Set any defaults
        dtpDate.Value = DateTime.Now()
    End Sub
#End Region


#Region " SUBROUTINE: cmdSave_Click() "
    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        ' Pre-prompt checks
        'If txtReference.Text = "" Then
        '    MsgBox("Please enter a reference.", MsgBoxStyle.Exclamation, "No Reference")

        '    txtReference.Focus()
        '    Exit Sub
        'End If


        If MsgBox("Are you sure you want to save?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.Yes Then
            Dim oActiveLock As Sage.Accounting.Common.ActiveLock = Nothing

            Try
                Dim lvItem As ListViewItem = Nothing

                Dim popOrder As Sage.Accounting.POP.POPOrderReturn = Nothing
                Dim popLine As Sage.Accounting.POP.POPOrderReturnLine = Nothing


                Dim intCurrentID As Int64 = 0
                Dim intRows As Integer = 0

                Dim decSellingUnit As Decimal = 0D
                Dim decAmount As Decimal = 0D


                bolCurrentlySaving = True


                Dim oUnits As Sage.Accounting.Stock.Units = Nothing
                Dim oUnit As Sage.Accounting.Stock.Unit = Nothing

                Dim oStockItemUnit As Sage.Accounting.Stock.StockItemUnit = Nothing

                Dim oStockItems As Sage.Accounting.Stock.StockItems = Nothing
                Dim oStockItem As Sage.Accounting.Stock.StockItem = Nothing


                For Each lvItem In lvDetails.Items
                    If (CDec(lvItem.SubItems(9).Text) > 0D) And (CDec(lvItem.SubItems(11).Text) > 0D) Then
                        ' Get the original POP line back - we'll adjust it later
                        popLine = DirectCast(lvItem.Tag, Sage.Accounting.POP.POPOrderReturnLine)
                        popOrder = popLine.POPOrderReturn
                        intCurrentID = CLng(popLine.POPOrderReturnLine)
                        decSellingUnit = DanRound(CDec(lvItem.SubItems(11).Text) / CDec(lvItem.SubItems(9).Text), "0.00000")

                        If IsWholeNumber(decSellingUnit) = False Then
                            decSellingUnit += 0.00001D
                        End If


                        ' NEW: Create a lock
                        oActiveLock = Sage.Accounting.Application.Lock(popOrder)

                        ' locate stock item
                        Dim oStockItems2 As New Sage.Accounting.Stock.StockItems
                        Dim oStockItem2 As Sage.Accounting.Stock.StockItem = oStockItems2(popLine.ItemCode)



                        ' edit stock item's unit 
                        Dim oStockItemUnits2 As Sage.Accounting.Stock.StockItemUnits = oStockItem2.Units
                        Dim oStockItemUnit2 As Sage.Accounting.Stock.StockItemUnit = Nothing

                        oUnits = New Sage.Accounting.Stock.Units()
                        oUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.Unit.FIELD_NAME, lvItem.SubItems(5).Text))
                        oUnit = oUnits.First

                        oStockItemUnits2.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_UNITOBJECT, oUnit))
                        oStockItemUnit2 = oStockItemUnits2.First

                        oStockItemUnits2.Dispose()
                        oStockItemUnits2 = Nothing

                        oUnit.Dispose()
                        oUnit = Nothing
                        oUnits.Dispose()
                        oUnits = Nothing


                        Dim oldMultiple As Decimal = oStockItemUnit2.MultipleOfBaseUnit
                        oStockItemUnit2.MultipleOfBaseUnit = decSellingUnit

                        oStockItemUnit2.Update()

                        POPLog(logType.changeUOM, String.Format("changed UOM from {0} to {1}", oldMultiple, decSellingUnit), CLng(popLine.POPOrderReturn.POPOrderReturn), 0)

                        ' now recreate this line on the sop
                        Dim popLine2 As New Sage.Accounting.POP.POPStandardItemLine
                        Dim popLine_Read As Sage.Accounting.POP.POPStandardItemLine = DirectCast(popLine, Sage.Accounting.POP.POPStandardItemLine)

                        Dim stockItems As New Sage.Accounting.Stock.StockItems
                        Dim stockItem As Sage.Accounting.Stock.StockItem = stockItems(popLine_Read.ItemCode)


                        ' NEW: Auto-complete the different if it's less than zero
                        If popLine_Read.LineQuantity - CDec(lvItem.SubItems(9).Text) <= 0D Then
                            lvItem.SubItems(12).Text = "Y"
                        End If


                        ' set required fields on new sopline
                        With popLine2
                            .POPOrderReturn = popOrder
                            .Item = stockItem
                            .WarehouseItem = popLine_Read.WarehouseItem
                            .BuyingUnit = popLine_Read.BuyingUnit
                            .PricingUnit = popLine_Read.PricingUnit
                            .LineQuantity = CDec(lvItem.SubItems(9).Text)
                            .UnitBuyingPrice = popLine_Read.UnitBuyingPrice
                            .UnitDiscountPercent = popLine_Read.UnitDiscountPercent
                            .TaxCode = popLine_Read.TaxCode

                            .Post(True)
                        End With

                        popOrder.Lines.Add(popLine2)
                        popOrder.Update()


                        POPLog(logType.removeLine, String.Format("add line (stock code={0}, details='{1}', qty={2}, buying unit='{3}', price='{4}', qty to book in={5}, receipt qty={6}, unit price={7}, stock item unit={8}, unit cost={9}, unit discount % ={10}, unit discount={11}, disc unit price={12})",
                               popLine2.ItemCode,
                               popLine2.ItemDescription,
                               popLine2.LineQuantity,
                               lvItem.SubItems(5).Text,
                               popLine2.LineTotalValue,
                               popLine2.LineQuantity - popLine.ReceiptReturnQuantity,
                               popLine2.ReceiptReturnQuantity,
                               popLine2.UnitBuyingPrice,
                               "",
                               0D,
                               popLine2.UnitDiscountPercent,
                               popLine2.UnitDiscountValue,
                               popLine2.UnitBuyingPrice - popLine2.UnitDiscountValue),
                               CLng(popLine2.POPOrderReturn.POPOrderReturn), CLng(popLine2.POPOrderReturnLine))

                        ' reset the stockitemunit uom multiple to its original value
                        oStockItemUnit2.MultipleOfBaseUnit = oldMultiple
                        oStockItemUnit2.Update()
                        oStockItemUnit2.Dispose()


                        POPLog(logType.changeUOM, String.Format("changed UOM back to {0}", oldMultiple), CLng(popLine.POPOrderReturn.POPOrderReturn), 0)


                        ' NEW: If not complete re-create another item with the difference on it
                        If lvItem.SubItems(12).Text = "N" Then
                            Dim popLine3 As New Sage.Accounting.POP.POPStandardItemLine

                            With popLine3
                                ' NEW: The order these are done in is CRITICAL!!!
                                .POPOrderReturn = popOrder
                                .Item = stockItem
                                .WarehouseItem = popLine_Read.WarehouseItem
                                .BuyingUnit = popLine_Read.BuyingUnit
                                .PricingUnit = popLine_Read.PricingUnit
                                .LineQuantity = popLine_Read.LineQuantity - CDec(lvItem.SubItems(9).Text)
                                .UnitBuyingPrice = popLine_Read.UnitBuyingPrice
                                .UnitDiscountPercent = popLine_Read.UnitDiscountPercent
                                .TaxCode = popLine_Read.TaxCode
                                .PrintSequenceNumber = popLine_Read.PrintSequenceNumber

                                .Post(True)
                            End With

                            popOrder.Lines.Add(popLine3)
                            popOrder.Update()
                        End If

                        popLine.Delete()
                        popOrder.Post()


                        ' NEW: Fire the GI every time...
                        Dim oPOPOrders As New Sage.Accounting.POP.POPOrders

                        oPOPOrders.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.POP.POPOrder.FIELD_DOCUMENTNO, popOrder.DocumentNo))
                        oPOPOrders.Find()

                        Dim oPOPOrder As Sage.Accounting.POP.POPOrder = oPOPOrders.First

                        If oPOPOrder Is Nothing Then
                            Throw New Exception("Unable to locate the POP order for updating the values.  Please try opening the order yourself and click on 'Save'.")
                        End If

                        With oPOPOrder
                            .CalculateValues()
                            .Update()
                        End With


                        ' NEW: Remove the lock
                        If Not oActiveLock Is Nothing Then
                            oActiveLock.Dispose()
                        End If



                        ' NEW: Fire up Sage's own dispatch form
                        Dim oArray(0) As Object
                        oArray(0) = popOrder.POPOrderReturn

                        Dim Args As New Sage.Common.UI.FormArguments(oArray, GetType(Sage.Accounting.POP.POPOrder), Sage.Common.CustomAttributes.eParameterType.DbKey)
                        Dim frmX As New Sage.MMS.POP.ConfirmGoodsReceivedForm(Args)

                        With frmX
                            .ShowDialog()

                            .Dispose()
                        End With
                        frmX = Nothing
                        oPOPOrder = Nothing
                    End If
                Next


                bolCurrentlySaving = False

                Me.Close()
                Exit Sub

            Catch ex As Exception
                MsgBox("An error has occurred while saving the goods in." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
                Exit Sub
            End Try
        End If
    End Sub
#End Region

#Region " SUBROUTINE: cmdClose_Click() "
    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub
#End Region

#Region " SUBROUTINE: lvDetails_DoubleClick() "
    Private Sub lvDetails_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvDetails.DoubleClick
        If bolBlockEditLine = False Then

            If lvDetails.SelectedItems.Count = 1 Then
                Dim frmX As New frmGoodsInPopup

                With frmX
                    .ItemSelected = lvDetails.SelectedItems(0)


                    .ShowDialog()


                    ' Update the details
                    If Not frmX.ItemSelected Is Nothing Then
                        With lvDetails.SelectedItems(0)
                            .SubItems(9).Text = frmX.ItemSelected.SubItems(9).Text
                            .SubItems(11).Text = frmX.ItemSelected.SubItems(11).Text
                            .SubItems(12).Text = frmX.ItemSelected.SubItems(12).Text
                        End With
                    End If
                End With

                frmX.Dispose()
                frmX = Nothing
            End If
        End If
    End Sub
#End Region


#Region " SUBROUTINE: SupplierLookup1_SupplierSelected() "
    Private Sub SupplierLookup1_SupplierSelected(ByVal sender As Object, ByVal args As Sage.MMS.Controls.SupplierLookupItemSelectedEventArgs) Handles SupplierLookup1.SupplierSelected
        Try
            If Not (args.SelectedSupplier) Is Nothing Then
                ' Now we need to get all the POP's that this supplier has against it
                Dim supSelected As Sage.Accounting.PurchaseLedger.Supplier = args.SelectedSupplier


                ' Get the name
                txtName.Text = supSelected.Name


                ' DOESN'T WORK DUE TO A BUG IN MMS THAT I FOUND!!!!
                'Dim popOrder As Sage.Accounting.POP.POPOrder = Nothing
                'Dim popOrders As New Sage.Accounting.POP.POPOrders

                'popOrders.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.POP.POPOrder.FIELD_SUPPLIEROBJECT, supSelected))

                'MsgBox(popOrders.Count)




                Dim popAdjustment As New Sage.Accounting.POP.POPReceiptReturnAdjustment()

                With popAdjustment
                    .AdjustmentDate = DateTime.Now()
                    .DoGoodsReceived = True
                    .DoConfirmOnReceipt = True
                    '.DoStockMiscItems = True
                    .Supplier = supSelected
                    .PopulateReceiptReturns()
                End With


                Dim frmSpec As New Sage.MMS.POP.SelectSpecificOrdersForm("POPOrder", popAdjustment.ReceiptReturnItems.DiscreteOrders, popAdjustment.LockingCoordinator)

                With frmSpec
                    Select Case popAdjustment.ReceiptReturnItems.DiscreteOrders.Count
                        Case 0
                            MsgBox("There are no POP orders for this supplier.", MsgBoxStyle.Information, "No POPs")
                            Exit Sub

                        Case 1

                        Case Else
                            If .ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                                With popAdjustment
                                    .LockingCoordinator.Lock()
                                    .PopulateReceiptReturns()
                                End With
                            End If
                    End Select
                End With

                ' Kill it to release any locks
                frmSpec.Dispose()
                frmSpec = Nothing





                Dim lstItem As ListViewItem = Nothing
                lvDetails.Items.Clear()


                Dim popOrderLines As Sage.Accounting.POP.POPOrderItems = Nothing
                Dim popOrderLine As Sage.Accounting.POP.POPOrderItem = Nothing
                Dim popOrderReturnLines As Sage.Accounting.POP.POPOrderReturnLines = Nothing
                Dim popOrderReturnLine As Sage.Accounting.POP.POPOrderReturnLine = Nothing

                Dim oStockItems As Sage.Accounting.Stock.StockItems = Nothing
                Dim oStockItem As Sage.Accounting.Stock.StockItem = Nothing

                Dim oUnits As Sage.Accounting.Stock.Units = Nothing
                Dim oUnit As Sage.Accounting.Stock.Unit = Nothing

                Dim oStockItemUnit As Sage.Accounting.Stock.StockItemUnit = Nothing


                Dim recReturn As Sage.Accounting.POP.POPReceiptReturnItem = Nothing


                Dim htbDuplicates As New Hashtable ' I'm getting duplicates, but the developers at Sages aren't - so cheat it!

                For Each recReturn In popAdjustment.ReceiptReturnItems
                    popOrderLines = New Sage.Accounting.POP.POPOrderItems

                    With popOrderLines.Query.Filters
                        .Add(New Sage.ObjectStore.Filter(Sage.Accounting.POP.POPOrderItem.FIELD_DOCUMENTNO, recReturn.OrderReturnNo))

                        .Add(New Sage.ObjectStore.Filter(Sage.Accounting.POP.POPOrderItem.FIELD_ACCOUNTREFERENCE, supSelected.Reference))
                        .Add(New Sage.ObjectStore.Filter(Sage.Accounting.POP.POPOrderItem.FIELD_DOCUMENTSTATUS, Sage.Accounting.OrderProcessing.DocumentStatusEnum.EnumDocumentStatusLive))
                    End With

                    popOrderLines.Query.Find()
                    popOrderLines.Find()



                    For Each popOrderLine In popOrderLines
                        ' Refresh the lines
                        popOrderReturnLines = New Sage.Accounting.POP.POPOrderReturnLines

                        With popOrderReturnLines.Query.Filters
                            .Add(New Sage.ObjectStore.Filter(Sage.Accounting.POP.POPOrderReturnLine.FIELD_POPORDERRETURNDBKEY, recReturn.POPOrderReturnID))
                            .Add(New Sage.ObjectStore.Filter(Sage.Accounting.POP.POPOrderReturnLine.FIELD_ONORDERQUANTITY, Sage.Common.Data.FilterOperator.GreaterThan, 0D))
                        End With

                        popOrderReturnLines.Query.Find()
                        popOrderReturnLines.Find()



                        ' Now we need to get the item details
                        For Each popOrderReturnLine In popOrderReturnLines

                            If htbDuplicates.ContainsKey(popOrderReturnLine.POPOrderReturnLine) = False Then
                                lstItem = lvDetails.Items.Add(popOrderLine.DocumentNo)
                                lstItem.Tag = popOrderReturnLine

                                With lstItem.SubItems
                                    .Add(Format(popOrderLine.DocumentDate, "dd/MM/yyyy"))
                                    .Add(recReturn.Supplier.Name)
                                    .Add(popOrderReturnLine.ItemCode)
                                    .Add(popOrderReturnLine.ItemDescription)
                                    .Add(popOrderReturnLine.BuyingUnitDescription)

                                    'Select Case popOrderLine.DocumentStatus
                                    '    Case Sage.Accounting.OrderProcessing.DocumentStatusEnum.EnumDocumentStatusCancelled
                                    '        .Add("Cancelled")

                                    '    Case Sage.Accounting.OrderProcessing.DocumentStatusEnum.EnumDocumentStatusComplete
                                    '        .Add("Complete")

                                    '    Case Sage.Accounting.OrderProcessing.DocumentStatusEnum.EnumDocumentStatusDispute
                                    '        .Add("Dispute")

                                    '    Case Sage.Accounting.OrderProcessing.DocumentStatusEnum.EnumDocumentStatusLive
                                    '        .Add("Live")

                                    '    Case Sage.Accounting.OrderProcessing.DocumentStatusEnum.EnumDocumentStatusOnHold
                                    '        .Add("On Hold")
                                    'End Select


                                    .Add(Format(popOrderReturnLine.LineQuantity, "0.00000"))
                                    .Add(Format(popOrderReturnLine.ReceiptReturnQuantity, "0.00000"))


                                    .Add(Format(popOrderReturnLine.OnOrderQuantity, "0.00000"))
                                    .Add("0.00000")


                                    ' Get the unit
                                    oStockItems = New Sage.Accounting.Stock.StockItems
                                    oStockItem = oStockItems(popOrderReturnLine.ItemCode)

                                    oUnits = New Sage.Accounting.Stock.Units

                                    oUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.Unit.FIELD_NAME, popOrderReturnLine.BuyingUnitDescription))
                                    oUnit = oUnits.First

                                    oStockItem.Units.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_UNITOBJECT, oUnit))
                                    oStockItemUnit = oStockItem.Units.First

                                    .Add(Format(popOrderReturnLine.OnOrderQuantity * oStockItemUnit.MultipleOfBaseUnit, "0.00000"))
                                    .Add("0.00000")

                                    .Add("N")
                                End With


                                htbDuplicates.Add(popOrderReturnLine.POPOrderReturnLine, popOrderReturnLine.POPOrderReturnLine)
                            End If
                        Next
                    Next
                Next


                ' Dispose when done
                recReturn.Dispose()

                popOrderLines.Dispose()
                popOrderLine.Dispose()
                popOrderReturnLines.Dispose()
                popOrderReturnLine.Dispose()
                popAdjustment.Dispose()

                oUnits.Dispose()
                oUnit.Dispose()

                oStockItems.Dispose()
                oStockItem.Dispose()
                oStockItemUnit.Dispose()


                recReturn = Nothing

                popOrderLines = Nothing
                popOrderLine = Nothing
                popOrderReturnLines = Nothing
                popOrderReturnLine = Nothing
                popAdjustment = Nothing

                oUnits = Nothing
                oUnit = Nothing

                oStockItems = Nothing
                oStockItem = Nothing
                oStockItemUnit = Nothing

                htbDuplicates = Nothing


                bolBlockEditLine = False

            Else
                bolBlockEditLine = True
            End If

        Catch ex As Exception
            bolBlockEditLine = True
            cmdSave.Enabled = False


            MsgBox("An error has occurred while loading the POP order lines to the grid." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region


#Region " SUBROUTINE: POPLog() "
    Private Sub POPLog(ByVal type As Integer, ByVal message As String, ByVal POPOrderReturnId As Int64, ByVal POPOrderReturnLineId As Int64)
        Dim s As New SqlClient.SqlCommand("INSERT INTO tblPOPNotes(LogType, Message, POPOrderReturnID, POPOrderReturnLineID, Date, SageUser) " & _
                                          "VALUES(@LogType, @Message, @POPOrderReturnID, @POPOrderReturnLineID, @Date, @SageUser)", sqlConnToSage)

        With s.Parameters
            .AddWithValue("LogType", type)
            .AddWithValue("Message", message)
            .AddWithValue("POPOrderReturnId", POPOrderReturnId)
            .AddWithValue("POPOrderReturnLineId", POPOrderReturnLineId)
            .AddWithValue("Date", Format(DateTime.Now(), "yyyy-MM-dd HH:mm:ss"))
            .AddWithValue("SageUser", Sage.Accounting.Application.ActiveUserName)
        End With

        s.ExecuteNonQuery()
        s.Dispose()
        s = Nothing
    End Sub
#End Region

End Class