﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSOPPrintPickingLists
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.fraSelection = New System.Windows.Forms.GroupBox
        Me.chkReprint = New System.Windows.Forms.CheckBox
        Me.lblReprint = New System.Windows.Forms.Label
        Me.cmdDisplay = New System.Windows.Forms.Button
        Me.optAllOrders = New System.Windows.Forms.RadioButton
        Me.fraDateRange = New System.Windows.Forms.GroupBox
        Me.dtpConfirmedDelDate = New System.Windows.Forms.DateTimePicker
        Me.lblConfirmedDelDate = New System.Windows.Forms.Label
        Me.dtpOrderDate = New System.Windows.Forms.DateTimePicker
        Me.lblOrderDate = New System.Windows.Forms.Label
        Me.lvOrders = New System.Windows.Forms.ListView
        Me.chOrderNo = New System.Windows.Forms.ColumnHeader
        Me.chOrderDate = New System.Windows.Forms.ColumnHeader
        Me.chACRef = New System.Windows.Forms.ColumnHeader
        Me.chCustomerName = New System.Windows.Forms.ColumnHeader
        Me.chCustomerOrderNo = New System.Windows.Forms.ColumnHeader
        Me.chValue = New System.Windows.Forms.ColumnHeader
        Me.cmdPrint = New System.Windows.Forms.Button
        Me.cmdPrintAll = New System.Windows.Forms.Button
        Me.cmdSelectAll = New System.Windows.Forms.Button
        Me.cmdDeselectAll = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.lblNoOfOrders = New System.Windows.Forms.Label
        Me.lblTotalValue = New System.Windows.Forms.Label
        Me.txtNoOfOrders = New System.Windows.Forms.TextBox
        Me.txtTotalValue = New System.Windows.Forms.TextBox
        Me.fraSelection.SuspendLayout()
        Me.fraDateRange.SuspendLayout()
        Me.SuspendLayout()
        '
        'fraSelection
        '
        Me.fraSelection.Controls.Add(Me.chkReprint)
        Me.fraSelection.Controls.Add(Me.lblReprint)
        Me.fraSelection.Controls.Add(Me.cmdDisplay)
        Me.fraSelection.Controls.Add(Me.optAllOrders)
        Me.fraSelection.Location = New System.Drawing.Point(8, 8)
        Me.fraSelection.Name = "fraSelection"
        Me.fraSelection.Size = New System.Drawing.Size(780, 52)
        Me.fraSelection.TabIndex = 0
        Me.fraSelection.TabStop = False
        Me.fraSelection.Text = "Select order for printing picking lists for..."
        '
        'chkReprint
        '
        Me.chkReprint.AutoSize = True
        Me.chkReprint.Location = New System.Drawing.Point(160, 28)
        Me.chkReprint.Name = "chkReprint"
        Me.chkReprint.Size = New System.Drawing.Size(15, 14)
        Me.chkReprint.TabIndex = 7
        Me.chkReprint.UseVisualStyleBackColor = True
        '
        'lblReprint
        '
        Me.lblReprint.AutoSize = True
        Me.lblReprint.Location = New System.Drawing.Point(96, 28)
        Me.lblReprint.Name = "lblReprint"
        Me.lblReprint.Size = New System.Drawing.Size(50, 13)
        Me.lblReprint.TabIndex = 6
        Me.lblReprint.Text = "Re-print:"
        '
        'cmdDisplay
        '
        Me.cmdDisplay.Location = New System.Drawing.Point(696, 20)
        Me.cmdDisplay.Name = "cmdDisplay"
        Me.cmdDisplay.Size = New System.Drawing.Size(75, 23)
        Me.cmdDisplay.TabIndex = 5
        Me.cmdDisplay.Text = "&Display"
        Me.cmdDisplay.UseVisualStyleBackColor = True
        '
        'optAllOrders
        '
        Me.optAllOrders.AutoSize = True
        Me.optAllOrders.Checked = True
        Me.optAllOrders.Location = New System.Drawing.Point(16, 24)
        Me.optAllOrders.Name = "optAllOrders"
        Me.optAllOrders.Size = New System.Drawing.Size(70, 17)
        Me.optAllOrders.TabIndex = 0
        Me.optAllOrders.TabStop = True
        Me.optAllOrders.Text = "All orders"
        Me.optAllOrders.UseVisualStyleBackColor = True
        '
        'fraDateRange
        '
        Me.fraDateRange.Controls.Add(Me.dtpConfirmedDelDate)
        Me.fraDateRange.Controls.Add(Me.lblConfirmedDelDate)
        Me.fraDateRange.Controls.Add(Me.dtpOrderDate)
        Me.fraDateRange.Controls.Add(Me.lblOrderDate)
        Me.fraDateRange.Location = New System.Drawing.Point(8, 64)
        Me.fraDateRange.Name = "fraDateRange"
        Me.fraDateRange.Size = New System.Drawing.Size(780, 48)
        Me.fraDateRange.TabIndex = 1
        Me.fraDateRange.TabStop = False
        Me.fraDateRange.Text = "Date range"
        '
        'dtpConfirmedDelDate
        '
        Me.dtpConfirmedDelDate.Checked = False
        Me.dtpConfirmedDelDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpConfirmedDelDate.Location = New System.Drawing.Point(484, 20)
        Me.dtpConfirmedDelDate.Name = "dtpConfirmedDelDate"
        Me.dtpConfirmedDelDate.ShowCheckBox = True
        Me.dtpConfirmedDelDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpConfirmedDelDate.TabIndex = 3
        '
        'lblConfirmedDelDate
        '
        Me.lblConfirmedDelDate.AutoSize = True
        Me.lblConfirmedDelDate.Location = New System.Drawing.Point(352, 24)
        Me.lblConfirmedDelDate.Name = "lblConfirmedDelDate"
        Me.lblConfirmedDelDate.Size = New System.Drawing.Size(126, 13)
        Me.lblConfirmedDelDate.TabIndex = 2
        Me.lblConfirmedDelDate.Text = "Confirmed delivery date:"
        '
        'dtpOrderDate
        '
        Me.dtpOrderDate.Checked = False
        Me.dtpOrderDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpOrderDate.Location = New System.Drawing.Point(132, 20)
        Me.dtpOrderDate.Name = "dtpOrderDate"
        Me.dtpOrderDate.ShowCheckBox = True
        Me.dtpOrderDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpOrderDate.TabIndex = 1
        '
        'lblOrderDate
        '
        Me.lblOrderDate.AutoSize = True
        Me.lblOrderDate.Location = New System.Drawing.Point(8, 24)
        Me.lblOrderDate.Name = "lblOrderDate"
        Me.lblOrderDate.Size = New System.Drawing.Size(64, 13)
        Me.lblOrderDate.TabIndex = 0
        Me.lblOrderDate.Text = "Order date:"
        '
        'lvOrders
        '
        Me.lvOrders.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.chOrderNo, Me.chOrderDate, Me.chACRef, Me.chCustomerName, Me.chCustomerOrderNo, Me.chValue})
        Me.lvOrders.FullRowSelect = True
        Me.lvOrders.GridLines = True
        Me.lvOrders.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lvOrders.HideSelection = False
        Me.lvOrders.Location = New System.Drawing.Point(8, 120)
        Me.lvOrders.Name = "lvOrders"
        Me.lvOrders.Size = New System.Drawing.Size(780, 388)
        Me.lvOrders.TabIndex = 2
        Me.lvOrders.UseCompatibleStateImageBehavior = False
        Me.lvOrders.View = System.Windows.Forms.View.Details
        '
        'chOrderNo
        '
        Me.chOrderNo.Text = "Order No"
        Me.chOrderNo.Width = 90
        '
        'chOrderDate
        '
        Me.chOrderDate.Text = "Order Date"
        Me.chOrderDate.Width = 80
        '
        'chACRef
        '
        Me.chACRef.Text = "A/C Ref"
        Me.chACRef.Width = 80
        '
        'chCustomerName
        '
        Me.chCustomerName.Text = "Customer Name"
        Me.chCustomerName.Width = 230
        '
        'chCustomerOrderNo
        '
        Me.chCustomerOrderNo.Text = "Customer Order No"
        Me.chCustomerOrderNo.Width = 180
        '
        'chValue
        '
        Me.chValue.Text = "Value"
        Me.chValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chValue.Width = 90
        '
        'cmdPrint
        '
        Me.cmdPrint.Location = New System.Drawing.Point(8, 540)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.Size = New System.Drawing.Size(75, 23)
        Me.cmdPrint.TabIndex = 3
        Me.cmdPrint.Text = "&Print"
        Me.cmdPrint.UseVisualStyleBackColor = True
        '
        'cmdPrintAll
        '
        Me.cmdPrintAll.Location = New System.Drawing.Point(84, 540)
        Me.cmdPrintAll.Name = "cmdPrintAll"
        Me.cmdPrintAll.Size = New System.Drawing.Size(75, 23)
        Me.cmdPrintAll.TabIndex = 4
        Me.cmdPrintAll.Text = "Print &All"
        Me.cmdPrintAll.UseVisualStyleBackColor = True
        '
        'cmdSelectAll
        '
        Me.cmdSelectAll.Location = New System.Drawing.Point(160, 540)
        Me.cmdSelectAll.Name = "cmdSelectAll"
        Me.cmdSelectAll.Size = New System.Drawing.Size(75, 23)
        Me.cmdSelectAll.TabIndex = 5
        Me.cmdSelectAll.Text = "&Select All"
        Me.cmdSelectAll.UseVisualStyleBackColor = True
        '
        'cmdDeselectAll
        '
        Me.cmdDeselectAll.Location = New System.Drawing.Point(236, 540)
        Me.cmdDeselectAll.Name = "cmdDeselectAll"
        Me.cmdDeselectAll.Size = New System.Drawing.Size(75, 23)
        Me.cmdDeselectAll.TabIndex = 6
        Me.cmdDeselectAll.Text = "D&eselect All"
        Me.cmdDeselectAll.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Location = New System.Drawing.Point(716, 540)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 7
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'lblNoOfOrders
        '
        Me.lblNoOfOrders.AutoSize = True
        Me.lblNoOfOrders.Location = New System.Drawing.Point(8, 516)
        Me.lblNoOfOrders.Name = "lblNoOfOrders"
        Me.lblNoOfOrders.Size = New System.Drawing.Size(73, 13)
        Me.lblNoOfOrders.TabIndex = 9
        Me.lblNoOfOrders.Text = "No of Orders:"
        '
        'lblTotalValue
        '
        Me.lblTotalValue.AutoSize = True
        Me.lblTotalValue.Location = New System.Drawing.Point(600, 516)
        Me.lblTotalValue.Name = "lblTotalValue"
        Me.lblTotalValue.Size = New System.Drawing.Size(64, 13)
        Me.lblTotalValue.TabIndex = 10
        Me.lblTotalValue.Text = "Total Value:"
        '
        'txtNoOfOrders
        '
        Me.txtNoOfOrders.Location = New System.Drawing.Point(100, 512)
        Me.txtNoOfOrders.Name = "txtNoOfOrders"
        Me.txtNoOfOrders.ReadOnly = True
        Me.txtNoOfOrders.Size = New System.Drawing.Size(80, 21)
        Me.txtNoOfOrders.TabIndex = 11
        Me.txtNoOfOrders.Text = "0"
        Me.txtNoOfOrders.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalValue
        '
        Me.txtTotalValue.Location = New System.Drawing.Point(668, 512)
        Me.txtTotalValue.Name = "txtTotalValue"
        Me.txtTotalValue.ReadOnly = True
        Me.txtTotalValue.Size = New System.Drawing.Size(92, 21)
        Me.txtTotalValue.TabIndex = 12
        Me.txtTotalValue.Text = "0.00"
        Me.txtTotalValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'frmSOPPrintPickingLists
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(794, 568)
        Me.Controls.Add(Me.txtTotalValue)
        Me.Controls.Add(Me.txtNoOfOrders)
        Me.Controls.Add(Me.lblTotalValue)
        Me.Controls.Add(Me.lblNoOfOrders)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdDeselectAll)
        Me.Controls.Add(Me.cmdSelectAll)
        Me.Controls.Add(Me.cmdPrintAll)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.lvOrders)
        Me.Controls.Add(Me.fraDateRange)
        Me.Controls.Add(Me.fraSelection)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmSOPPrintPickingLists"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SOP - Print Picking Lists"
        Me.fraSelection.ResumeLayout(False)
        Me.fraSelection.PerformLayout()
        Me.fraDateRange.ResumeLayout(False)
        Me.fraDateRange.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents fraSelection As System.Windows.Forms.GroupBox
    Friend WithEvents cmdDisplay As System.Windows.Forms.Button
    Friend WithEvents optAllOrders As System.Windows.Forms.RadioButton
    Friend WithEvents fraDateRange As System.Windows.Forms.GroupBox
    Friend WithEvents dtpConfirmedDelDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblConfirmedDelDate As System.Windows.Forms.Label
    Friend WithEvents dtpOrderDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblOrderDate As System.Windows.Forms.Label
    Friend WithEvents lvOrders As System.Windows.Forms.ListView
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents cmdPrintAll As System.Windows.Forms.Button
    Friend WithEvents cmdSelectAll As System.Windows.Forms.Button
    Friend WithEvents cmdDeselectAll As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents chOrderNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents chOrderDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents chACRef As System.Windows.Forms.ColumnHeader
    Friend WithEvents chCustomerName As System.Windows.Forms.ColumnHeader
    Friend WithEvents chCustomerOrderNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents chValue As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkReprint As System.Windows.Forms.CheckBox
    Friend WithEvents lblReprint As System.Windows.Forms.Label
    Friend WithEvents lblNoOfOrders As System.Windows.Forms.Label
    Friend WithEvents lblTotalValue As System.Windows.Forms.Label
    Friend WithEvents txtNoOfOrders As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalValue As System.Windows.Forms.TextBox
End Class
