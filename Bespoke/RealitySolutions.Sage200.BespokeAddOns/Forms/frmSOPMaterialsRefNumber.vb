Option Explicit On
Option Strict On
Imports RealitySolutions.Licensing.Core.Client

Public Class frmSOPMaterialsRefNumber
    Private strReference As String = ""

#Region " PROPERTY: Reference() "
    Public Property Reference() As String
        Get
            Return strReference
        End Get
        Set(ByVal value As String)
            strReference = value
        End Set
    End Property
#End Region


#Region " SUBROUTINE: frmSOPMaterialsRefNumber_Load() "
    Private Sub frmSOPMaterialsRefNumber_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        txtReference.Text = strReference
    End Sub
#End Region


#Region " SUBROUTINE: cmdOK_Click() "
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        strReference = txtReference.Text
        Me.Close()
    End Sub
#End Region

#Region " SUBROUTINE: cmdCancel_Click() "
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        strReference = Nothing
        Me.Close()
    End Sub
#End Region

End Class