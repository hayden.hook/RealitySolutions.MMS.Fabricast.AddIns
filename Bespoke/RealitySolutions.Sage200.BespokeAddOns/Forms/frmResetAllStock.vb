Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports RealitySolutions.Licensing.Core.Client

Public Class frmResetAllStock

#Region " SUBROUTINE: frmResetAllStock_Load() "
    Private Sub frmResetAllStock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        lblInfo.Text = "By clicking on this button, you'll set the stock level" & vbCrLf & "to 100000 for all stock items."
    End Sub
#End Region


#Region " SUBROUTINE: cmdResetStockLevels_Click() "
    Private Sub cmdResetStockLevels_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdResetStockLevels.Click
        If MsgBox("Are you sure you want to reset the stock level to 100000 for all stock items?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.Yes Then
            ' Screw this, let's do the SQL way!
            Try
                'Dim sqlComm As New SqlCommand("UPDATE WarehouseItem SET ConfirmedQtyInStock = 100000", sqlConnection)
                'sqlComm.ExecuteNonQuery()

                'sqlComm = New SqlCommand("UPDATE BinItem SET ConfirmedQtyInStock = 100000", sqlConnection)
                'sqlComm.ExecuteNonQuery()

                'sqlComm = New SqlCommand("UPDATE StockItem SET FreeStockQuantity = 100000", sqlConnection)
                'sqlComm.ExecuteNonQuery()

                'Try
                Dim oStockItems As New Sage.Accounting.Stock.StockItems
                Dim oStockItem As Sage.Accounting.Stock.StockItem = Nothing


                ' Find the default internal area
                Dim iRs As New Sage.Accounting.Stock.InternalAreas
                Dim iR As Sage.Accounting.Stock.InternalArea = Nothing

                For Each iR In iRs
                    If iR.ThisIsDefault = True Then
                        Exit For
                    End If
                Next

                ' Make sure we have something
                If iR Is Nothing Then
                    Throw New Exception("Failed to find any default internal areas.")
                End If


                oStockItems.Query.Sorts.Add(New Sage.ObjectStore.Sort("Code", True))


                For Each oStockItem In oStockItems

                    Using oTransaction As New Sage.ObjectStore.PersistentObjectTransaction
                        Dim oWarehouses As Sage.Accounting.Stock.WarehouseItems = oStockItem.Warehouses
                        Dim oWarehouse As Sage.Accounting.Stock.WarehouseItem = Nothing

                        For Each oWarehouse In oWarehouses
                            Dim oBins As Sage.Accounting.Stock.BinItems = oWarehouse.Bins
                            Dim oBin As Sage.Accounting.Stock.BinItem = Nothing

                            For Each oBin In oBins
                                Dim bolBelowZero As Boolean = False

                                If oBin.ConfirmedQtyInStock < 0D Then
                                    oBin.ConfirmedQtyInStock = CDec(-oBin.ConfirmedQtyInStock)
                                    oBin.Update()

                                    bolBelowZero = True
                                End If


                                If (bolBelowZero = False) And (oBin.ConfirmedQtyInStock > 0D) Then
                                    Dim oWriteOff As New Sage.Accounting.Stock.StockWriteOffMovementActivity

                                    'MsgBox(oStockItem.Code & vbCrLf _
                                    '     & oWarehouse.Warehouse.Name & vbCrLf _
                                    '     & oBin.BinName & vbCrLf _
                                    '     & oBin.ConfirmedQtyInStock)

                                    Sage.Accounting.Application.AllowableWarnings.Add(oWriteOff, GetType(Sage.Accounting.Exceptions.StockLevelBelowZeroException))
                                    Sage.Accounting.Application.AllowableWarnings.Add(oWriteOff, GetType(Sage.Accounting.Exceptions.StockItemHoldingBelowReorderLevelException))
                                    Sage.Accounting.Application.AllowableWarnings.Add(oWriteOff, GetType(Sage.Accounting.Exceptions.StockItemHoldingBelowMinimumLevelException))
                                    Sage.Accounting.Application.AllowableWarnings.Add(oWriteOff, GetType(Sage.Accounting.Exceptions.InsufficientFreeStockException))

                                    With oWriteOff
                                        .StockItem = oStockItem
                                        .WarehouseItem = oWarehouse
                                        .Location = oBin

                                        .Quantity = oBin.ConfirmedQtyInStock

                                        .Recipient = iR
                                        .ActivityDate = DateTime.Now()

                                        .Reference = "STOCK RESET"

                                        .Update()
                                    End With
                                End If


                                ' Reset level
                                Dim oActivity As New Sage.Accounting.Stock.StockDiscoveryGoodsInMovementActivity

                                Sage.Accounting.Application.AllowableWarnings.Add(oActivity, GetType(Sage.Accounting.Exceptions.StockItemHoldingExceedsMaximumException))

                                With oActivity
                                    .StockItem = oStockItem
                                    .WarehouseItem = oWarehouse
                                    .Location = oBin

                                    .Quantity = 100000D

                                    .ActivityDate = DateTime.Now()

                                    .Reference = "STOCK RESET"


                                    ' Save the stock addition to the database.
                                    Try
                                        oActivity.Update()
                                        oTransaction.Commit()

                                    Catch

                                    End Try
                                End With
                            Next
                        Next
                    End Using
                Next

                MsgBox("All stock levels have been reset to 100000.", MsgBoxStyle.Information, "Success")
                Exit Sub

            Catch ex As Exception
                MsgBox("An error has occurred while resetting the stock levels." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
                Exit Sub
            End Try
        End If
    End Sub
#End Region

#Region " SUBROUTINE: cmdResetPrecision_Click() "
    Private Sub cmdResetPrecision_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdResetPrecision.Click
        Dim oUnits As New Sage.Accounting.Stock.StockItemUnits

        For Each oUnit As Sage.Accounting.Stock.StockItemUnit In oUnits
            Dim decPrecision As Decimal = 0D

            With oUnit
                decPrecision = .UnitPrecision

                Try
                    .UnitPrecision = decPrecision

                Catch
                    ' It does gets set, it's not an allowable exception
                End Try

                .Update()
                .Dispose()
            End With
        Next

        MsgBox("Stock UoM precision has been reset.", MsgBoxStyle.Information, "All Done")
        Exit Sub
    End Sub
#End Region

End Class