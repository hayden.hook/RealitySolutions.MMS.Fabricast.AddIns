﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStockItemUnitFactor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblStockCode = New System.Windows.Forms.Label
        Me.lblBaseUnit = New System.Windows.Forms.Label
        Me.lblFactor = New System.Windows.Forms.Label
        Me.txtStockCode = New Sage.Common.Controls.MaskedTextBox
        Me.txtBaseUnit = New Sage.Common.Controls.MaskedTextBox
        Me.txtFactor = New Sage.Common.Controls.MaskedTextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtStockUnit = New Sage.Common.Controls.MaskedTextBox
        Me.lblStockUnit = New System.Windows.Forms.Label
        Me.txtDescription = New Sage.Common.Controls.MaskedTextBox
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtSellingUnit = New Sage.Common.Controls.MaskedTextBox
        Me.lblSellingUnit = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'lblStockCode
        '
        Me.lblStockCode.AutoSize = True
        Me.lblStockCode.Location = New System.Drawing.Point(8, 8)
        Me.lblStockCode.Name = "lblStockCode"
        Me.lblStockCode.Size = New System.Drawing.Size(65, 13)
        Me.lblStockCode.TabIndex = 0
        Me.lblStockCode.Text = "Stock Code:"
        '
        'lblBaseUnit
        '
        Me.lblBaseUnit.AutoSize = True
        Me.lblBaseUnit.Location = New System.Drawing.Point(8, 56)
        Me.lblBaseUnit.Name = "lblBaseUnit"
        Me.lblBaseUnit.Size = New System.Drawing.Size(56, 13)
        Me.lblBaseUnit.TabIndex = 1
        Me.lblBaseUnit.Text = "Base Unit:"
        '
        'lblFactor
        '
        Me.lblFactor.AutoSize = True
        Me.lblFactor.Location = New System.Drawing.Point(8, 128)
        Me.lblFactor.Name = "lblFactor"
        Me.lblFactor.Size = New System.Drawing.Size(42, 13)
        Me.lblFactor.TabIndex = 2
        Me.lblFactor.Text = "Factor:"
        '
        'txtStockCode
        '
        Me.txtStockCode.BackColor = System.Drawing.SystemColors.Window
        Me.txtStockCode.Enabled = False
        Me.txtStockCode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtStockCode.Location = New System.Drawing.Point(80, 4)
        Me.txtStockCode.Name = "txtStockCode"
        Me.txtStockCode.ReadOnly = True
        Me.txtStockCode.Size = New System.Drawing.Size(120, 21)
        Me.txtStockCode.TabIndex = 3
        '
        'txtBaseUnit
        '
        Me.txtBaseUnit.BackColor = System.Drawing.SystemColors.Window
        Me.txtBaseUnit.Enabled = False
        Me.txtBaseUnit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBaseUnit.Location = New System.Drawing.Point(80, 52)
        Me.txtBaseUnit.Name = "txtBaseUnit"
        Me.txtBaseUnit.ReadOnly = True
        Me.txtBaseUnit.Size = New System.Drawing.Size(120, 21)
        Me.txtBaseUnit.TabIndex = 4
        '
        'txtFactor
        '
        Me.txtFactor.BackColor = System.Drawing.SystemColors.Window
        Me.txtFactor.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFactor.Location = New System.Drawing.Point(80, 124)
        Me.txtFactor.Name = "txtFactor"
        Me.txtFactor.Size = New System.Drawing.Size(120, 21)
        Me.txtFactor.TabIndex = 5
        Me.txtFactor.Text = "0.00000"
        Me.txtFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmdOK
        '
        Me.cmdOK.Location = New System.Drawing.Point(4, 152)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 6
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Location = New System.Drawing.Point(344, 152)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 7
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'txtStockUnit
        '
        Me.txtStockUnit.BackColor = System.Drawing.SystemColors.Window
        Me.txtStockUnit.Enabled = False
        Me.txtStockUnit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtStockUnit.Location = New System.Drawing.Point(80, 76)
        Me.txtStockUnit.Name = "txtStockUnit"
        Me.txtStockUnit.ReadOnly = True
        Me.txtStockUnit.Size = New System.Drawing.Size(120, 21)
        Me.txtStockUnit.TabIndex = 9
        '
        'lblStockUnit
        '
        Me.lblStockUnit.AutoSize = True
        Me.lblStockUnit.Location = New System.Drawing.Point(8, 80)
        Me.lblStockUnit.Name = "lblStockUnit"
        Me.lblStockUnit.Size = New System.Drawing.Size(59, 13)
        Me.lblStockUnit.TabIndex = 8
        Me.lblStockUnit.Text = "Stock Unit:"
        '
        'txtDescription
        '
        Me.txtDescription.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescription.Enabled = False
        Me.txtDescription.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDescription.Location = New System.Drawing.Point(80, 28)
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ReadOnly = True
        Me.txtDescription.Size = New System.Drawing.Size(340, 21)
        Me.txtDescription.TabIndex = 11
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(8, 32)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(64, 13)
        Me.lblDescription.TabIndex = 10
        Me.lblDescription.Text = "Description:"
        '
        'txtSellingUnit
        '
        Me.txtSellingUnit.BackColor = System.Drawing.SystemColors.Window
        Me.txtSellingUnit.Enabled = False
        Me.txtSellingUnit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSellingUnit.Location = New System.Drawing.Point(80, 100)
        Me.txtSellingUnit.Name = "txtSellingUnit"
        Me.txtSellingUnit.ReadOnly = True
        Me.txtSellingUnit.Size = New System.Drawing.Size(120, 21)
        Me.txtSellingUnit.TabIndex = 13
        '
        'lblSellingUnit
        '
        Me.lblSellingUnit.AutoSize = True
        Me.lblSellingUnit.Location = New System.Drawing.Point(8, 104)
        Me.lblSellingUnit.Name = "lblSellingUnit"
        Me.lblSellingUnit.Size = New System.Drawing.Size(63, 13)
        Me.lblSellingUnit.TabIndex = 12
        Me.lblSellingUnit.Text = "Selling Unit:"
        '
        'frmStockItemUnitFactor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(425, 179)
        Me.Controls.Add(Me.txtSellingUnit)
        Me.Controls.Add(Me.lblSellingUnit)
        Me.Controls.Add(Me.txtDescription)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.txtStockUnit)
        Me.Controls.Add(Me.lblStockUnit)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtFactor)
        Me.Controls.Add(Me.txtBaseUnit)
        Me.Controls.Add(Me.txtStockCode)
        Me.Controls.Add(Me.lblFactor)
        Me.Controls.Add(Me.lblBaseUnit)
        Me.Controls.Add(Me.lblStockCode)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStockItemUnitFactor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Stock Item Unit Factor"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblStockCode As System.Windows.Forms.Label
    Friend WithEvents lblBaseUnit As System.Windows.Forms.Label
    Friend WithEvents lblFactor As System.Windows.Forms.Label
    Friend WithEvents txtStockCode As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents txtBaseUnit As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents txtFactor As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents txtStockUnit As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents lblStockUnit As System.Windows.Forms.Label
    Friend WithEvents txtDescription As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtSellingUnit As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents lblSellingUnit As System.Windows.Forms.Label
End Class
