<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSOPLines
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvLines = New System.Windows.Forms.ListView
        Me.chItem = New System.Windows.Forms.ColumnHeader
        Me.chDescription = New System.Windows.Forms.ColumnHeader
        Me.chBaseQty = New System.Windows.Forms.ColumnHeader
        Me.chBaseUnit = New System.Windows.Forms.ColumnHeader
        Me.chSellingQty = New System.Windows.Forms.ColumnHeader
        Me.chSellingUnit = New System.Windows.Forms.ColumnHeader
        Me.chCost = New System.Windows.Forms.ColumnHeader
        Me.chSellingUnitPrice = New System.Windows.Forms.ColumnHeader
        Me.chTotalSP = New System.Windows.Forms.ColumnHeader
        Me.chLineTotal = New System.Windows.Forms.ColumnHeader
        Me.chProfit = New System.Windows.Forms.ColumnHeader
        Me.chProfitPercent = New System.Windows.Forms.ColumnHeader
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdClose = New System.Windows.Forms.Button
        Me.cmdEdit = New System.Windows.Forms.Button
        Me.txtCost = New System.Windows.Forms.TextBox
        Me.txtSellingPrice = New System.Windows.Forms.TextBox
        Me.txtProfit = New System.Windows.Forms.TextBox
        Me.txtProfitPercentage = New System.Windows.Forms.TextBox
        Me.txtTotalSP = New System.Windows.Forms.TextBox
        Me.cmdStockEnquiry = New System.Windows.Forms.Button
        Me.txtTotalLT = New System.Windows.Forms.TextBox
        Me.chLatestCost = New System.Windows.Forms.ColumnHeader
        Me.SuspendLayout()
        '
        'lvLines
        '
        Me.lvLines.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.chItem, Me.chDescription, Me.chBaseQty, Me.chBaseUnit, Me.chSellingQty, Me.chSellingUnit, Me.chLatestCost, Me.chCost, Me.chSellingUnitPrice, Me.chTotalSP, Me.chLineTotal, Me.chProfit, Me.chProfitPercent})
        Me.lvLines.FullRowSelect = True
        Me.lvLines.GridLines = True
        Me.lvLines.HideSelection = False
        Me.lvLines.Location = New System.Drawing.Point(4, 4)
        Me.lvLines.MultiSelect = False
        Me.lvLines.Name = "lvLines"
        Me.lvLines.Size = New System.Drawing.Size(1184, 156)
        Me.lvLines.TabIndex = 0
        Me.lvLines.UseCompatibleStateImageBehavior = False
        Me.lvLines.View = System.Windows.Forms.View.Details
        '
        'chItem
        '
        Me.chItem.Text = "Item"
        Me.chItem.Width = 100
        '
        'chDescription
        '
        Me.chDescription.Text = "Description"
        Me.chDescription.Width = 250
        '
        'chBaseQty
        '
        Me.chBaseQty.Text = "Base Qty"
        Me.chBaseQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chBaseQty.Width = 70
        '
        'chBaseUnit
        '
        Me.chBaseUnit.Text = "Base Unit"
        Me.chBaseUnit.Width = 80
        '
        'chSellingQty
        '
        Me.chSellingQty.Text = "Selling Qty"
        Me.chSellingQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chSellingQty.Width = 70
        '
        'chSellingUnit
        '
        Me.chSellingUnit.Text = "Selling Unit"
        Me.chSellingUnit.Width = 80
        '
        'chCost
        '
        Me.chCost.Text = "Line Cost"
        Me.chCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chCost.Width = 72
        '
        'chSellingUnitPrice
        '
        Me.chSellingUnitPrice.Text = "Selling Price"
        Me.chSellingUnitPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chSellingUnitPrice.Width = 72
        '
        'chTotalSP
        '
        Me.chTotalSP.Text = "Total SP"
        Me.chTotalSP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chTotalSP.Width = 72
        '
        'chLineTotal
        '
        Me.chLineTotal.Text = "Line Total"
        Me.chLineTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chLineTotal.Width = 72
        '
        'chProfit
        '
        Me.chProfit.Text = "Profit"
        Me.chProfit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chProfit.Width = 72
        '
        'chProfitPercent
        '
        Me.chProfitPercent.Text = "Profit %"
        Me.chProfitPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chProfitPercent.Width = 72
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(4, 192)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(75, 23)
        Me.cmdSave.TabIndex = 1
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdClose
        '
        Me.cmdClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdClose.Location = New System.Drawing.Point(1112, 192)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(75, 23)
        Me.cmdClose.TabIndex = 2
        Me.cmdClose.Text = "&Close"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(80, 192)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(75, 23)
        Me.cmdEdit.TabIndex = 3
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'txtCost
        '
        Me.txtCost.Location = New System.Drawing.Point(736, 164)
        Me.txtCost.Name = "txtCost"
        Me.txtCost.ReadOnly = True
        Me.txtCost.Size = New System.Drawing.Size(68, 21)
        Me.txtCost.TabIndex = 4
        Me.txtCost.Text = "0.00"
        Me.txtCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtSellingPrice
        '
        Me.txtSellingPrice.Location = New System.Drawing.Point(808, 164)
        Me.txtSellingPrice.Name = "txtSellingPrice"
        Me.txtSellingPrice.ReadOnly = True
        Me.txtSellingPrice.Size = New System.Drawing.Size(68, 21)
        Me.txtSellingPrice.TabIndex = 5
        Me.txtSellingPrice.Text = "0.00"
        Me.txtSellingPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtProfit
        '
        Me.txtProfit.Location = New System.Drawing.Point(1024, 164)
        Me.txtProfit.Name = "txtProfit"
        Me.txtProfit.ReadOnly = True
        Me.txtProfit.Size = New System.Drawing.Size(68, 21)
        Me.txtProfit.TabIndex = 6
        Me.txtProfit.Text = "0.00"
        Me.txtProfit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtProfitPercentage
        '
        Me.txtProfitPercentage.Location = New System.Drawing.Point(1096, 164)
        Me.txtProfitPercentage.Name = "txtProfitPercentage"
        Me.txtProfitPercentage.ReadOnly = True
        Me.txtProfitPercentage.Size = New System.Drawing.Size(68, 21)
        Me.txtProfitPercentage.TabIndex = 7
        Me.txtProfitPercentage.Text = "0.00"
        Me.txtProfitPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalSP
        '
        Me.txtTotalSP.Location = New System.Drawing.Point(880, 164)
        Me.txtTotalSP.Name = "txtTotalSP"
        Me.txtTotalSP.ReadOnly = True
        Me.txtTotalSP.Size = New System.Drawing.Size(68, 21)
        Me.txtTotalSP.TabIndex = 8
        Me.txtTotalSP.Text = "0.00"
        Me.txtTotalSP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmdStockEnquiry
        '
        Me.cmdStockEnquiry.Location = New System.Drawing.Point(156, 192)
        Me.cmdStockEnquiry.Name = "cmdStockEnquiry"
        Me.cmdStockEnquiry.Size = New System.Drawing.Size(116, 23)
        Me.cmdStockEnquiry.TabIndex = 9
        Me.cmdStockEnquiry.Text = "&Stock Enquiry"
        Me.cmdStockEnquiry.UseVisualStyleBackColor = True
        '
        'txtTotalLT
        '
        Me.txtTotalLT.Location = New System.Drawing.Point(952, 164)
        Me.txtTotalLT.Name = "txtTotalLT"
        Me.txtTotalLT.ReadOnly = True
        Me.txtTotalLT.Size = New System.Drawing.Size(68, 21)
        Me.txtTotalLT.TabIndex = 10
        Me.txtTotalLT.Text = "0.00"
        Me.txtTotalLT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chLatestCost
        '
        Me.chLatestCost.Text = "Latest Cost"
        Me.chLatestCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chLatestCost.Width = 80
        '
        'frmSOPLines
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdClose
        Me.ClientSize = New System.Drawing.Size(1193, 221)
        Me.Controls.Add(Me.txtTotalLT)
        Me.Controls.Add(Me.cmdStockEnquiry)
        Me.Controls.Add(Me.txtTotalSP)
        Me.Controls.Add(Me.txtProfitPercentage)
        Me.Controls.Add(Me.txtProfit)
        Me.Controls.Add(Me.txtSellingPrice)
        Me.Controls.Add(Me.txtCost)
        Me.Controls.Add(Me.cmdEdit)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.lvLines)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmSOPLines"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit SOP Lines"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chItem As System.Windows.Forms.ColumnHeader
    Friend WithEvents chDescription As System.Windows.Forms.ColumnHeader
    Friend WithEvents chSellingQty As System.Windows.Forms.ColumnHeader
    Friend WithEvents chSellingUnit As System.Windows.Forms.ColumnHeader
    Friend WithEvents chSellingUnitPrice As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents lvLines As System.Windows.Forms.ListView
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents chCost As System.Windows.Forms.ColumnHeader
    Friend WithEvents chProfit As System.Windows.Forms.ColumnHeader
    Friend WithEvents chProfitPercent As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtCost As System.Windows.Forms.TextBox
    Friend WithEvents txtSellingPrice As System.Windows.Forms.TextBox
    Friend WithEvents txtProfit As System.Windows.Forms.TextBox
    Friend WithEvents txtProfitPercentage As System.Windows.Forms.TextBox
    Friend WithEvents chTotalSP As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtTotalSP As System.Windows.Forms.TextBox
    Friend WithEvents cmdStockEnquiry As System.Windows.Forms.Button
    Friend WithEvents chBaseUnit As System.Windows.Forms.ColumnHeader
    Friend WithEvents chLineTotal As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtTotalLT As System.Windows.Forms.TextBox
    Friend WithEvents chBaseQty As System.Windows.Forms.ColumnHeader
    Friend WithEvents chLatestCost As System.Windows.Forms.ColumnHeader
End Class
