Option Explicit On
Option Strict On
Imports RealitySolutions.Licensing.Core.Client

Public Class frmSOPConfirmGoodsDispatched
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents fraSelectItems As System.Windows.Forms.GroupBox
    Friend WithEvents grdItems As Sage.Common.Controls.Grid
    Friend WithEvents fraOrderLine As System.Windows.Forms.GroupBox
    Friend WithEvents optSingleOrder As System.Windows.Forms.RadioButton
    Friend WithEvents optRangeOfOrderNos As System.Windows.Forms.RadioButton
    Friend WithEvents optRangeOfOrderDates As System.Windows.Forms.RadioButton
    Friend WithEvents optSingleCustomer As System.Windows.Forms.RadioButton
    Friend WithEvents sopOrders As Sage.MMS.Controls.SOPOrderLookup
    Friend WithEvents lblOrderNo As System.Windows.Forms.Label
    Friend WithEvents lblWarehouse As System.Windows.Forms.Label
    Friend WithEvents whlWarehouses As Sage.MMS.Controls.WarehousesLookup
    Friend WithEvents cmdDisplay As System.Windows.Forms.Button
    Friend WithEvents colOrderNo As Sage.Common.Controls.GridColumn
    Friend WithEvents colOrderDate As Sage.Common.Controls.GridColumn
    Friend WithEvents colReqDate As Sage.Common.Controls.GridColumn
    Friend WithEvents colConfDate As Sage.Common.Controls.GridColumn
    Friend WithEvents colAccRef As Sage.Common.Controls.GridColumn
    Friend WithEvents colItemCode As Sage.Common.Controls.GridColumn
    Friend WithEvents colWarehouse As Sage.Common.Controls.GridColumn
    Friend WithEvents colQtyReq As Sage.Common.Controls.GridColumn
    Friend WithEvents colQtyDispatch As Sage.Common.Controls.GridColumn
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents NumberTextBox1 As Sage.ObjectStore.Controls.NumberTextBox
    Friend WithEvents MaskedTextBox1 As Sage.ObjectStore.Controls.MaskedTextBox
    Friend WithEvents colFinished As Sage.Common.Controls.GridColumn
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.fraSelectItems = New System.Windows.Forms.GroupBox
        Me.cmdDisplay = New System.Windows.Forms.Button
        Me.whlWarehouses = New Sage.MMS.Controls.WarehousesLookup
        Me.lblWarehouse = New System.Windows.Forms.Label
        Me.lblOrderNo = New System.Windows.Forms.Label
        Me.sopOrders = New Sage.MMS.Controls.SOPOrderLookup
        Me.optSingleCustomer = New System.Windows.Forms.RadioButton
        Me.optRangeOfOrderDates = New System.Windows.Forms.RadioButton
        Me.optRangeOfOrderNos = New System.Windows.Forms.RadioButton
        Me.optSingleOrder = New System.Windows.Forms.RadioButton
        Me.grdItems = New Sage.Common.Controls.Grid
        Me.colOrderNo = New Sage.Common.Controls.GridColumn
        Me.colOrderDate = New Sage.Common.Controls.GridColumn
        Me.colReqDate = New Sage.Common.Controls.GridColumn
        Me.colConfDate = New Sage.Common.Controls.GridColumn
        Me.colAccRef = New Sage.Common.Controls.GridColumn
        Me.colItemCode = New Sage.Common.Controls.GridColumn
        Me.colWarehouse = New Sage.Common.Controls.GridColumn
        Me.colQtyReq = New Sage.Common.Controls.GridColumn
        Me.colQtyDispatch = New Sage.Common.Controls.GridColumn
        Me.colFinished = New Sage.Common.Controls.GridColumn
        Me.fraOrderLine = New System.Windows.Forms.GroupBox
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.NumberTextBox1 = New Sage.ObjectStore.Controls.NumberTextBox
        Me.MaskedTextBox1 = New Sage.ObjectStore.Controls.MaskedTextBox
        Me.fraSelectItems.SuspendLayout()
        CType(Me.whlWarehouses, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sopOrders, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'fraSelectItems
        '
        Me.fraSelectItems.Controls.Add(Me.MaskedTextBox1)
        Me.fraSelectItems.Controls.Add(Me.NumberTextBox1)
        Me.fraSelectItems.Controls.Add(Me.cmdDisplay)
        Me.fraSelectItems.Controls.Add(Me.whlWarehouses)
        Me.fraSelectItems.Controls.Add(Me.lblWarehouse)
        Me.fraSelectItems.Controls.Add(Me.lblOrderNo)
        Me.fraSelectItems.Controls.Add(Me.sopOrders)
        Me.fraSelectItems.Controls.Add(Me.optSingleCustomer)
        Me.fraSelectItems.Controls.Add(Me.optRangeOfOrderDates)
        Me.fraSelectItems.Controls.Add(Me.optRangeOfOrderNos)
        Me.fraSelectItems.Controls.Add(Me.optSingleOrder)
        Me.fraSelectItems.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.fraSelectItems.Location = New System.Drawing.Point(8, 8)
        Me.fraSelectItems.Name = "fraSelectItems"
        Me.fraSelectItems.Size = New System.Drawing.Size(856, 144)
        Me.fraSelectItems.TabIndex = 0
        Me.fraSelectItems.TabStop = False
        Me.fraSelectItems.Text = "Select items to dispatch"
        '
        'cmdDisplay
        '
        Me.cmdDisplay.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cmdDisplay.Location = New System.Drawing.Point(764, 112)
        Me.cmdDisplay.Name = "cmdDisplay"
        Me.cmdDisplay.Size = New System.Drawing.Size(84, 23)
        Me.cmdDisplay.TabIndex = 6
        Me.cmdDisplay.Text = "&Display"
        '
        'whlWarehouses
        '
        Me.whlWarehouses.AllowSinglePartialMatch = True
        Me.whlWarehouses.BackColor = System.Drawing.SystemColors.Window
        Me.whlWarehouses.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.whlWarehouses.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.whlWarehouses.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.whlWarehouses.Location = New System.Drawing.Point(568, 24)
        Me.whlWarehouses.MaxLength = 32767
        Me.whlWarehouses.Name = "whlWarehouses"
        Me.whlWarehouses.Size = New System.Drawing.Size(280, 21)
        Me.whlWarehouses.TabIndex = 3
        Me.whlWarehouses.Tooltip = ""
        Me.whlWarehouses.Value = Nothing
        '
        'lblWarehouse
        '
        Me.lblWarehouse.AutoSize = True
        Me.lblWarehouse.Location = New System.Drawing.Point(500, 28)
        Me.lblWarehouse.Name = "lblWarehouse"
        Me.lblWarehouse.Size = New System.Drawing.Size(66, 13)
        Me.lblWarehouse.TabIndex = 5
        Me.lblWarehouse.Text = "Warehouse:"
        '
        'lblOrderNo
        '
        Me.lblOrderNo.AutoSize = True
        Me.lblOrderNo.Location = New System.Drawing.Point(180, 28)
        Me.lblOrderNo.Name = "lblOrderNo"
        Me.lblOrderNo.Size = New System.Drawing.Size(54, 13)
        Me.lblOrderNo.TabIndex = 4
        Me.lblOrderNo.Text = "Order no:"
        '
        'sopOrders
        '
        Me.sopOrders.AllowSinglePartialMatch = True
        Me.sopOrders.BackColor = System.Drawing.SystemColors.Window
        Me.sopOrders.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.sopOrders.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.sopOrders.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sopOrders.Location = New System.Drawing.Point(240, 24)
        Me.sopOrders.MaxLength = 32767
        Me.sopOrders.Name = "sopOrders"
        Me.sopOrders.ShowTaxOnlyInvoices = False
        Me.sopOrders.Size = New System.Drawing.Size(132, 21)
        Me.sopOrders.TabIndex = 3
        Me.sopOrders.Tooltip = ""
        Me.sopOrders.Value = Nothing
        '
        'optSingleCustomer
        '
        Me.optSingleCustomer.Enabled = False
        Me.optSingleCustomer.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optSingleCustomer.Location = New System.Drawing.Point(12, 100)
        Me.optSingleCustomer.Name = "optSingleCustomer"
        Me.optSingleCustomer.Size = New System.Drawing.Size(140, 20)
        Me.optSingleCustomer.TabIndex = 3
        Me.optSingleCustomer.Text = "Single customer"
        '
        'optRangeOfOrderDates
        '
        Me.optRangeOfOrderDates.Enabled = False
        Me.optRangeOfOrderDates.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optRangeOfOrderDates.Location = New System.Drawing.Point(12, 76)
        Me.optRangeOfOrderDates.Name = "optRangeOfOrderDates"
        Me.optRangeOfOrderDates.Size = New System.Drawing.Size(140, 20)
        Me.optRangeOfOrderDates.TabIndex = 2
        Me.optRangeOfOrderDates.Text = "Range of order dates"
        '
        'optRangeOfOrderNos
        '
        Me.optRangeOfOrderNos.Enabled = False
        Me.optRangeOfOrderNos.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optRangeOfOrderNos.Location = New System.Drawing.Point(12, 52)
        Me.optRangeOfOrderNos.Name = "optRangeOfOrderNos"
        Me.optRangeOfOrderNos.Size = New System.Drawing.Size(140, 20)
        Me.optRangeOfOrderNos.TabIndex = 1
        Me.optRangeOfOrderNos.Text = "Range of order nos"
        '
        'optSingleOrder
        '
        Me.optSingleOrder.Checked = True
        Me.optSingleOrder.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optSingleOrder.Location = New System.Drawing.Point(12, 28)
        Me.optSingleOrder.Name = "optSingleOrder"
        Me.optSingleOrder.Size = New System.Drawing.Size(140, 20)
        Me.optSingleOrder.TabIndex = 0
        Me.optSingleOrder.TabStop = True
        Me.optSingleOrder.Text = "Single order"
        '
        'grdItems
        '
        Me.grdItems.AllowSort = True
        Me.grdItems.AllowTabNavigation = True
        Me.grdItems.BackColor = System.Drawing.SystemColors.Window
        Me.grdItems.CardWidth = 300
        Me.grdItems.CheckedDisplayMember = Nothing
        Me.grdItems.ChildMember = Nothing
        Me.grdItems.Columns.AddRange(New Sage.Common.Controls.GridColumn() {Me.colOrderNo, Me.colOrderDate, Me.colReqDate, Me.colConfDate, Me.colAccRef, Me.colItemCode, Me.colWarehouse, Me.colQtyReq, Me.colQtyDispatch, Me.colFinished})
        Me.grdItems.CustomContextMenu = Nothing
        Me.grdItems.DataSource = Me.sopOrders.Columns
        Me.grdItems.FaintHighlightColor = System.Drawing.Color.LightGray
        Me.grdItems.GroupMember = Nothing
        Me.grdItems.IsEmptyOnAnySubItem = False
        Me.grdItems.Location = New System.Drawing.Point(8, 160)
        Me.grdItems.Name = "grdItems"
        Me.grdItems.Size = New System.Drawing.Size(856, 271)
        Me.grdItems.TabIndex = 1
        Me.grdItems.Text = "Grid1"
        Me.grdItems.TotalText = "Totals"
        '
        'colOrderNo
        '
        Me.colOrderNo.AutoSize = True
        Me.colOrderNo.Caption = "Order No"
        Me.colOrderNo.DisplayMember = "DocumentNo"
        '
        'colOrderDate
        '
        Me.colOrderDate.AutoSize = True
        Me.colOrderDate.Caption = "Order Date"
        Me.colOrderDate.DisplayMember = "DocumentDate"
        '
        'colReqDate
        '
        Me.colReqDate.AutoSize = True
        Me.colReqDate.Caption = "Req'd Date"
        Me.colReqDate.DisplayMember = "RequestedDeliveryDate"
        '
        'colConfDate
        '
        Me.colConfDate.AutoSize = True
        Me.colConfDate.Caption = "Conf'd Date"
        Me.colConfDate.DisplayMember = "PromisedDeliveryDate"
        '
        'colAccRef
        '
        Me.colAccRef.AutoSize = True
        Me.colAccRef.Caption = "A/C Ref"
        Me.colAccRef.DisplayMember = "CustomerDocumentNo"
        '
        'colItemCode
        '
        Me.colItemCode.AutoSize = True
        Me.colItemCode.Caption = "Item Code"
        Me.colItemCode.DisplayMember = "ItemCode"
        '
        'colWarehouse
        '
        Me.colWarehouse.AutoSize = True
        Me.colWarehouse.Caption = "Warehouse"
        Me.colWarehouse.Formatting = Sage.Common.Controls.ColumnFormatting.YesBlank
        '
        'colQtyReq
        '
        Me.colQtyReq.Alignment = Sage.Common.Controls.CaptionAlignment.Right
        Me.colQtyReq.AutoSize = True
        Me.colQtyReq.Caption = "Qty Required"
        Me.colQtyReq.DisplayMember = "LineQuantity"
        '
        'colQtyDispatch
        '
        Me.colQtyDispatch.Alignment = Sage.Common.Controls.CaptionAlignment.Right
        Me.colQtyDispatch.AutoSize = True
        Me.colQtyDispatch.Caption = "Qty to Dispatch"
        Me.colQtyDispatch.DisplayMember = "LineQuantity"
        '
        'colFinished
        '
        Me.colFinished.Alignment = Sage.Common.Controls.CaptionAlignment.Center
        Me.colFinished.AutoSize = True
        Me.colFinished.Caption = "Finished?"
        Me.colFinished.DisplayMember = "SpareBit1"
        '
        'fraOrderLine
        '
        Me.fraOrderLine.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.fraOrderLine.Location = New System.Drawing.Point(8, 437)
        Me.fraOrderLine.Name = "fraOrderLine"
        Me.fraOrderLine.Size = New System.Drawing.Size(856, 106)
        Me.fraOrderLine.TabIndex = 2
        Me.fraOrderLine.TabStop = False
        Me.fraOrderLine.Text = "Order line details"
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(8, 558)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(75, 23)
        Me.cmdSave.TabIndex = 3
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Location = New System.Drawing.Point(789, 558)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 4
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'NumberTextBox1
        '
        Me.NumberTextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.NumberTextBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumberTextBox1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.NumberTextBox1.Location = New System.Drawing.Point(380, 72)
        Me.NumberTextBox1.Name = "NumberTextBox1"
        Me.NumberTextBox1.Size = New System.Drawing.Size(100, 21)
        Me.NumberTextBox1.TabIndex = 7
        Me.NumberTextBox1.Text = "NumberTextBox1"
        '
        'MaskedTextBox1
        '
        Me.MaskedTextBox1.BackColor = System.Drawing.SystemColors.Window
        Me.MaskedTextBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MaskedTextBox1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.MaskedTextBox1.Location = New System.Drawing.Point(532, 76)
        Me.MaskedTextBox1.Name = "MaskedTextBox1"
        Me.MaskedTextBox1.Size = New System.Drawing.Size(100, 21)
        Me.MaskedTextBox1.TabIndex = 8
        Me.MaskedTextBox1.Text = "MaskedTextBox1"
        '
        'frmSOPConfirmGoodsDispatched
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(870, 588)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.fraOrderLine)
        Me.Controls.Add(Me.grdItems)
        Me.Controls.Add(Me.fraSelectItems)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmSOPConfirmGoodsDispatched"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SOP - Confirm Goods Dispatched"
        Me.fraSelectItems.ResumeLayout(False)
        Me.fraSelectItems.PerformLayout()
        CType(Me.whlWarehouses, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sopOrders, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region " SUBROUTINE: frmSOPConfirmGoodsDispatched_Load() "
    Private Sub frmSOPConfirmGoodsDispatched_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If
    End Sub
#End Region


#Region " SUBROUTINE: cmdDisplay_Click() "
    Private Sub cmdDisplay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDisplay.Click
        If sopOrders.Text = "" Then
            MsgBox("Please select an order to display.", MsgBoxStyle.Exclamation, "No Order Selected")

            sopOrders.Focus()
            Exit Sub
        End If


        ' Now put the order lines onto the grid
        Dim oStockItems As New Sage.Accounting.Stock.StockItems
        Dim oStockItem As Sage.Accounting.Stock.StockItem = Nothing

        Dim oSalesOrders As New Sage.Accounting.SOP.SOPOrders
        Dim oSalesOrder As Sage.Accounting.SOP.SOPOrder = Nothing


        ' Find the sales order in the collection
        oSalesOrders.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.PersistentSOPOrderReturn.FIELD_DOCUMENTNO, Sage.Common.Data.FilterOperator.Equal, sopOrders.Text))
        oSalesOrders.Find()

        'oSalesOrders.Query.Filters.Add(New Sage.ObjectStore.Filter(oSalesOrder.FIELD_DOCUMENTNO, Sage.Common.Data.FilterOperator.Equal, sopOrders.Text))
        'oSalesOrders.Find()

        If oSalesOrders.IsEmpty = True Then
            MsgBox("Failed to locate the sales order '" & sopOrders.Text & "'.", MsgBoxStyle.Exclamation, "Sales Order Not Found")

            sopOrders.Focus()
            Exit Sub

        Else
            'Dim oSOPLine As Sage.Accounting.SOP.SOPOrderReturnLine

            'For Each oSOPLine In oSalesOrders(0).Lines
            '    MsgBox(oSOPLine.CostPrice)
            'Next

            'oStockItems.Query.Filters.Add(New Sage.ObjectStore.Filter(oStockItem.FIELD_DOCUMENTN, Sage.Common.Data.FilterOperator.Equal, sopOrders.Text))
            'MsgBox("Found it!", MsgBoxStyle.Information, "Found")

            grdItems.DataSource = oSalesOrders(0).Lines
        End If
    End Sub
#End Region


#Region " SUBROUTINE: cmdSave_Click() "
    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click

    End Sub
#End Region

#Region " SUBROUTINE: cmdCancel_Click() "
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub
#End Region

End Class
