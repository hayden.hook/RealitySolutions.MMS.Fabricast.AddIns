<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGoodsOut
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblSOP = New System.Windows.Forms.Label
        Me.sopLookup = New Sage.MMS.Controls.SOPOrderLookup
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdClose = New System.Windows.Forms.Button
        Me.lvSOPitems = New System.Windows.Forms.ListView
        Me.chOrderNumber = New System.Windows.Forms.ColumnHeader
        Me.chCustomerName = New System.Windows.Forms.ColumnHeader
        Me.chItemCode = New System.Windows.Forms.ColumnHeader
        Me.chItemName = New System.Windows.Forms.ColumnHeader
        Me.chSellingUnit = New System.Windows.Forms.ColumnHeader
        Me.chQtyOrdered = New System.Windows.Forms.ColumnHeader
        Me.chQtyDispatched = New System.Windows.Forms.ColumnHeader
        Me.chQtyRequired1 = New System.Windows.Forms.ColumnHeader
        Me.chQtyRequired2 = New System.Windows.Forms.ColumnHeader
        Me.chQtyRequired3 = New System.Windows.Forms.ColumnHeader
        Me.chQtyRequired4 = New System.Windows.Forms.ColumnHeader
        Me.chComplete = New System.Windows.Forms.ColumnHeader
        Me.chMaterialRef = New System.Windows.Forms.ColumnHeader
        Me.lblOrderNo = New System.Windows.Forms.Label
        Me.lblCustomerName = New System.Windows.Forms.Label
        Me.txtOrderNo = New System.Windows.Forms.TextBox
        Me.txtCustomerName = New System.Windows.Forms.TextBox
        Me.chLineType = New System.Windows.Forms.ColumnHeader
        CType(Me.sopLookup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblSOP
        '
        Me.lblSOP.AutoSize = True
        Me.lblSOP.Location = New System.Drawing.Point(8, 8)
        Me.lblSOP.Name = "lblSOP"
        Me.lblSOP.Size = New System.Drawing.Size(31, 13)
        Me.lblSOP.TabIndex = 0
        Me.lblSOP.Text = "SOP:"
        '
        'sopLookup
        '
        Me.sopLookup.AllowSinglePartialMatch = True
        Me.sopLookup.BackColor = System.Drawing.SystemColors.Window
        Me.sopLookup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.sopLookup.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.sopLookup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sopLookup.Location = New System.Drawing.Point(100, 4)
        Me.sopLookup.MaxLength = 32767
        Me.sopLookup.Name = "sopLookup"
        Me.sopLookup.ShowTaxOnlyInvoices = False
        Me.sopLookup.Size = New System.Drawing.Size(148, 21)
        Me.sopLookup.TabIndex = 3
        Me.sopLookup.Tooltip = ""
        Me.sopLookup.Value = Nothing
        '
        'cmdSave
        '
        Me.cmdSave.Location = New System.Drawing.Point(8, 356)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(75, 23)
        Me.cmdSave.TabIndex = 5
        Me.cmdSave.Text = "&Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'cmdClose
        '
        Me.cmdClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdClose.Location = New System.Drawing.Point(544, 356)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(75, 23)
        Me.cmdClose.TabIndex = 6
        Me.cmdClose.Text = "&Close"
        Me.cmdClose.UseVisualStyleBackColor = True
        '
        'lvSOPitems
        '
        Me.lvSOPitems.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.chOrderNumber, Me.chCustomerName, Me.chItemCode, Me.chItemName, Me.chSellingUnit, Me.chQtyOrdered, Me.chQtyDispatched, Me.chQtyRequired1, Me.chQtyRequired2, Me.chQtyRequired3, Me.chQtyRequired4, Me.chComplete, Me.chMaterialRef, Me.chLineType})
        Me.lvSOPitems.FullRowSelect = True
        Me.lvSOPitems.GridLines = True
        Me.lvSOPitems.Location = New System.Drawing.Point(8, 80)
        Me.lvSOPitems.MultiSelect = False
        Me.lvSOPitems.Name = "lvSOPitems"
        Me.lvSOPitems.Size = New System.Drawing.Size(612, 268)
        Me.lvSOPitems.TabIndex = 7
        Me.lvSOPitems.UseCompatibleStateImageBehavior = False
        Me.lvSOPitems.View = System.Windows.Forms.View.Details
        '
        'chOrderNumber
        '
        Me.chOrderNumber.Text = "Order No"
        Me.chOrderNumber.Width = 0
        '
        'chCustomerName
        '
        Me.chCustomerName.Text = "Customer Name"
        Me.chCustomerName.Width = 0
        '
        'chItemCode
        '
        Me.chItemCode.Text = "Item Code"
        Me.chItemCode.Width = 80
        '
        'chItemName
        '
        Me.chItemName.Text = "Item Name"
        Me.chItemName.Width = 100
        '
        'chSellingUnit
        '
        Me.chSellingUnit.Text = "Selling Unit"
        Me.chSellingUnit.Width = 70
        '
        'chQtyOrdered
        '
        Me.chQtyOrdered.Text = "Qty Ordered"
        Me.chQtyOrdered.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chQtyOrdered.Width = 90
        '
        'chQtyDispatched
        '
        Me.chQtyDispatched.Text = "Qty Dispatched"
        Me.chQtyDispatched.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chQtyDispatched.Width = 90
        '
        'chQtyRequired1
        '
        Me.chQtyRequired1.Text = "Qty Required"
        Me.chQtyRequired1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chQtyRequired1.Width = 90
        '
        'chQtyRequired2
        '
        Me.chQtyRequired2.Width = 0
        '
        'chQtyRequired3
        '
        Me.chQtyRequired3.Width = 0
        '
        'chQtyRequired4
        '
        Me.chQtyRequired4.Width = 0
        '
        'chComplete
        '
        Me.chComplete.Text = "Complete"
        Me.chComplete.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'chMaterialRef
        '
        Me.chMaterialRef.Text = "Materials Ref"
        Me.chMaterialRef.Width = 0
        '
        'lblOrderNo
        '
        Me.lblOrderNo.AutoSize = True
        Me.lblOrderNo.Location = New System.Drawing.Point(8, 32)
        Me.lblOrderNo.Name = "lblOrderNo"
        Me.lblOrderNo.Size = New System.Drawing.Size(55, 13)
        Me.lblOrderNo.TabIndex = 8
        Me.lblOrderNo.Text = "Order No:"
        '
        'lblCustomerName
        '
        Me.lblCustomerName.AutoSize = True
        Me.lblCustomerName.Location = New System.Drawing.Point(8, 56)
        Me.lblCustomerName.Name = "lblCustomerName"
        Me.lblCustomerName.Size = New System.Drawing.Size(87, 13)
        Me.lblCustomerName.TabIndex = 9
        Me.lblCustomerName.Text = "Customer Name:"
        '
        'txtOrderNo
        '
        Me.txtOrderNo.Enabled = False
        Me.txtOrderNo.Location = New System.Drawing.Point(100, 28)
        Me.txtOrderNo.Name = "txtOrderNo"
        Me.txtOrderNo.ReadOnly = True
        Me.txtOrderNo.Size = New System.Drawing.Size(148, 21)
        Me.txtOrderNo.TabIndex = 10
        '
        'txtCustomerName
        '
        Me.txtCustomerName.Enabled = False
        Me.txtCustomerName.Location = New System.Drawing.Point(100, 52)
        Me.txtCustomerName.Name = "txtCustomerName"
        Me.txtCustomerName.ReadOnly = True
        Me.txtCustomerName.Size = New System.Drawing.Size(236, 21)
        Me.txtCustomerName.TabIndex = 11
        '
        'chLineType
        '
        Me.chLineType.Text = "LineType"
        Me.chLineType.Width = 0
        '
        'frmGoodsOut
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdClose
        Me.ClientSize = New System.Drawing.Size(627, 384)
        Me.Controls.Add(Me.txtCustomerName)
        Me.Controls.Add(Me.txtOrderNo)
        Me.Controls.Add(Me.lblCustomerName)
        Me.Controls.Add(Me.lblOrderNo)
        Me.Controls.Add(Me.lvSOPitems)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.sopLookup)
        Me.Controls.Add(Me.lblSOP)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmGoodsOut"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Goods Out"
        CType(Me.sopLookup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblSOP As System.Windows.Forms.Label
    Friend WithEvents sopLookup As Sage.MMS.Controls.SOPOrderLookup
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents lvSOPitems As System.Windows.Forms.ListView
    Friend WithEvents chOrderNumber As System.Windows.Forms.ColumnHeader
    Friend WithEvents chCustomerName As System.Windows.Forms.ColumnHeader
    Friend WithEvents chItemCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents chItemName As System.Windows.Forms.ColumnHeader
    Friend WithEvents chSellingUnit As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyOrdered As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyDispatched As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyRequired1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chComplete As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyRequired2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyRequired3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chQtyRequired4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblOrderNo As System.Windows.Forms.Label
    Friend WithEvents lblCustomerName As System.Windows.Forms.Label
    Friend WithEvents txtOrderNo As System.Windows.Forms.TextBox
    Friend WithEvents txtCustomerName As System.Windows.Forms.TextBox
    Friend WithEvents chMaterialRef As System.Windows.Forms.ColumnHeader
    Friend WithEvents chLineType As System.Windows.Forms.ColumnHeader
End Class
