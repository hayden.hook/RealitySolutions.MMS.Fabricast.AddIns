﻿Option Explicit On
Option Strict On

Imports System
Imports System.Windows.Forms
Imports RealitySolutions.Licensing.Core.Client

Public Class frmStockEnquiry
    Private dtTransactions As New DataTable
    Private dcTransactionDate As New DataColumn("Transaction Date", System.Type.GetType("System.DateTime"))
    Private dcType As New DataColumn("Type", System.Type.GetType("System.String"))
    Private dcCustomerName As New DataColumn("Customer Name", System.Type.GetType("System.String"))
    Private dcReference As New DataColumn("Reference", System.Type.GetType("System.String"))
    Private dcSecondReference As New DataColumn("Second Reference", System.Type.GetType("System.String"))
    Private dcQuantityIn As New DataColumn("Quantity In", System.Type.GetType("System.Decimal"))
    Private dcQuantityOut As New DataColumn("Quantity Out", System.Type.GetType("System.Decimal"))
    Private dcUnitCostPrice As New DataColumn("Unit Cost Price", System.Type.GetType("System.Decimal"))
    Private dcUnitIssuePrice As New DataColumn("Unit Issue Price", System.Type.GetType("System.Decimal"))
    Private dcMargin As New DataColumn("Margin", System.Type.GetType("System.Decimal"))



    Private oStockItem As Sage.Accounting.Stock.StockItem = Nothing


#Region " PROPERTY: StockItem() "
    Public Property StockItem() As Sage.Accounting.Stock.StockItem
        Get
            Return oStockItem
        End Get
        Set(ByVal value As Sage.Accounting.Stock.StockItem)
            oStockItem = value
        End Set
    End Property
#End Region


#Region " SUBROUTINE: frmStockEnquiry_Load() "
    Private Sub frmStockEnquiry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        SetupDirectSQLConnection()


        With dtTransactions
            With .Columns
                .Add(dcTransactionDate)
                .Add(dcType)
                .Add(dcCustomerName)
                .Add(dcReference)
                .Add(dcSecondReference)
                .Add(dcQuantityIn)
                .Add(dcQuantityOut)
                .Add(dcUnitCostPrice)
                .Add(dcUnitIssuePrice)
                .Add(dcMargin)
            End With
        End With


        If Not oStockItem Is Nothing Then
            Dim args As New Sage.MMS.Controls.StockItemSelectedEventArgs
            args.SelectedStockItem = oStockItem

            slStock.StockItem = oStockItem
            slStock.OnStockItemSelected(args)
        End If
    End Sub
#End Region


#Region " SUBROUTINE: cmdSearch_Click() "
    Private Sub cmdSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSearch.Click
        Dim frmX As New frmTreeviewSearching

        With frmX
            .ShowDialog()

            If .StockCode <> "" Then
                Dim oStockCodes As New Sage.Accounting.Stock.StockItems
                Dim oStockCode As Sage.Accounting.Stock.StockItem = oStockCodes(.StockCode)

                Dim args As New Sage.MMS.Controls.StockItemSelectedEventArgs
                args.SelectedStockItem = oStockCode

                With slStock
                    .StockItem = oStockCode
                    .OnStockItemSelected(args)
                End With
            End If
        End With
    End Sub
#End Region

#Region " SUBROUTINE: cmdClose_Click() "
    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Me.Close()
    End Sub
#End Region


#Region " SUBROUTINE: slStock_StockItemSelected() "
    Private Sub slStock_StockItemSelected(ByVal sender As Object, ByVal args As Sage.MMS.Controls.StockItemSelectedEventArgs) Handles slStock.StockItemSelected
        Try
            dtTransactions.Rows.Clear()


            Dim decIn As Decimal = 0D
            Dim decOut As Decimal = 0D


            Dim lvNew As ListViewItem = Nothing

            Dim dtTEMP As DataTable = ExecuteQuery("SELECT TransactionDate, TransactionTypeName, CustomerSupplier, Reference, SecondReference, QtyIn, QtyOut, UnitCostPrice, UnitIssuePrice, MarginPC " _
                                                 & "FROM qryStockHistoryEnquiry " _
                                                 & "WHERE ItemID = " & CLng(slStock.StockItem.Item) & " " _
                                                 & "ORDER BY TransactionDate DESC", sqlConnToSage)

            For Each rowEach As DataRow In dtTEMP.Rows
                Dim rowNew As DataRow = dtTransactions.NewRow()

                With rowNew
                    .Item("Transaction Date") = rowEach.Item("TransactionDate")
                    .Item("Type") = rowEach.Item("TransactionTypeName")
                    .Item("Customer Name") = rowEach.Item("CustomerSupplier")
                    .Item("Reference") = rowEach.Item("Reference")
                    .Item("Second Reference") = rowEach.Item("SecondReference")
                    .Item("Quantity In") = rowEach.Item("QtyIn")
                    .Item("Quantity Out") = rowEach.Item("QtyOut")
                    .Item("Unit Cost Price") = rowEach.Item("UnitCostPrice")
                    .Item("Unit Issue Price") = rowEach.Item("UnitIssuePrice")
                    .Item("Margin") = rowEach.Item("MarginPC")
                End With

                dtTransactions.Rows.Add(rowNew)


                decIn += CDec(rowEach.Item("QtyIn"))
                decOut += CDec(rowEach.Item("QtyOut"))
            Next

            dtTEMP.Dispose()
            dtTEMP = Nothing


            ' NEW: Now the extras
            txtBaseUnit.Text = slStock.StockItem.BaseUnitName

            With slStock.StockItem.Holding
                txtOnOrder.Text = Format(.QuantityOnPOPOrder, "0.00000")
                txtInStock.Text = Format(.QuantityInStock, "0.00000")
                txtAllocated.Text = Format(.QuantityAllocated, "0.00000")
                txtFreeStock.Text = Format(.FreeStockAvailable, "0.00000")
            End With



            Dim oUnits As New Sage.Accounting.Stock.Units
            Dim oUnit As Sage.Accounting.Stock.Unit = Nothing

            oUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.Unit.FIELD_NAME, "Metres"))
            oUnits.Find()

            oUnit = oUnits.First

            If oUnit Is Nothing Then
                Throw New Exception("Unable to locate the 'metres' unit.")
            End If



            Dim oMetresSIU As Sage.Accounting.Stock.StockItemUnit = Nothing

            Dim oStockItemUnits As New Sage.Accounting.Stock.StockItemUnits

            oStockItemUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_UNITOBJECT, oUnit))
            oStockItemUnits.Query.Filters.Add(New Sage.ObjectStore.Filter(Sage.Accounting.Stock.StockItemUnit.FIELD_ITEMOBJECT, slStock.StockItem.Item))
            oStockItemUnits.Find()

            oMetresSIU = oStockItemUnits.First

            If oMetresSIU Is Nothing Then
                ' The item doesn't have metres, ignore it
                txtWeightMtr.Text = Format(slStock.StockItem.Weight, "0.00000")

            Else
                txtWeightMtr.Text = Format(CDec(slStock.StockItem.Weight * oMetresSIU.MultipleOfBaseUnit), "0.00000")
            End If


            dtTEMP = ExecuteQuery("SELECT TOP 1 TransactionDate, UnitCostPrice" & vbCrLf _
                                & "FROM TransactionHistory" & vbCrLf _
                                & "WHERE ItemID = " & CLng(slStock.StockItem.Item) & " AND SourceAreaTypeID = 0 AND TransactionTypeID = 17" & vbCrLf _
                                & "ORDER BY TransactionHistoryID DESC", sqlConnToSage)

            Select Case dtTEMP.Rows.Count
                Case 0
                    txtLastPurchaseDate.Text = ""
                    txtLastPurchasePrice.Text = "0.00000"

                Case Else
                    Dim rowEach As DataRow = dtTEMP.Rows(0)


                    txtLastPurchaseDate.Text = Format(CDate(rowEach.Item("TransactionDate")), "dd/MM/yyyy")
                    txtLastPurchasePrice.Text = Format(CDec(rowEach.Item("UnitCostPrice")), "0.00")
            End Select

            dtTEMP.Dispose()
            dtTEMP = Nothing



            txtIn.Text = Format(decIn, "0.00")
            txtOut.Text = Format(decOut, "0.00")
            txtBalance.Text = Format(decIn - decOut, "0.00")


            With dgTransactions
                .DataSource = dtTransactions


                With .Columns("Transaction Date")
                    .ReadOnly = True

                    With .DefaultCellStyle
                        .Format = "dd/MM/yyyy"
                    End With
                End With

                With .Columns("Type")
                    .ReadOnly = True
                End With

                With .Columns("Customer Name")
                    .ReadOnly = True
                End With

                With .Columns("Reference")
                    .ReadOnly = True
                End With

                With .Columns("Second Reference")
                    .ReadOnly = True
                End With

                With .Columns("Quantity In")
                    .ReadOnly = True

                    With .DefaultCellStyle
                        .Format = "0.00"
                        .Alignment = DataGridViewContentAlignment.MiddleRight
                    End With
                End With

                With .Columns("Quantity Out")
                    .ReadOnly = True

                    With .DefaultCellStyle
                        .Format = "0.00"
                        .Alignment = DataGridViewContentAlignment.MiddleRight
                    End With
                End With

                With .Columns("Unit Cost Price")
                    .ReadOnly = True

                    With .DefaultCellStyle
                        .Format = "0.00"
                        .Alignment = DataGridViewContentAlignment.MiddleRight
                    End With
                End With

                With .Columns("Unit Issue Price")
                    .ReadOnly = True

                    With .DefaultCellStyle
                        .Format = "0.00"
                        .Alignment = DataGridViewContentAlignment.MiddleRight
                    End With
                End With

                With .Columns("Margin")
                    .ReadOnly = True

                    With .DefaultCellStyle
                        .Format = "0.00"
                        .Alignment = DataGridViewContentAlignment.MiddleRight
                    End With
                End With


                .AutoResizeColumns()
            End With

        Catch ex As Exception
            MsgBox("An error has occurred while searching for transactions." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
            Exit Sub
        End Try
    End Sub
#End Region

End Class