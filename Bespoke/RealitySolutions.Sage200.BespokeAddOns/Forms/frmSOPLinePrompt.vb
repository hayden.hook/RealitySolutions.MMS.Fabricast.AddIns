Option Explicit On
Option Strict On
Imports RealitySolutions.Licensing.Core.Client

Public Class frmSOPLinePrompt
    Private decUnitPrice As Decimal = 0D
    Private decUnitCost As Decimal = 0D

    Private sopCurrentLine As Sage.Accounting.SOP.SOPOrderReturnLine = Nothing


#Region " PROPERTY: UnitPrice() "
    Public Property UnitPrice() As Decimal
        Get
            Return decUnitPrice
        End Get
        Set(ByVal value As Decimal)
            decUnitPrice = value
        End Set
    End Property
#End Region

#Region " PROPERTY: UnitCost() "
    Public Property UnitCost() As Decimal
        Get
            Return decUnitCost
        End Get
        Set(ByVal value As Decimal)
            decUnitCost = value
        End Set
    End Property
#End Region


#Region " PROPERTY: SOPReturnLine() "
    Public Property SOPReturnLine() As Sage.Accounting.SOP.SOPOrderReturnLine
        Get
            Return sopCurrentLine
        End Get
        Set(ByVal value As Sage.Accounting.SOP.SOPOrderReturnLine)
            sopCurrentLine = value
        End Set
    End Property
#End Region


#Region " SUBROUTINE: frmSOPLinePrompt_Load() "
    Private Sub frmSOPLinePrompt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        txtUnitPrice.Text = Format(decUnitPrice, "0.00")
        txtUnitCost.Text = Format(decUnitCost, "0.00")
    End Sub
#End Region


#Region " SUBROUTINE: cmdOK_Click() "
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        decUnitPrice = CDec(txtUnitPrice.Text)
        decUnitCost = CDec(txtUnitCost.Text)

        'If decUnitPrice = 0D Then
        '    MsgBox("Please enter a valid unit price.", MsgBoxStyle.Exclamation, "Unit Price")

        '    txtUnitPrice.Focus()
        '    Exit Sub
        'End If

        'If decUnitCost = 0D Then
        '    MsgBox("Please enter a valid net price.", MsgBoxStyle.Exclamation, "Net Price")

        '    txtUnitCost.Focus()
        '    Exit Sub
        'End If

        Me.Close()
    End Sub
#End Region

#Region " SUBROUTINE: cmdCancel_Click() "
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        decUnitPrice = -1D
        decUnitCost = -1D

        Me.Close()
    End Sub
#End Region


#Region " SUBROUTINE: txtUnitCost_LostFocus() "
    Private Sub txtUnitCost_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnitCost.LostFocus
        If IsNumeric(txtUnitCost.Text) = False Then
            txtUnitCost.Text = "0.00"

        Else
            txtUnitCost.Text = Format(CDec(txtUnitCost.Text), "0.00")
        End If
    End Sub
#End Region

#Region " SUBROUTINE: txtUnitPrice_LostFocus() "
    Private Sub txtUnitPrice_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnitPrice.LostFocus
        If IsNumeric(txtUnitPrice.Text) = False Then
            txtUnitPrice.Text = "0.00"

        Else
            txtUnitPrice.Text = Format(CDec(txtUnitPrice.Text), "0.00")
        End If
    End Sub
#End Region

End Class