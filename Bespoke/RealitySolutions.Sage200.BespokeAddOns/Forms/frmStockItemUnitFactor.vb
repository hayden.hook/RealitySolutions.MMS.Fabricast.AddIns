﻿Option Explicit On
Option Strict On

Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports RealitySolutions.Licensing.Core.Client

Public Class frmStockItemUnitFactor
    Private decFactor As Decimal = 0D

    Private oStockItemUnit As Sage.Accounting.Stock.StockItemUnit = Nothing

    Private strSellingUnitName As String = ""


#Region " PROPERTY: Factor() "
    Public Property Factor() As Decimal
        Get
            Return decFactor
        End Get
        Set(ByVal value As Decimal)
            decFactor = value
        End Set
    End Property
#End Region

#Region " PROPERTY: SellingUnitName() "
    Public Property SellingUnitName() As String
        Get
            Return strSellingUnitName
        End Get
        Set(ByVal value As String)
            strSellingUnitName = value
        End Set
    End Property
#End Region

#Region " PROPERTY: StockItemUnit() "
    Public Property StockItemUnit() As Sage.Accounting.Stock.StockItemUnit
        Get
            Return oStockItemUnit
        End Get
        Set(ByVal value As Sage.Accounting.Stock.StockItemUnit)
            oStockItemUnit = value
        End Set
    End Property
#End Region


#Region " SUBROUTINE: frmStockItemUnitFactor_Load() "
    Private Sub frmStockItemUnitFactor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' Licensing check
        Dim intLicense As Integer = RealitySolutions.Licensing.Core.Base.FileBasedLicense.Run(Of License)(Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Close()
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license has almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If

        txtStockCode.Text = oStockItemUnit.Item.Code
        txtDescription.Text = oStockItemUnit.Item.Name

        txtBaseUnit.Text = oStockItemUnit.Item.BaseUnitName
        txtStockUnit.Text = oStockItemUnit.Item.StockUnitName
        txtSellingUnit.Text = strSellingUnitName

        txtFactor.Text = Format(decFactor, "0.00000")
    End Sub
#End Region


#Region " SUBROUTINE: txtFactor_LostFocus() "
    Private Sub txtFactor_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFactor.LostFocus
        If IsNumeric(txtFactor.Text) = False Then
            txtFactor.Text = "0.00000"

        Else
            If CDec(txtFactor.Text) < 0D Then
                txtFactor.Text = "0.00000"

            Else
                txtFactor.Text = Format(CDec(txtFactor.Text), "0.00000")
            End If
        End If
    End Sub
#End Region


#Region " SUBROUTINE: cmdOK_Click() "
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If CDec(txtFactor.Text) = 0D Then
            MsgBox("Please enter a valid factor.", MsgBoxStyle.Exclamation, "Factor Not Invalid")

            txtFactor.Focus()
            Exit Sub
        End If


        If MsgBox("Are you sure you want to update the unit factor to " & txtFactor.Text & "?", MsgBoxStyle.Question Or MsgBoxStyle.YesNo Or MsgBoxStyle.DefaultButton2, "Question") = MsgBoxResult.Yes Then
            Try
                StartTransaction()


                Dim sqlComm As New SqlCommand("UPDATE StockItemUnit SET MultipleOfBaseUnit = @MultipleOfBaseUnit WHERE StockItemUnitID = @StockItemUnitID", sqlConnToSage)

                With sqlComm.Parameters
                    .AddWithValue("@MultipleOfBaseUnit", CDec(txtFactor.Text))
                    .AddWithValue("@StockItemUnitID", CLng(oStockItemUnit.StockItemUnit))
                End With

                If sqlComm.ExecuteNonQuery() <> 1 Then
                    Throw New Exception("Failed to locate the stock item unit to update.")
                End If

                sqlComm.Dispose()
                sqlComm = Nothing


                CommitTransaction()


                decFactor = CDec(txtFactor.Text)

                Me.Close()
                Exit Sub

            Catch ex As Exception
                RollbackTransaction()

                MsgBox("An error has occurred while updating the stock item unit factor." & vbCrLf & vbCrLf & ex.ToString(), MsgBoxStyle.Critical, "Error")
                Exit Sub
            End Try
        End If
    End Sub
#End Region

#Region " SUBROUTINE: cmdCancel_Click() "
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        decFactor = 0D

        Me.Close()
    End Sub
#End Region

End Class