<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPriceCheck
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvOrders = New System.Windows.Forms.ListView
        Me.chDocumentNo = New System.Windows.Forms.ColumnHeader
        Me.chDocumentDate = New System.Windows.Forms.ColumnHeader
        Me.chCustomerName = New System.Windows.Forms.ColumnHeader
        Me.chAmount = New System.Windows.Forms.ColumnHeader
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdEdit = New System.Windows.Forms.Button
        Me.lblFilter = New System.Windows.Forms.Label
        Me.txtFilter = New Sage.Common.Controls.MaskedTextBox
        Me.cmdFilter = New System.Windows.Forms.Button
        Me.chkPriceChecked = New System.Windows.Forms.CheckBox
        Me.SuspendLayout()
        '
        'lvOrders
        '
        Me.lvOrders.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.chDocumentNo, Me.chDocumentDate, Me.chCustomerName, Me.chAmount})
        Me.lvOrders.FullRowSelect = True
        Me.lvOrders.GridLines = True
        Me.lvOrders.HideSelection = False
        Me.lvOrders.Location = New System.Drawing.Point(4, 32)
        Me.lvOrders.Name = "lvOrders"
        Me.lvOrders.Size = New System.Drawing.Size(624, 384)
        Me.lvOrders.TabIndex = 0
        Me.lvOrders.UseCompatibleStateImageBehavior = False
        Me.lvOrders.View = System.Windows.Forms.View.Details
        '
        'chDocumentNo
        '
        Me.chDocumentNo.Text = "Order No"
        Me.chDocumentNo.Width = 100
        '
        'chDocumentDate
        '
        Me.chDocumentDate.Text = "Date"
        Me.chDocumentDate.Width = 90
        '
        'chCustomerName
        '
        Me.chCustomerName.Text = "Customer Name"
        Me.chCustomerName.Width = 320
        '
        'chAmount
        '
        Me.chAmount.Text = "Net"
        Me.chAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.chAmount.Width = 70
        '
        'cmdOK
        '
        Me.cmdOK.Location = New System.Drawing.Point(4, 420)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = True
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Location = New System.Drawing.Point(556, 420)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdEdit
        '
        Me.cmdEdit.Location = New System.Drawing.Point(80, 420)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(75, 23)
        Me.cmdEdit.TabIndex = 3
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = True
        '
        'lblFilter
        '
        Me.lblFilter.AutoSize = True
        Me.lblFilter.Location = New System.Drawing.Point(4, 8)
        Me.lblFilter.Name = "lblFilter"
        Me.lblFilter.Size = New System.Drawing.Size(35, 13)
        Me.lblFilter.TabIndex = 4
        Me.lblFilter.Text = "Filter:"
        '
        'txtFilter
        '
        Me.txtFilter.BackColor = System.Drawing.SystemColors.Window
        Me.txtFilter.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFilter.Location = New System.Drawing.Point(44, 4)
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(116, 21)
        Me.txtFilter.TabIndex = 5
        '
        'cmdFilter
        '
        Me.cmdFilter.Location = New System.Drawing.Point(164, 4)
        Me.cmdFilter.Name = "cmdFilter"
        Me.cmdFilter.Size = New System.Drawing.Size(75, 23)
        Me.cmdFilter.TabIndex = 6
        Me.cmdFilter.Text = "&Filter"
        Me.cmdFilter.UseVisualStyleBackColor = True
        '
        'chkPriceChecked
        '
        Me.chkPriceChecked.AutoSize = True
        Me.chkPriceChecked.Location = New System.Drawing.Point(244, 8)
        Me.chkPriceChecked.Name = "chkPriceChecked"
        Me.chkPriceChecked.Size = New System.Drawing.Size(98, 17)
        Me.chkPriceChecked.TabIndex = 7
        Me.chkPriceChecked.Text = "Price Checked?"
        Me.chkPriceChecked.UseVisualStyleBackColor = True
        '
        'frmPriceCheck
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(634, 448)
        Me.Controls.Add(Me.chkPriceChecked)
        Me.Controls.Add(Me.cmdFilter)
        Me.Controls.Add(Me.txtFilter)
        Me.Controls.Add(Me.lblFilter)
        Me.Controls.Add(Me.cmdEdit)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lvOrders)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmPriceCheck"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Price Check"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lvOrders As System.Windows.Forms.ListView
    Friend WithEvents chDocumentNo As System.Windows.Forms.ColumnHeader
    Friend WithEvents chDocumentDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents chCustomerName As System.Windows.Forms.ColumnHeader
    Friend WithEvents chAmount As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents lblFilter As System.Windows.Forms.Label
    Friend WithEvents txtFilter As Sage.Common.Controls.MaskedTextBox
    Friend WithEvents cmdFilter As System.Windows.Forms.Button
    Friend WithEvents chkPriceChecked As System.Windows.Forms.CheckBox
End Class
