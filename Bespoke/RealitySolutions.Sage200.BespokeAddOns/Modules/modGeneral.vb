﻿Imports System.Data.SqlClient

Module modGeneral
    Public Sub ShowExceptionForm(ex As Exception)
        Using frmException As New RSExceptionForm
            frmException.Exception = ex
            frmException.ShowDialog()
        End Using
    End Sub

    Function ExecuteQuery(strQuery)
        Dim dtTEMP = New DataTable("dtTEMP")
        Dim sqlDA = New SqlDataAdapter(CStr(strQuery), CStr(sqlConn))

        Try
            dtTEMP.Clear()
            sqlDA.Fill(dtTEMP)
            sqlDA.Dispose()
            sqlDA = Nothing

        Catch ex As Exception
            dtTEMP.Clear()

        End Try
        Return dtTEMP
    End Function


End Module
