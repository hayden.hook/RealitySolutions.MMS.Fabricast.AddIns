﻿Imports System.IO

Public Class License
    Inherits RealitySolutions.Licensing.Core.Base.FileBasedLicense



    Public Sub New(gProductID As Guid)
        _gidProduct = gProductID
        _strPathToGraceFile = IO.Path.Combine(Sage.Accounting.Application.LogonPath, String.Format("RealitySolutions.Licenses.{0}.cfg", _gidProduct))
        _strPathToLicenseFile = IO.Path.Combine(Sage.Accounting.Application.LogonPath, "RealitySolutions.Licenses.xml")
        _strPathToLicenseCheck = IO.Path.Combine(Sage.Accounting.Application.LogonPath, String.Format("RealitySolutions.Licenses.Check_{0}.cfg", _gidProduct))
        _intGraceDays = 30
    End Sub

    Public Overrides Function GetEnvironment() As Dictionary(Of String, String)
        Return Nothing
    End Function


    Public Overrides Function ShowLicense(productID As Guid, Optional strMessage As String = "") As Boolean
        Dim frX As New RealitySolutions.Licensing.Core.Base.frmEnterLicense

        With frX
            .Instance = Me
            .Message = strMessage
            .ProductID = productID
        End With
        If frX.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub LicensingCheck()
        ' Licensing check, copy paste this code to whever you need it to go.
        Dim intLicense As Integer = Licensing.Core.Base.FileBasedLicense.Run(Of License)(Licensing.Core.Client.Factory.GetAssemblyGUID(Reflection.Assembly.GetExecutingAssembly()))

        If intLicense <= 0 Then
            MsgBox(String.Format("License is invalid or has expired. {0}{0}Please contact Reality Solutions.", vbCrLf), MsgBoxStyle.Critical, "RS - License")
            Exit Sub
        ElseIf intLicense > 0 And intLicense <= 7 Then
            MsgBox(String.Format("Your license is almost expired. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Critical, "RS - License")
        ElseIf intLicense > 7 And intLicense <= 30 Then
            MsgBox(String.Format("Your license is expiring. {0}{0}No. of days left on license: {1}.", vbCrLf, intLicense), MsgBoxStyle.Exclamation, "RS - License")
        End If
    End Sub
End Class

