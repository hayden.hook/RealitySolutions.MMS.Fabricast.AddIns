﻿Option Explicit On
Option Strict On

Public Class clsLostFunctions

#Region " FUNCTION: CBool() "
    Public Function [CBool](ByVal obj As Object) As Boolean
        Return CBool(obj)
    End Function
#End Region

#Region " FUNCTION: CDate() "
    Public Function [CDate](ByVal obj As Object) As Date
        Return CDate(obj)
    End Function
#End Region

#Region " FUNCTION: CDec() "
    Public Function [CDec](ByVal obj As Object) As Decimal
        Return CDec(obj)
    End Function
#End Region

#Region " FUNCTION: CInt() "
    Public Function [CInt](ByVal obj As Object) As Integer
        Return CInt(obj)
    End Function
#End Region

#Region " FUNCTION: CLng() "
    Public Function [CLng](ByVal obj As Object) As Long
        Return CLng(obj)
    End Function
#End Region

#Region " FUNCTION: ColumnIndex() "
    Public Function ColumnIndex(ByVal dtData As DataTable, ByVal strColumnName As String) As Integer
        Dim intX As Integer = 0

        For Each col As DataColumn In dtData.Columns
            If col.ColumnName = strColumnName Then
                Return intX
            End If

            intX += 1
        Next

        Return -1
    End Function
#End Region

#Region " FUNCTION: IIf() "
    Public Function IIf(ByVal Expression As Boolean, ByVal TruePart As Object, ByVal FalsePart As Object) As Object
        Return Microsoft.VisualBasic.IIf(Expression, TruePart, FalsePart)
    End Function
#End Region

#Region " FUNCTION: IIfDBNull() "
    Public Function IIfDBNull(ByVal Value As Object, Optional ByVal NullValue As Object = "") As Object

        Return Microsoft.VisualBasic.IIf(Value Is DBNull.Value, NullValue, Value)
    End Function
#End Region

#Region " FUNCTION: InStr() "
    Public Function InStr(ByVal String1 As String, ByVal String2 As String, Optional ByVal Compare As CompareMethod = CompareMethod.Binary) As Integer
        Return Microsoft.VisualBasic.InStr(String1, String2, Compare)
    End Function
#End Region

#Region " FUNCTION: Format() "
    Public Function Format(ByVal Expression As Object, Optional ByVal Style As String = "") As String
        Return Microsoft.VisualBasic.Format(Expression, Style)
    End Function
#End Region

#Region " FUNCTION: MsgBox() "
    Public Function MsgBox(ByVal Prompt As Object, Optional ByVal Buttons As MsgBoxStyle = MsgBoxStyle.OkOnly, Optional ByVal Title As Object = Nothing) As MsgBoxResult
        Return Microsoft.VisualBasic.MsgBox(Prompt, Buttons, Title)
    End Function
#End Region

#Region " FUNCTION: Split() "
    Public Function Split(ByVal Expression As String, Optional ByVal Delimiter As String = " ", Optional ByVal Limit As Integer = -1, Optional ByVal Compare As CompareMethod = CompareMethod.Binary) As String()
        Return Microsoft.VisualBasic.Split(Expression, Delimiter, Limit, Compare)
    End Function
#End Region

End Class
