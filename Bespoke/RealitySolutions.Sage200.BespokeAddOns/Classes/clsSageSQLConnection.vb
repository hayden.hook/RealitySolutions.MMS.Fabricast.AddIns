﻿Option Explicit On
Option Strict On

Imports System.Data.SqlClient

Public Class SageSQLConnection
    Private Const POOLING_FALSE As String = "Pooling=False"


#Region " FUNCTION: CreateConnection() "
    Public Shared Function CreateConnection(Optional bolOpenConnection As Boolean = True) As SqlConnection
        Dim strCurrentConnection As String = CreateConnectionString()

        If strCurrentConnection.Contains(POOLING_FALSE) = False Then strCurrentConnection = String.Format("{0};{1}", strCurrentConnection, POOLING_FALSE)


        Dim sqlConn As New SqlConnection(strCurrentConnection)

        If bolOpenConnection = True Then

            If sqlConn.State <> ConnectionState.Open Then
                sqlConn.Open()
            End If
        End If

        Return sqlConn
    End Function
#End Region

#Region " FUNCTION: CreateConnectionString() "
    Public Shared Function CreateConnectionString() As String
        Dim strConnectionStringWithoutCreds As String = Sage.Accounting.Application.StaticActiveCompany.ConnectString

        If strConnectionStringWithoutCreds.Contains("SSPI") Then Return strConnectionStringWithoutCreds


        Dim oBuilder As New System.Data.Common.DbConnectionStringBuilder With {.ConnectionString = strConnectionStringWithoutCreds}
        Dim oManager As New Sage.Common.Credentials.DatabaseCredentialManager

        Dim oLabelForUser As Sage.Common.Credentials.DatabaseLabel = Sage.Common.Credentials.DatabaseLabel.GetLabelForUser(TryCast(oBuilder.Item("Database"), String), "MMSUser")
        Dim oCredentialByLabel As Sage.Common.Credentials.DatabaseCredential = oManager.GetCredentialByLabel(oLabelForUser)

        If oCredentialByLabel IsNot Nothing Then
            oBuilder.Item("UID") = oCredentialByLabel.UserName
            oBuilder.Item("PWD") = oCredentialByLabel.Password

        Else
            Throw New Exception("Unable to find the database credentials for MMSUser for the current database.")
        End If


        Return oBuilder.ConnectionString
    End Function
#End Region

End Class
